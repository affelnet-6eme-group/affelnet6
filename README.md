Affelnet 6<sup>e</sup>
==============

Version 22.3.0.1

# Description

Ce dépôt contient les publications des algorithmes de l'application *Affelnet 6<sup>e</sup>*
utilisée dans les académies pour gérer l'affectation des élèves dans les collèges publics.

Académies utilisatrices :

Aix-Marseille, Amiens, Besançon, Bordeaux, Clermont-Ferrand, Corse, Créteil, Dijon, Grenoble, Guadeloupe, Guyane,
La Réunion, Lille, Limoges, Lyon, Martinique, Mayotte, Montpellier, Nancy-Metz, Nantes, Nice, Normandie,
Orléans-Tours, Paris, Poitiers, Polynésie Française, Reims, Rennes, Strasbourg, Toulouse et Versailles.


# Extrait des sources

L'application est écrite en langage Java et construite avec l'outil Apache Maven.

Elle utilise principalement les frameworks Spring et Hibernate.

Les sources publiées sont extraites de la base de code Affelnet 6<sup>e</sup>.

*Remarque :*

Le projet *Affelnet 6<sup>e</sup>* entre dans une démarche de publication du code source. 

Cette extraction fournie en l'état, __ne constitue pas à ce jour__, un ensemble compilable et exécutable.

# Algorithmes métiers

Les principaux algorithmes métiers sont les suivants :

- Détermination automatique des collèges de secteur
- Aide à la décision pour assurer la mixité sociale dans les secteurs multi-collèges
- Pré-affectation 

## Détermination automatique des collèges de secteur

Classe et méthode principale : `DeterminationAutomatiqueManagerImpl.calculCollegeSecteur()`

Cet algorithme permet d'établir la correspondance entre l'adresse d'un élève et la carte scolaire afin de déterminer son ou ses collèges de secteur.

## Aide à la décision pour assurer la mixité sociale dans les secteurs multi-collèges

Classe principale : `MixiteManagerImpl`

Ces fonctionnalités aident la Direction des services départementaux de l'Éducation nationale (DSDEN) à répartir les élèves dans les collèges d'un secteur multi-collèges afin d'assurer la mixité sociale.

## Pré-affectation 

Classe et méthode principale : `AffectationManagerImpl.affectationAuto()`

Cet algorithme permet de proposer une affectation pour les élèves d'un département en fonction de leurs voeux.

# Éditeur

Ministère de l'Éducation nationale, de la Jeunesse et des Sports

Direction générale de l'enseignement scolaire

*107 rue de Grenelle,*
*75007 Paris*

