/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.dao.interfaces.nomenclature;

import java.util.Date;
import java.util.List;

import fr.edu.nancy.affelnet6.domain.nomenclature.CirconscriptionEtabOrigine;
import fr.edu.nancy.affelnet6.domain.nomenclature.CirconscriptionEtabOrigineId;
import fr.edu.nancy.commun.dao.interfaces.BaseDao;
import fr.edu.nancy.commun.exceptions.ApplicationException;
import fr.edu.nancy.commun.exceptions.DaoException;

/**
 * Interface pour les objets CirconscriptionEtabOrigine.
 * 
 * @see fr.edu.nancy.affelnet6.domain.nomenclature.CirconscriptionEtabOrigine
 */
public interface CirconscriptionEtabOrigineDao
        extends BaseDao<CirconscriptionEtabOrigine, CirconscriptionEtabOrigineId> {

    /**
     * Donne le lien circonscription-établissement ouvert pour l'étab origine.
     *
     * @param codeUaiEtabOrigine
     *            code de l'établissement d'origine
     * @param dateOuverture
     *            date d'ouverture sert de référence
     * @return la rangée de la vue circoEtab filtrée sur les dates d'ouverture
     *         et de fermeture
     * @throws DaoException
     *             En cas de problème d'accès aux données
     */
    CirconscriptionEtabOrigine chargerByEtabOrigine(String codeUaiEtabOrigine, Date dateOuverture)
            throws DaoException;

    /**
     * Teste si un lien circonscription-établissement ouvert existe.
     *
     * @param codeUaiEtabOrigine
     *            code de l'établissement d'origine
     * @param dateOuverture
     *            date d'ouverture sert de référence
     * @return la rangée de la vue circoEtab filtrée sur les dates d'ouverture
     *         et de fermeture
     * @throws ApplicationException
     *             En cas de problème d'accès aux données
     */
    boolean existeByEtabOrigine(String codeUaiEtabOrigine, Date dateOuverture) throws ApplicationException;

    /**
     * Donne la liste des écoles primaires publiques ouvertes dans une circonscription.
     * 
     * @param codeUaiCirco
     *            Le code de la circonscription
     * @param dateOuverture
     *            date d'ouverture sert de référence
     * @return la liste des écoles primaires
     * @throws DaoException
     *             En cas de problème d'accès aux données
     */
    List<CirconscriptionEtabOrigine> chargerEcolesPrimairesPub(String codeUaiCirco, Date dateOuverture)
            throws DaoException;

    /**
     * Donne la liste des codes UAI des écoles d'origine pour une circonscription donné en paramètre.
     *
     * @param uaiCirco
     *            code UAI de la circonscription
     * @param dateOuverture
     *            date d'ouverture sert de référence
     * @return la liste des codes UAI des écoles d'origine
     * @throws DaoException
     *             en cas d'erreur d'accès aux données
     */
    List<String> listerDistinctUaiEtbPourUaiCirco(String uaiCirco, Date dateOuverture) throws DaoException;
}
