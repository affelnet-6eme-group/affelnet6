/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.dao.interfaces.dossier;

import java.util.List;
import java.util.Map;

import fr.edu.nancy.affelnet6.domain.dossier.DemandeAffectation;
import fr.edu.nancy.commun.dao.interfaces.BaseDao;
import fr.edu.nancy.commun.exceptions.DaoException;

/**
 * Interface pour les objets DemandeAffectation.
 * 
 * @see fr.edu.nancy.affelnet6.domain.dossier.DemandeAffectation
 */

public interface DemandeAffectationDao extends BaseDao<DemandeAffectation, Long> {

    /**
     * Comptage du nombre total des demandes sur cette offre.
     * 
     * @param codeOffre
     *            code de l'offre de formation
     * @return nombre total des demandes
     */
    int compteDemandesTotales(String codeOffre);

    /**
     * Nombre de demandes du secteur.
     * 
     * @param codeOffre
     *            code de l'offre de formation
     * @return compte de demandes de secteur
     *
     *         Compte les demandes de rang 1 en formation 6ème sur le secteur.
     */
    int compteDemandesSecteur(String codeOffre);

    /**
     * Nombre de demandes du secteur.
     * 
     * @param codeOffre
     *            code de l'offre de formation
     * @return compte de demandes de secteur
     *
     *         Compte les demandes de rang 1 en formation 6ème sur le secteur.
     */
    int compteDemandesMaxSecteur(String codeOffre);

    /**
     * Nombre de demandes de derogation sur cette offre.
     * 
     * @param codeOffre
     *            code de l'offre de formation
     * @return nombre de demandes de derogation
     */
    int compteDemandesDerogation(String codeOffre);

    /**
     * Nombre d'affectations sur cette offre qui correspondent à la 6ème sur le collège de secteur.
     * 
     * @param codeOffre
     *            code de l'offre de formation
     * @param isDemandeRang1
     *            vrai si on se restreint aux demandes du secteur (en rang 1)
     * @return nombre d'affectations en 6ème sur le collège de secteur
     */
    int compteAffectesSecteur(String codeOffre, boolean isDemandeRang1);

    /**
     * Nombre d'affectations sur cette offre qui correspondent à une demande de dérogation.
     * 
     * @param codeOffre
     *            code de l'offre de formation
     * @return nombre d'affectations en dérogation
     */
    int compteAffectesDerog(String codeOffre);

    /**
     * Nombre d'affectations manuelles sur cette offre.
     * 
     * @param codeOffre
     *            code de l'offre de formation
     * @return nombre d'affectations manuelles
     */
    int compteAffectesManuellement(String codeOffre);

    /**
     * Nombre de demandes de dérogation en attente sur cette offre.
     * 
     * @param codeOffre
     *            code de l'offre de formation
     * @return nombre de demandes de dérogation en attente sur cette offre
     */
    int compteDemandesAttenteDerog(String codeOffre);

    /**
     * Nombre de demandes du secteur en attente (i.e élèves pouvant potentiellement revenir).
     * 
     * @param codeOffre
     *            code de l'offre de formation
     * @return nombre de demandes du secteur en attente
     */
    int compteAttenteSecteur(String codeOffre);

    /**
     * Nombre de demandes pour l'établissement pour les formations SEGPA.
     * 
     * @param codeOffre
     *            le code de l'offre de formation
     * @return nombre de demandes pour l'établissement pour les formations SEGPA.
     */
    int compteDemandesSegpa(String codeOffre);

    /**
     * @param codeOffre
     *            code de l'offre de formation
     * @return nombre de demandes en attente pour une offre de formation de type SEGPA et/ou ULIS.
     */
    int compteDemandesAttentesSegpaOuUlis(String codeOffre);

    /**
     * Nombre de demandes de formations SEGPA sans décision finale pour le département.
     * 
     * @param codDeptMen
     *            code du département MEN
     * @return nombre de demandes SEGPA sans décision finale.
     */
    int compteDemandesSegpaSansDecisionFinale(String codDeptMen);

    /**
     * @param codeOffre
     *            le code de l'offre de formation.
     * @return Nombre de demandes affectées pour une offre de formation donnée.
     */
    int compteDemandesAffecteesSegpaOuUlis(String codeOffre);

    /**
     * Nombre de demandes sur une formation SEGPA banalisées pour le département.
     * 
     * @param codDeptMen
     *            code du département MEN
     * @return nombre de demandes sur une formation SEGPA banalisées
     */
    int compteDemandesSegpaBanalisees(String codDeptMen);

    /**
     * Nombre de demandes sur une formation SEGPA banalisées prises pour le département.
     * 
     * @param codDeptMen
     *            code du département MEN
     * @return nombre de demandes sur une formation SEGPA banalisées et prises
     */
    int compteDemandesSegpaBanaliseesPrises(String codDeptMen);

    /**
     * Nombre de demandes non traitées.
     * 
     * @param codeOffre
     *            code de l'offre de formation
     * @return nombre de demandes non traitées.
     */
    int compteDemandesNotTraitees(String codeOffre);

    /**
     * Liste les demandes de dérogation concernant les élèves pour lesquels l'établissement
     * est collège de secteur et dont ce n'est pas une demande de secteur.
     * Les demandes en 6EME SEGPA et en ULIS ne sont pas retournées.
     * 
     * @param codeEtab
     *            code de l'etab
     * @return liste des demandes.
     */
    List<DemandeAffectation> listeDemandesDerogation(String codeEtab);

    /**
     * Liste les affectations en dérogation des élèves pour lesquels l'établissement
     * passé en paramètre est collège de secteur et qui sont affectés
     * dans un autre établissement.
     * Les affectations en 6EME SEGPA et en ULIS ne sont pas retournées.
     * 
     * @param codeEtab
     *            code de l'etab
     * @return liste des affectations.
     */
    List<DemandeAffectation> listeAffectationsDerogation(String codeEtab);

    /**
     * @param codeDeptMen
     *            Le code département ("054" par ex)
     * @return Le rang maximum de toute les demande d'affectation
     *         du département.
     */
    int getRangMax(String codeDeptMen);

    /**
     * Donne la liste des ids des demandes du département
     * ayant comme justificatif de dérogation le motif passé en paramètre.
     * 
     * @param codeDeptMen
     *            Le code MEN du département.
     * @param codeMotif
     *            Le code du motif de dérogation.
     * @return La liste des ids des demandes du département
     *         ayant comme justificatif de dérogation le motif passé en paramètre.
     */
    List<Long> getDemandeAffectationSelonMotif(String codeDeptMen, String codeMotif);

    /**
     * @param codeDeptMen
     *            Le code département MEN de l'utilisateur.
     * @param requeteNommee
     *            Le nom de la requête nommée.
     * @return Map&lt;codeRne, Map&lt;codeMotif, compteur&gt;&gt; : Une map contenant tous les totaux par
     *         établissement
     *         des dérogations faites pour les formations de l'établissement.
     */
    Map<String, Map<String, Integer>> getCompteurDerogEtab(String codeDeptMen, String requeteNommee);

    /**
     * @param codeDeptMen
     *            Le code département MEN de l'utilisateur.
     * @param requeteNommee
     *            Le nom de la requête nommée.
     * @return Map&lt;codeRne, Map&lt;codeOffre, Map&lt;codeMotif, compteur&gt;&gt;&gt; : Une map contenant les
     *         totaux par offre de formation des dérogations faites pour les formations de l'établissement.
     */
    Map<String, Map<String, Map<String, Integer>>> getCompteurDerogOffre(String codeDeptMen, String requeteNommee);

    /**
     * @param codeDeptMen
     *            Le code département MEN de l'utilisateur.
     * @param requeteNommee
     *            La requête nommée.
     * @return Map&lt;codeRne, compteur&gt; : Le compteur correspond aux nombres de demandes faites
     *         pour sur un autre collège que le collège de secteur.
     */
    Map<String, Integer> getCompteurTotalDemandeExterneEtab(String codeDeptMen, String requeteNommee);

    /**
     * @param codeDeptMen
     *            Le code département MEN de l'utilisateur.
     * @return Le nombre de demandes ULIS banalisées du département.
     */
    int compteDemandesUlisBanalisees(String codeDeptMen);

    /**
     * @param codeDeptMen
     *            Le code département MEN de l'utilisateur.
     * @return Le nombre de demandes SEGPA qui sont à la fois banalisées et ULIS dans le département.
     */
    int compteDemandesSegpaUlisBanalisees(String codeDeptMen);

    /**
     * @param codeDeptMen
     *            Le code département MEN de l'utilisateur.
     * @return Le nombre de derogation accepte dans le departement.
     */
    int compteDemandeDerogAffectees(String codeDeptMen);

    /**
     * @param codeOffreFormation
     *            Le code d'une offre de formation.
     * @return Le nombre de demandes sur une offre de formation.
     */
    int compteDemandes(String codeOffreFormation);

    /**
     * Recherche les numéros de dossier élèves qui sont refusés sur toutes leurs demandes d'affectation.
     * Qui n'ont pas d'autres décisions de demandes en dehors de 'QO','RA','RM'
     * 
     * @param codeDeptMen
     *            Le code département MEN de l'utilisateur.
     * @return la liste des numéros de dossier
     * @throws DaoException
     *             Une erreur lors de l'accès aux données.
     */
    List<Long> chercheNumDossierRefusesPartout(String codeDeptMen) throws DaoException;
}
