/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.dao.interfaces.nomenclature;

import java.util.List;

import fr.edu.nancy.affelnet6.domain.nomenclature.StatutAdresse;
import fr.edu.nancy.commun.dao.interfaces.BaseDao;
import fr.edu.nancy.commun.exceptions.DaoException;

/**
 * Interface pour l'accès aux données StatutAdresse.
 */
public interface StatutAdresseDao extends BaseDao<StatutAdresse, String> {

    /**
     * Charge la liste des statuts sauf le statut à l'étranger.
     * 
     * @return La liste des statuts sauf le statut à l'étranger
     * @throws DaoException
     *             En cas de problème d'accès aux données
     */
    List<StatutAdresse> listerToutSaufEtranger() throws DaoException;

}
