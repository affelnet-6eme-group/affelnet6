/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.dao.interfaces.nomenclature;

import fr.edu.nancy.affelnet6.domain.nomenclature.Pays;
import fr.edu.nancy.commun.dao.interfaces.BaseDao;
import fr.edu.nancy.commun.exceptions.ApplicationException;

/**
 * Interface pour les objets Pays.
 * 
* @see fr.edu.nancy.affelnet6.domain.nomenclature.Pays
 */

public interface PaysDao extends BaseDao<Pays, Integer> {

    /**
     * Test si un Pays est présent en base par son code.
     * 
     * @param code
     *            l'id de l'Pays à tester
     * @return true si le pays est trouvé
     * @throws ApplicationException
     *             Exception lancée en cas de problème
     */
    boolean existePaysParCode(String code) throws ApplicationException;

    Pays chargerPaysParCode(String code) throws ApplicationException;

}
