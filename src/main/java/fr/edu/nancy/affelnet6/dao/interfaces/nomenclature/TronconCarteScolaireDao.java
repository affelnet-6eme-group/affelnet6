/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.dao.interfaces.nomenclature;

import java.util.List;
import java.util.Map;

import fr.edu.nancy.affelnet6.domain.dossier.Adresse;
import fr.edu.nancy.affelnet6.domain.nomenclature.TronconCarteScolaire;
import fr.edu.nancy.commun.dao.interfaces.BaseDao;
import fr.edu.nancy.commun.exceptions.ApplicationException;
import fr.edu.nancy.commun.exceptions.DaoException;

/**
 * Interface pour les objets TronconCarteScolaire.
 */
public interface TronconCarteScolaireDao extends BaseDao<TronconCarteScolaire, Long> {

    /**
     * Efface les adresse de la carte scolaire du département en vue d'un nouvel import.
     * 
     * @param dptMenId
     *            Identifiant du département MEN concerné
     * @return nombre de suppressions effectuées
     * @throws DaoException
     *             en cas de problème
     */
    int supprimerPourRemplacerDepartement(int dptMenId) throws DaoException;

    /**
     * Met à jour la date du dernier appel au service rnvp pour la carte scolaire d'un département.
     * 
     * @param deptMenId
     *            Code du département MEN concerné
     * @return le nombre de ligne mises à jour
     * @throws DaoException
     *             en cas de problème
     */
    int majDateRNVP(int deptMenId) throws DaoException;

    /**
     * Rafraîchit le tronçon de la carte scolaire passé en paramètre dans la session hibernate.
     * 
     * @param troncon
     *            L e tronçon que l'on veut rafraîchir.
     */
    void rafraichit(TronconCarteScolaire troncon);

    /**
     * Recherche le tronçon de carte scolaire correspondant à l'adresse d'un élève.
     * 
     * @param adresseEleve
     *            L'adresse de l'élève
     * @param codeDeptMen
     *            Code MEN du département
     * @return le tronçon correspondant ou null si aucune correspondance
     * @throws ApplicationException
     *             Exception remontant en cas de problème
     */
    List<TronconCarteScolaire> chercherTronconPourEleve(Adresse adresseEleve, String codeDeptMen)
            throws ApplicationException;

    /**
     * Recherche le tronçon de carte scolaire correspondant à une adresse d'un élève qui n'a pas de numéro de voie.
     * 
     * @param adresseEleve
     *            L'adresse de l'élève
     * @param codeDeptMen
     *            Code MEN du département
     * @return le tronçon correspondant ou null si aucune correspondance
     * @throws ApplicationException
     *             Exception remontant en cas de problème
     */
    List<TronconCarteScolaire> chercherTronconPourEleveSansNumeroVoie(Adresse adresseEleve, String codeDeptMen)
            throws ApplicationException;

    /**
     * Recherche la liste des tronçons de la carte scolaire d'un département en secteur unique.
     * 
     * @param codeDeptMen
     *            code MEN du département
     * @return la liste des tronçons
     * @throws ApplicationException
     *             Exception remontant en cas de problème
     */
    Map<Integer, TronconCarteScolaire> listerTronconCarteScolaireDepartementSecteurUnique(String codeDeptMen)
            throws ApplicationException;

    /**
     * Compte le nombre de communes d'un département qui n'ont pas de collège de secteur associé dans la carte
     * scolaire.
     * 
     * @param codeDeptMen
     *            Code MEN du département
     * @return le nombre de communes d'un département qui n'ont pas de collège de secteur
     */
    int compteCommunesSansCollegeSecteur(String codeDeptMen);

    /**
     * Récupère les tronçons doublons (collège(s) de secteur non vérifié(s)).
     * 
     * @param troncon
     *            tronçon pour lequel on recherche les doublons
     * 
     * @return une liste de tronçons identiques avec un id différent
     */
    List<TronconCarteScolaire> listerDoublons(TronconCarteScolaire troncon);

    /**
     * Méthode qui recherche si un tronçon de la carte scolaire a un doublon et renvoie ce doublon s'il existe.
     * 
     * @param troncon
     *            tronçon pour lequel on recherche la présence d'un doublon
     * 
     * @return un tronçon identique avec un id différent
     */
    TronconCarteScolaire chercherDoublon(TronconCarteScolaire troncon);

    /**
     * Efface les établissement rattachés à un tronçon de la carte scolaire.
     * 
     * @param troncon
     *            Le tronçon pour lequel on souhaite purger les collèges de secteur
     * @return nombre de suppressions effectuées
     * @throws DaoException
     *             en cas de problème
     */
    int supprimerCollegesDeSecteurPourTroncon(TronconCarteScolaire troncon) throws DaoException;

    /**
     * Recherche la liste des identifiants techniques des tronçons pour collège de secteur donné.
     * 
     * @param codeDeptMen
     *            Code MEN du département
     * @param codeUaiCollege
     *            Code uai du collège de secteur recherché
     * @return la liste des identifiants
     * @throws DaoException
     *             Exception remontant en cas de problème
     */
    List<Long> listerDistinctIdTronconPourCodeUai(String codeDeptMen, String codeUaiCollege) throws DaoException;

    /**
     * Recherche la liste des codes UAI des collèges de la carte scolaire sans offre de formation 6ème.
     * 
     * @param codeDeptMen
     *            Code MEN du département
     * @return la liste des codes UAI
     * @throws DaoException
     *             Exception remontant en cas de problème
     */
    List<String> listerDistinctCodesUaiSansOffreFormation6eme(String codeDeptMen) throws DaoException;

    /**
     * Donne les codes INSEE des communes présentes dans la carte scolaire du département.
     * 
     * @param codeDeptMen
     *            Le code MEN du département pour lequel il faut recherche les communes présentes dans la carte
     * @return Les codes INSEE des communes présentes dans la carte scolaire du département
     * @throws DaoException
     *             Exception remontant en cas de problème avec la base de données
     */
    List<String> listerCodesCommuneInseeCarteScolaireDuDepartement(String codeDeptMen) throws DaoException;
}
