/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.dao.interfaces.nomenclature;

import java.util.List;

import fr.edu.nancy.affelnet6.domain.nomenclature.CommuneInseePtt;
import fr.edu.nancy.affelnet6.domain.nomenclature.CommuneInseePttId;
import fr.edu.nancy.commun.dao.interfaces.BaseDao;
import fr.edu.nancy.commun.exceptions.ApplicationException;

/**
 * Interface pour les objets CommuneInseePtt.
 */
public interface CommuneInseePttDao extends BaseDao<CommuneInseePtt, CommuneInseePttId> {
    /**
     * Charge un(e) CommuneInseePtt à partir de son libelle postal et de l'id du code postal.
     *
     * @param cpId
     *            Le code postal de la commune à charger
     * @param libellePostal
     *            le libelle postal du CommuneInseePtt à charger
     * @return le/la <code>CommuneInseePtt</code> correspondant au libelle
     * @throws ApplicationException
     *             Exception remontant en cas de problème
     */
    CommuneInseePtt chargerCommuneInseeParCodePostalEtLibelle(Integer cpId, String libellePostal)
            throws ApplicationException;

    /**
     * Charge une commune INSEE PTT à partir d'un identifiant de commune INSEE si elle est en CEDEX.
     *
     * @param communeInseePttId
     *            L'identifiant de la commune INSEE PTT
     * @return la commune INSEE PTT si elle est en CEDEX
     * @throws ApplicationException
     *             Exception remontant en cas de problème
     */
    CommuneInseePtt chargerCommuneInseePttSiCedex(CommuneInseePttId communeInseePttId) throws ApplicationException;

    /**
     * Liste les communes INSEE PTT à partir d'un identifiant de commune INSEE.
     * 
     * @param communeInseeId
     *            l'identifiant de la commune INSEE
     * @return les communes INSEE correspondants l'identifiant de la commune INSEE
     * @throws ApplicationException
     *             Exception remontant en cas de problème
     */
    List<CommuneInseePtt> listerCommuneInseePttSansCedexParCommuneInseeId(Integer communeInseeId)
            throws ApplicationException;
}
