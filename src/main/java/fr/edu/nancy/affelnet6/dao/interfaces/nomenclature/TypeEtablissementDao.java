/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.dao.interfaces.nomenclature;

import fr.edu.nancy.affelnet6.domain.nomenclature.TypeEtablissement;
import fr.edu.nancy.commun.dao.interfaces.BaseDao;
import fr.edu.nancy.commun.exceptions.ApplicationException;

/**
 * Interface pour les objets Type Etablissement.
 * 
 * @see TypeEtablissement
 */

public interface TypeEtablissementDao extends BaseDao<TypeEtablissement, Integer> {

    /**
     * Test si un type d'établissement est présent en base par son code.
     *
     * @param code
     *            le code du type d'établissement
     * @return vrai si le type d'établissement existe sinon faux
     * @throws ApplicationException
     *             Exception lancée en cas de problème
     */
    boolean existeTypeEtablissementParCode(String code) throws ApplicationException;

    /**
     * Retourne un type d'établissement par son code.
     *
     * @param code
     *            le code du type d'établissement
     * @return le type d'établissement
     * @throws ApplicationException
     *             Exception lancée en cas de problème
     */
    TypeEtablissement chargerTypeEtablissementParCode(String code) throws ApplicationException;
}
