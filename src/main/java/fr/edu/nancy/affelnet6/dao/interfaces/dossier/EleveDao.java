/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.dao.interfaces.dossier;

import java.util.List;
import java.util.Map;

import fr.edu.nancy.affelnet6.domain.dossier.Eleve;
import fr.edu.nancy.affelnet6.domain.dossier.SuiviEcole;
import fr.edu.nancy.affelnet6.domain.nomenclature.Matiere;
import fr.edu.nancy.commun.dao.interfaces.BaseDao;
import fr.edu.nancy.commun.dao.interfaces.Pagination;
import fr.edu.nancy.commun.dao.interfaces.Selector;
import fr.edu.nancy.commun.dao.interfaces.Tri;
import fr.edu.nancy.commun.dao.interfaces.filtres.Filtre;
import fr.edu.nancy.commun.exceptions.ApplicationException;
import fr.edu.nancy.commun.exceptions.DaoException;
import fr.edu.nancy.commun.web.session.InfoListe;

/**
 * Interface pour les objets Eleve.
 * 
 * @see fr.edu.nancy.affelnet6.domain.dossier.Eleve
 */

public interface EleveDao extends BaseDao<Eleve, Long> {

    /**
     * Efface les dossiers élève du département en vue d'un nouvel import.
     * 
     * @param dptMenId
     *            Identifiant du département MEN Concené
     * @return nombre de suppressions effectuées
     * @throws ApplicationException
     *             en cas de problème
     */
    int supprimerPourRemplacerDepartement(int dptMenId) throws ApplicationException;

    /**
     * Compte les élèves dans des écoles publiques du département passé en paramètre.
     * 
     * @param dept
     *            Le code MEN du département.
     * @param uaiDsden
     *            Le code UAI de la DSDEN du département.
     * @return Le nombre d'élèves trouvés.
     */
    int compteElevesEcolesPubDept(String dept, String uaiDsden);

    /**
     * compte les élèves en appel pour le dept men.
     * 
     * @param dept
     *            le code MEN du département
     * @param typeSaisie
     *            l'origine du dossier
     * @return le nombre d'élèves correspondants
     */
    int compteElevesAppelDept(String dept, String typeSaisie);

    /**
     * compte les élèves pour l'école.
     * 
     * @param codeUai
     *            Le code UAI de l'école.
     * @param typeSaisie
     *            l'origine du dossier
     * @param codeDept
     *            Le code MEN du département.
     * @return le nombre d'élèves trouvés *
     */
    int compteElevesEcole(String codeUai, String typeSaisie, String codeDept);

    /**
     * Une map d'élève ayant au moins une dérogation et issu d'une école fourni
     * en paramètre.
     * 
     * @param codeUai
     *            le code de l'établissement.
     * @return le nombre d'élève d'une école ayant au moins une dérogation
     */
    Map<String, Eleve> mapEleveAvecDerogation(String codeUai);

    /**
     * compte le nombre d'élève en appel pour l'école <code>codeUai</code>.
     * 
     * @param codeUai
     *            le code de l'établissement.
     * @param typeSaisie
     *            l'origine du dossier.
     * @param codeDept
     *            Le code MEN du département.
     * @return le nombre d'élève d'une école ayant au moins une dérogation
     */
    int compteElevesAppelEcole(String codeUai, String typeSaisie, String codeDept);

    /**
     * compte le nombre d'élève dans le département.
     * 
     * @param codeDepMen
     *            le code MEN du département.
     * @return le nombre d'élève d'une école ayant au moins une dérogation
     */
    int compteElevesDept(String codeDepMen);

    /**
     * Donne le nombre d'élèves sans collège de secteur potentiel dans le département.
     * 
     * @param codeDepMen
     *            Le code MEN du département
     * @return Le nombre d'élèves sans collège de secteur potentiel dans le département.
     */
    int compteElevesSansCollegeSecteurPotentiel(String codeDepMen);

    /**
     * Donne le nombre d'élèves sans collège de secteur potentiel pour l'école.
     * 
     * @param codeUai
     *            Le code UAI de l'école.
     * @param codeDepMen
     *            Le code MEN du département
     * @return Le nombre d'élèves sans collège de secteur potentiel pour l'école.
     */
    int compteElevesSansCollegeSecteurPotentielEcole(String codeUai, String codeDepMen);

    /**
     * Met à jour la date d'édition du volet de la fiche de liaison d'une liste d'élève.
     * 
     * @param numVolet
     *            numéro du volet à mettre à jour
     * @param numDossierEleves
     *            liste des numéros de dossier des élèves
     * @return le nombre de dossier mis à jour
     */
    int majDateEditionVolet(int numVolet, Long[] numDossierEleves);

    /**
     * Met à jour le flag parents séparés - garantie de la confidentialité d'une liste d'élèves.
     *
     * @param numDossierEleves
     *            liste des numéros de dossier des élèves dont on édite le volet 1
     * @param confidentialiteResp
     *            liste des numéros de dossier des élèves qui ont le flag parents séparés coché
     * @return le nombre de dossier mis à jour
     */
    int majConfidentialiteResponsables(Long[] numDossierEleves, Long[] confidentialiteResp);

    /**
     * Compte le nombre d'élèves issus de BE1D dans le département <code>codeDepMen</code>.
     * 
     * @param codeDepMen
     *            le code MEN du département
     * @return le nombre d'élèves issus de BE1D dans le département
     */
    int compteElevesDeptBe1d(String codeDepMen);

    /**
     * Compte le nombre d'élèves saisi en candidature individuelle dans le département.
     * 
     * @param codeDepMen
     *            le code MEN du département
     * @return le nombre d'élèves correspondant
     */
    int compteElevesDeptInd(String codeDepMen);

    /**
     * Compte le nombre d'élèves maintenus à l'école pour le département.
     * 
     * @param codeDepMen
     *            le code MEN du département
     * @return le nombre d'élèves correspondant
     */
    int compteElevesMaintenus(String codeDepMen);

    /**
     * Compte le nombre d'élèves affectés à l'école pour le département.
     * 
     * @param codeDepMen
     *            le code MEN du département
     * @return le nombre d'élèves correspondant
     */
    int compteElevesAffectes(String codeDepMen);

    /**
     * Compte le nombre d'élèves refusés à l'école pour le département.
     * 
     * @param codeDepMen
     *            le code MEN du département
     * @return le nombre d'élèves correspondant
     */
    int compteElevesRefuses(String codeDepMen);

    /**
     * Donne le nombre d'élèves sans choix de la famille dans le département (ne compte pas
     * les élèves maintenus à l'école).
     * 
     * @param codeDepMen
     *            Le code MEN du département
     * @return Le nombre d'élèves sans choix de la famille dans le département
     */
    int compteElevesSansDemandePasMaintenus(String codeDepMen);

    /**
     * Donne le nombre d'élèves hors collège public du département.
     * 
     * @param codeDepMen
     *            Le code MEN du département
     * @return Le nombre d'élèves hors collège public du département
     */
    int compteElevesHorsCPD(String codeDepMen);

    /**
     * Donne le nombre d'élèves d'un département dont le premier voeu est une
     * demande de dérogation dont le motif le plus prioritaire est celui passé
     * en paramètre.
     * 
     * @param codeDeptMen
     *            Le code d'un département MEN
     * @param idMtfDrg
     *            L'identifiant d'un motif de dérogation du département
     * @return Nombre d'élèves d'un département dont le premier voeu est une
     *         demande de dérogation dont le motif
     * @throws DaoException
     *             En cas de problème d'accès à la base de données
     */
    int compteElevesAvec1erVoeuDerogEtMotifLePlusPrioritaire(String codeDeptMen, Integer idMtfDrg)
            throws DaoException;

    /**
     * Donne le nombre de dérogations accordées d'un département dont le motif
     * le plus prioritaire est celui passé en paramètre.
     * 
     * @param codeDeptMen
     *            Le code d'un département MEN
     * @param idMtfDrg
     *            L'identifiant d'un motif de dérogation du département
     * @return Nombre de dérogations accordées d'un département dont le motif le
     *         plus prioritaire est celui passé en paramètre.
     * @throws DaoException
     *             En cas de problème d'accès à la base de données
     */
    int compteElevesAvecDerogAccordeeEtMotifLePlusPrioritaire(String codeDeptMen, Integer idMtfDrg)
            throws DaoException;

    /**
     * Liste les élèves ayant toutes leurs dérogation refusées.
     * 
     * @param filtreNom
     *            s'il y a un filtre sur le nom de l'élève
     * @param filtreCollege
     *            s'il y a un filtre sur le college de secteur
     * @param tri
     *            s'il y a un tri descendant sur le nom, alors "desc".
     * @param codeDeptMenUser
     *            Le code MEN d'un département
     * @return La liste les élèves ayant toutes leurs dérogation refusées
     */
    List<Eleve> listerRefusesDerog(String filtreNom, String filtreCollege, String tri, String codeDeptMenUser);

    /**
     * Liste les élèves qui n'ont pas de représentant légal pour une école.
     * 
     * @param uai
     *            le code uai de l'école
     * @param codeDept
     *            Le code d'un département
     * @return le nombre d'élève sans responsable
     */
    List<Eleve> listerEleveSansRepLegal(String uai, String codeDept);

    /**
     * compte les adresses à traiter des élèves et des responsables pour une école.
     * 
     * @param codeUaiOrigine
     *            Le code UAI de l'école d'origine
     * @param codeDept
     *            Le code MEN du département
     * @return le nombre d'élève trouvés
     */
    int compteAdressesATraiter(String codeUaiOrigine, String codeDept);

    /**
     * compte les adresses non contrôlées des élèves et des responsables pour une école.
     * 
     * @param codeUaiOrigine
     *            Le code UAI de l'école d'origine
     * @param codeDept
     *            Le code MEN du département
     * @return le nombre d'élève trouvés
     */
    int compteAdressesNonControlees(String codeUaiOrigine, String codeDept);

    /**
     * compte les adresses non contrôlées des élèves et des responsables pour un collège.
     * 
     * @param codeUaiCollege
     *            Le code UAI du collège
     * @param codeDept
     *            Le code MEN du département
     * @return le nombre d'élève trouvés
     */
    int compteAdressesNonControleesCollegeAffectationFinale(String codeUaiCollege, String codeDept);

    /**
     * Nombre d'élèves sans demande non maintenus dans une école.
     * 
     * @param codeRne
     *            Le code RNE
     * @param codeDept
     *            Le code MEN du département
     * @return le nombre d'élève trouvés
     * 
     */
    int compteElevesSansDemandePasMaintenusEcole(String codeRne, String codeDept);

    /**
     * @param codeUai
     *            Le code UAI (RNE) de l'école dont on souhaite obtenir les
     *            indicateurs
     * @return Les indicateurs associées à l'école
     */
    SuiviEcole suiviEcole(String codeUai);

    /**
     * @param codeDeptMen
     *            Le département pour lequel on veut la liste des doublons.
     * @return La liste des doublons problables sur un département.
     * @throws DaoException
     *             une erreur d'accès aux données.
     */
    List<Eleve> listerDoublonsProbables(String codeDeptMen) throws DaoException;

    /**
     * Liste paginée sans doublon des élèves correspondants à l'infoListe.
     * 
     * @param infoListe
     *            les paramètres de tri, pagination et filtrage
     * @return la liste
     * @throws DaoException
     *             une erreur d'accès aux données.
     */
    List<Eleve> listerDistinct(InfoListe<Eleve> infoListe) throws DaoException;

    /**
     * Liste paginée sans doublon des élèves correspondants au tri et filtre.
     * 
     * @param pagination
     *            les paramètres de pagination et filtrage
     * @param tris
     *            les paramètres de tri
     * @param filtre
     *            les paramètres de filtrage
     * @return la liste
     * @throws DaoException
     *             une erreur d'accès aux données.
     */
    List<Eleve> listerDistinct(Pagination<Eleve> pagination, List<Tri> tris, Filtre filtre) throws DaoException;

    /**
     * Met à jour l'état de l'export vers BEE d'un élève.
     * 
     * @param numDossier
     *            numéro de dossier de l'élève
     * @param codeEtatExportBee
     *            code état de l'export BEE
     * @throws DaoException
     *             une erreur d'accès aux données.
     */
    void majEtatExportBee(Long numDossier, String codeEtatExportBee) throws DaoException;

    /**
     * Met à jour l'état de l'export vers BEE et l'identifiant d'échange BEE pour une liste d'élève.
     * 
     * @param numDossierEleves
     *            liste des numéros de dossier des élèves
     * @param codeEtatExportBee
     *            code état de l'export BEE
     * @param echangeIdSiecleSynchroBee
     *            l'identifiant de l'échange avec BEE
     * @return le nombre de dossiers mis à jour
     * @throws DaoException
     *             une erreur d'accès aux données.
     */
    int majEtatExportBeeEtEchangeIdSiecleSynchroBee(List<Long> numDossierEleves, String codeEtatExportBee,
            String echangeIdSiecleSynchroBee) throws DaoException;

    /**
     * Met à jour l'état de l'export vers BEE d'un élève à partir de l'identifiant d'échange.
     * 
     * @param codeEtatExportBee
     *            code état de l'export BEE
     * @param echangeIdSiecleSynchroBee
     *            l'identifiant de l'échange avec BEE
     * @return le nombre de dossiers mis à jour
     * @throws DaoException
     *             une erreur d'accès aux données.
     */
    int majEtatExportBeeParEchangeIdSiecleSynchroBee(String codeEtatExportBee, String echangeIdSiecleSynchroBee)
            throws DaoException;

    /**
     * Donne les élèves sans collège de secteur final désigné par l'IA-DASEN et avec un ou plusieurs collèges
     * potentiels.
     * 
     * @param codeDept
     *            Le code MEN d'un département
     * @return nombre d'élèves sans collège de secteur final désigné par l'IA-DASEN et avec au moins un collège de
     *         secteur potentiel.
     */
    int compteElevesSansCollegeSecteurFinalAvecCollegesSecteursPotentiels(String codeDept);

    /**
     * Liste les élèves ayant leur adresse au statut à traiter (non référencée ou à valider) OU ayant au moins un
     * responsable avec une adresse à traiter.
     * 
     * @param codeDept
     *            Le code MEN d'un département.
     * @param codeUaiOrigine
     *            Le code UAI de l'école.
     * @param codeUaiCollege
     *            Le code UAI du collège de secteur potentiel des élèves de l'école.
     * @return La liste des numéros de dossiers des élèves qui sont dans l'école <code>codeUaiOrigine</code> et
     *         ayant dans leur liste de collège de secteur potentiel <code>codeUaiCollege</code>
     * @throws DaoException
     *             Une erreur lors de l'accès aux données.
     */
    List<Long> listerElevesAvecAdresseATraiter(String codeDept, String codeUaiOrigine, String codeUaiCollege)
            throws DaoException;

    /**
     * Calcul la valeur du flag pour élève.
     * 
     * @param numDossier
     *            Le numéro de dossier de l'élève
     * @return vrai si l'élève a au moins une adresse à traiter (lui même ou un de ses responsables)
     * @throws DaoException
     *             Une erreur lors de l'accès aux données.
     */
    boolean calculFlagExistenceAdresseATraiterPourEleve(Long numDossier) throws DaoException;

    /**
     * Met à jour le flag d'existence d'une adresse à traiter pour une liste d'adresses.
     * 
     * @param listeIdAdresse
     *            liste des id adresses
     * @return le nombre de dossier mis à jour
     * @throws DaoException
     *             Une erreur lors de l'accès aux données.
     */
    int majFlagExisteAdresseATraiter(List<Long> listeIdAdresse) throws DaoException;

    /**
     * Met à jour le flag d'existence d'une adresse à traiter pour une liste d'élève.
     * 
     * @param numDossierEleves
     *            liste des numéros de dossier des élèves
     * @param aTraiter
     *            valeur du flag
     * @return le nombre de dossier mis à jour
     * @throws DaoException
     *             Une erreur lors de l'accès aux données.
     */
    int majFlagExisteAdresseATraiter(List<Long> numDossierEleves, boolean aTraiter) throws DaoException;

    /**
     * Met à jour le flag d'existence d'une adresse à traiter pour un élève.
     * 
     * @param numDossierEleve
     *            numéro de dossier de l'élève
     * @param aTraiter
     *            valeur du flag
     * @throws DaoException
     *             Une erreur lors de l'accès aux données.
     * @return le nombre de dossier mis à jour
     */
    int majFlagExisteAdresseATraiter(Long numDossierEleve, boolean aTraiter) throws DaoException;

    /**
     * Donne les élèves sans collège de secteur potentiel pour un département donné.
     * 
     * @param codeDepMen
     *            Le code MEN d'un département
     * @param estEtatAffectation
     *            indique si l'application est à l'état affectation
     * @param numDossier
     *            numéro de dossier de l'élève
     * @return La liste des élèves sans collège de secteur potentiel.
     */
    List<Eleve> listerElevesSansClgSecteurPotentiel(String codeDepMen, boolean estEtatAffectation, int numDossier);

    /**
     * <p>
     * Donne s'il existe des voeux avec décision finale pour l'élève.
     * </p>
     *
     * @param eleve
     *            Un élève
     * @return Vrai si une décision finale existe.
     * @throws DaoException
     *             Une erreur lors de l'accès aux données.
     */
    boolean existeVoeuxAvecDecisionFinale(Eleve eleve) throws DaoException;

    /**
     * Recherche les numéros de dossier élèves qui ont une adresse à traiter.
     * 
     * @param listeIdAdresse
     *            liste des identifiants des adresses
     * @param aTraiter
     *            indique si les adresses recherchées sont à traiter ou non
     * @return la liste des numéros de dossier
     * @throws DaoException
     *             Une erreur lors de l'accès aux données.
     */
    List<Long> chercheNumDossierAdresseATraiter(List<Long> listeIdAdresse, Boolean aTraiter) throws DaoException;

    /**
     * Compte les élèves sans collège de secteur potentiel pour un département donné.
     * 
     * @param codeDepMen
     *            Le code MEN d'un département
     * @param estEtatAffectation
     *            indique si l'application est à l'état affectation
     * @return La liste des élèves sans collège de secteur potentiel.
     */
    int compteElevesSansClgSecteurPotentiel(String codeDepMen, boolean estEtatAffectation);

    /**
     * Méthode qui met à jour les langues étudiées à l'école pour une liste d'élèves.
     * 
     * @param langueEtrangereOblEcole
     *            la langue étrangère obligatoire étudiée à l'école
     * @param langueEtrangereFacEcole
     *            la langue étrangère facultative étudiée à l'école
     * @param langueRegionaleEcole
     *            la langue régionale étudiée à l'école
     * @param numDossierEleves
     *            la liste des numéros de dossiers des élèves à mettre à jour
     * @return le nombre de lignes mises à jour
     */
    int majLanguesEcole(Matiere langueEtrangereOblEcole, Matiere langueEtrangereFacEcole,
            Matiere langueRegionaleEcole, Long[] numDossierEleves);

    /**
     * Compte le nombre d'élèves avec un calcul de collège de secteur abouti ou abouti hors département.
     * 
     * @param codeDeptMen
     *            le code MEN du département
     * @return le nombre d'élèves correspondant
     */
    int compteElevesAvecCalculCollegeSecteurAbouti(String codeDeptMen);

    /**
     * Compte le nombre d'élèves avec un calcul de collège de secteur déjà effectué.
     * soit abouti ou abouti hors département ou non abouti ou impossible
     * 
     * @param codeDeptMen
     *            le code MEN du département
     * @return le nombre d'élèves correspondant
     */
    int compteElevesAvecCalculCollegeSecteurDejaEffectue(String codeDeptMen);

    /**
     * @param tris
     *            les tris à appliquer
     * @return un sélecteur limité aux élèves ayant au moins un collège de secteur
     */
    Selector<Object[]> selectorCollegesSecteursId(List<Tri> tris);

    /**
     * Nombre d'élèves avec une demande de dérogation sans accusé réception édité.
     * 
     * @param codeUai
     *            Le code UAI de l'école
     * @param codeDept
     *            Le code MEN du département
     * @return le nombre d'élève trouvés
     */
    int compteElevesAvecDerogSansEditionAccuseReceptionEcole(String codeUai, String codeDept);

    /**
     * Liste les élèves avec une demande de dérogation sans accusé réception édité.
     * 
     * @param codeUai
     *            Le code UAI de l'école
     * @param codeDept
     *            Le code MEN du département
     * @return la liste des élèves trouvés
     */
    List<Eleve> listerElevesAvecDerogSansEditionAccuseReceptionEcole(String codeUai, String codeDept);

    /**
     * Liste les élèves par leur numéro de dossier.
     *
     * @param pagination
     *            les paramètres de pagination et filtrage
     * @param tris
     *            les paramètres de tri
     * @param numDossiers
     *            liste des numéros de dossier
     * @return la liste des élèves demandés
     * @throws DaoException
     *             Une erreur lors de l'accès aux données.
     */
    List<Eleve> listerElevesParNumDossier(Pagination<Eleve> pagination, List<Tri> tris, List<Long> numDossiers)
            throws DaoException;
}
