/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.dao.interfaces.nomenclature;

import java.util.List;

import fr.edu.nancy.affelnet6.domain.nomenclature.PropositionCarteScolaireRNVP;
import fr.edu.nancy.commun.dao.interfaces.BaseDao;
import fr.edu.nancy.commun.exceptions.DaoException;

/**
 * Interface pour les objets PropositionCarteScolaireRNVP.
 */
public interface PropositionCarteScolaireRNVPDao extends BaseDao<PropositionCarteScolaireRNVP, Long> {
    /**
     * Efface les propositions d'adresse des tronçons de la carte scolaire du département en vue d'un nouvel
     * import.
     * 
     * @param deptMenId
     *            Identifiant du département MEN concerné
     * @return nombre de suppressions effectuées
     * @throws DaoException
     *             Une erreur lors de l'accès aux données.
     */
    int supprimerPourRemplacerDepartement(int deptMenId) throws DaoException;

    /**
     * Efface les propositions d'une liste de tronçons de la carte scolaire.
     * 
     * @param listeIdTronconCarteScolaire
     *            Liste des identifiant des tronçons de la carte scolaire pour lesquels on veut purger les
     *            propositions
     * @return nombre de suppressions effectuées
     * @throws DaoException
     *             Une erreur lors de l'accès aux données.
     */
    int supprimerPourListeTronconCarteScolaire(List<Long> listeIdTronconCarteScolaire) throws DaoException;

    /**
     * Efface les propositions d'un tronçon de la carte scolaire.
     * 
     * @param idTronconCarteScolaire
     *            Identifiant du tronçon de la carte pour lequel on veut purger les propositions
     * @return nombre de suppressions effectuées
     * @throws DaoException
     *             Une erreur lors de l'accès aux données.
     */
    int supprimerPourTronconCarteScolaire(Long idTronconCarteScolaire) throws DaoException;
}
