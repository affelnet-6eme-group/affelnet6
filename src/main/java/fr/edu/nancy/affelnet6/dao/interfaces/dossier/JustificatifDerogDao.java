/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.dao.interfaces.dossier;

import fr.edu.nancy.affelnet6.domain.dossier.JustificatifDerog;
import fr.edu.nancy.affelnet6.domain.nomenclature.MotifDerog;
import fr.edu.nancy.commun.dao.interfaces.BaseDao;

/**
 * Interface pour les objets JustificatifDerog.
 * 
* @see fr.edu.nancy.affelnet6.domain.dossier.JustificatifDerog
 */

public interface JustificatifDerogDao extends BaseDao<JustificatifDerog, Long> {

    /**
     * Compte les justificatifs par motif concernant une offre de formation .
     * 
     * @param codeOffre
     *            code de l'offre
     * @param codeMotif
     *            code du motif
     * @return Le nombre de demandes avec cette dérogation sur cette offre
     */
    int compteDemandesDerog(String codeOffre, String codeMotif);

    /**
     * Compte les justificatifs par motif .
     * 
     * @param motif
     *            motif de dérog
     * @return Le nombre de demandes avec cette dérogation
     */
    int compteDemandesDerogMotif(MotifDerog motif);

    /**
     * Compte les justificatifs par motif concernant une offre de formation pour les élèves affectés.
     * 
     * @param codeOffre
     *            code de l'offre
     * @param codeMotif
     *            code du motif
     * @return Le nombre d'affectations avec cette dérogation sur cette offre
     * 
     */
    int compteAffectationsDerog(String codeOffre, String codeMotif);
}
