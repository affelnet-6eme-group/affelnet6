/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.dao.interfaces.nomenclature;

import java.util.List;

import fr.edu.nancy.affelnet6.domain.nomenclature.MotifDerog;
import fr.edu.nancy.commun.dao.interfaces.BaseDao;
import fr.edu.nancy.commun.exceptions.ApplicationException;
import fr.edu.nancy.commun.exceptions.BusinessException;
import fr.edu.nancy.commun.exceptions.TechnicalException;

/**
 * Interface pour les objets MotifDerog.
 * 
* @see fr.edu.nancy.affelnet6.domain.nomenclature.MotifDerog
 */

public interface MotifDerogDao extends BaseDao<MotifDerog, Integer> {

    /**
     * Teste s'il existe un enregistrement dans les données correspondant à la
     * code passée en paramètre.
     * 
     * @param code
     *            la clé de l'enregistrement recherché
     * @param deptMenId
     *            le dept concerné
     * @return <code>true</code> si il existe déjà un enregistrement
     * @throws ApplicationException
     *             Exception remontant en cas de problème
     */
    boolean existeParCode(String code, int deptMenId) throws ApplicationException;

    /**
     * Liste les motifs de dérogation d'un département.
     *
     * @param idDeptMen
     *            L'identifiant MEN du département
     * @return Une liste de motifs de dérogation
     */
    List<MotifDerog> listerParDept(int idDeptMen);

    /**
     * Liste les motifs de dérogation d'un département.
     *
     * @param codeDeptMen
     *            Le code du département MEN
     * @return Une liste de motifs de dérogation
     */
    List<MotifDerog> listerParDept(String codeDeptMen);

    /**
     * Donne un motif de dérogation par son code et le département.
     *
     * @param code
     *            Le code du motif de dérogation
     * @param codeDeptMen
     *            Le code du département MEN
     * @return Un motif de dérogation
     */
    MotifDerog chargerByCode(String code, String codeDeptMen);

    /**
     * Compte le nombre de motifs de dérogation (non national) pour un
     * département donné.
     * 
     * @param idDept
     *            ID du département
     * @return nombre de motifs de dérogation
     * @throws TechnicalException
     *             exception remontant en cas de problème
     * @throws BusinessException
     *             exception remontant en cas de problème
     */
    int compterParDept(int idDept) throws TechnicalException, BusinessException;
}
