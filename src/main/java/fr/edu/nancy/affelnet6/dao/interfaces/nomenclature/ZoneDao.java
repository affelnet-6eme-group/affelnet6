/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.dao.interfaces.nomenclature;

import java.util.Date;
import java.util.List;

import fr.edu.nancy.affelnet6.domain.nomenclature.Zone;
import fr.edu.nancy.affelnet6.domain.nomenclature.ZoneId;
import fr.edu.nancy.commun.dao.interfaces.BaseDao;
import fr.edu.nancy.commun.exceptions.DaoException;

/**
 * Interface pour les objets Zone.
 * 
 * @see fr.edu.nancy.affelnet6.domain.nomenclature.Zone
 */
public interface ZoneDao extends BaseDao<Zone, ZoneId> {

    /**
     * @param codeDeptMen
     *            code du département MEN
     * @param dateOuvertureRef
     *            Date d'ouverture de référence pour rechercher les
     *            zones ouvertes
     * @param dateFermetureRef
     *            Date de fermeture de référence pour rechercher les
     *            zones ouvertes
     * @return liste d'UID des circonscriptions du département MEN.
     */
    List<String> getUAIAccesCirconscription(String codeDeptMen, Date dateOuvertureRef, Date dateFermetureRef);

    /**
     * Teste si une circonscription est présente en base par son code UAI.
     * 
     * @param uai
     *            l'uai de la circonscription à tester
     * @throws DaoException
     *             Exception lancée en cas de problème
     * @return vrai si la circonscription existe sinon faux
     */
    boolean existeCirconscriptionParUai(String uai) throws DaoException;

    /**
     * Charge la zone géographique correspondant à l'UAI de zone donné.
     * 
     * @param uai
     *            code UAI de la zone
     * @return Zone géographique
     * @throws DaoException
     *             Exception lancée en cas de problème
     */
    Zone chargerCirconscriptionParUai(String uai) throws DaoException;

    /**
     * Charge la zone géographique correspondant à l'UAI de zone donné.
     *
     * @param type
     *            type de la zone
     * @param uai
     *            code UAI de la zone
     * @return Zone géographique
     * @throws DaoException
     *             Exception lancée en cas de problème
     */
    Zone chargerParTypeEtUai(String type, String uai) throws DaoException;

    /**
     * Teste si une zone est présente en base par son code UAI et son type.
     *
     * @param type
     *            type de la zone
     * @param uai
     *            code UAI de la zone
     * @throws DaoException
     *             Exception lancée en cas de problème
     * @return vrai si la zone existe sinon faux
     */
    boolean existeParTypeEtUai(String type, String uai) throws DaoException;

}
