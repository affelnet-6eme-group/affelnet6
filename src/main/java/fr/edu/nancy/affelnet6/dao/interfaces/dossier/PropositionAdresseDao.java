/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.dao.interfaces.dossier;

import java.util.List;

import fr.edu.nancy.affelnet6.domain.dossier.PropositionAdresse;
import fr.edu.nancy.commun.dao.interfaces.BaseDao;
import fr.edu.nancy.commun.exceptions.DaoException;

/**
 * Interface pour les objets PropositionAdresse.
 */
public interface PropositionAdresseDao extends BaseDao<PropositionAdresse, Long> {
    /**
     * Efface les propositions d'adresse du département en vue d'un nouvel import.
     * 
     * @param dptMenId
     *            Identifiant du département MEN Concerné
     * @return nombre de suppressions effectuées
     * @throws DaoException
     *             Une erreur lors de l'accès aux données.
     */
    int supprimerPourRemplacerDepartement(int dptMenId) throws DaoException;

    /**
     * Efface les propositions d'adresse d'une liste d'adresses.
     * 
     * @param listeAdresseId
     *            liste d'identifiant des l'adresse pour lesquelles on veut purger les propositions
     * @return nombre de suppressions effectuées
     * @throws DaoException
     *             Une erreur lors de l'accès aux données.
     */
    int supprimerPourListeAdresses(List<Long> listeAdresseId) throws DaoException;

    /**
     * Efface les propositions d'adresse d'une adresse.
     * 
     * @param adresseId
     *            Identifiant de l'adresse pour laquelle on veut purger les propositions
     * @return nombre de suppressions effectuées
     * @throws DaoException
     *             Une erreur lors de l'accès aux données.
     */
    int supprimerPourAdresse(Long adresseId) throws DaoException;
}
