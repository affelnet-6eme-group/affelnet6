/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.dao.interfaces.nomenclature;

import java.util.Date;
import java.util.List;

import fr.edu.nancy.affelnet6.domain.nomenclature.EtablissementOrigine;
import fr.edu.nancy.commun.dao.interfaces.BaseDao;
import fr.edu.nancy.commun.exceptions.ApplicationException;
import fr.edu.nancy.commun.exceptions.DaoException;

/**
 * Interface pour les objets EtablissementOrigine.
 * 
 * @see fr.edu.nancy.affelnet6.domain.nomenclature.EtablissementOrigine
 */
public interface EtablissementOrigineDao extends BaseDao<EtablissementOrigine, String> {

    /**
     * Teste si un EtablissementOrigine est présent en base par son code Uai.
     * 
     * @param ecoleUai
     *            l'uai de l'EtablissementOrigine à tester
     * @return vrai si l'UAI existe
     * @throws ApplicationException
     *             Exception lancée en cas de problème
     */
    boolean existeParUai(String ecoleUai) throws ApplicationException;

    /**
     * Donne la liste des UAI des écoles origine possibles pour une académie donnée.
     * Pour l'académie d'implantation, cela correspond à la liste des écoles qui ont
     * potentiellement accès à l'application.
     * 
     * @param codeAcademie
     *            Code de l'académie cible
     * @param dateOuvertureRef
     *            Date d'ouverture de référence pour rechercher les
     *            établissements ouverts
     * @param dateFermetureRef
     *            Date de fermeture de référence pour rechercher les
     *            établissements ouverts
     * @return liste des identifiants d'écoles
     */
    List<String> getListeUAIAccesEcoles(String codeAcademie, Date dateOuvertureRef, Date dateFermetureRef);

    /**
     * Donne le code UAI d'une DSDEN pour un departement donné.
     * 
     * @param codeDeptMen
     *            le code MEN du departement
     * @return le code UAI de la DSDEN d'un departement
     * @throws ApplicationException
     *             Exception lancée en cas de problème
     */
    String getUAIAccesDsdenDept(String codeDeptMen) throws ApplicationException;

    /**
     * Charge un EtablissementOrigine à partir de son codeUai.
     *
     * @param codeUai
     *            Le code UAI de l'établissement à charger
     * @return L'<code>Etablissement</code> correspondant au code UAI
     * @throws DaoException
     *             En cas de problème d'accès à la base de données
     */
    EtablissementOrigine chargerParUai(String codeUai) throws DaoException;

    /**
     * Charge une liste d'EtablissementOrigine à partir de leur codeUai.
     *
     * @param codUaiEtabs
     *            Liste des codes UAI des établissements à charger
     * @return La liste des établissements d'origine correspondants aux codes UAI.
     * @throws ApplicationException
     *             Exception lancée en cas de problème
     */
    List<EtablissementOrigine> chargerParListeUai(List<String> codUaiEtabs) throws ApplicationException;

    /**
     * Repositionne à "Non" les flags "import élève Onde" des établissements du département.
     * 
     * @param codeDeptMen
     *            code département du MEN
     * @throws DaoException
     *             en cas d'erreur
     */
    void razFlagsImportElveOnde(String codeDeptMen) throws DaoException;
}
