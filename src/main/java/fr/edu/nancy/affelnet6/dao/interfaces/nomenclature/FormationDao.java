/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.dao.interfaces.nomenclature;

import java.util.List;

import fr.edu.nancy.affelnet6.domain.nomenclature.Formation;
import fr.edu.nancy.commun.dao.interfaces.BaseDao;

/**
 * Interface pour l'accès aux données des formations.
 */
public interface FormationDao extends BaseDao<Formation, String> {

    /**
     * {@link #lister6emeDept(String, boolean)}.
     * 
     * @param codeDeptMen
     *            Le code du département.
     * @return La liste des formations enseignées en 6ème dans le département (avec la formation SEGPA)
     */
    List<Formation> lister6emeDept(String codeDeptMen);

    /**
     * Donne la liste des formations proposées dans le département.
     * 
     * Ce sont les formations pour lesquelles il existe une offre de formation
     * dans le département.
     * Les offres avec une capacité d'accueil à 0 sont incluses.
     * 
     * Les formations sont triées par libellé de formation ascendant.
     * 
     * @param codeDeptMen
     *            Le code du département.
     * @param avecSegpa
     *            Booléen indiquant si l'on prend la formation SEGPA ou non.
     * @return La liste des formations proposée dans le département.
     */
    List<Formation> lister6emeDept(String codeDeptMen, boolean avecSegpa);

}
