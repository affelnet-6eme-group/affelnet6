/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.dao.interfaces.dossier;

import java.util.List;

import org.hibernate.ScrollableResults;

import fr.edu.nancy.affelnet6.domain.dossier.Adresse;
import fr.edu.nancy.commun.dao.interfaces.BaseDao;
import fr.edu.nancy.commun.dao.interfaces.Pagination;
import fr.edu.nancy.commun.dao.interfaces.Tri;
import fr.edu.nancy.commun.dao.interfaces.filtres.Filtre;
import fr.edu.nancy.commun.exceptions.DaoException;
import fr.edu.nancy.commun.web.session.InfoListe;

/**
 * Interface pour les objets Adresse.
 * 
 * @see fr.edu.nancy.affelnet6.domain.dossier.Adresse
 */

public interface AdresseDao extends BaseDao<Adresse, Long> {

    /**
     * Efface une adresse .
     *
     * @param adresse
     *            L'adresse à supprimer
     * @throws DaoException
     *             en cas de problème
     */
    void supprimer(Adresse adresse) throws DaoException;

    /**
     * Supprime une adresse à partir de son identifiant.
     * 
     * @param adresseId
     *            identifiant de l'adresse
     * @throws DaoException
     *             en cas de problème
     */
    void supprimer(Long adresseId) throws DaoException;

    /**
     * Supprime une liste d'adresses à partir de leurs identifiants.
     *
     * @param adressesId
     *            liste des identifiants d'adresses
     * @return nombre de suppressions effectuées
     * @throws DaoException
     *             en cas de problème
     */
    int supprimer(List<Long> adressesId) throws DaoException;

    /**
     * Efface les adresses du département en vue d'un nouvel import.
     * 
     * @param dptMenId
     *            Identifiant du département MEN Concené
     * @return nombre de suppressions effectuées
     * @throws DaoException
     *             en cas de problème
     */
    int supprimerPourRemplacerDepartement(int dptMenId) throws DaoException;

    /**
     * Raffraîchit l'adresse passée en paramètre dans la session hibernate.
     * 
     * @param adresse
     *            L'adresse que l'on veut rafraîchir.
     */
    void rafraichit(Adresse adresse);

    /**
     * Liste les adresses non contrôlées des élèves et des responsables pour une école.
     * 
     * @param codeUaiOrigine
     *            code de l'école d'origine des élèves
     * @param codeDept
     *            Identifiant du département MEN concerné
     * @return la liste des adresses
     * @throws DaoException
     *             en cas de problème
     */
    List<Adresse> listeAdressesNonControleesEcoleOrigine(String codeUaiOrigine, String codeDept)
            throws DaoException;

    /**
     * Liste les adresses non contrôlées des élèves et des responsables pour un collège.
     * 
     * @param codeUaiCollege
     *            code du collège d'affectation finale des élèves
     * @param codeDept
     *            Identifiant du département MEN concerné
     * @return la liste des adresses
     * @throws DaoException
     *             en cas de problème
     */
    List<Adresse> listeAdressesNonControleesCollegeSaisie(String codeUaiCollege, String codeDept)
            throws DaoException;

    /**
     * 
     * @param deptMenId
     *            Identifiant du département MEN Concené
     * @param uaiEcole
     *            Identifiant de l'école d'origine
     * @param uaiCollege
     *            Identifiant du collège d'affectation
     * @return le nombre de lignes mises à jour
     * @throws DaoException
     *             Exception lancée en cas de problème à la mise à jour
     */
    int majDateRNVP(int deptMenId, String uaiEcole, String uaiCollege) throws DaoException;

    /**
     * Donne le nombre de voies manquantes dans la carte scolaire à partir de la simulation du calcul de collège de
     * secteur.
     * 
     * @param codeDept
     *            code département
     * @return le nombre de voies manquantes dans la carte scolaire à partir de la simulation du calcul de collège
     *         de secteur
     * @throws DaoException
     *             en cas d'erreur
     */
    int compteNbVoiesManquantesCarteScoalire(String codeDept) throws DaoException;

    /**
     * Donne le nombre de numéros de voie manquants dans la carte scolaire à partir de la simulation du calcul de
     * collège de secteur.
     * 
     * @param codeDept
     *            code département
     * @return le nombre de numéros de voie manquants dans la carte scolaire à partir de la simulation du calcul de
     *         collège de secteur
     * @throws DaoException
     *             en cas d'erreur
     */
    int compteNbNumerosVoieManquantsCarteScoalire(String codeDept) throws DaoException;

    /**
     * Donne le nombre de chevauchements de voies dans la carte scolaire à partir de la simulation du calcul de
     * collège de secteur.
     * 
     * @param codeDept
     *            code département
     * @return le nombre de chevauchements de voies dans la carte scolaire à partir de la simulation du calcul de
     *         collège de secteur
     * @throws DaoException
     *             en cas d'erreur
     */
    int compteNbChevauchementsVoiesCarteScoalire(String codeDept) throws DaoException;

    /**
     * Liste les adresses avec un distinct sur le type et libellé de voie, le code postal et la commune.
     * 
     * @param infoListe
     *            les informations de pagination, tri, filtre
     * @param avecDistinctNumeroVoie
     *            indique s'il faut aussi faire un distinct sur le numéro de voie et l'indice de répétition
     * @return la liste
     * @throws DaoException
     *             en cas d'erreur
     */
    List<Adresse> listerDistinctLibelleVoieCommune(InfoListe<Adresse> infoListe, boolean avecDistinctNumeroVoie)
            throws DaoException;

    /**
     * Liste les adresses avec un distinct sur le type et libellé de voie, le code postal et la commune.
     * 
     * @param paginationSupport
     *            les paramètres de pagination et filtrage
     * @param listTri
     *            les paramètres de tri
     * @param filtre
     *            les paramètres de filtrage
     * @param avecDistinctNumeroVoie
     *            indique s'il faut aussi faire un distinct sur le numéro de voie et l'indice de répétition
     * @return la liste
     * @throws DaoException
     *             en cas d'erreur
     */
    List<Adresse> listerDistinctLibelleVoieCommune(Pagination<Adresse> paginationSupport, List<Tri> listTri,
            Filtre filtre, boolean avecDistinctNumeroVoie) throws DaoException;

    /**
     * Liste les adresses à partir du code INSEE de la commune.
     * 
     * @param codeInsee
     *            code INSEE de la commune
     * @return la liste des adresses
     * @throws DaoException
     *             en cas d'erreur
     */
    List<Adresse> listerParCodeCommuneInsee(String codeInsee) throws DaoException;

    /**
     * Liste les adresses à rapprocher.
     *
     * @param codeDepMen
     *            Code MEN du département concerné
     * @return la liste des adresses à rapprocher
     */
    ScrollableResults listerAdressesARapprocher(String codeDepMen);
}
