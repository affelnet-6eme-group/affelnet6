/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.dao.interfaces.nomenclature;

import java.util.Date;
import java.util.List;

import fr.edu.nancy.affelnet6.domain.nomenclature.ZoneEtab;
import fr.edu.nancy.affelnet6.domain.nomenclature.ZoneEtabId;
import fr.edu.nancy.commun.dao.interfaces.BaseDao;
import fr.edu.nancy.commun.exceptions.DaoException;

/**
 * Interface pour les objets ZoneEtab.
 * 
 * @see ZoneEtab
 */
public interface ZoneEtabDao extends BaseDao<ZoneEtab, ZoneEtabId> {

    /**
     * Charge la zone établissement correspondant à l'UAI de zone donné.
     * 
     * @param type
     *            type de zone
     * @param codeZone
     *            code zone
     * @param uai
     *            code UAI de la zone
     * @return Zone géographique
     * @throws DaoException
     *             Exception lancée en cas de problème
     */
    ZoneEtab chargerParId(String type, String codeZone, String uai) throws DaoException;

    /**
     * Teste si une zone établissement est présente en base par son code UAI et son type.
     *
     * 
     * @param type
     *            type de zone
     * @param codeZone
     *            code zone
     * @param uai
     *            code UAI de la zone
     * @throws DaoException
     *             Exception lancée en cas de problème
     * @return vrai si la zone existe
     */
    boolean existeParId(String type, String codeZone, String uai) throws DaoException;

    /**
     * Liste les zones établissement qui sont ouvertes lors de la campagne en cours.
     * 
     * @param type
     *            type de zone
     * @param codeZone
     *            code zone
     * @param codeUaiEtab
     *            code UAI de l'établissement
     * @param dateOuvertureRef
     *            Date d'ouverture de référence pour rechercher les zones ouvertes
     * @param dateFermetureRef
     *            Date de fermeture de référence pour rechercher les zones ouvertes
     * @return la liste des zones d'établissements
     * @throws DaoException
     *             Exception lancée en cas de problème
     */
    List<ZoneEtab> listerLiensEncours(String type, String codeZone, String codeUaiEtab, Date dateOuvertureRef,
            Date dateFermetureRef) throws DaoException;
}
