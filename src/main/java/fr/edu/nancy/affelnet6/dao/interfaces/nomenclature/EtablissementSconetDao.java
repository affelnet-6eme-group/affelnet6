/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.dao.interfaces.nomenclature;

import java.util.Date;
import java.util.List;
import java.util.Map;

import fr.edu.nancy.affelnet6.domain.affectation.CompteurEtablissementOffre;
import fr.edu.nancy.affelnet6.domain.nomenclature.EtablissementSconet;
import fr.edu.nancy.commun.dao.interfaces.BaseDao;
import fr.edu.nancy.commun.exceptions.DaoException;

/**
 * Interface pour les objets Etablissement.
 * 
 * @see fr.edu.nancy.affelnet6.domain.nomenclature.EtablissementSconet
 */

public interface EtablissementSconetDao extends BaseDao<EtablissementSconet, Integer> {

    /**
     * Test si un Etablissement est présent en base.
     * 
     * @param etablissementRne
     *            le code rne de l'Etablissement sconet à tester
     * @return Vrai si l'établissement Sconet existe, faux sinon
     * @throws DaoException
     *             En cas de problème d'accès à la base de données
     */
    boolean existeParRne(String etablissementRne) throws DaoException;

    /**
     * Charge un établissement Sconet à partir de son codeRne.
     *
     * @param codeRne
     *            Le code RNE de l'établissement à charger
     * @return L'<code>Etablissement</code> correspondant au code RNE
     * @throws DaoException
     *             En cas de problème d'accès à la base de données
     */
    EtablissementSconet chargerParCodeRne(String codeRne) throws DaoException;

    /**
     * Donne la liste des codes UAI des établissements d'accueil qui ont accès à l'application.
     * Pour une académie donnée, cela correspond à la liste des collèges publics ouverts dans l'académie.
     * 
     * @param codeAcademie
     *            Code de l'académie cible
     * @param dateOuvertureRef
     *            Date d'ouverture de référence pour rechercher les
     *            établissements ouverts
     * @param dateFermetureRef
     *            Date de fermeture de référence pour rechercher les
     *            établissements ouverts
     * @return Liste des identifiants des établissements d'accueil autorisés à accés à l'application
     */
    List<String> getListeUAIAccesColleges(String codeAcademie, Date dateOuvertureRef, Date dateFermetureRef);

    /**
     * 
     * @param codeDeptMen
     *            Le département de l'utilisateur.
     * @return Map&lt;codeRne, CompteurEtablissementOffre&gt; : La Map de tous les établissements avec leur
     *         compteur dans un département donné.
     * @throws DaoException
     *             Un problème d'accès aux données.
     */
    Map<String, CompteurEtablissementOffre> getCompteurEtablissementOffre(String codeDeptMen) throws DaoException;

    /**
     * Test si dans la carte scolaire il existe un tronçon avec plusieurs collèges.
     * 
     * @param codeDeptMen
     * pour un département donné
     * @return Vrai si aucun troncon n'a plus d'un collège
     * @throws DaoException
     *             Exception lancée en cas de problème
     */
    boolean isCarteScolaireMonoCollege(String codeDeptMen) throws DaoException;

    /**
     * Recupère la date de dernière mise à jour des établissements et zone.
     * 
     * @return Date de dernière mise à jour.
     * @throws DaoException
     *             En cas de problème d'accès à la base de données.
     */
    Date getDateDerniereModif() throws DaoException;
}
