/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.dao.interfaces.nomenclature;

import fr.edu.nancy.affelnet6.domain.nomenclature.CodePostal;
import fr.edu.nancy.commun.dao.interfaces.BaseDao;
import fr.edu.nancy.commun.exceptions.ApplicationException;

/**
 * Interface pour les objets CodePostal.
 * 
* @see fr.edu.nancy.affelnet6.domain.nomenclature.CodePostal
 */

public interface CodePostalDao extends BaseDao<CodePostal, Integer> {

    /**
     * Test si un CodePostal est présent en base par son code postal.
     * 
     * @param cp
     *            le code postal à tester
     * @return true si le code postal est trouvé
     * @throws ApplicationException
     *             Exception lancée en cas de problème
     */
    boolean existeCodePostalParCP(String cp) throws ApplicationException;

    /**
     * Charge un objet CodePostal à partir de son code
     * 
     * @param code
     *            Le code à rechercher
     * @return le CodePostal trouvé
     * @throws ApplicationException
     *             Exception lancée en cas de problème
     */
    CodePostal chargerParCP(String code) throws ApplicationException;
}
