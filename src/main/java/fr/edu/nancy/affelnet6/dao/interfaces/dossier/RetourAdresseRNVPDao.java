/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.dao.interfaces.dossier;

import java.util.List;

import fr.edu.nancy.affelnet6.domain.dossier.RetourAdresseRNVP;
import fr.edu.nancy.commun.dao.interfaces.BaseDao;
import fr.edu.nancy.commun.exceptions.DaoException;

/**
 * Interface pour les objets RetourRNVP.
 */
public interface RetourAdresseRNVPDao extends BaseDao<RetourAdresseRNVP, Long> {
    /**
     * Efface les codes retour RNVP d'adresse du département en vue d'un nouvel import.
     * 
     * @param dptMenId
     *            Identifiant du département MEN Concené
     * @return nombre de suppressions effectuées
     * @throws DaoException
     *             Une erreur lors de l'accès aux données.
     */
    int supprimerPourRemplacerDepartement(int dptMenId) throws DaoException;

    /**
     * Efface les codes retour RNVP d'adresse d'une adresse.
     * 
     * @param adresseId
     *            Identifiant de l'adresse pour laquelle on veut purger les codes retour RNVP
     * @return nombre de suppressions effectuées
     * @throws DaoException
     *             Une erreur lors de l'accès aux données.
     */
    int supprimerPourAdresse(Long adresseId) throws DaoException;

    /**
     * Efface les codes retour RNVP d'adresse d'une liste d'adresses.
     * 
     * @param listeAdresseId
     *            Liste des identifiant des adresses pour lesquelles on veut purger les codes retour RNVP
     * @return nombre de suppressions effectuées
     * @throws DaoException
     *             Une erreur lors de l'accès aux données.
     */
    int supprimerPourListeAdresses(List<Long> listeAdresseId) throws DaoException;
}
