/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.dao.interfaces.nomenclature;

import fr.edu.nancy.affelnet6.domain.nomenclature.OffreFormation;
import fr.edu.nancy.commun.dao.interfaces.BaseDao;

/**
 * Interface pour les objets OffreFormation.
 * 
* @see fr.edu.nancy.affelnet6.domain.nomenclature.OffreFormation
 */

public interface OffreFormationDao extends BaseDao<OffreFormation, String> {

    /**
     * Retourne le dernier ID inséré en base par rapport a son departement.
     *
     * @param dept
     *            le département concerné par la recherche
     * @return le dernier ID inséré en base par rapport a son departement
     */
    String getDernierId(String dept);

    int getNbOffre(String code);
}
