/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.dao.interfaces.nomenclature;

import java.util.List;
import java.util.Set;

import fr.edu.nancy.affelnet6.domain.nomenclature.LangueVivante;
import fr.edu.nancy.affelnet6.domain.nomenclature.Matiere;
import fr.edu.nancy.commun.dao.interfaces.BaseDao;
import fr.edu.nancy.commun.exceptions.TechnicalException;

/** Interface pour les objets d'accès aux données des matières. */
public interface MatiereDao extends BaseDao<Matiere, String> {

    /**
     * Constitue la liste des matières correspondant à un ensemble de langues vivantes dispensées.
     * 
     * @param langueVivantes
     *            ensemble de langues vivantes
     * @return liste des matières
     * @throws TechnicalException
     *             en cas de problème d'accès aux données
     */
    List<Matiere> listerMatiereDisponible(Set<LangueVivante> langueVivantes) throws TechnicalException;

    /**
     * Charge une matiere à partir de sa clé de gestion.
     * 
     * @param cleGestionMatiere
     *            Clé de gestion de la matiere à charger
     * @return <code>null</code> si la matière n'est pas trouvée
     * @throws TechnicalException
     *             en cas de problème d'accès aux données
     */
    Matiere chargerByCleGestion(String cleGestionMatiere) throws TechnicalException;

    /**
     * Teste si une matière existe à partir de sa clé de gestion.
     * 
     * @param cleGestionMatiere
     *            Clé de gestion de la matiere à tester
     * @return vrai si la matière existe, sinon faux
     * @throws TechnicalException
     *             en cas de problème d'accès aux données
     */
    boolean existeByCleGestion(String cleGestionMatiere) throws TechnicalException;

}
