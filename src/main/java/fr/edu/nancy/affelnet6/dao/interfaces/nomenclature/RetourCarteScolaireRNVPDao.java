/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.dao.interfaces.nomenclature;

import fr.edu.nancy.affelnet6.domain.nomenclature.RetourCarteScolaireRNVP;
import fr.edu.nancy.commun.dao.interfaces.BaseDao;
import fr.edu.nancy.commun.exceptions.DaoException;

/**
 * Interface pour les objets RetourCarteScolaireRNVPDao.
 */
public interface RetourCarteScolaireRNVPDao extends BaseDao<RetourCarteScolaireRNVP, Long> {
    /**
     * Efface les codes retour RNVP des tronçons de la carte scolaire du département en vue d'un nouvel import.
     * 
     * @param deptMenId
     *            Identifiant du département MEN concerné
     * @return nombre de suppressions effectuées
     * @throws DaoException
     *             Une erreur lors de l'accès aux données.
     */
    int supprimerPourRemplacerDepartement(int deptMenId) throws DaoException;

    /**
     * Efface les codes retour RNVP d'un tronçon de la carte scolaire.
     * 
     * @param idTronconCarteScolaire
     *            Identifiant du tronçon de la carte pour lequel on veut purger les codes retour RNVP
     * @return nombre de suppressions effectuées
     * @throws DaoException
     *             Une erreur lors de l'accès aux données.
     */
    int supprimerPourTronconCarteScolaire(Long idTronconCarteScolaire) throws DaoException;

}
