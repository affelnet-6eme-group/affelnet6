/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.dao.interfaces.nomenclature;

import fr.edu.nancy.affelnet6.domain.nomenclature.CommuneInsee;
import fr.edu.nancy.commun.dao.interfaces.BaseDao;
import fr.edu.nancy.commun.exceptions.ApplicationException;

/**
 * Interface pour les objets CommuneInsee.
 * 
 * @see fr.edu.nancy.affelnet6.domain.nomenclature.CommuneInsee
 */

public interface CommuneInseeDao extends BaseDao<CommuneInsee, Integer> {

    /**
     * Charge un(e) CommuneInsee à partir de son code.
     *
     * @param code
     *            l'identifiant du CommuneInsee à charger
     * @return le/la <code>CommuneInsee</code> correspondant à l'id
     * @throws ApplicationException
     *             Exception remontant en cas de problème
     */
    CommuneInsee chargerCommuneInseeParCode(String code) throws ApplicationException;

    /**
     * Charge un(e) CommuneInsee à partir de son libelle et du département.
     *
     * @param departementInseeId
     *            l'identifiant Insee du département
     * @param libelle
     *            le libelle du CommuneInsee à charger
     * @return le/la <code>CommuneInsee</code> correspondant au libelle
     * @throws ApplicationException
     *             Exception remontant en cas de problème
     */
    CommuneInsee chargerCommuneInseeParDeptEtLibelle(Integer departementInseeId, String libelle)
            throws ApplicationException;

    /**
     * Teste s'il existe un enregistrement dans les données correspondant à la
     * clé passée en paramètre.
     * 
     * @param code
     *            la clé de l'enregistrement recherché
     * @return <code>true</code> si il existe déjà un enregistrement
     * @throws ApplicationException
     *             Exception remontant en cas de problème
     */
    boolean existeCommuneInseeParCode(String code) throws ApplicationException;
}
