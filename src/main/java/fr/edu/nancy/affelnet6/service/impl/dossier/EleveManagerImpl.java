/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.service.impl.dossier;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.edu.nancy.affelnet6.dao.interfaces.dossier.AdresseDao;
import fr.edu.nancy.affelnet6.dao.interfaces.dossier.CollegeSecteurDao;
import fr.edu.nancy.affelnet6.dao.interfaces.dossier.DemandeAffectationDao;
import fr.edu.nancy.affelnet6.dao.interfaces.dossier.EleveDao;
import fr.edu.nancy.affelnet6.dao.interfaces.dossier.PropositionAdresseDao;
import fr.edu.nancy.affelnet6.dao.interfaces.dossier.RetourAdresseRNVPDao;
import fr.edu.nancy.affelnet6.dao.interfaces.nomenclature.IndicateurPcsDao;
import fr.edu.nancy.affelnet6.domain.affectation.DecisionDepartement;
import fr.edu.nancy.affelnet6.domain.affectation.IndicateurPcsEnum;
import fr.edu.nancy.affelnet6.domain.calcul.DonneesCalculCollegeSecteur;
import fr.edu.nancy.affelnet6.domain.dossier.Adresse;
import fr.edu.nancy.affelnet6.domain.dossier.AdresseCaseSensitiveStatutComparator;
import fr.edu.nancy.affelnet6.domain.dossier.CollegeSecteur;
import fr.edu.nancy.affelnet6.domain.dossier.CollegeSecteurPK;
import fr.edu.nancy.affelnet6.domain.dossier.DemandeAffectation;
import fr.edu.nancy.affelnet6.domain.dossier.Eleve;
import fr.edu.nancy.affelnet6.domain.dossier.PropositionAdresse;
import fr.edu.nancy.affelnet6.domain.dossier.Responsable;
import fr.edu.nancy.affelnet6.domain.dossier.ResponsableId;
import fr.edu.nancy.affelnet6.domain.dossier.RetourAdresseRNVP;
import fr.edu.nancy.affelnet6.domain.dossier.SauvegardeEleve;
import fr.edu.nancy.affelnet6.domain.dossier.SecteurAffectation;
import fr.edu.nancy.affelnet6.domain.dossier.SecteurComparator;
import fr.edu.nancy.affelnet6.domain.dossier.SuiviEcole;
import fr.edu.nancy.affelnet6.domain.nomenclature.CirconscriptionEtabOrigine;
import fr.edu.nancy.affelnet6.domain.nomenclature.CodeEtatDecision;
import fr.edu.nancy.affelnet6.domain.nomenclature.CommuneInsee;
import fr.edu.nancy.affelnet6.domain.nomenclature.DecisionPassage;
import fr.edu.nancy.affelnet6.domain.nomenclature.DecisionPassageEnum;
import fr.edu.nancy.affelnet6.domain.nomenclature.DeptMen;
import fr.edu.nancy.affelnet6.domain.nomenclature.EtablissementOrigine;
import fr.edu.nancy.affelnet6.domain.nomenclature.EtablissementSconet;
import fr.edu.nancy.affelnet6.domain.nomenclature.EtatCollegeSecteur;
import fr.edu.nancy.affelnet6.domain.nomenclature.EtatDecision;
import fr.edu.nancy.affelnet6.domain.nomenclature.EtatExportBee;
import fr.edu.nancy.affelnet6.domain.nomenclature.Formation;
import fr.edu.nancy.affelnet6.domain.nomenclature.Matiere;
import fr.edu.nancy.affelnet6.domain.nomenclature.Niveau1d;
import fr.edu.nancy.affelnet6.domain.nomenclature.NiveauResponsabilite;
import fr.edu.nancy.affelnet6.domain.nomenclature.OffreFormation;
import fr.edu.nancy.affelnet6.domain.nomenclature.StatutAdresse;
import fr.edu.nancy.affelnet6.domain.nomenclature.TypeDemandeAffec;
import fr.edu.nancy.affelnet6.domain.nomenclature.TypeOrigineDossier;
import fr.edu.nancy.affelnet6.exceptions.RNVPException;
import fr.edu.nancy.affelnet6.helper.AccesHelper;
import fr.edu.nancy.affelnet6.helper.AdresseHelper;
import fr.edu.nancy.affelnet6.helper.EleveHelper;
import fr.edu.nancy.affelnet6.helper.EtablissementHelper;
import fr.edu.nancy.affelnet6.helper.FormulaireHelper;
import fr.edu.nancy.affelnet6.helper.ParametreHelper;
import fr.edu.nancy.affelnet6.service.dossier.DemandeAffectationManager;
import fr.edu.nancy.affelnet6.service.dossier.EleveManager;
import fr.edu.nancy.affelnet6.service.dossier.SaisieChoixFamilleManager;
import fr.edu.nancy.affelnet6.service.interfaces.AppAutomateManager;
import fr.edu.nancy.affelnet6.service.interfaces.DeterminationAutomatiqueManager;
import fr.edu.nancy.affelnet6.service.interfaces.ValidationAdresseRNVPManager;
import fr.edu.nancy.affelnet6.service.interfaces.dossier.SauvegardeEleveManager;
import fr.edu.nancy.affelnet6.service.nomenclature.CirconscriptionEtabOrigineManager;
import fr.edu.nancy.affelnet6.service.nomenclature.DeptMenManager;
import fr.edu.nancy.affelnet6.service.nomenclature.EtablissementOrigineManager;
import fr.edu.nancy.affelnet6.service.nomenclature.EtablissementSconetManager;
import fr.edu.nancy.affelnet6.service.nomenclature.EtatCollegeSecteurManager;
import fr.edu.nancy.affelnet6.service.nomenclature.MatiereManager;
import fr.edu.nancy.affelnet6.service.nomenclature.Niveau1dManager;
import fr.edu.nancy.affelnet6.service.nomenclature.OffreFormationManager;
import fr.edu.nancy.affelnet6.utils.ListeUtils;
import fr.edu.nancy.affelnet6.web.struts.forms.dossier.EleveChoixFamilleForm;
import fr.edu.nancy.commun.dao.impl.filtres.FiltreFactory;
import fr.edu.nancy.commun.dao.impl.jointures.Jointure;
import fr.edu.nancy.commun.dao.impl.jointures.JointureFactory;
import fr.edu.nancy.commun.dao.interfaces.Pagination;
import fr.edu.nancy.commun.dao.interfaces.Selector;
import fr.edu.nancy.commun.dao.interfaces.Tri;
import fr.edu.nancy.commun.dao.interfaces.filtres.Filtre;
import fr.edu.nancy.commun.dao.interfaces.filtres.operateurs.FiltreOpChaines;
import fr.edu.nancy.commun.dao.interfaces.filtres.operateurs.FiltreOpCollection;
import fr.edu.nancy.commun.dao.interfaces.filtres.operateurs.FiltreOpComparaison;
import fr.edu.nancy.commun.dao.interfaces.filtres.operateurs.FiltreOpLogique;
import fr.edu.nancy.commun.dao.interfaces.filtres.operateurs.FiltreOpNull;
import fr.edu.nancy.commun.dao.interfaces.filtres.operateurs.FiltreOpProprietes;
import fr.edu.nancy.commun.dao.interfaces.filtres.operateurs.FiltreOpSelectif;
import fr.edu.nancy.commun.domain.parametre.ParametreId;
import fr.edu.nancy.commun.exceptions.ApplicationException;
import fr.edu.nancy.commun.exceptions.BusinessException;
import fr.edu.nancy.commun.exceptions.DaoException;
import fr.edu.nancy.commun.exceptions.TechnicalException;
import fr.edu.nancy.commun.service.interfaces.parametre.ParametreManager;
import fr.edu.nancy.commun.service.securite.acces.TypeContexte;
import fr.edu.nancy.commun.service.securite.engine.ProfilUtilisateur;
import fr.edu.nancy.commun.utils.BatchProgressMonitor;
import fr.edu.nancy.commun.utils.BeanUtils;
import fr.edu.nancy.commun.utils.ChaineUtils;
import fr.edu.nancy.commun.utils.DateUtils;
import fr.edu.nancy.commun.utils.cache.tasks.ReloadParametresTask;
import fr.edu.nancy.commun.web.session.InfoListe;
import fr.edu.nancy.commun.web.utils.Constantes;

/**
 * Gestionnaire de Eleve.
 * 
 * @see fr.edu.nancy.affelnet6.domain.dossier.Eleve
 */
@Service("eleveManager")
public class EleveManagerImpl implements EleveManager {

    /**
     * La longueur maximale dans BEE d'un nom ou d'un prénom d'un élève.
     */
    public static final int MAX_LENGTH_NAME_BEE = 50;

    /** La date de naissance la plus ancienne que l'on accepte. */
    public static final Date DATE_NAISSANCE_MINIMALE;
    /**
     * La date de naissance la plus ancienne à partir duquel on lance un avertissement.
     */
    public static final Date DATE_NAISSANCE_MINIMALE_AVERTISSEMNT;
    /** La date de naissance la plus récente que l'on accepte. */
    public static final Date DATE_NAISSANCE_MAXIMALE;

    /** Logger de la classe. **/
    private static final Logger LOG = LoggerFactory.getLogger(EleveManagerImpl.class);

    /** Taille des paquets d'élèves par défaut. */
    private static final int TAILLE_PAQUET_ELEVES_DEFAUT = 100;

    static {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(new Date());
        calendar.add(Calendar.YEAR, AGE_MAX * -1);

        DATE_NAISSANCE_MINIMALE = calendar.getTime();
    }
    static {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(new Date());
        calendar.add(Calendar.YEAR, AGE_MAX_AV * -1);

        DATE_NAISSANCE_MINIMALE_AVERTISSEMNT = calendar.getTime();
    }

    static {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(new Date());
        calendar.add(Calendar.YEAR, AGE_MIN * -1);

        DATE_NAISSANCE_MAXIMALE = calendar.getTime();
    }

    /** Le gestionnaire des demandes d'affectation. */
    @Autowired
    protected DemandeAffectationManager demandeAffectationManager;

    /**
     * Le service de gestion du choix de la famille.
     */
    @Autowired
    protected SaisieChoixFamilleManager saisieChoixFamilleManager;

    /** Le gestionnaire des paramètres. */
    @Autowired
    private ParametreManager parametreManager;

    /** Le gestionnaire des liens circonscriptions-établissement. */
    @Autowired
    private CirconscriptionEtabOrigineManager circonscriptionEtabOrigineManager;

    /** Gestionnaire d'adresses. */
    @Autowired
    private AdresseDao adresseDao;

    /** Gestionnaire de propositions d'adresse RNVP. */
    @Autowired
    private PropositionAdresseDao propositionAdresseDao;

    /** Gestionnaire de retour RNVP. */
    @Autowired
    private RetourAdresseRNVPDao retourRNVPDao;

    /** Gestionnaire de collège de secteur. */
    @Autowired
    private CollegeSecteurDao collegeSecteurDao;

    /** Gestionnaire de niveau du premier degré. */
    @Autowired
    private Niveau1dManager niveau1dManager;

    /** Gestionnaire d'élèves. */
    @Resource(name = "eleveManager")
    private EleveManager eleveManager;

    /** Objet d'accès aux données des Eleve. */
    @Autowired
    private EleveDao eleveDao;

    /** Objet d'accès aux données des DemandeAffectation. */
    @Autowired
    private DemandeAffectationDao demandeAffectationDao;

    /** Tâche de rechargement des paramètres. */
    @Autowired
    private ReloadParametresTask reloadParametresTask;

    /** Objet d'accès aux données des départements du MEN. */
    @Autowired
    private DeptMenManager deptMenManager;

    /** Le gestionnaire des établissements d'accueil. */
    @Autowired
    private EtablissementSconetManager etablissementSconetManager;

    /** Le gestionnaire des établissements d'origine. */
    @Autowired
    private EtablissementOrigineManager etablissementOrigineManager;

    /** Gestionnaire pour la validation des adresses par le service RNVP. */
    @Autowired
    private ValidationAdresseRNVPManager validationAdresseRNVPManager;

    /** Gestionnaire offre de formation. */
    @Autowired
    private OffreFormationManager offreFormationManager;

    /** Dao des indicateurs de PCS. */
    @Autowired
    private IndicateurPcsDao indicateurPcsDao;

    /** Gestionnaire des états applicatifs. */
    @Autowired
    private AppAutomateManager appAutomateManager;

    /** Le gestionnaire des états collège. */
    @Autowired
    private EtatCollegeSecteurManager etatCollegeManager;

    /** Gestionnaire de sauvegarde des élèves */
    @Autowired
    private SauvegardeEleveManager sauvegardeEleveManager;

    @Override
    public List<Eleve> lister(Pagination<Eleve> paginationSupport, List<Tri> listTri, List<Filtre> filtres)
            throws DaoException {
        Filtre filtre = FiltreFactory.createFiltreLogique(FiltreOpLogique.ET, filtres);
        return lister(paginationSupport, listTri, filtre);
    }

    @Override
    public List<Eleve> lister(Pagination<Eleve> paginationSupport, List<Tri> listTri, Filtre filtre)
            throws DaoException {
        return eleveDao.lister(paginationSupport, listTri, filtre);
    }

    @Override
    public List<Eleve> lister(InfoListe<Eleve> inf) throws TechnicalException, BusinessException {
        return eleveDao.lister(inf.getPaginationSupport(), inf.getListTri(), inf.composeTousFiltres());
    }

    @Override
    public List<Eleve> listerDistinct(InfoListe<Eleve> inf) throws DaoException {
        return eleveDao.listerDistinct(inf);
    }

    @Override
    public List<Eleve> listerDistinct(Pagination<Eleve> pagination, List<Tri> tris, Filtre filtre)
            throws DaoException {
        return eleveDao.listerDistinct(pagination, tris, filtre);
    }

    @Override
    public List<Eleve> listerDemandeAffectation(InfoListe<Eleve> inf)
            throws TechnicalException, BusinessException {
        List<Jointure> listeJointure = new ArrayList<Jointure>();

        Selector<Eleve> selectorEleve = eleveDao.creerSelector();
        selectorEleve.addJointures(listeJointure);
        selectorEleve.addFiltre(inf.composeTousFiltres());
        selectorEleve.addTris(inf.getListTri());
        selectorEleve.setPaginationSupport(inf.getPaginationSupport());

        return eleveDao.listerAvecSelector(selectorEleve);
    }

    @Override
    public List<Eleve> listerCollegeSecteur(InfoListe<Eleve> inf) throws TechnicalException, BusinessException {
        List<Jointure> listeJointure = new ArrayList<Jointure>();

        listeJointure.add(JointureFactory.creerJointureExterneGauche("niveau.libCourtNiveau"));
        listeJointure.add(JointureFactory.creerJointureExterneGauche("collegeSecteur.patronyme"));
        listeJointure.add(JointureFactory.creerJointureExterneGauche("adresseResidence.codePostal.codePostal"));
        listeJointure
                .add(JointureFactory.creerJointureExterneGauche("adresseResidence.communeInsee.llInseeCommune"));
        listeJointure
                .add(JointureFactory.creerJointureExterneGauche("collegeSecteur.communeInsee.llInseeCommune"));

        Selector<Eleve> selectorEleve = eleveDao.creerSelector();
        selectorEleve.addJointures(listeJointure);
        selectorEleve.addFiltre(inf.composeTousFiltres());
        selectorEleve.addTris(inf.getListTri());
        selectorEleve.setPaginationSupport(inf.getPaginationSupport());

        return eleveDao.listerAvecSelector(selectorEleve);
    }

    @Override
    public SortedSet<CollegeSecteur> listerCollegeSecteurs(Long numDossier) {
        return collegeSecteurDao.listerCollegesSecteurEleve(numDossier);
    }

    @Override
    public List<Eleve> listerCollegeVoeux(InfoListe<Eleve> inf) throws TechnicalException, BusinessException {
        List<Jointure> listeJointure = new ArrayList<Jointure>();
        listeJointure.add(JointureFactory.creerJointureExterneGauche("niveau.libCourtNiveau"));
        listeJointure.add(JointureFactory.creerJointureExterneGauche("ecoleOrigine.codUai"));
        listeJointure.add(JointureFactory.creerJointureExterneGauche("collegeSecteur.patronyme"));
        listeJointure.add(JointureFactory.creerJointureExterneGauche("adresseResidence.codePostal.codePostal"));
        listeJointure
                .add(JointureFactory.creerJointureExterneGauche("adresseResidence.communeInsee.llInseeCommune"));
        listeJointure
                .add(JointureFactory.creerJointureExterneGauche("collegeSecteur.communeInsee.llInseeCommune"));

        Selector<Eleve> selectorEleve = eleveDao.creerSelector();
        selectorEleve.addJointures(listeJointure);
        selectorEleve.addFiltre(inf.getFiltreFormulaire());
        selectorEleve.addTris(inf.getListTri());
        selectorEleve.setPaginationSupport(inf.getPaginationSupport());

        return eleveDao.listerAvecSelector(selectorEleve);
    }

    @Override
    public List<Eleve> lister() throws TechnicalException, BusinessException {
        return eleveDao.lister();
    }

    @Override
    public List<Eleve> listerParIne(String ine) throws DaoException {
        Filtre f = FiltreFactory.createFiltreChaines(FiltreOpChaines.EGAL,
                EleveManager.FILTRE_INE[Constantes.FILTRE_COLONNE], ine);
        return eleveDao.lister(null, f);
    }

    @Override
    public Eleve charger(Long id) throws ApplicationException {
        return eleveDao.charger(id);
    }

    @Override
    public Eleve chargerParIne(String ine) throws DaoException {
        return eleveDao.chargerParPropriete("ine", ine);
    }

    @Override
    public void majSansFlush(Eleve e) throws ApplicationException {
        eleveDao.maj(e, false);
    }

    @Override
    public void maj(Eleve e) throws ApplicationException {
        maj(e, true);
    }

    @Override
    public void maj(Eleve e, boolean majDateMaj) throws ApplicationException {
        if (majDateMaj) {
            e.setDateMaj(new Date());
        }
        eleveDao.maj(e);
    }

    @Override
    public void supprimer(Eleve eleve) throws ApplicationException {
        videDemandesAffect(eleve);
        eleve.setCollegeSecteur(null);
        eleveDao.maj(eleve);

        Set<Adresse> adressesASupprimer = new HashSet<>();

        adressesASupprimer.add(eleve.getAdresseResidence());

        if (eleve.getAdresseSecondaire() != null) {
            adressesASupprimer.add(eleve.getAdresseSecondaire());
        }

        if (eleve.getAdresseResidenceSimu() != null) {
            adressesASupprimer.add(eleve.getAdresseResidenceSimu());
        }

        // suppression physique des adresses des responsables
        for (Responsable responsable : eleve.getResponsables()) {
            if (responsable.getAdresseResponsable() != null) {
                adressesASupprimer.add(responsable.getAdresseResponsable());
            }
        }

        // suppression de l'elève (et de ses responsables)
        eleveDao.supprimer(eleve);

        // suppression physique de l'adresse de l'élève et celles de ses responsables
        for (Adresse adresse : adressesASupprimer) {
            adresseDao.supprimer(adresse);
        }
    }

    @Override
    public Long creer(Eleve p) throws ApplicationException {
        if (p.getIndicateurPcs() == null) {
            p.setIndicateurPcs(indicateurPcsDao.chargerParCode(IndicateurPcsEnum.NP.getCode()));
        }
        return eleveDao.creer(p);
    }

    @Override
    public boolean existe(Long id) throws ApplicationException {
        return eleveDao.existe(id);
    }

    @Override
    public boolean existeParIne(String ine) throws DaoException {
        return eleveDao.existeParPropriete("ine", ine);
    }

    @Override
    public boolean existePlusieursParIne(String ine) throws DaoException {
        return eleveDao.existePlusieursParPropriete("ine", ine);
    }

    @Override
    public int supprimerPourRemplacerDepartement(int dptMenId) throws ApplicationException {
        return eleveDao.supprimerPourRemplacerDepartement(dptMenId);
    }

    @Override
    public int compteElevesEcolesPubDept(String dept) throws ApplicationException {
        String uaiDsden = etablissementOrigineManager.getUAIAccesDsdenDept(dept);
        return eleveDao.compteElevesEcolesPubDept(dept, uaiDsden);
    }

    @Override
    public int compteElevesAppelDept(String dept, String typeSaisie) throws ApplicationException {
        return eleveDao.compteElevesAppelDept(dept, typeSaisie);
    }

    @Override
    public int compteElevesCirco(String codeUaiCirco, String typeSaisie) throws ApplicationException {
        // recherche des écoles de la circo
        List<CirconscriptionEtabOrigine> circoEtabsOrigine = listerEcolesPrimairesPubliquesCirco(codeUaiCirco);

        int nbEleves = 0;
        // parcours des écoles
        if (circoEtabsOrigine != null && circoEtabsOrigine.size() != 0) {
            for (CirconscriptionEtabOrigine circoEtabOrigine : circoEtabsOrigine) {
                // compte les élèves de l'école et l'ajoute au resultat final
                String codeUaiEcole = circoEtabOrigine.getId().getCodUaiEtb();
                nbEleves += compteElevesEcole(codeUaiEcole, typeSaisie, codeUaiEcole.substring(0, 3));
            }
        }

        return nbEleves;
    }

    @Override
    public int compteElevesAppelCirco(String codeUaiCirco, String typeSaisie) throws ApplicationException {
        // recherche des écoles de la circo
        List<CirconscriptionEtabOrigine> circoEtabsOrigine = listerEcolesPrimairesPubliquesCirco(codeUaiCirco);

        int nbElevesAppel = 0;
        // parcours des écoles
        if (circoEtabsOrigine != null && circoEtabsOrigine.size() != 0) {
            for (CirconscriptionEtabOrigine circoEtabOrigine : circoEtabsOrigine) {
                // compte les élèves de l'école faisant appel et l'ajoute au resultat final
                String codeUaiEcole = circoEtabOrigine.getId().getCodUaiEtb();
                nbElevesAppel += compteElevesAppelEcole(codeUaiEcole, typeSaisie, codeUaiEcole.substring(0, 3));
            }
        }

        return nbElevesAppel;
    }

    @Override
    public int compteEleveSansRepLegal(String uai, String codeDept) {
        return eleveDao.listerEleveSansRepLegal(uai, codeDept).size();
    }

    @Override
    public int compteAdressesATraiter(String codeUaiOrigine, String codeDept) {
        return eleveDao.compteAdressesATraiter(codeUaiOrigine, codeDept);
    }

    @Override
    public int compteAdressesATraiter(InfoListe<Eleve> infoListe) throws DaoException {
        // adresses à traiter pour l'adresse principale de l'élève
        Filtre fStatutAdresseEleve = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                FILTRE_STATUT_ADR_CODE_ETAT[Constantes.FILTRE_COLONNE], StatutAdresse.ADR_A_TRAITER);

        Selector<Eleve> selectorEleveAdrPrincipale = eleveDao.creerSelector();
        selectorEleveAdrPrincipale.addFiltre(infoListe.composeTousFiltres());
        selectorEleveAdrPrincipale.addFiltre(fStatutAdresseEleve);

        // adresses à traiter pour l'adresse secondaire de l'élève
        Filtre fStatutAdresse2Eleve = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                FILTRE_STATUT_ADR2_CODE_ETAT[Constantes.FILTRE_COLONNE], StatutAdresse.ADR_A_TRAITER);

        Selector<Eleve> selectorEleveAdrSecondaire = eleveDao.creerSelector();
        selectorEleveAdrSecondaire.addFiltre(infoListe.composeTousFiltres());
        selectorEleveAdrSecondaire.addFiltre(fStatutAdresse2Eleve);

        // adresses à traiter pour les responsables
        Filtre fStatutAdresseResp = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                FILTRE_RESP_STATUT_ADR_CODE_ETAT[Constantes.FILTRE_COLONNE], StatutAdresse.ADR_A_TRAITER);

        // l'adresse ne doit pas être partagée avec l'adresse principale de l'élève
        List<Filtre> listFiltresAdrPrincipale = new ArrayList<>();
        listFiltresAdrPrincipale.add(FiltreFactory.createFiltreNull(FiltreOpNull.EST_NULL,
                FILTRE_STATUT_ADR_ID[Constantes.FILTRE_COLONNE]));
        listFiltresAdrPrincipale.add(FiltreFactory.createFiltreProprietes(FiltreOpProprietes.DIFFERENT,
                FILTRE_RESP_STATUT_ADR_ID[Constantes.FILTRE_COLONNE],
                FILTRE_STATUT_ADR_ID[Constantes.FILTRE_COLONNE]));

        // l'adresse ne doit pas être partagée avec l'adresse secondaire de l'élève
        List<Filtre> listFiltresAdrSecondaire = new ArrayList<>();
        listFiltresAdrSecondaire.add(FiltreFactory.createFiltreNull(FiltreOpNull.EST_NULL,
                FILTRE_STATUT_ADR2_ID[Constantes.FILTRE_COLONNE]));
        listFiltresAdrSecondaire.add(FiltreFactory.createFiltreProprietes(FiltreOpProprietes.DIFFERENT,
                FILTRE_RESP_STATUT_ADR_ID[Constantes.FILTRE_COLONNE],
                FILTRE_STATUT_ADR2_ID[Constantes.FILTRE_COLONNE]));

        Selector<Eleve> selectorResp = eleveDao.creerSelector();
        selectorResp.addFiltre(infoListe.composeTousFiltres());
        selectorResp.addFiltre(fStatutAdresseResp);
        selectorResp.addFiltre(FiltreFactory.createFiltreLogique(FiltreOpLogique.OU, listFiltresAdrPrincipale));
        selectorResp.addFiltre(FiltreFactory.createFiltreLogique(FiltreOpLogique.OU, listFiltresAdrSecondaire));

        int nbAdressesPrincipales = eleveDao.countAvecSelector(selectorEleveAdrPrincipale);
        int nbAdressesSecondaires = eleveDao.countAvecSelector(selectorEleveAdrSecondaire);
        int nbAdressesResponsables = eleveDao
                .countDistinctAvecSelector(FILTRE_RESP_STATUT_ADR_ID[Constantes.FILTRE_COLONNE], selectorResp);

        return nbAdressesPrincipales + nbAdressesSecondaires + nbAdressesResponsables;
    }

    @Override
    public int compteAdressesNonControlees(String codeUaiOrigine, String codeDept) {
        return eleveDao.compteAdressesNonControlees(codeUaiOrigine, codeDept);
    }

    @Override
    public List<Eleve> listerEleveSansDemande(Filtre fEcole, List<Tri> tris) throws DaoException {
        Selector<Eleve> selector = eleveDao.creerSelector();

        List<Filtre> filtres = new ArrayList<Filtre>(3);
        // @formatter : off
        filtres.add(FiltreFactory.createFiltreLogique(FiltreOpLogique.ET,
                FiltreFactory.createFiltreNull(FiltreOpNull.EST_NULL,
                        FILTRE_AFFECTATION_DEMANDEE_CLG_PUB_DEP[Constantes.FILTRE_COLONNE]),
                FiltreFactory.createFiltreLogique(FiltreOpLogique.OU,
                        FiltreFactory.createFiltreNull(FiltreOpNull.EST_NULL,
                                FILTRE_DECISION_PASSAGE[Constantes.FILTRE_COLONNE]),
                        FiltreFactory.createFiltreLogique(FiltreOpLogique.NON,
                                FiltreFactory.createFiltreChaines(FiltreOpChaines.EGAL,
                                        FILTRE_DECISION_PASSAGE[Constantes.FILTRE_COLONNE],
                                        DecisionPassageEnum.MAINTENU.getCode(), true, true)))));
        // @formatter : on
        selector.addFiltre(FiltreFactory.createFiltreLogique(FiltreOpLogique.OU, filtres));
        selector.addFiltre(fEcole);

        selector.addTris(tris);

        return selector.lister();
    }

    @Override
    public List<Eleve> listerEleveSansRepLegal(String codeRne, String codeDept) throws DaoException {
        return eleveDao.listerEleveSansRepLegal(codeRne, codeDept);
    }

    @Override
    public List<Eleve> listerElevesSansVolet1Edite(Filtre fEcole, List<Tri> tris) throws DaoException {
        Selector<Eleve> selector = eleveDao.creerSelector();

        selector.addFiltre(fEcole);
        selector.addFiltre(FiltreFactory.createFiltreNull(FiltreOpNull.EST_NULL,
                EleveManager.FILTRE_EDITION_VOLET1[Constantes.FILTRE_COLONNE]));

        selector.addTris(tris);

        return selector.lister();
    }

    @Override
    public List<Eleve> listerElevesSansVolet2Edite(Filtre fEcole, List<Tri> tris) throws DaoException {
        Selector<Eleve> selector = eleveDao.creerSelector();

        selector.addFiltre(fEcole);
        selector.addFiltre(FiltreFactory.createFiltreNull(FiltreOpNull.EST_NULL,
                EleveManager.FILTRE_EDITION_VOLET2[Constantes.FILTRE_COLONNE]));

        selector.addTris(tris);

        return selector.lister();
    }

    /**
     * Donne les écoles primaires publiques ouvertes liées à une circonscription.
     * 
     * @param codeUaiCirco
     *            le code UAI de la circonscription
     * @return la liste des écoles liées à la circonscription
     * @throws ApplicationException
     *             en cas d'erreur
     */
    private List<CirconscriptionEtabOrigine> listerEcolesPrimairesPubliquesCirco(String codeUaiCirco)
            throws ApplicationException {
        // Filtre sur la circo
        Filtre fCirco = FiltreFactory.createFiltreChaines(FiltreOpChaines.EGAL,
                CirconscriptionEtabOrigineManager.FILTRE_UAI_DEPT[Constantes.FILTRE_COLONNE],
                FormulaireHelper.preparerCodeMetier(codeUaiCirco), true, true);

        // On vérifie l'actualité du lien établissement-circonscription
        Filtre fDateOuvFerm = circonscriptionEtabOrigineManager.getFiltreZoneOuverte();

        // Filtre sur les établissements publics
        Filtre fSecteurPublic = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                CirconscriptionEtabOrigineManager.FILTRE_SECT[Constantes.FILTRE_COLONNE],
                EtablissementOrigineManager.SECTEUR_PUBLIC);

        // Composition de tous ces filtres
        List<Filtre> filtres = new ArrayList<>();
        filtres.add(fCirco);
        filtres.add(fDateOuvFerm);
        filtres.add(fSecteurPublic);

        List<CirconscriptionEtabOrigine> circoEtabsOrigine = circonscriptionEtabOrigineManager.lister(null, null,
                FiltreFactory.createFiltreLogique(FiltreOpLogique.ET, filtres));

        return circoEtabsOrigine;
    }

    @Override
    public int compteElevesEcole(String codeUai, String typeSaisie, String codeDept) throws ApplicationException {
        return eleveDao.compteElevesEcole(codeUai, typeSaisie, codeDept);
    }

    @Override
    public Map<String, Eleve> mapEleveAvecDerogation(String codeUai) throws ApplicationException {
        return eleveDao.mapEleveAvecDerogation(codeUai);
    }

    @Override
    public int compteElevesAppelEcole(String codeUai, String typeSaisie, String codeDept)
            throws ApplicationException {
        return eleveDao.compteElevesAppelEcole(codeUai, typeSaisie, codeDept);
    }

    @Override
    public int compteElevesDept(String codeDepMen) {
        return eleveDao.compteElevesDept(codeDepMen);
    }

    @Override
    public int compteElevesSansCollegeSecteurPotentiel(String codeDepMen) {
        return eleveDao.compteElevesSansCollegeSecteurPotentiel(codeDepMen);
    }

    @Override
    public int compteElevesSansCollegeSecteurPotentielEcole(String codeUai, String codeDepMen) {
        return eleveDao.compteElevesSansCollegeSecteurPotentielEcole(codeUai, codeDepMen);
    }

    @Override
    public int compteElevesDeptBe1d(String codeDepMen) {
        return eleveDao.compteElevesDeptBe1d(codeDepMen);
    }

    @Override
    public int compteElevesSansDecisionPassage(String codeDeptMen) throws DaoException {
        Selector<Eleve> selector = eleveDao.creerSelector();
        selector.addFiltre(filtreElevesDept(codeDeptMen));
        selector.addFiltre(filtreElevesSansDecisionPassage());
        return selector.getNombreElements();
    }

    @Override
    public int compteElevesEnAppel(String codeDeptMen) throws DaoException {
        Selector<Eleve> selector = eleveDao.creerSelector();
        selector.addFiltre(filtreElevesDept(codeDeptMen));
        selector.addFiltre(filtreElevesEnAppel());
        return selector.getNombreElements();
    }

    @Override
    public int compteElevesMaintenus(String codeDepMen) {
        return eleveDao.compteElevesMaintenus(codeDepMen);
    }

    @Override
    public int compteElevesDeptInd(String codeDepMen) {
        return eleveDao.compteElevesDeptInd(codeDepMen);
    }

    @Override
    public int compteElevesAffectes(String codeDepMen) {
        return eleveDao.compteElevesAffectes(codeDepMen);
    }

    @Override
    public int compteElevesAffectesSansDecisionPassage(String codeDeptMen) throws DaoException {
        Selector<Eleve> selector = selectorElevesAffectes(codeDeptMen);
        selector.addFiltre(filtreElevesSansDecisionPassage());
        return selector.getNombreElements();
    }

    @Override
    public int compteElevesAffectesPassageEn6eme(String codeDeptMen) throws DaoException {
        Selector<Eleve> selector = selectorElevesAffectesPassageEn6eme(codeDeptMen);
        return selector.getNombreElements();
    }

    @Override
    public int compteElevesAffectesPassageEn6eme(String codeDeptMen, boolean dejaExporte) throws DaoException {
        Selector<Eleve> selector = selectorElevesAffectesPassageEn6eme(codeDeptMen);
        selector.addFiltre(filtreElevesTransferesBee(dejaExporte));
        return selector.getNombreElements();
    }

    @Override
    public int compteElevesAffectesEnAppel(String codeDeptMen) throws DaoException {
        Selector<Eleve> selector = selectorElevesAffectes(codeDeptMen);
        selector.addFiltre(filtreElevesEnAppel());
        return selector.getNombreElements();
    }

    @Override
    public int compteElevesRefuses(String codeDepMen) {
        return eleveDao.compteElevesRefuses(codeDepMen);
    }

    @Override
    public int compteElevesSansDemandePasMaintenus(String codeDepMen) {
        return eleveDao.compteElevesSansDemandePasMaintenus(codeDepMen);
    }

    @Override
    public int compteElevesSansDemandePasMaintenusEcole(String codeRne, String codeDept) {
        return eleveDao.compteElevesSansDemandePasMaintenusEcole(codeRne, codeDept);
    }

    @Override
    public int compteElevesAvecDerog(String codeDeptMen) throws DaoException {

        Filtre filtreDeptMen = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                FILTRE_CODE_DEPT[Constantes.FILTRE_COLONNE], codeDeptMen);

        Filtre filtreAUneDemandeDerog = FiltreFactory.createFiltreCollection(FiltreOpCollection.EST_NON_VIDE,
                FILTRE_JUSTIF_DEROG[Constantes.FILTRE_COLONNE]);

        Filtre filtre = FiltreFactory.createFiltreLogique(FiltreOpLogique.ET, filtreDeptMen,
                filtreAUneDemandeDerog);

        return eleveDao.countDistinct(FILTRE_NUM_DOSSIER[Constantes.FILTRE_COLONNE], filtre);
    }

    @Override
    public int compteElevesAvecDerogEntrante(Integer etabId) throws DaoException {

        Filtre filtreClgSecteurDifferentOuNull = FiltreFactory.createFiltreLogique(FiltreOpLogique.OU,
                FiltreFactory.createFiltreComparaison(FiltreOpComparaison.DIFFERENT,
                        FILTRE_ID_COLLEGE_SECTEUR[Constantes.FILTRE_COLONNE], etabId),
                FiltreFactory.createFiltreNull(FiltreOpNull.EST_NULL,
                        FILTRE_ID_COLLEGE_SECTEUR[Constantes.FILTRE_COLONNE]));

        Filtre filtreAUneDemandeDerog = FiltreFactory.createFiltreCollection(FiltreOpCollection.EST_NON_VIDE,
                FILTRE_JUSTIF_DEROG[Constantes.FILTRE_COLONNE]);

        Filtre filtreAUneDemandeSurEtab = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                FILTRE_ID_COLLEGE_DEMANDE[Constantes.FILTRE_COLONNE], etabId);

        Filtre filtre = FiltreFactory.createFiltreLogique(FiltreOpLogique.ET, filtreClgSecteurDifferentOuNull,
                filtreAUneDemandeDerog, filtreAUneDemandeSurEtab);

        return eleveDao.countDistinct(FILTRE_NUM_DOSSIER[Constantes.FILTRE_COLONNE], filtre);
    }

    @Override
    public int compteElevesAvecDerogSortante(Integer etabId) throws DaoException {
        Filtre filtreClgSecteurEgal = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                FILTRE_ID_COLLEGE_SECTEUR[Constantes.FILTRE_COLONNE], etabId);

        Filtre filtreAUneDemandeDerog = FiltreFactory.createFiltreCollection(FiltreOpCollection.EST_NON_VIDE,
                FILTRE_JUSTIF_DEROG[Constantes.FILTRE_COLONNE]);

        Filtre filtreAUneDemandeSurAutreEtab = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.DIFFERENT,
                FILTRE_ID_COLLEGE_DEMANDE[Constantes.FILTRE_COLONNE], etabId);

        Filtre filtre = FiltreFactory.createFiltreLogique(FiltreOpLogique.ET, filtreClgSecteurEgal,
                filtreAUneDemandeDerog, filtreAUneDemandeSurAutreEtab);

        return eleveDao.countDistinct(FILTRE_NUM_DOSSIER[Constantes.FILTRE_COLONNE], filtre);
    }

    @Override
    public int compteElevesAvecDerogEntranteAccordee(Integer etabId) throws DaoException {
        Filtre filtreClgSecteurDifferentOuNull = FiltreFactory.createFiltreLogique(FiltreOpLogique.OU,
                FiltreFactory.createFiltreComparaison(FiltreOpComparaison.DIFFERENT,
                        FILTRE_ID_COLLEGE_SECTEUR[Constantes.FILTRE_COLONNE], etabId),
                FiltreFactory.createFiltreNull(FiltreOpNull.EST_NULL,
                        FILTRE_ID_COLLEGE_SECTEUR[Constantes.FILTRE_COLONNE]));

        Filtre filtreAffFinaleEstUneDemandeDerog = FiltreFactory.createFiltreCollection(
                FiltreOpCollection.EST_NON_VIDE, FILTRE_JUSTIF_DEROG_AFF_FINALE[Constantes.FILTRE_COLONNE]);

        Filtre filtreAffFinalePorteSurEtab = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                FILTRE_ID_CLG_AFF_FINALE[Constantes.FILTRE_COLONNE], etabId);

        Filtre filtreAffFinaleAcceptee = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                FILTRE_CODE_ETAT_FINALE[Constantes.FILTRE_COLONNE], EtatDecision.CODE_ETAT_FINAL_ACCEPTE);

        Filtre filtre = FiltreFactory.createFiltreLogique(FiltreOpLogique.ET, filtreClgSecteurDifferentOuNull,
                filtreAffFinaleEstUneDemandeDerog, filtreAffFinalePorteSurEtab, filtreAffFinaleAcceptee);

        return eleveDao.countDistinct(FILTRE_NUM_DOSSIER[Constantes.FILTRE_COLONNE], filtre);
    }

    @Override
    public int compteElevesAvecDerogSortanteAccordee(Integer etabId) throws DaoException {
        Filtre filtreClgSecteurEgal = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                FILTRE_ID_COLLEGE_SECTEUR[Constantes.FILTRE_COLONNE], etabId);

        Filtre filtreAffFinaleEstUneDemandeDerog = FiltreFactory.createFiltreCollection(
                FiltreOpCollection.EST_NON_VIDE, FILTRE_JUSTIF_DEROG_AFF_FINALE[Constantes.FILTRE_COLONNE]);

        Filtre filtreAffFinalePorteSurAutreEtab = FiltreFactory.createFiltreComparaison(
                FiltreOpComparaison.DIFFERENT, FILTRE_ID_CLG_AFF_FINALE[Constantes.FILTRE_COLONNE], etabId);

        Filtre filtreAffFinaleAcceptee = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                FILTRE_CODE_ETAT_FINALE[Constantes.FILTRE_COLONNE], EtatDecision.CODE_ETAT_FINAL_ACCEPTE);

        Filtre filtre = FiltreFactory.createFiltreLogique(FiltreOpLogique.ET, filtreClgSecteurEgal,
                filtreAffFinaleEstUneDemandeDerog, filtreAffFinalePorteSurAutreEtab, filtreAffFinaleAcceptee);

        return eleveDao.countDistinct(FILTRE_NUM_DOSSIER[Constantes.FILTRE_COLONNE], filtre);
    }

    @Override
    public int compteElevesAvec1erVoeuDerogEtMotifLePlusPrioritaire(String codeDeptMen, Integer idMtfDrg)
            throws DaoException {
        return eleveDao.compteElevesAvec1erVoeuDerogEtMotifLePlusPrioritaire(codeDeptMen, idMtfDrg);
    }

    @Override
    public int compteElevesAvecDerogAccordeeEtMotifLePlusPrioritaire(String codeDeptMen, Integer idMtfDrg)
            throws DaoException {
        return eleveDao.compteElevesAvecDerogAccordeeEtMotifLePlusPrioritaire(codeDeptMen, idMtfDrg);
    }

    @Override
    public int compteElevesHorsCPD(String codeDepMen) {
        return eleveDao.compteElevesHorsCPD(codeDepMen);
    }

    @Override
    public int compteElevesAvecDerogSansEditionAccuseReceptionEcole(String codeUai, String codeDept) {
        return eleveDao.compteElevesAvecDerogSansEditionAccuseReceptionEcole(codeUai, codeDept);
    }

    @Override
    public boolean verifieVerrouBatchImport(int dptMenId) throws ApplicationException {

        LOG.debug("Verification du verrou du batch");

        int nbEleveBe1dMaj = nbEleveBe1dMaj(dptMenId);

        // Le batch est verrouillé si pour un département, on a au moins un élève BE1D mis a jour
        if (nbEleveBe1dMaj > 0) {
            LOG.debug("La batch est verrouillé :");
            LOG.debug("Nombre d'élèves BE1D modifiés : " + nbEleveBe1dMaj);
            return false;
        }
        return true;
    }

    /**
     * Donne le nombre d'élèves issus de BE1D ayant été modifiés après l'import.
     * 
     * @param dptMenId
     *            Le département concerné
     * @return Le nombre d'élèves BE1D modifiés
     * @throws DaoException
     *             En cas de problème d'accès aux données
     */
    private int nbEleveBe1dMaj(int dptMenId) throws DaoException {
        // selector de l'eleve
        Selector<Eleve> selectorEleve = eleveDao.creerSelector();

        // filtre sur le departement
        Filtre fDept = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                EleveManager.FILTRE_DEPT[Constantes.FILTRE_COLONNE], dptMenId);
        selectorEleve.addFiltre(fDept);

        // filtre sur la date de maj
        Filtre fDtMaj = FiltreFactory.createFiltreNull(FiltreOpNull.EST_NON_NULL,
                EleveManager.FILTRE_DT_MAJ[Constantes.FILTRE_COLONNE]);
        selectorEleve.addFiltre(fDtMaj);

        // filtre sur les élèves provenant de BE1D
        Filtre fProvenanceBe1d = FiltreFactory.createFiltreLogique(FiltreOpLogique.OU,
                FiltreFactory.createFiltreChaines(FiltreOpChaines.EGAL,
                        EleveManager.FILTRE_TYPE_SAISIE[Constantes.FILTRE_COLONNE],
                        TypeOrigineDossier.ORIGINE_DOSSIER_DIRECTEUR_ECOLE, true, true),
                FiltreFactory.createFiltreChaines(FiltreOpChaines.EGAL,
                        EleveManager.FILTRE_TYPE_SAISIE[Constantes.FILTRE_COLONNE],
                        TypeOrigineDossier.ORIGINE_DOSSIER_ECOLE_VIRTUELLE, true, true));
        selectorEleve.addFiltre(fProvenanceBe1d);

        return selectorEleve.getNombreElements();
    }

    @Override
    public int majDateEditionVolet(int numVolet, Long[] numDossierEleves) {
        return eleveDao.majDateEditionVolet(numVolet, numDossierEleves);
    }

    @Override
    public int majConfidentialiteResponsables(Long[] numDossierEleves, Long[] confidentialiteResp) {
        return eleveDao.majConfidentialiteResponsables(numDossierEleves, confidentialiteResp);
    }

    @Override
    public void majCollegeSecteur(Eleve eleve, EtablissementSconet nouveauCollegeSecteur)
            throws ApplicationException {
        // on supprime les demandes d'affectation
        videDemandesAffect(eleve);

        // on modifie le collège de secteur
        eleve.setCollegeSecteur(nouveauCollegeSecteur);
        maj(eleve);
    }

    @Override
    public void videDemandesAffect(Eleve eleve, boolean avecPropositionSegpa) throws ApplicationException {

        if (LOG.isDebugEnabled()) {
            LOG.debug("Suppression des demandes d'affectation de l'élève " + EleveHelper.getIdentite(eleve));
        }

        // La manager regarde s'il connaît l'élève
        boolean estEleveConnu = existe(eleve.getNumDossier());
        if (estEleveConnu) {

            Set<DemandeAffectation> demandesAffectation = eleve.getDemandeAffectation();

            if (CollectionUtils.isNotEmpty(demandesAffectation)) {
                // il faut supprimer la demande validée manuellement (si elle existe) en premier car celle-ci
                // restreint les valeurs possibles des décisions pour les autres demandes
                Iterator<DemandeAffectation> iterator = demandesAffectation.iterator();
                while (iterator.hasNext()) {
                    DemandeAffectation demande = iterator.next();
                    if ((!demandeAffectationManager.estProposition(demande) || avecPropositionSegpa)
                            && demande.getEtatDemande() != null
                            && EtatDecision.CODE_PRIS_MANUEL.equals(demande.getEtatDemande().getCode())) {
                        demandeAffectationManager.supprimer(demande);
                        iterator.remove();
                    }
                }
                // suppression des autres demandes
                iterator = demandesAffectation.iterator();
                while (iterator.hasNext()) {
                    DemandeAffectation demande = iterator.next();

                    if (!demandeAffectationManager.estProposition(demande) || avecPropositionSegpa) {
                        // supprime la demande
                        demandeAffectationManager.supprimer(demande);
                        iterator.remove();
                    } else if (demandeAffectationManager.estProposition(demande)) {
                        demande.setRang(-1);
                        demandeAffectationManager.maj(demande);
                    }
                }

                // maj date de dernière modif de l'élève
                eleve.setDateMaj(new Date());
                maj(eleve);
            }

        } else {
            // L'élève n'est pas connu du manager, le manager traite simplement
            // l'objet présenté
            if (CollectionUtils.isNotEmpty(eleve.getDemandeAffectation())) {
                for (Iterator<DemandeAffectation> itDem = eleve.getDemandeAffectation().iterator(); itDem
                        .hasNext();) {
                    DemandeAffectation demande = itDem.next();
                    if (!demandeAffectationManager.estProposition(demande) || avecPropositionSegpa) {
                        itDem.remove();
                    } else if (demandeAffectationManager.estProposition(demande)) {
                        demande.setRang(-1);
                        demandeAffectationManager.maj(demande);
                    }
                }
            }

            // maj date de dernière modif de l'élève
            eleve.setDateMaj(new Date());
        }
    }

    @Override
    public boolean isCollegeSecteurUpdated(Eleve eleve, String codeRneNewCollegeSecteur) {
        if (eleve == null) {
            return false;
        }

        EtablissementSconet oldCollegeSecteur = eleve.getCollegeSecteur();
        if (oldCollegeSecteur == null && StringUtils.isEmpty(codeRneNewCollegeSecteur)) {
            return false;
        } else if (oldCollegeSecteur != null && StringUtils.isNotEmpty(codeRneNewCollegeSecteur)) {
            String codeRneOldCollegeSecteur = oldCollegeSecteur.getCodeRne();
            return !codeRneNewCollegeSecteur.equals(codeRneOldCollegeSecteur);
        } else {
            return true;
        }
    }

    @Override
    public boolean hasDemandesDifferentesNonTraiteesEnAttenteHorsAffectation(Eleve eleve) {
        Set<DemandeAffectation> demandes = eleve.getDemandeAffectation();
        if (demandes == null || demandes.size() == 0) {
            return false;
        }

        for (DemandeAffectation demande : demandes) {
            EtatDecision etat = demande.getEtatDemande();
            if (etat != null && !EtatDecision.CODE_NON_TRAITE.equals(etat.getCode())
                    && !EtatDecision.CODE_ATTENTE.equals(etat.getCode())
                    && !EtatDecision.CODE_HORS_AFFECTATION.equals(etat.getCode())) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean hasDemandesAvecDecisionFinale(Eleve eleve) {
        Set<DemandeAffectation> demandes = eleve.getDemandeAffectation();
        if (demandes == null || demandes.size() == 0) {
            return false;
        }

        for (DemandeAffectation demande : demandes) {
            EtatDecision etat = demande.getEtatDemande();

            if (etat.getFlagFinal()) {
                return true;
            }
        }

        return false;
    }

    @Override
    public List<DemandeAffectation> listerDemandesEleve(Eleve eleve) throws DaoException {
        Selector<DemandeAffectation> selector = demandeAffectationDao.creerSelector();
        Filtre filtreEleve = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                DemandeAffectationManager.FILTRE_NUMDOSS_ELEVE[Constantes.FILTRE_COLONNE], eleve.getNumDossier());
        selector.addFiltre(filtreEleve);
        selector.addTri(new Tri("rang", true));
        return selector.lister();
    }

    @Override
    public String genereIneEleveProvisoire(String codeDeptMen) throws ApplicationException {
        int increment = getParamIncrementEtIncremente(codeDeptMen);

        String num = codeDeptMen + new DecimalFormat("000000").format(increment);

        char modulo = ChaineUtils.lettreAttenduePourModulo23(Integer.parseInt(num));

        return "P" + num + modulo;
    }

    @Override
    public EtatDemandeAffectation creerDemandesDefautEleve(Eleve eleve) {
        try {
            // crée la demande par défaut pour l'élève courant
            EtatDemandeAffectation demandeCree = creeDemandeDefaut(eleve);

            // si la demande a bien été créée
            if (EtatDemandeAffectation.CREEE.equals(demandeCree)) {
                // enregistrement en base
                maj(eleve);

                // log
                logDemandeDefautForcee(eleve);
            }

            return demandeCree;
        } catch (ApplicationException e) {
            LOG.error("La demande par défaut n'a pas pu être créée pour l'élève " + eleve.getNumDossier(), e);
            return EtatDemandeAffectation.IMPOSSIBLE_ERREUR_TECHNIQUE;
        }
    }

    @Override
    public EtatDemandeAffectation creeDemandeDefaut(Eleve eleve) throws ApplicationException {

        // si l'élève est maintenu à l'école, alors on ne crée pas de demande par défaut
        DecisionPassage decisionPassage = eleve.getDecisionPassage();
        if (decisionPassage != null && DecisionPassageEnum.MAINTENU.getCode().equals(decisionPassage.getCod())) {
            logMaintenuC3(eleve);
            return EtatDemandeAffectation.IMPOSSIBLE_DECISION_PASSAGE_MAINTENU;
        }

        // si un collège de secteur est désigné par le DASEN, on vérifie que la formation 6ème existe
        // pour ce collège sinon on vérifie que l'offre 6ème existe pour tous les collèges de secteur
        EtatDemandeAffectation controleOffreFormation6eme = controleOffreDeFormation6emeDemandeAffectation(eleve);

        if (EtatDemandeAffectation.IMPOSSIBLE_OFFRE_FORMATION_6EME_ABSENTE_COLLEGE_DESIGNE
                .equals(controleOffreFormation6eme)
                || EtatDemandeAffectation.IMPOSSIBLE_OFFRE_FORMATION_6EME_ABSENTE_TOUS_LES_COLLEGES
                        .equals(controleOffreFormation6eme)) {
            return controleOffreFormation6eme;
        }

        EleveChoixFamilleForm choixFamilleForm = new EleveChoixFamilleForm();

        choixFamilleForm.setCodeMefFormation(Formation.CODE_MEF_6EME);
        choixFamilleForm.setTypeDemande(TypeDemandeAffec.CODE_TYPE_DEMANDE_DANS_COLLEGE_PUBLIC_DEPT);
        choixFamilleForm.setDemandeCollegeSecteur(EleveChoixFamilleForm.DEMANDE_COLLEGE_SECTEUR);

        Matiere langueEcole = eleve.getLangueEtrangereOblEcole();
        if (langueEcole == null) {
            choixFamilleForm.setLangueVivante1CleGestion(MatiereManager.CLE_ANGLAIS_LV1);
        } else {
            choixFamilleForm.setLangueVivante1CleGestion(langueEcole.getCle());
        }

        langueEcole = eleve.getLangueEtrangereFacEcole();
        if (langueEcole != null) {
            choixFamilleForm.setLangueVivante2CleGestion(langueEcole.getCle());
        }

        langueEcole = eleve.getLangueRegionaleEcole();
        if (langueEcole != null) {
            choixFamilleForm.setLangueVivante3CleGestion(langueEcole.getCle());
        }

        String codeDeptMen = eleve.getDept().getDeptCode();

        saisieChoixFamilleManager.gereSaisieChoixFamille(eleve, choixFamilleForm, codeDeptMen);
        demandeAffectationManager.gereDemandeAffectation(eleve);

        return EtatDemandeAffectation.CREEE;
    }

    /**
     * Loggue au niveau DEBUG qu'un élève est maintenu à l'école.
     * 
     * @param eleve
     *            L'élève maintenu à l'école.
     */
    private void logMaintenuC3(Eleve eleve) {

        StringBuffer msgDebug = new StringBuffer();
        msgDebug.append("Impossible d'ajouter la demande par défaut pour l'élève ");
        msgDebug.append(eleve.getPrenom());
        msgDebug.append(" ");
        msgDebug.append(eleve.getNom());
        msgDebug.append(" - ");
        msgDebug.append(eleve.getIne());
        msgDebug.append(" car il est maintenu à l'école primaire");

        LOG.debug(msgDebug.toString());
    }

    /**
     * Loggue au niveau DEBUG que la demande ne peut pas être créé pour un élève car la formation n'existe pas dans
     * l'établissement.
     * 
     * @param eleve
     *            L'élève pour lequel la demande par défaut n'a pas pu être créé.
     */
    private void logFormationInexistante(Eleve eleve) {

        StringBuffer msgDebug = new StringBuffer();
        msgDebug.append("Impossible d'ajouter la demande par défaut pour l'élève ");
        msgDebug.append(eleve.getPrenom());
        msgDebug.append(" ");
        msgDebug.append(eleve.getNom());
        msgDebug.append(" - ");
        msgDebug.append(eleve.getIne());
        msgDebug.append(" car la formation 6EME n'existe pas pour cet établissement");

        LOG.debug(msgDebug.toString());
    }

    /**
     * Loggue au niveau INFO qu'on a créé une demande par défaut pour l'élève passé en paramètre.
     * 
     * @param eleve
     *            L'élève pour lequel on a créé une demande par défaut.
     */
    private void logDemandeDefautForcee(Eleve eleve) {
        if (LOG.isInfoEnabled()) {
            StringBuffer msg = new StringBuffer();
            msg.append("Une demande par défaut a été créée pour l'élève ");
            msg.append(eleve.getPrenom());
            msg.append(" ");
            msg.append(eleve.getNom());
            msg.append(" - ");
            msg.append(eleve.getIne());

            LOG.info(msg.toString());
        }
    }

    /**
     * Donne le paramètre Incrément INE Provisoire, puis l'incrémente et
     * l'enregistre en base, puis recherche le cache.
     * 
     * @param codeDeptMen
     *            Le code du département
     * @return La valeur du paramètre avant l'incrémentation
     * @throws ApplicationException
     *             en cas d'erreur applicative
     */
    private Integer getParamIncrementEtIncremente(String codeDeptMen) throws ApplicationException {
        ParametreId paramIdIncrementIneProvisoire = new ParametreId(
                ParametreHelper.KEY_PARAMETRE_INCREMENT_INE_PROVISOIRE, TypeContexte.DEP.toString(), codeDeptMen);

        Integer paramAvantIncrementation = parametreManager.incrementer(paramIdIncrementIneProvisoire);

        // rechargement du cache
        try {
            this.reloadParametresTask.runTask();
        } catch (Exception e) {
            LOG.error("Erreur au rechargement du cache des paramètres", e);
        }

        return paramAvantIncrementation;
    }

    @Override
    public List<Eleve> listerElevesSansDemande(Pagination<Eleve> pagination, List<Tri> listeDesTris,
            String departement, String uaiEcole) throws DaoException {

        Filtre filtreTotal = FiltreFactory.createFiltreCollection(FiltreOpCollection.EST_VIDE,
                EleveManager.FILTRE_DEMANDES[Constantes.FILTRE_COLONNE]);

        // On écarte les élèves dont la décision de passage est à "Maintien à l'école"
        filtreTotal = FiltreFactory.createFiltreLogique(FiltreOpLogique.ET, filtreTotal, filtrePasMaintenu());

        if (departement != null && !departement.isEmpty()) {
            Filtre filtreDepartement = FiltreFactory.createFiltreChaines(FiltreOpChaines.EGAL,
                    EleveManager.FILTRE_CODE_DEPT[Constantes.FILTRE_COLONNE], departement);
            filtreTotal = FiltreFactory.createFiltreLogique(FiltreOpLogique.ET, filtreTotal, filtreDepartement);
        }

        if (uaiEcole != null && !uaiEcole.isEmpty()) {
            Filtre filtreEcole = FiltreFactory.createFiltreChaines(FiltreOpChaines.EGAL,
                    EleveManager.FILTRE_ECOLE_ORIGINE[Constantes.FILTRE_COLONNE], uaiEcole);
            filtreTotal = FiltreFactory.createFiltreLogique(FiltreOpLogique.ET, filtreTotal, filtreEcole);
        }

        return eleveDao.lister(pagination, listeDesTris, filtreTotal);
    }

    @Override
    public Filtre filtreElevesSansDecisionPassage() {
        return FiltreFactory.createFiltreNull(FiltreOpNull.EST_NULL,
                FILTRE_DECISION_PASSAGE[Constantes.FILTRE_COLONNE]);
    }

    @Override
    public Filtre filtreElevesPassageEn6eme() {
        return FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                FILTRE_DECISION_PASSAGE[Constantes.FILTRE_COLONNE], DecisionPassageEnum.PASSAGE_6EME.getCode());
    }

    @Override
    public Filtre filtreElevesEnAppel() {
        return FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                FILTRE_DECISION_PASSAGE[Constantes.FILTRE_COLONNE], DecisionPassageEnum.APPEL.getCode());
    }

    @Override
    public Filtre filtreElevesPasEnAppel() {
        Filtre fPasAppel = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.DIFFERENT,
                FILTRE_DECISION_PASSAGE[Constantes.FILTRE_COLONNE], DecisionPassageEnum.APPEL.getCode());
        Filtre fIsNull = FiltreFactory.createFiltreNull(FiltreOpNull.EST_NULL,
                FILTRE_DECISION_PASSAGE[Constantes.FILTRE_COLONNE]);

        return FiltreFactory.createFiltreLogique(FiltreOpLogique.OU, fIsNull, fPasAppel);
    }

    @Override
    public Filtre filtreMaintenu() {

        Filtre filtreMaintenu = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                EleveManager.FILTRE_DECISION_PASSAGE[Constantes.FILTRE_COLONNE],
                DecisionPassageEnum.MAINTENU.getCode());
        filtreMaintenu.setLibelle(EleveManager.FILTRE_DECISION_PASSAGE[Constantes.FILTRE_LIBELLE]);

        return filtreMaintenu;
    }

    @Override
    public Filtre filtrePasMaintenu() {
        Filtre filtrePasMaintenu = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.DIFFERENT,
                EleveManager.FILTRE_DECISION_PASSAGE[Constantes.FILTRE_COLONNE],
                DecisionPassageEnum.MAINTENU.getCode());
        filtrePasMaintenu.setIsAffiche(true);
        filtrePasMaintenu.setLibelle(EleveManager.FILTRE_DECISION_PASSAGE[Constantes.FILTRE_LIBELLE]);

        Filtre filtreDecisionPassageNull = FiltreFactory.createFiltreNull(FiltreOpNull.EST_NULL,
                EleveManager.FILTRE_DECISION_PASSAGE[Constantes.FILTRE_COLONNE]);

        return FiltreFactory.createFiltreLogique(FiltreOpLogique.OU, filtrePasMaintenu, filtreDecisionPassageNull);
    }

    @Override
    public Filtre filtreDemandesRefuseesPartout(String codeDeptMen) throws ApplicationException {

        List<Long> listNumDossier = demandeAffectationDao.chercheNumDossierRefusesPartout(codeDeptMen);

        // HACK : si la liste est vide on ajoute 0, un id improbable
        if (CollectionUtils.isEmpty(listNumDossier)) {
            listNumDossier.add(0L);
        }

        return FiltreFactory.createFiltreSelectif(FiltreOpSelectif.IN,
                EleveManager.FILTRE_NUM_DOSSIER[Constantes.FILTRE_COLONNE], listNumDossier);
    }

    @Override
    public List<Eleve> listerElevesAffectablesDepartement(String codeDeptMen) throws ApplicationException {

        // Filtre départemental
        Filtre filtreDep = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                EleveManager.FILTRE_CODE_DEPT[Constantes.FILTRE_COLONNE], codeDeptMen);

        // Filtre retirant les élèves maintenus à l'école
        // Remarque : on garde les élèves sans décision de passage (null)

        Filtre filtreElevesNonMaintenusCycle = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.DIFFERENT,
                EleveManager.FILTRE_DECISION_PASSAGE[Constantes.FILTRE_COLONNE],
                DecisionPassageEnum.MAINTENU.getCode());

        Filtre filtreElevesDecisionPassageNulle = FiltreFactory.createFiltreNull(FiltreOpNull.EST_NULL,
                EleveManager.FILTRE_DECISION_PASSAGE[Constantes.FILTRE_COLONNE]);

        Filtre filtreDecisionPassage = FiltreFactory.createFiltreLogique(FiltreOpLogique.OU,
                filtreElevesDecisionPassageNulle, filtreElevesNonMaintenusCycle);

        // Sélecteur
        Selector<Eleve> selector = eleveDao.creerSelector();
        selector.addFiltre(filtreDep);
        selector.addFiltre(filtreDecisionPassage);

        return eleveDao.listerAvecSelector(selector);
    }

    @Override
    public void positionneDecisionFinale(Eleve eleve, DemandeAffectation demandeFinale,
            EtatDecision etatDecisionFinale) throws ApplicationException {

        if (LOG.isDebugEnabled()) {
            LOG.debug("Attribution de décision finale pour l'élève " + eleve.toShortString() + " : "
                    + demandeFinale.toGuruString() + " " + etatDecisionFinale);
        }

        eleve.setAffectationFinale(demandeFinale);
        eleve.setCodeDecisionFinale(etatDecisionFinale);

        // Mise à jour de la date de dernière modif de l'élève
        eleve.setDateMaj(new Date());

        // Enregistrement
        majSansFlush(eleve);
    }

    @Override
    public void listerElevesCompteRendu(StringBuffer cr, List<Long> listeIdElv) {

        cr.append("<ul>");
        for (Long id : listeIdElv) {

            try {
                Eleve e = charger(id);
                cr.append("<li>" + e.getNom() + " " + e.getPrenom() + " (INE " + e.getIne() + ") </li>");

            } catch (ApplicationException ex) {
                LOG.error("Impossible de charger l'élève " + id);
                cr.append("<li>Élève n<sup>o</sup>" + id + "</li>");
            }

        }

        cr.append("</ul>");
    }

    @Override
    public Filtre filtreElevesCirco(String codeUaiCirco) throws DaoException {
        // on recherche les établissements d'origine de la circo
        List<CirconscriptionEtabOrigine> circoEtabs = circonscriptionEtabOrigineManager
                .chargerEcolesPrimairesPub(codeUaiCirco);

        // on récupère les codes UAI des écoles de la circo
        List<String> codesUaiEcolesOrigine = new ArrayList<String>();
        if (circoEtabs != null) {
            for (CirconscriptionEtabOrigine circoEtab : circoEtabs) {
                String codeUaiEtab = circoEtab.getId().getCodUaiEtb();
                codesUaiEcolesOrigine.add(codeUaiEtab);
            }
        }

        if (codesUaiEcolesOrigine.size() == 0) {
            return null;
        }

        // création du filtre pour les élèves
        Filtre filtreCirco = FiltreFactory.createFiltreSelectif(FiltreOpSelectif.IN,
                EleveManager.FILTRE_ECOLE_ORIGINE[Constantes.FILTRE_COLONNE], codesUaiEcolesOrigine);

        return filtreCirco;
    }

    @Override
    public Filtre filtreElevesAffectes() {
        Filtre fPris = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                FILTRE_CODE_ETAT_FINALE[Constantes.FILTRE_COLONNE], EtatDecision.CODE_ETAT_FINAL_ACCEPTE);

        return fPris;
    }

    @Override
    public Filtre filtreElevesDept(String codeDeptMen) {
        Filtre filtreDept = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                EleveManager.FILTRE_CODE_DEPT[Constantes.FILTRE_COLONNE], codeDeptMen);
        return filtreDept;
    }

    @Override
    public Filtre filtreEtabOrigine(String uiaEcoleOrigine) {
        return FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                EleveManager.FILTRE_ECOLE_ORIGINE[Constantes.FILTRE_COLONNE],
                FormulaireHelper.preparerCodeMetier(uiaEcoleOrigine));
    }

    @Override
    public Filtre filtreEtabAccueil(String uidEtabAccueil) {
        Filtre fEtabAccueil = FiltreFactory.createFiltreChaines(FiltreOpChaines.COMMENCE_PAR,
                EleveManager.FILTRE_RNE_CLG_AFFECT_FINALE[Constantes.FILTRE_COLONNE],
                FormulaireHelper.preparerCodeMetier(uidEtabAccueil), true, true);
        return fEtabAccueil;
    }

    @Override
    public Filtre filtreEchangeIdSiecleSynchroBee(String echangeIdSiecleSynchroBee) {
        return FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                EleveManager.FILTRE_ECHANGE_ID_SIECLE_SYNCHRO_BEE[Constantes.FILTRE_COLONNE],
                echangeIdSiecleSynchroBee);
    }

    @Override
    public Filtre filtreIne(String ine) {
        Filtre fIne = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                EleveManager.FILTRE_INE[Constantes.FILTRE_COLONNE], ine);
        return fIne;
    }

    @Override
    public Filtre filtrePassage6eme() {
        Filtre fPassage = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                EleveManager.FILTRE_DECISION_PASSAGE[Constantes.FILTRE_COLONNE],
                DecisionPassageEnum.PASSAGE_6EME.getCode());
        return fPassage;
    }

    @Override
    public List<Filtre> filtresElevesAffectesParEtabAccueil(String codeDeptMen, String codeUaiEtabAccueil) {
        List<Filtre> filtres = new ArrayList<>();
        filtres.add(filtreElevesAffectes());
        filtres.add(filtreElevesDept(codeDeptMen));
        filtres.add(filtreEtabAccueil(codeUaiEtabAccueil));
        return filtres;
    }

    @Override
    public List<Filtre> filtresElevesAffectesPassageParEtabAccueil(String codeDeptMen, String codeUaiEtabAccueil) {
        List<Filtre> filtres = new ArrayList<>();
        filtres.addAll(filtresElevesAffectesParEtabAccueil(codeDeptMen, codeUaiEtabAccueil));
        filtres.add(filtrePassage6eme());
        return filtres;
    }

    @Override
    public List<Eleve> listerElevesAffectesParDept(String codeDeptMen, boolean dejaExporte) throws DaoException {
        Selector<Eleve> selector = selectorElevesAffectesPassageEn6eme(codeDeptMen);
        selector.addFiltre(filtreElevesTransferesBee(dejaExporte));

        addFetchModeElevesAffectes(selector);

        return eleveDao.listerAvecSelector(selector);
    }

    @Override
    public List<Eleve> listerElevesAffectesParCirco(String codeDeptMen, String codeUaiCirco, boolean dejaExporte)
            throws DaoException {
        Selector<Eleve> selector = selectorElevesAffectesPassageEn6eme(codeDeptMen);
        selector.addFiltre(filtreElevesCirco(codeUaiCirco));
        selector.addFiltre(filtreElevesTransferesBee(dejaExporte));

        addFetchModeElevesAffectes(selector);

        return eleveDao.listerAvecSelector(selector);
    }

    @Override
    public List<Eleve> listerElevesAffectesParEtabAccueil(String codeDeptMen, String codeUaiEtabAccueil,
            boolean dejaExporte) throws DaoException {
        Selector<Eleve> selector = selectorElevesAffectesPassageEn6eme(codeDeptMen);
        selector.addFiltre(filtreEtabAccueil(codeUaiEtabAccueil));
        selector.addFiltre(filtreElevesTransferesBee(dejaExporte));

        addFetchModeElevesAffectes(selector);

        return eleveDao.listerAvecSelector(selector);
    }

    @Override
    public List<Eleve> listerElevesAffectesParEtabAccueilEtEchangeIdSiecleSynchroBee(String codeUaiEtabAccueil,
            String echangeIdSiecleSynchroBee) throws DaoException {

        Selector<Eleve> selector = eleveDao.creerSelector();
        selector.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        selector.addFiltre(filtreElevesAffectes());
        selector.addFiltre(filtreEtabAccueil(codeUaiEtabAccueil));
        selector.addFiltre(filtreEchangeIdSiecleSynchroBee(echangeIdSiecleSynchroBee));

        return eleveDao.listerAvecSelector(selector);
    }

    @Override
    public Eleve eleveAffecteParIne(String codeDeptMen, String ine, boolean dejaExporte) throws DaoException {
        Selector<Eleve> selector = selectorElevesAffectesPassageEn6eme(codeDeptMen);
        selector.addFiltre(filtreIne(ine));
        selector.addFiltre(filtreElevesTransferesBee(dejaExporte));

        addFetchModeElevesAffectes(selector);

        List<Eleve> eleves = eleveDao.listerAvecSelector(selector);

        if (eleves != null && eleves.size() == 1) {
            return eleves.get(0);
        } else {
            return null;
        }
    }

    @Override
    public List<Eleve> listerRefusesDerog(String filtreNom, String filtreCollege, String tri,
            String codeDeptMenUser) throws DaoException {
        return eleveDao.listerRefusesDerog(filtreNom, filtreCollege, tri, codeDeptMenUser);
    }

    @Override
    public String codeUaiEtabOri(Eleve eleve) {
        String codeUaiEtabOri = null;
        if (eleve.getEcoleOrigine() != null) {
            codeUaiEtabOri = eleve.getEcoleOrigine().getCodUai();
        }
        return codeUaiEtabOri;
    }

    @Override
    public String codeMefNiveau1d(Eleve eleve) {
        String codeMefNiveau1d = null;
        Niveau1d niveau1d = eleve.getNiveau();

        if (niveau1d != null) {
            try {
                return niveau1dManager.getCodeMef(niveau1d);
            } catch (DaoException e) {
                LOG.error("Le code mef du niveau du 1er degré de l'élève " + EleveHelper.getIdentite(eleve)
                        + " n'a pas été trouvé", e);
                return null;
            }
        }

        return codeMefNiveau1d;
    }

    @Override
    public String ineExportSconet(Eleve eleve) {
        return "Affelnet-6";
    }

    @Override
    public String idNational(Eleve eleve) {
        if (StringUtils.isEmpty(eleve.getIne()) || eleve.getIne().substring(0, 1).equals("P")) {
            return null;
        } else {
            return eleve.getIne();
        }
    }

    @Override
    public List<Eleve> listerDoublonsProbables(String codeDeptMen) throws DaoException {
        return eleveDao.listerDoublonsProbables(codeDeptMen);
    }

    @Override
    public List<Eleve> listerElevesDept(String codeDeptMen) throws DaoException {
        Pagination<Eleve> pagination = null;
        List<Tri> tris = null;
        Filtre fDept = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                EleveManager.FILTRE_CODE_DEPT[Constantes.FILTRE_COLONNE], codeDeptMen);

        return lister(pagination, tris, fDept);
    }

    @Override
    public Map<String, Eleve> listerMapElevesDept(String codeDeptMen) throws DaoException {

        Map<String, Eleve> mapEleves = new HashMap<>();

        List<Eleve> listeEleves = listerElevesDept(codeDeptMen);

        for (Eleve eleve : listeEleves) {
            mapEleves.put(eleve.getIne(), eleve);
        }

        return mapEleves;
    }

    // TODO : ci-dessous c'est le début de l'implémentation pour
    // factoriser le code commun aux changements de décision de passage
    // par la saisie individuelle et par la saisie de masse.
    // Cf le todo correspondant
    // dans SaisieIdentiteEleveAction.java et
    // SaisieMasseDecisionPassageEditeAction.java
    // A décommenter et à modifier car pour le moment l'exception
    // "Illegal attempt to associate a collection with two open sessions"
    // est remontée !
    /*
     * Modifie le champ <code>decisionPassage</code> de l'objet
     * <code>eleve</code> en fonction du
     * <code>codeNouvelleDecisionPassage</code> passé en paramètre.<br>
     * <b>Attention</b> : ne persiste pas les changements.
     * 
     * @param eleve L'élève à modifier
     * 
     * @param codeNouvelleDecisionPassage La nouvelle valeur du champ
     * <code>decisionPassage</code> de l'élève. Un objet DecisionPassage doit
     * correspondre à ce code.
     * 
     * @throws ApplicationException public void updateDecisionPassage( Eleve
     * eleve, String codeNouvelleDecisionPassage) throws ApplicationException {
     * DecisionPassage decisionPassage =
     * decisionPassageManager.charger(codeNouvelleDecisionPassage);
     * updateDecisionPassage(eleve, decisionPassage); }
     * 
     * 
     * Modifie le champ <code>decisionPassage</code> de l'objet
     * <code>eleve</code> en fonction de la <code>nouvelleDecisionPassage</code>
     * passée en paramètre.<br> <b>Attention</b> : ne persiste pas les
     * changements.
     * 
     * @param eleve L'élève à modifier
     * 
     * @param nouvelleDecisionPassage La nouvelle valeur du champ
     * <code>decisionPassage</code> de l'élève. Ne doit pas être
     * <code>null</code>.
     * 
     * @throws ApplicationException
     */
    @Override
    public String verifieDateNaissance(String datNais) {
        Date dateNaissance;
        String msg = null;
        try {
            dateNaissance = DateUtils.parseDateFormatter(datNais);

            // on teste si l'élève a plus de 5ans (age min)
            if (dateNaissance.after(DATE_NAISSANCE_MAXIMALE)) {
                msg = "Sa date de naissance " + datNais + " est " + "supérieure à la date du jour plus " + AGE_MIN
                        + " ans " + "(" + DateUtils.format(DATE_NAISSANCE_MAXIMALE, DateUtils.DATE_PATTERN) + ")";
            } else if (dateNaissance.before(DATE_NAISSANCE_MINIMALE)) {
                // on teste si l'age est inférieur à l'age max
                msg = "Sa date de naissance " + datNais + " est " + "antérieure à la date du jour moins " + AGE_MAX
                        + " ans " + "(" + DateUtils.format(DATE_NAISSANCE_MINIMALE, DateUtils.DATE_PATTERN) + ")";
            }
        } catch (ParseException e) {
            LOG.error("Erreur de parsing", e);
            msg = "La date de naissance \"" + datNais + "\" n'est pas au format jj/mm/yyyy.";
        } catch (NumberFormatException e) {
            LOG.error("Erreur de format", e);
            msg = "La date de naissance \"" + datNais + "\" n'est pas au format jj/mm/yyyy.";
        }

        return msg;
    }

    @Override
    public boolean verifieAge(String datNais) {
        Date dateNaissance;
        try {
            dateNaissance = DateUtils.parseDateFormatter(datNais);
            // on vérifie que l'élève a moins de 18 ans
            if (dateNaissance.before(DATE_NAISSANCE_MINIMALE_AVERTISSEMNT)) {
                return false;
            }
        } catch (ParseException e) {
            return false;
        }
        return true;
    }

    /**
     * Permet de récupérer en une seule requête l'élève, son adresse, son sexe,
     * son affectation finale, sa formation d'affectation et son établissement
     * d'accueil.
     * 
     * @param selector
     *            Un sélecteur d'élève.
     */
    private void addFetchModeElevesAffectes(Selector<Eleve> selector) {
        selector.setFetchMode(EleveManager.FILTRE_ADR[Constantes.FILTRE_COLONNE], FetchMode.JOIN);
        selector.setFetchMode(EleveManager.FILTRE_SEXE[Constantes.FILTRE_COLONNE], FetchMode.JOIN);
        selector.setFetchMode(EleveManager.FILTRE_AFFECTATION_FINALE[Constantes.FILTRE_COLONNE], FetchMode.JOIN);
        selector.setFetchMode(EleveManager.FILTRE_CLG_AFF_FINALE[Constantes.FILTRE_COLONNE], FetchMode.JOIN);
        selector.setFetchMode(EleveManager.FILTRE_FORMATION_AFF_FINALE[Constantes.FILTRE_COLONNE], FetchMode.JOIN);
        selector.setFetchMode(EleveManager.FILTRE_EXISTENCE_RESP[Constantes.FILTRE_COLONNE], FetchMode.JOIN);
        selector.setFetchMode(EleveManager.FILTRE_RESP_ADR[Constantes.FILTRE_COLONNE], FetchMode.JOIN);
    }

    @Override
    public List<Filtre> getFiltresStatiques(ProfilUtilisateur profilUtilisateur) {
        List<Filtre> filtres = new ArrayList<Filtre>();

        // élèves affectés
        Filtre filtreAffectes = eleveManager.filtreElevesAffectes();
        filtres.add(filtreAffectes);

        // si Principal, filtre sur les élèves affectés chez lui
        if (AccesHelper.isPrinc(profilUtilisateur)) {
            String codeUaiClgChefEtab = AccesHelper.getUaiChefEtablissement(profilUtilisateur);

            Filtre filtreCollegeAffectation = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                    EleveManager.FILTRE_RNE_CLG_AFFECT_FINALE[Constantes.FILTRE_COLONNE], codeUaiClgChefEtab);
            filtres.add(filtreCollegeAffectation);

        } else {

            // A défaut, on filtre sur les élèves du département
            // pour l'accès courant de l'utilisateur
            filtres.add(getFiltresStatiquesDepartement(profilUtilisateur));
        }

        return filtres;
    }

    @Override
    public Filtre getFiltresStatiquesDepartement(ProfilUtilisateur profilUtilisateur) {
        // A défaut, on filtre sur les élèves du département
        // pour l'accès courant de l'utilisateur
        // Récupération du département concerné
        String codeDeptMenUser = profilUtilisateur.getAccesCourant().getValCtx();
        // Filtre pour les élèves du dept
        Filtre filtreDept = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                EleveManager.FILTRE_CODE_DEPT[Constantes.FILTRE_COLONNE], codeDeptMenUser);
        return filtreDept;
    }

    @Override
    public List<List<Responsable>> regroupeResponsablesParAdresse(Eleve eleve, boolean seulementRepLegal,
            boolean adresseComplete, boolean adresseInconnueOuIncomplete) {
        Map<Adresse, List<Responsable>> adressesDifferentes = new HashMap<Adresse, List<Responsable>>();

        List<List<Responsable>> responsablesRegroupes = new ArrayList<>();

        // parcours des responsables de l'élève
        SortedSet<Responsable> responsables = eleve.getResponsables();
        boolean possedeRepresentantLegal = false;
        for (Responsable responsable : responsables) {
            if (responsable.getNiveauResponsabilite() != null && NiveauResponsabilite.CODE_REPRESENTANT_LEGAL
                    .equals(responsable.getNiveauResponsabilite().getCode())) {
                possedeRepresentantLegal = true;
            }
            if (!seulementRepLegal || (responsable.getNiveauResponsabilite() != null
                    && NiveauResponsabilite.CODE_REPRESENTANT_LEGAL
                            .equals(responsable.getNiveauResponsabilite().getCode()))) {
                Adresse adresse = responsable.getAdresseResponsable();

                if (adresse == null || adresse.getIncomplete()) {
                    if (adresseInconnueOuIncomplete) {
                        // on ne regroupe pas les responsables qui ont une adresse inconnue
                        responsablesRegroupes.add(Arrays.asList(responsable));
                    }
                } else {
                    if (adresseComplete) {
                        // si adresse distincte, alors on l'ajoute
                        if (!adressesDifferentes.containsKey(adresse)) {
                            List<Responsable> responsablesACetteAdresse = new ArrayList<Responsable>();
                            responsablesACetteAdresse.add(responsable);
                            adressesDifferentes.put(adresse, responsablesACetteAdresse);
                            responsablesRegroupes.add(responsablesACetteAdresse);
                        } else {
                            List<Responsable> responsablesACetteAdresse = adressesDifferentes.get(adresse);
                            responsablesACetteAdresse.add(responsable);
                        }
                    }
                }
            }
        }

        // si l'élève n'a pas de représentant légal, on ajoute une liste vide
        if (seulementRepLegal && !possedeRepresentantLegal) {
            responsablesRegroupes.add(new ArrayList<Responsable>());
        }

        return responsablesRegroupes;
    }

    @Override
    public SuiviEcole suiviEcole(String codeUai) {
        return eleveDao.suiviEcole(codeUai);
    }

    @Override
    public List<Eleve> listerElevesEtablissementOrigine(String uaiEcoleOrigine, String codeDeptMen)
            throws DaoException {
        List<Tri> tris = new ArrayList<>();
        tris.add(new Tri("nom"));
        tris.add(new Tri("prenom"));

        List<Filtre> filtres = new ArrayList<>();
        if (codeDeptMen != null) {
            filtres.add(filtreElevesDept(codeDeptMen));
        }
        filtres.add(filtreEtabOrigine(uaiEcoleOrigine));

        Selector<Eleve> selector = eleveDao.creerSelector();
        selector.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        selector.addTris(tris);
        selector.addFiltres(filtres);
        selector.setFetchMode(FILTRE_COLLEGE_POTENTIEL[Constantes.FILTRE_COLONNE], FetchMode.JOIN);
        selector.setFetchMode(FILTRE_DEMANDES[Constantes.FILTRE_COLONNE], FetchMode.JOIN);
        selector.setFetchMode(FILTRE_JUSTIF_DEROG[Constantes.FILTRE_COLONNE], FetchMode.JOIN);

        return eleveDao.listerAvecSelector(selector);
    }

    @Override
    public Eleve importeDossierAutreDepartement(Long numDossier, String codeDeptMen, boolean estMonoCollege)
            throws ApplicationException {
        Eleve eleveSource = charger(numDossier);
        Eleve eleveImport = new Eleve();

        DeptMen deptImport = deptMenManager.chargerParDepartement(codeDeptMen);

        boolean isClgSecteurDansDepartement = eleveSource.getCollegeSecteurs().stream()
                .anyMatch(c -> EtablissementHelper.estEtablissementDansDept(c, codeDeptMen));

        // adresse de résidence de l'élève
        Adresse adresseResidenceEleveImport = creerCopieAdresse(eleveSource.getAdresseResidence(), deptImport);
        eleveImport.setAdresseResidence(adresseResidenceEleveImport);

        // adresse secondaire éventuelle de l'élève
        if (eleveSource.getAdresseSecondaire() != null) {
            Adresse adresseSecondaireEleveImport = creerCopieAdresse(eleveSource.getAdresseSecondaire(),
                    deptImport);
            eleveImport.setAdresseSecondaire(adresseSecondaireEleveImport);
        }

        eleveImport.setCommuneNaissance(eleveSource.getCommuneNaissance());
        eleveImport.setCycle(eleveSource.getCycle());
        eleveImport.setDateCreation(new Date());
        eleveImport.setDateNaissance(eleveSource.getDateNaissance());
        eleveImport.setDept(deptImport);
        eleveImport.setEcoleOrigine(eleveSource.getEcoleOrigine());
        eleveImport.setIne(eleveSource.getIne());
        eleveImport.setLangueEtrangereOblEcole(eleveSource.getLangueEtrangereOblEcole());
        eleveImport.setLangueEtrangereFacEcole(eleveSource.getLangueEtrangereFacEcole());
        eleveImport.setLangueRegionaleEcole(eleveSource.getLangueRegionaleEcole());
        eleveImport.setNaissanceFrance(eleveSource.getNaissanceFrance());
        eleveImport.setNiveau(eleveSource.getNiveau());
        eleveImport.setNom(eleveSource.getNom());
        eleveImport.setOrigineDossier(TypeOrigineDossier.ORIGINE_DOSSIER_IMPORT_AUTRE_DEPT);
        eleveImport.setPrenom(eleveSource.getPrenom());
        eleveImport.setPrenom2(eleveSource.getPrenom2());
        eleveImport.setPrenom3(eleveSource.getPrenom3());
        eleveImport.setEstCollegeSecteurDansDepartement(isClgSecteurDansDepartement);
        eleveImport.setNumDossierOrigine(eleveSource);
        eleveImport.setPossedeHandicap(eleveSource.getPossedeHandicap());
        eleveImport.setDecisionPassage(eleveSource.getDecisionPassage());
        eleveImport.setEstCollegeSecteurSaisiParDsden(eleveSource.getEstCollegeSecteurSaisiParDsden());
        eleveImport.setExisteAdresseATraiter(eleveSource.getExisteAdresseATraiter());
        eleveImport.setIndicateurPcs(eleveSource.getIndicateurPcs());

        Responsable respImport = null;
        for (Responsable resp : eleveSource.getResponsables()) {
            respImport = new Responsable();
            respImport.setId(new ResponsableId());

            BeanUtils.copyProperties(respImport, resp);

            respImport.getId().setEleve(eleveImport);
            respImport.getId().setNumResponsable(resp.getId().getNumResponsable());
            respImport.setDept(deptImport);

            // On prend une nouvelle adresse pour décoréler les dossiers.
            if (resp.getAdresseResponsable() != null) {
                Adresse adresseResponsableImport = creerCopieAdresse(resp.getAdresseResponsable(), deptImport);
                respImport.setAdresseResponsable(adresseResponsableImport);
            }

            eleveImport.getResponsables().add(respImport);
        }

        eleveImport.setSexe(eleveSource.getSexe());

        Long numDossierEleveImport = creer(eleveImport);

        // on ne récupère pas les collèges de secteur et le collège de secteur désigné
        // si l'élève a plusieurs collèges de secteur et que le département est en mono-collège
        if (!(CollectionUtils.size(eleveSource.getCollegeSecteurs()) > 1 && estMonoCollege)) {
            CollegeSecteur collegeSecteurImport = null;
            for (CollegeSecteur collegeSecteur : eleveSource.getCollegeSecteurs()) {
                Long identifiantEtab = collegeSecteur.getEtablissementSconet().getEtabId().longValue();
                collegeSecteurImport = new CollegeSecteur();
                BeanUtils.copyProperties(collegeSecteurImport, collegeSecteur);
                collegeSecteurImport.setId(new CollegeSecteurPK(numDossierEleveImport, identifiantEtab));

                eleveImport.getCollegeSecteurs().add(collegeSecteurImport);
            }
            // collège de secteur désigné
            eleveImport.setCollegeSecteur(eleveSource.getCollegeSecteur());
        }

        maj(eleveImport);

        // liste des adresses non contrôlées (pour l'élève et ses responsables)
        List<Adresse> listeAdresses = validationAdresseRNVPManager
                .rechercheAdressesNonControleeDossierEleve(eleveImport);
        // redressement des adresses non contrôlées
        if (listeAdresses != null) {
            try {
                validationAdresseRNVPManager.controlerListeAdresses(listeAdresses);
            } catch (RNVPException e) {
                LOG.warn(RNVPException.ERREUR_MESSAGE, e);
            }
        }

        // creation du voeux de secteur
        if (eleveImport.getCollegeSecteur() != null) {
            // création automatique du voeu de secteur
            eleveManager.gereDemandeAffectation(eleveImport, codeDeptMen);
        }

        return eleveImport;
    }

    @Override
    public Map<String, Eleve> listerElevesImportesDuDepartement(String codeDeptMen) throws ApplicationException {
        return listerElevesDejaImportes(codeDeptMen, TypeOrigineDossier.ORIGINE_DOSSIER_IMPORT_AUTRE_DEPT);
    }

    @Override
    public Map<String, Eleve> listerElevesImportesDuPrive(String codeDeptMen) throws ApplicationException {

        Map<String, Eleve> mapElevesImport = listerElevesDejaImportes(codeDeptMen,
                TypeOrigineDossier.ORIGINE_DOSSIER_IMPORT_PRIVE);
        Map<String, Eleve> mapElevesImportPrincipal = listerElevesDejaImportes(codeDeptMen,
                TypeOrigineDossier.ORIGINE_DOSSIER_IMPORT_PRIVE_PRINCIPAL_COLLEGE);
        mapElevesImport.putAll(mapElevesImportPrincipal);

        return mapElevesImport;
    }

    /**
     * @param codeDeptMen
     *            Le code du département de l'utilisateur.
     * @param origineDossier
     *            code de l'origine du dosser
     * @return Une map des élèves importés depuis l'origine du dossier donnée avec en clé l'INE de l'élève et en
     *         valeur l'élève.
     * @throws DaoException
     *             Un problème d'accès aux données.
     */
    private Map<String, Eleve> listerElevesDejaImportes(String codeDeptMen, String origineDossier)
            throws DaoException {
        Map<String, Eleve> mapEleves = new HashMap<String, Eleve>();
        Filtre filtreDepartement = filtreElevesDept(codeDeptMen);
        Filtre filtreEleveImporte = FiltreFactory.createFiltreChaines(FiltreOpChaines.EGAL,
                FILTRE_ORIGINE_SAISIE[Constantes.FILTRE_COLONNE], origineDossier);

        Filtre filtreCompose = FiltreFactory.createFiltreLogique(FiltreOpLogique.ET, filtreDepartement,
                filtreEleveImporte);

        List<Eleve> listeEleves = lister(null, null, filtreCompose);

        for (Eleve eleve : listeEleves) {
            mapEleves.put(eleve.getIne(), eleve);
        }

        return mapEleves;
    }

    @Override
    public boolean estDejaImporteAutreDepartement(String ine, String codeDeptMen) throws ApplicationException {
        return estDejaImporte(ine, codeDeptMen, TypeOrigineDossier.ORIGINE_DOSSIER_IMPORT_AUTRE_DEPT);
    }

    @Override
    public boolean estDejaImporteEleveDuPrive(String ine, String codeDeptMen) throws ApplicationException {
        return estDejaImporte(ine, codeDeptMen, TypeOrigineDossier.ORIGINE_DOSSIER_IMPORT_PRIVE) || estDejaImporte(
                ine, codeDeptMen, TypeOrigineDossier.ORIGINE_DOSSIER_IMPORT_PRIVE_PRINCIPAL_COLLEGE);
    }

    /**
     * @param ine
     *            L'ine de l'élève à contrôler.
     * @param codeDeptMen
     *            Le département de l'utilisateur.
     * @param typeImport
     *            le type de l'import à tester
     * @return
     *         <ul>
     *         <li>true si l'élève a déjà été importé</li>
     *         <li>false sinon</li>
     *         </ul>
     * @throws ApplicationException
     *             Un problème d'accès aux données.
     */
    private boolean estDejaImporte(String ine, String codeDeptMen, String typeImport) throws ApplicationException {

        List<Filtre> listeFiltres = new ArrayList<Filtre>();

        listeFiltres.add(FiltreFactory.createFiltreChaines(FiltreOpChaines.EGAL,
                FILTRE_ORIGINE_SAISIE[Constantes.FILTRE_COLONNE], typeImport));
        listeFiltres.add(filtreIne(ine));
        listeFiltres.add(FiltreFactory.createFiltreChaines(FiltreOpChaines.EGAL,
                EleveManager.FILTRE_CODE_DEPT[Constantes.FILTRE_COLONNE], codeDeptMen));

        Filtre filtreCompose = FiltreFactory.createFiltreLogique(FiltreOpLogique.ET, listeFiltres);

        List<Eleve> listeEleves = lister(null, null, filtreCompose);

        return !listeEleves.isEmpty();
    }

    @Override
    public Filtre filtreElevesTransferesBee(boolean dejaExporte) {

        List<Filtre> listeFiltres = new ArrayList<>();

        // déjà transféré : état de l'export RC (réceptionné) ou EN (envoyé)
        if (dejaExporte) {

            listeFiltres.add(FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                    FILTRE_ETAT_EXPORT_BEE[Constantes.FILTRE_COLONNE], EtatExportBee.RECEPTIONNE));

            listeFiltres.add(FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                    FILTRE_ETAT_EXPORT_BEE[Constantes.FILTRE_COLONNE], EtatExportBee.ENVOYE));
            // pas encore transféré : état de l'export NE (non envoyé) ou ER (erreur à la réception)
        } else {
            listeFiltres.add(FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                    FILTRE_ETAT_EXPORT_BEE[Constantes.FILTRE_COLONNE], EtatExportBee.NON_ENVOYE));

            listeFiltres.add(FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                    FILTRE_ETAT_EXPORT_BEE[Constantes.FILTRE_COLONNE], EtatExportBee.ERREUR_RECEPTION));
        }

        return FiltreFactory.createFiltreLogique(FiltreOpLogique.OU, listeFiltres);
    }

    @Override
    public boolean existePlusieursEleveParIne(String ine, String codeDeptMen) throws DaoException {
        return rechercheEleveParIne(ine, codeDeptMen, 1);
    }

    @Override
    public boolean existeEleveParIne(String ine, String codeDeptMen) throws DaoException {
        return rechercheEleveParIne(ine, codeDeptMen, 0);
    }

    /**
     * @param ine
     *            L'INE de l'élève.
     * @param codeDeptMen
     *            Le code département de l'utilisateur.
     * @param seuil
     *            Le seuil du nombre d'élèments à franchir.
     * @return
     *         <ul>
     *         <li>true s'il existe plus d'élèves avec l'INE donné dans le département donné que le seuil
     *         fixé.</li>
     *         <li>false sinon.</li>
     *         </ul>
     * @throws DaoException
     *             Une erreur d'accès aux données.
     */
    private boolean rechercheEleveParIne(String ine, String codeDeptMen, int seuil) throws DaoException {
        Selector<Eleve> selector = eleveDao.creerSelector();
        selector.addFiltre(FiltreFactory.createFiltreChaines(FiltreOpChaines.EGAL,
                FILTRE_INE[Constantes.FILTRE_COLONNE], ine));
        selector.addFiltre(FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                FILTRE_CODE_DEPT[Constantes.FILTRE_COLONNE], codeDeptMen));
        return selector.getNombreElements() > seuil;
    }

    @Override
    public Eleve chargerParIne(String ine, String codeDeptMen) throws DaoException {
        Selector<Eleve> selector = eleveDao.creerSelector();
        selector.addFiltre(FiltreFactory.createFiltreChaines(FiltreOpChaines.EGAL,
                FILTRE_INE[Constantes.FILTRE_COLONNE], ine));
        selector.addFiltre(FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                FILTRE_CODE_DEPT[Constantes.FILTRE_COLONNE], codeDeptMen));

        return selector.uniqueResult();
    }

    @Override
    public void majEtatExportBee(Long numDossier, String codeEtatExportBee) throws DaoException {
        eleveDao.majEtatExportBee(numDossier, codeEtatExportBee);
    }

    @Override
    public int majEtatExportBeeEtEchangeIdSiecleSynchroBee(List<Eleve> eleves, String codeEtatExportBee,
            String echangeIdSiecleSynchroBee, BatchProgressMonitor progMon) throws DaoException {

        int elevesMaj = 0;
        boolean monitor = progMon != null;

        if (monitor) {
            progMon.beginTask("Etape 6 - Mise à jour de l'état d'exportation des élèves");
        }

        if (CollectionUtils.isNotEmpty(eleves)) {

            List<Long> numDossierEleves = eleves.stream().map(Eleve::getNumDossier).collect(Collectors.toList());
            List<List<Long>> paquetsNumDossierEleves = ListeUtils.listeLongEnPaquets(numDossierEleves,
                    TAILLE_PAQUET_ELEVES_DEFAUT);

            // mise à jour de l'état de l'exportation par paquet de n élèves
            for (List<Long> paquetNumDossierEleves : paquetsNumDossierEleves) {
                elevesMaj += eleveDao.majEtatExportBeeEtEchangeIdSiecleSynchroBee(paquetNumDossierEleves,
                        codeEtatExportBee, echangeIdSiecleSynchroBee);

                if (monitor) {
                    progMon.setProgression(elevesMaj, eleves.size());
                }
            }
        }

        if (monitor) {
            progMon.endTask();
        }

        return elevesMaj;
    }

    @Override
    public int majEtatExportBeeParEtabEtEchangeIdSiecleSynchroBee(String codeUaiEtab, String codeEtatExportBee,
            String echangeIdSiecleSynchroBee) throws DaoException {

        List<Eleve> eleves = listerElevesAffectesParEtabAccueilEtEchangeIdSiecleSynchroBee(codeUaiEtab,
                echangeIdSiecleSynchroBee);

        LOG.info("Export BEE code état : " + codeEtatExportBee + " - nombre de dossiers mis à jour : "
                + CollectionUtils.size(eleves));

        return majEtatExportBeeEtEchangeIdSiecleSynchroBee(eleves, codeEtatExportBee, null, null);
    }

    @Override
    public int majEtatExportBeeParEchangeIdSiecleSynchroBee(String codeEtatExportBee,
            String echangeIdSiecleSynchroBee) throws DaoException {
        return eleveDao.majEtatExportBeeParEchangeIdSiecleSynchroBee(codeEtatExportBee, echangeIdSiecleSynchroBee);
    }

    @Override
    public String nomElevePourBee(Eleve eleve) {
        return StringUtils.substring(eleve.getNom(), 0, MAX_LENGTH_NAME_BEE);

    }

    @Override
    public String prenomElevePourBee(Eleve eleve, String prenom) {
        return StringUtils.substring(prenom, 0, MAX_LENGTH_NAME_BEE);

    }

    @Override
    public void ajouteCollegeSecteur(Eleve eleve, EtablissementSconet etablissementSconet) throws DaoException {
        CollegeSecteurPK id = new CollegeSecteurPK(eleve.getNumDossier(),
                etablissementSconet.getEtabId().longValue());
        if (collegeSecteurDao.existe(id)) {
            eleve.getCollegeSecteurs().add(collegeSecteurDao.charger(id));
        } else {
            eleve.addCollegeSecteur(etablissementSconet);
        }
    }

    @Override
    public void updateCollegeSecteur(DonneesCalculCollegeSecteur donneesCalcul) throws ApplicationException {

        Eleve eleve = donneesCalcul.getEleve();
        String identiteEleve = EleveHelper.getIdentite(eleve);

        if (DeterminationAutomatiqueManager.CALCUL_AVORTE.equals(donneesCalcul.getEtatCalcul())) {
            return;
        }

        LOG.debug("Mise à jour des collèges de secteur (" + donneesCalcul.toString() + ") de l'élève "
                + identiteEleve);

        Boolean estCollegeSecteurDansDept = donneesCalcul.getEstCollegeDansDepartement();

        boolean passageHorsDept = BooleanUtils.isFalse(estCollegeSecteurDansDept)
                && !estCollegeSecteurDansDept.equals(eleve.getEstCollegeSecteurDansDepartement());

        boolean passageDansDept = BooleanUtils.isTrue(estCollegeSecteurDansDept)
                && !estCollegeSecteurDansDept.equals(eleve.getEstCollegeSecteurDansDepartement());

        // code RNE du collège de pré affectation
        String oldCollegeSecteur = null;
        if (aCollegeSecteurDesigneParIA(eleve)) {
            oldCollegeSecteur = eleve.getCollegeSecteur().getCodeRne();
        }

        boolean estCollegesSecteurModifies = EleveHelper.estCollegesSecteurModifies(eleve,
                donneesCalcul.getCodeCollegesSecteur().toArray(new String[] {}));

        boolean collegeSecteurDesigneParDasenDifferent = oldCollegeSecteur != null
                && !oldCollegeSecteur.equals(donneesCalcul.getCodeRneCollegeChoisiParIA())
                && StringUtils.isNotEmpty(donneesCalcul.getCodeRneCollegeChoisiParIA())
                && AccesHelper.isDsden(donneesCalcul.getProfilUtilisateur());

        boolean adresseEtrangere = !eleve.getAdresseResidence().getAdresseEnFrance();

        EtatCollegeSecteur etatCollegeSecteurEleve = eleve.getEtatCollegeSecteur();

        updateEtatCollegeSecteur(eleve, donneesCalcul, estCollegeSecteurDansDept, estCollegesSecteurModifies,
                passageHorsDept);

        updateRoleMajCollegeSecteur(eleve, donneesCalcul, estCollegesSecteurModifies, passageHorsDept,
                passageDansDept);

        // si les collèges de secteur sont différents
        // OU dans le cas d'un changement de localisation du collège de secteur passage de dans à hors département
        // OU Si passage de collège(s) Hors département avec voeux à demande banalisée
        // OU si le collège désigné par le gestionnaire a été modifié
        // OU si adresse à l'étranger et collège pas saisi manuellement
        if (estCollegesSecteurModifies || passageHorsDept
                || (BooleanUtils.isTrue(estCollegeSecteurDansDept)
                        && CollectionUtils.isEmpty(donneesCalcul.getCodeCollegesSecteur()))
                || collegeSecteurDesigneParDasenDifferent || (adresseEtrangere
                        && !DeterminationAutomatiqueManager.SAISIE_MANUELLE.equals(etatCollegeSecteurEleve))) {

            // s'il existe au moins un voeu avec une décision d'affectation finale, on arrête la mise à jour des
            // collèges de l'élève
            if (existeVoeuxAvecDecisionFinale(eleve)) {
                StringBuffer messageErreur = new StringBuffer(
                        "Impossible de mettre à jour les collèges de secteur de l'élève ")
                                .append(eleve.getPrenom()).append(" ").append(eleve.getNom())
                                .append(" car il existe un voeu avec une décision finale d'affectation");
                LOG.debug(messageErreur.toString());
                throw new BusinessException(messageErreur.toString());
            }

            if (
            // si la liste est vide on doit tout supprimer
            CollectionUtils.isEmpty(donneesCalcul.getCollegesSecteur())
                    || DeterminationAutomatiqueManager.SAISIE_MANUELLE.equals(donneesCalcul.getEtatCalcul())
                    || donneesCalcul.isAdresseEleveModifiee()
                    || !DeterminationAutomatiqueManager.SAISIE_MANUELLE.equals(etatCollegeSecteurEleve)
                    || (donneesCalcul.getEtatCalcul() == null && passageHorsDept)) {
                LOG.debug("Suppression des collèges et voeux de secteur de l'élève " + identiteEleve);

                eleveManager.purgerCollegeEtVoeux(eleve, estCollegeSecteurDansDept, estCollegesSecteurModifies,
                        donneesCalcul.getCodeCollegesSecteur().toArray(new String[] {}), adresseEtrangere);
            }
        }
        if (estCollegeSecteurDansDept != null) {
            eleve.setEstCollegeSecteurDansDepartement(estCollegeSecteurDansDept);
        }

        // gestion du collège de secteur désigné par l'IA-DASEN
        // et du flag indiquant que l'IA-DASEN a désigné le collège de secteur
        if (eleve.getEstCollegeSecteurDansDepartement() != null) {
            LOG.debug("Collège de secteur dans département pour l'élève " + identiteEleve + " : "
                    + eleve.getEstCollegeSecteurDansDepartement());
            if (oldCollegeSecteur == null && StringUtils.isNotEmpty(donneesCalcul.getCodeRneCollegeChoisiParIA())
                    && AccesHelper.isDsden(donneesCalcul.getProfilUtilisateur())) {
                // on enregistre le collège de secteur de pré-affectation saisi par l'IA-DASEN
                LOG.debug("Mise à jour du collège de secteur saisi par l'IA-DASEN pour l'élève " + identiteEleve);
                eleve.setCollegeSecteur(etablissementSconetManager
                        .chargerParCodeRne(donneesCalcul.getCodeRneCollegeChoisiParIA()));
                eleve.setEstCollegeSecteurSaisiParDsden(true);
            }

            enregistrerCollegesSecteur(eleve, donneesCalcul, collegeSecteurDesigneParDasenDifferent);
        }

        if (!eleve.getEstCollegeSecteurDansDepartement()) {
            LOG.debug("Collège de secteur hors département pour l'élève " + identiteEleve);

            eleve.getCollegeSecteurs().clear();

            // Dans le cas où le flag du collège de secteur indique hors département mais que le collège désigné
            // par l'IA-DASEN est dans le département, on annule la saisie
            if (aCollegeSecteurDesigneParIA(eleve) && EtablissementHelper
                    .estEtablissementDansDept(eleve.getCollegeSecteur(), donneesCalcul.getCodeDeptMen())) {
                LOG.debug("Suppression du collège de secteur (" + eleve.getCollegeSecteur().getCodeRne()
                        + ") saisi par l'IA-DASEN pour l'élève " + identiteEleve);
                eleve.setCollegeSecteur(null);
                eleve.setEstCollegeSecteurSaisiParDsden(false);
            }

            if (CollectionUtils.isNotEmpty(donneesCalcul.getCollegesSecteur())) {
                for (EtablissementSconet etablissementSconet : donneesCalcul.getCollegesSecteur()) {
                    LOG.debug("Ajout du collège de secteur " + etablissementSconet.getCodeRne() + " pour l'élève "
                            + identiteEleve);
                    if (!EtablissementHelper.estEtablissementDansDept(etablissementSconet,
                            donneesCalcul.getCodeDeptMen())) {
                        ajouteCollegeSecteur(eleve, etablissementSconet);
                    }
                }

            }
        }
    }

    @Override
    public void updateCollegeSecteurSimule(DonneesCalculCollegeSecteur donneesCalcul) throws ApplicationException {
        Eleve eleve = donneesCalcul.getEleve();
        eleve.getCollegeSecteursSimules().clear();
        // Il est important de ne pas modifier la date de mise à jour
        maj(eleve, false);
        eleve.setEtatCollegeSecteurSimule(donneesCalcul.getEtatCalcul());
        if (donneesCalcul.getCollegesSecteur() != null) {
            for (EtablissementSconet etab : donneesCalcul.getCollegesSecteur()) {
                eleve.addCollegeSecteurSimule(etab);
            }
        }
        // Il est important de ne pas modifier la date de mise à jour
        maj(eleve, false);
    }

    @Override
    public void updateCauseCalculCollegeSecteur(DonneesCalculCollegeSecteur donneesCalcul) {
        Eleve eleve = donneesCalcul.getEleve();
        eleve.setCauseCalculCollegeSecteur(donneesCalcul.getCauseCalculCollegeSecteur());
    }

    @Override
    public void updateAdresseVersAdresseSimu(DonneesCalculCollegeSecteur donneesCalcul) throws TechnicalException {

        Eleve eleve = donneesCalcul.getEleve();

        Adresse adresse = eleve.getAdresseResidence();
        Adresse adresseSimu = eleve.getAdresseResidenceSimu();

        Comparator<Adresse> comparator = new AdresseCaseSensitiveStatutComparator();

        // si les 2 adresses sont différentes
        if (comparator.compare(adresse, adresseSimu) != 0) {

            // si une adresse de simu existe déjà
            if (adresseSimu != null) {
                // elle est supprimée
                eleve.setAdresseResidenceSimu(null);
                adresseDao.supprimer(adresseSimu);
            }

            Adresse adresseCopie = AdresseHelper.copieAdresse(adresse, adresse.getDept());
            adresseDao.creer(adresseCopie);
            eleve.setAdresseResidenceSimu(adresseCopie);
        }
    }

    @Override
    public void initCauseCalculCollegeSecteur(DonneesCalculCollegeSecteur donneesCalcul) {
        Eleve eleve = donneesCalcul.getEleve();
        eleve.setCauseCalculCollegeSecteur(null);
    }

    /**
     * Méthode qui enregistre les collèges de secteur potentiels d'un élève.
     * 
     * @param eleve
     *            L'élève.
     * @param donneesCalcul
     *            les données pour le positionnement du/des collèges de secteur
     * @param collegeSecteurDesigneParDasenDifferent
     *            booléen indiquant si l'utilisateur a saisi un collège de secteur de pré affectation différent du
     *            précédent.
     * @throws ApplicationException
     *             Une erreur d'accès aux données.
     */
    public void enregistrerCollegesSecteur(Eleve eleve, DonneesCalculCollegeSecteur donneesCalcul,
            boolean collegeSecteurDesigneParDasenDifferent) throws ApplicationException {

        eleve.getCollegeSecteurs().clear();
        eleveDao.maj(eleve);

        String[] codeRneCollegeSecteursPotentiels = donneesCalcul.getCodeCollegesSecteur()
                .toArray(new String[] {});

        if (codeRneCollegeSecteursPotentiels != null && codeRneCollegeSecteursPotentiels.length > 0) {

            String identiteEleve = EleveHelper.getIdentite(eleve);

            for (String codeRne : codeRneCollegeSecteursPotentiels) {
                LOG.debug("Ajout du collège de secteur " + codeRne + " pour l'élève " + identiteEleve);
                EtablissementSconet etablissementSconet = etablissementSconetManager.chargerParCodeRne(codeRne);
                ajouteCollegeSecteur(eleve, etablissementSconet);
            }

            ProfilUtilisateur profilUtilisateur = donneesCalcul.getProfilUtilisateur();

            boolean isDsden = profilUtilisateur == null || AccesHelper.isDsden(profilUtilisateur);

            controleSaisieCollegeSecteur(eleve, isDsden);

            if (codeRneCollegeSecteursPotentiels.length == 1) {
                // Si on ne met qu'un seul collège dans la liste des collèges de secteurs,
                // alors on le fixe comme collège pour la préaffectation.
                String codeRneCollegeSaisiDasen = codeRneCollegeSecteursPotentiels[0];
                LOG.debug("Mise à jour du collège de secteur (" + codeRneCollegeSaisiDasen
                        + ") saisi par l'IA-DASEN pour l'élève " + identiteEleve);
                eleve.setCollegeSecteur(etablissementSconetManager.chargerParCodeRne(codeRneCollegeSaisiDasen));

            } else if (collegeSecteurDesigneParDasenDifferent) {

                String codeRneCollegePreAffectation = donneesCalcul.getCodeRneCollegeChoisiParIA();

                // Si modification du collège désigné par l'IA-DASEN
                // on enregistre le collège de secteur désigné
                LOG.debug("Mise à jour du collège de secteur (" + codeRneCollegePreAffectation
                        + ") saisi par l'IA-DASEN pour l'élève " + identiteEleve);
                eleve.setCollegeSecteur(
                        etablissementSconetManager.chargerParCodeRne(codeRneCollegePreAffectation));
                eleve.setEstCollegeSecteurSaisiParDsden(true);
            }
        }
    }

    @Override
    public void controleSaisieCollegeSecteur(Eleve eleve, boolean isDsden) throws DaoException {
        // Dans le cas où on retire le collège de secteur de la liste des collèges potentiels
        // alors que la DSDEN a déjà choisi un collège de secteur,
        // alors on remet ce collège dans la liste.
        if (!isDsden && eleve.getEstCollegeSecteurSaisiParDsden() && eleve.getCollegeSecteur() != null) {
            ajouteCollegeSecteur(eleve, eleve.getCollegeSecteur());
        }
    }

    @Override
    public boolean estSaisieVoeuxPossible(Eleve eleve) {
        if (eleve == null) {
            return false;
        }

        if (!eleve.getEstCollegeSecteurDansDepartement()
                || CollectionUtils.isNotEmpty(eleve.getCollegeSecteurs())) {
            return true;
        }
        return false;
    }

    @Override
    public void videDemandesAffect(Eleve eleve) throws ApplicationException {
        LOG.debug("Suppression des demandes d'affectation existantes pour l'élève "
                + EleveHelper.getIdentite(eleve));
        videDemandesAffect(eleve, true);
    }

    @Override
    public void videDemandesAffectEtDecisionFinale(Eleve eleve) throws ApplicationException {
        LOG.debug("Suppression des demandes d'affectation existantes et de la décision finale pour l'élève "
                + EleveHelper.getIdentite(eleve));
        eleve.setAffectationFinale(null);
        eleve.setCodeDecisionFinale(null);
        videDemandesAffect(eleve);
    }

    @Override
    public void videDemandesAffecSansPropositionSegpa(Eleve eleve) throws ApplicationException {
        LOG.debug("Suppression des demandes d'affectation hors SEGPA existantes pour l'élève " + eleve.getPrenom()
                + " " + eleve.getNom() + " (" + eleve.getIne() + ") ");
        videDemandesAffect(eleve, false);
    }

    @Override
    public List<SecteurAffectation> regrouperElevesParCollegeSecteur(Collection<Eleve> eleves) {
        List<Eleve> listeElevesTravail = new ArrayList<Eleve>();
        listeElevesTravail.addAll(eleves);
        List<Long> listeElevesDejaTraites = new ArrayList<Long>();

        List<SecteurAffectation> elevesParCollegesSecteurs = new ArrayList<SecteurAffectation>();

        for (Eleve eleve : eleves) {
            if (!listeElevesDejaTraites.contains(eleve.getNumDossier())) {
                SecteurAffectation elevesParCollegesSecteur = new SecteurAffectation();
                elevesParCollegesSecteur.ajouteEleve(eleve);
                elevesParCollegesSecteur.ajouteCollegesSecteur(eleve.getCollegeSecteurs());

                if (LOG.isDebugEnabled()) {
                    LOG.debug("Traitement de l'élève " + eleve.getNumDossier());
                }

                listeElevesDejaTraites.add(eleve.getNumDossier());

                for (Eleve eleveTravail : listeElevesTravail) {
                    if (!listeElevesDejaTraites.contains(eleveTravail.getNumDossier())) {
                        if (LOG.isDebugEnabled()) {
                            LOG.debug("Traitement de l'élève travail " + eleveTravail.getNumDossier());
                        }
                        if (EleveHelper.possedeMemeListeCollegesSecteur(eleve, eleveTravail)) {
                            if (LOG.isDebugEnabled()) {
                                LOG.debug("Élèves " + eleve.getNumDossier() + " et " + eleveTravail.getNumDossier()
                                        + " ont la même liste");
                            }
                            elevesParCollegesSecteur.ajouteEleve(eleveTravail);
                            listeElevesDejaTraites.add(eleveTravail.getNumDossier());
                        }
                    }
                }
                elevesParCollegesSecteurs.add(elevesParCollegesSecteur);
            }

        }

        return elevesParCollegesSecteurs;
    }

    @Override
    public boolean estCollegeSecteurFinalConnu(Eleve eleve) {
        if (eleve == null || eleve.getEstCollegeSecteurDansDepartement() == null) {
            return false;
        }

        boolean estCollegeSecteurDansDepartement = eleve.getEstCollegeSecteurDansDepartement();
        boolean aCollegeSecteurDesigneParIA = aCollegeSecteurDesigneParIA(eleve);

        if ((estCollegeSecteurDansDepartement && aCollegeSecteurDesigneParIA)
                || !estCollegeSecteurDansDepartement) {
            return true;
        }

        return false;
    }

    /**
     * Donne si un élève a un collège de secteur désigné par l'IA-DASEN.
     * 
     * @param eleve
     *            Un élève
     * @return Vrai si l'élève a un collège de secteur désigné par l'IA-DASEN.
     */
    private boolean aCollegeSecteurDesigneParIA(Eleve eleve) {
        return (eleve != null && eleve.getCollegeSecteur() != null);
    }

    @Override
    public int compteElevesSansCollegeSecteurFinalAvecCollegesSecteursPotentiels(String codeDept) {
        return eleveDao.compteElevesSansCollegeSecteurFinalAvecCollegesSecteursPotentiels(codeDept);
    }

    @Override
    public void initialiserSaisieVoeux(Eleve eleve) {
        eleve.setEstAffectationDemandeeCollegePublicDept(null);
        eleve.setEstScolarisationDansCollegeSecteur(null);
        eleve.setFormationDemandeSecteur(null);
        eleve.setLv1(null);
        eleve.setLv2(null);
    }

    @Override
    public int compterElevesPourDetermination(String codeDeptMen, boolean sansCollegeUniquement)
            throws DaoException {
        return creerSelectorPourDetermination(codeDeptMen, 0, sansCollegeUniquement).getNombreElements();
    }

    @Override
    public int compteElevesParEchangeIdSiecleSynchroBee(String echangeIdSiecleSynchroBee) throws DaoException {
        return eleveDao.count(filtreEchangeIdSiecleSynchroBee(echangeIdSiecleSynchroBee));
    }

    @Override
    public List<Eleve> lister1EleveParEchangeIdSiecleSynchroBee(String echangeIdSiecleSynchroBee)
            throws DaoException {
        Selector<Eleve> selector = eleveDao.creerSelector();
        selector.addFiltre(filtreEchangeIdSiecleSynchroBee(echangeIdSiecleSynchroBee));
        selector.setMaxResultats(1);
        return selector.lister();
    }

    @Override
    public List<Eleve> listerElevesPourDetermination(String codeDeptMen, int numDossier,
            boolean sansCollegeUniquement) throws DaoException {
        Selector<Eleve> selector = creerSelectorPourDetermination(codeDeptMen, numDossier, sansCollegeUniquement);
        selector.addTri(new Tri("numDossier"));
        selector.setMaxResultats(100);
        return selector.lister();
    }

    /**
     * Selector des élèves du département.
     * 
     * @param codeDeptMen
     *            le code du département
     * @param numDossier
     *            le numéro de dossier
     * @param sansCollegeUniquement
     *            si l'on sélectionne uniquement les élèves sans collège de secteur
     * @return le selector
     */
    private Selector<Eleve> creerSelectorPourDetermination(String codeDeptMen, int numDossier,
            boolean sansCollegeUniquement) {
        Selector<Eleve> selector = eleveDao.creerSelector();
        selector.addFiltre(FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                EleveManager.FILTRE_CODE_DEPT[Constantes.FILTRE_COLONNE], codeDeptMen));
        selector.addFiltre(FiltreFactory.createFiltreComparaison(FiltreOpComparaison.SUPERIEUR,
                EleveManager.FILTRE_NUM_DOSSIER[Constantes.FILTRE_COLONNE], (long) numDossier));

        if (sansCollegeUniquement) {

            // on récupère uniquement les élèves sans collèges de secteur
            Filtre fCollegePotentielVide = FiltreFactory.createFiltreCollection(FiltreOpCollection.EST_VIDE,
                    EleveManager.FILTRE_COLLEGE_POTENTIEL[Constantes.FILTRE_COLONNE]);
            // et qui n'ont pas de collège de secteur hors département
            Filtre fCollegeSecteurHorsDept = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.DIFFERENT,
                    EleveManager.FILTRE_COLLEGE_SECTEUR_DANS_DEPARTEMENT[Constantes.FILTRE_COLONNE], false);

            List<Filtre> filtres = new ArrayList<>();
            filtres.add(fCollegePotentielVide);
            filtres.add(fCollegeSecteurHorsDept);

            selector.addFiltre(FiltreFactory.createFiltreLogique(FiltreOpLogique.ET, filtres));
        }
        // TODO pour la phase d'expérimentation, il a été décidé d'autoriser les gestionnaires a lancé le
        // traitement de calcul des collèges pour la totalité des élèves en état applicatif "Saisie" au lieu de les
        // limiter aux candidatures individuelles
        // if (!estEtatAffectation) {
        // requete.append(" and elv.origineDossier <> :typeOrigineDossier");
        // }
        return selector;
    }

    @Override
    public boolean existeVoeuxAvecDecisionFinale(Eleve eleve) throws DaoException {
        return eleveDao.existeVoeuxAvecDecisionFinale(eleve);
    }

    @Override
    public void purgerCollegeEtVoeux(Eleve eleve, Boolean estCollegeSecteurDansDept,
            boolean estCollegesSecteurModifies, String[] codeRneCollegeSecteurs, boolean adresseEtrangere)
            throws ApplicationException {

        // indique si le collège de secteur était déjà hors du département
        boolean estCollegeSecteurDejaHorsDept = BooleanUtils.isFalse(estCollegeSecteurDansDept)
                && estCollegeSecteurDansDept.equals(eleve.getEstCollegeSecteurDansDepartement());

        // suppression des Voeux si ils existent
        if ((BooleanUtils.isFalse(estCollegeSecteurDansDept)
                && BooleanUtils.isTrue(eleve.getEstCollegeSecteurDansDepartement())) || adresseEtrangere) {
            // Dans le cas d'un changement de localisation du collège de secteur
            // passage de dans à hors département,
            purgerDemandesAffecEtSaisieChoixFamille(eleve, true, true);
            eleve.setCollegeSecteur(null);
            eleve.setEstCollegeSecteurSaisiParDsden(false);

            // si le collège est modifié
        } else if (estCollegesSecteurModifies) {
            // et qu'il n'était pas déjà hors du département, les voeux sont écrasés
            if (!estCollegeSecteurDejaHorsDept) {
                purgerDemandesAffecEtSaisieChoixFamille(eleve, false, true);
            }
            eleve.setCollegeSecteur(null);
            eleve.setEstCollegeSecteurSaisiParDsden(false);

            // Si passage à "collège(s) de secteur dans le département"
        } else if (BooleanUtils.isTrue(estCollegeSecteurDansDept) && ArrayUtils.isEmpty(codeRneCollegeSecteurs)) {
            purgerDemandesAffecEtSaisieChoixFamille(eleve, false, true);
        }
    }

    @Override
    public void purgerDemandesAffecEtSaisieChoixFamille(Eleve eleve, boolean avecPropositionSegpa,
            boolean avecTypeDemande) throws ApplicationException {

        if (avecPropositionSegpa) {
            videDemandesAffect(eleve);
        } else {
            videDemandesAffecSansPropositionSegpa(eleve);
        }

        saisieChoixFamilleManager.razSaisieChoixFamille(eleve, avecTypeDemande);
    }

    @Override
    public void majFlagExisteAdresseATraiter(Long numDossier) throws ApplicationException {
        Eleve eleve = eleveManager.charger(numDossier);

        // adresse principale de l'élève
        boolean estEleveAvecAdresseATraiter = StatutAdresse.ADR_A_TRAITER
                .equals(eleve.getAdresseResidence().getStatutAdresse().getCodeEtat());

        // adresse secondaire de l'élève
        if (eleve.getAdresseSecondaire() != null) {
            estEleveAvecAdresseATraiter |= StatutAdresse.ADR_A_TRAITER
                    .equals(eleve.getAdresseSecondaire().getStatutAdresse().getCodeEtat());
        }

        // adresses des responsables de l'élève
        for (Responsable responsableLegal : eleve.getResponsables()) {
            if (responsableLegal.getAdresseResponsable() != null) {
                estEleveAvecAdresseATraiter |= StatutAdresse.ADR_A_TRAITER
                        .equals(responsableLegal.getAdresseResponsable().getStatutAdresse().getCodeEtat());
            }
        }

        eleve.setExisteAdresseATraiter(estEleveAvecAdresseATraiter);
        eleveDao.majFlagExisteAdresseATraiter(numDossier, estEleveAvecAdresseATraiter);
    }

    @Override
    public void majFlagExisteAdresseATraiter(Set<Long> numDossiers) throws ApplicationException {
        for (Long numeroDossierEleve : numDossiers) {
            eleveManager.majFlagExisteAdresseATraiter(numeroDossierEleve);
        }
    }

    @Override
    public void majLanguesEcole(Matiere langueEtrangereOblEcole, Matiere langueEtrangereFacEcole,
            Matiere langueRegionaleEcole, Long[] numDossierEleves) {
        eleveDao.majLanguesEcole(langueEtrangereOblEcole, langueEtrangereFacEcole, langueRegionaleEcole,
                numDossierEleves);
    }

    @Override
    public int count(Filtre filtre) throws DaoException {
        return eleveDao.count(filtre);
    }

    @Override
    public int compteElevesAvecCalculCollegeSecteurAbouti(String codeDeptMen) throws DaoException {
        return eleveDao.compteElevesAvecCalculCollegeSecteurAbouti(codeDeptMen);
    }

    @Override
    public int compteElevesAvecCalculCollegeSecteurDejaEffectue(String codeDeptMen) throws DaoException {
        return eleveDao.compteElevesAvecCalculCollegeSecteurDejaEffectue(codeDeptMen);
    }

    @Override
    public Map<List<EtablissementSconet>, List<Eleve>> listerAvecSecteur(InfoListe<Eleve> infoListe)
            throws DaoException {
        return listerAvecSecteur(infoListe.getPaginationSupport(), infoListe.getListTri(),
                infoListe.composeTousFiltres());
    }

    @Override
    public Map<List<EtablissementSconet>, List<Eleve>> listerAvecSecteur(Pagination<Eleve> p, List<Tri> tris,
            Filtre filtre) throws DaoException {

        Filtre f = filtre;
        List<Tri> t = tris;
        Map<List<EtablissementSconet>, List<Eleve>> elevesParSecteur = new TreeMap<>(new SecteurComparator());
        // on liste les numéros de dossier pour la pagination (ordonnés par secteur)
        // IMPORTANT : il est impératif de trier par rapport à la propriété collegeSecteursCodes, qui est une
        // agrégation des collèges potentiels. Cette agrégation remplace la notion de secteur qui manque à
        // l'application
        Selector<Object[]> sel = eleveDao.selectorCollegesSecteursId(t);
        @SuppressWarnings({ "unchecked", "rawtypes" })
        Pagination<Object[]> pagination = (Pagination) p;
        sel.setPaginationSupport(pagination);
        sel.addFiltre(f);

        List<Object[]> results = sel.lister();
        if (results.isEmpty()) {
            return elevesParSecteur;
        }
        List<Long> numDossiers = new ArrayList<>(results.size());
        for (Object o : results) {
            if (o instanceof Object[]) {
                numDossiers.add((Long) (((Object[]) o)[0]));
            } else {
                numDossiers.add((Long) o);
            }
        }

        List<Eleve> eleves = new ArrayList<>(eleveDao.listerElevesParNumDossier(null, t, numDossiers));

        Map<String, EtablissementSconet> cacheEtab = new HashMap<>();
        /* On choisit une LinkedHashMap pour afficher les éléments dans l'ordre d'insertion */
        for (Eleve e : eleves) {
            List<EtablissementSconet> etabs = new ArrayList<>(e.getCollegeSecteurs().size());
            for (CollegeSecteur collegeSecteur : e.getCollegeSecteurs()) {
                String etabRne = collegeSecteur.getEtablissementSconet().getCodeRne();
                if (etabRne.startsWith(e.getDept().getDeptCode())) {
                    EtablissementSconet etab = cacheEtab.get(etabRne);
                    if (etab == null) {
                        etab = etablissementSconetManager.chargerParCodeRne(etabRne);
                        cacheEtab.put(etabRne, etab);
                    }
                    etabs.add(etab);
                }
            }
            List<Eleve> elevesDuSecteur = elevesParSecteur.get(etabs);
            if (elevesDuSecteur == null) {
                elevesDuSecteur = new ArrayList<>();
                elevesParSecteur.put(etabs, elevesDuSecteur);
            }
            elevesDuSecteur.add(e);
        }

        return elevesParSecteur;
    }

    @Override
    public EtatDemandeAffectation controleOffreDeFormation6emeDemandeAffectation(Eleve eleve)
            throws ApplicationException {

        String codeDeptMen = eleve.getDept().getDeptCode();

        boolean offreDeFormation6emeExiste = true;

        EtablissementSconet collegeSecteurDesigneDasen = eleve.getCollegeSecteur();

        // si un collège de secteur est désigné par le DASEN, on vérifie que la formation 6ème existe
        // pour ce collège
        if (collegeSecteurDesigneDasen != null) {
            offreDeFormation6emeExiste = offreFormationManager.existeByFormationAndEtab(Formation.CODE_MEF_6EME,
                    collegeSecteurDesigneDasen.getCodeRne());
            if (!offreDeFormation6emeExiste) {
                logFormationInexistante(eleve);
                return EtatDemandeAffectation.IMPOSSIBLE_OFFRE_FORMATION_6EME_ABSENTE_COLLEGE_DESIGNE;
            }
            // sinon on vérifie que l'offre 6ème existe pour tous les collèges de secteur
        } else {
            for (CollegeSecteur collegeSecteur : eleve.getCollegeSecteurs()) {
                OffreFormation offreFormationParDefaut = offreFormationManager.chargerByFormationAndEtab(
                        Formation.CODE_MEF_6EME, collegeSecteur.getEtablissementSconet().getCodeRne(),
                        codeDeptMen);
                offreDeFormation6emeExiste &= (offreFormationParDefaut != null);
            }

            if (!offreDeFormation6emeExiste) {

                logFormationInexistante(eleve);

                // si 1 seul collège de secteur il est automatiquement désigné
                if (eleve.getCollegeSecteurs().size() == 1) {
                    return EtatDemandeAffectation.IMPOSSIBLE_OFFRE_FORMATION_6EME_ABSENTE_COLLEGE_DESIGNE;
                }

                return EtatDemandeAffectation.IMPOSSIBLE_OFFRE_FORMATION_6EME_ABSENTE_TOUS_LES_COLLEGES;
            }
        }

        return EtatDemandeAffectation.RIEN;
    }

    @Override
    public EtatDemandeAffectation gereDemandeAffectation(Eleve eleve, String codeDeptMen)
            throws ApplicationException {

        demandeAffectationManager.gereDemandeAffectation(eleve);

        EtatDemandeAffectation etatGenererDemandesAutomatique = demandeAffectationManager
                .genererDemandesAutomatique(eleve, codeDeptMen);

        // le contrôle doit être effectué uniquement si le collège de secteur est dans le département
        // car une offre de formation n'est pas créée pour un collège de secteur hors du département
        if (BooleanUtils.isTrue(eleve.getEstCollegeSecteurDansDepartement())) {

            // TODO : refactoring nécessaire car contrôle déjà fait dans la méthode genererDemandesAutomatique
            // si le contrôle n'est pas refait ici => problème lorsque le DASEN désigne un collège de secteur
            EtatDemandeAffectation controleOffreFormation6eme = controleOffreDeFormation6emeDemandeAffectation(
                    eleve);

            // si le ou tous les collèges de secteur n'ont pas d'offre de formation 6ème
            if (EtatDemandeAffectation.IMPOSSIBLE_OFFRE_FORMATION_6EME_ABSENTE_COLLEGE_DESIGNE
                    .equals(controleOffreFormation6eme)
                    || EtatDemandeAffectation.IMPOSSIBLE_OFFRE_FORMATION_6EME_ABSENTE_TOUS_LES_COLLEGES
                            .equals(controleOffreFormation6eme)) {

                // purge nécessaire quand l'IA DASEN modifie un collège de secteur désigné (en multi-collège)
                // /!\ si l'élève a une demande dans un collège hors public du département la demande ne doit pas
                // être supprimée
                purgerDemandesAffecEtSaisieChoixFamille(eleve, false, false);

                // on retourne l'info pour afficher le message d'avertissement concernant l'absence de formation
                // 6ème cet avertissement sera affiché uniquement si un collège de secteur est désigné
                return controleOffreFormation6eme;
            }
        }

        return etatGenererDemandesAutomatique;
    }

    @Override
    public boolean estAdresseDansDepartement(Eleve eleve) {

        if (eleve.getAdresseResidence() != null && eleve.getAdresseResidence().getCommuneInsee() != null) {

            CommuneInsee communeInsee = eleve.getAdresseResidence().getCommuneInsee();
            String codeDepartement = communeInsee.getDept().getCodeDepartement();

            return eleve.getDept().getDeptCode().equals(codeDepartement);
        }

        return false;
    }

    @Override
    public boolean possedeRepresentantLegal(Eleve eleve) {
        if (eleve.getResponsables() != null) {
            for (Responsable r : eleve.getResponsables()) {
                if (r.getNiveauResponsabilite() != null && NiveauResponsabilite.CODE_REPRESENTANT_LEGAL
                        .equals(r.getNiveauResponsabilite().getCode())) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public Map<Long, DecisionDepartement> getDecisionsParNumDossier(Collection<Eleve> eleves) {
        Map<Long, DecisionDepartement> decisions = new HashMap<>();
        Map<String, Boolean> cacheEtatResultatParDepartement = new HashMap<>();
        for (Eleve eleve : eleves) {
            decisions.put(eleve.getNumDossier(),
                    calculeDecisionAffectation(eleve, cacheEtatResultatParDepartement));
        }
        return decisions;
    }

    /**
     * 
     * @param eleve
     *            l'élève
     * @param cacheEtatParDepartement
     *            une Map stockant l'état applicatif par département (performances)
     * @return la décision
     */
    public DecisionDepartement calculeDecisionAffectation(Eleve eleve,
            Map<String, Boolean> cacheEtatParDepartement) {
        if (eleve.getAffectationFinale() != null && eleve.getAffectationFinale().getEtatDemande() != null) {
            return new DecisionDepartement(eleve.getAffectationFinale().getEtatDemande().getLibelleEtat(), true);
        }

        boolean refuse = false;
        for (DemandeAffectation da : eleve.getDemandeAffectation()) {
            String etatDemande = null;
            if (da.getEtatDemande() != null) {
                etatDemande = da.getEtatDemande().getCodeEtatFinal();
            }
            if (CodeEtatDecision.AFFECTE.getCode().equals(etatDemande)) {
                return new DecisionDepartement(CodeEtatDecision.AFFECTE, false);
            } else if (CodeEtatDecision.REFUSE.getCode().equals(etatDemande)) {
                refuse = true;
            } else {
                return new DecisionDepartement(CodeEtatDecision.NON_TRAITE, false);
            }
        }
        if (refuse) {
            return new DecisionDepartement(CodeEtatDecision.REFUSE, false);
        }

        String codeDepMen = eleve.getDept().getDeptCode();
        Boolean etatResultat = cacheEtatParDepartement.get(codeDepMen);
        if (etatResultat == null) {
            etatResultat = appAutomateManager.isEtatFonctionnelDepartement(codeDepMen,
                    AppAutomateManager.ETAT_DEPT_RESULTATS, true);
            cacheEtatParDepartement.put(codeDepMen, etatResultat);
        }

        if (eleve.getDecisionPassage() != null
                && DecisionPassageEnum.MAINTENU.getCode().equals(eleve.getDecisionPassage().getCod())) {
            return new DecisionDepartement("Maintenu à l'école primaire", etatResultat);
        }
        return new DecisionDepartement((String) null, etatResultat);
    }

    /**
     * Met à jour l'état collège secteur d'un élève.
     * 
     * @param eleve
     *            l'élève modifié
     * @param donneesCalcul
     *            les données pour le positionnement du/des collèges de secteur
     * @param estCollegeSecteurDansDept
     *            indique si le collège de secteur est dans le département
     * @param estCollegesSecteurModifies
     *            indique si les collèges de secteur sont modifiés
     * @param passageHorsDept
     *            indique si on passe d'un collège de secteur dans le département à hors département
     * @throws DaoException
     *             en cas de problème
     */
    private void updateEtatCollegeSecteur(Eleve eleve, DonneesCalculCollegeSecteur donneesCalcul,
            Boolean estCollegeSecteurDansDept, boolean estCollegesSecteurModifies, boolean passageHorsDept)
            throws DaoException {

        boolean estSaisieManuelle = DeterminationAutomatiqueManager.SAISIE_MANUELLE
                .equals(donneesCalcul.getEtatCalcul());

        if (BooleanUtils.isTrue(estCollegeSecteurDansDept)
                && CollectionUtils.isEmpty(donneesCalcul.getCollegesSecteur()) && estSaisieManuelle) {
            eleve.setEtatCollegeSecteur(null);
        } else {

            EtatCollegeSecteur etatCollegeSecteur = donneesCalcul.getEtatCalcul();

            if (!estSaisieManuelle || (estCollegesSecteurModifies || passageHorsDept)) {
                if (etatCollegeSecteur != null && etatCollegeManager.existe(etatCollegeSecteur.getCode())) {
                    eleve.setEtatCollegeSecteur(etatCollegeManager.charger(etatCollegeSecteur.getCode()));
                } else {
                    eleve.setEtatCollegeSecteur(null);
                }
            }
        }
    }

    /**
     * Met à jour le profil de l'utilisateur lors de la modification des collèges de secteur.
     * 
     * @param eleve
     *            l'élève modifié
     * @param donneesCalcul
     *            les données pour le positionnement du/des collèges de secteur
     * @param estCollegesSecteurModifies
     *            indique si les collèges de secteur sont modifiés
     * @param passageHorsDept
     *            indique si on passe d'un collège de secteur dans le département à hors département
     * @param passageDansDept
     *            indique si on passe d'un collège de secteur hors département dans le département
     */
    private void updateRoleMajCollegeSecteur(Eleve eleve, DonneesCalculCollegeSecteur donneesCalcul,
            boolean estCollegesSecteurModifies, boolean passageHorsDept, boolean passageDansDept) {

        if (estCollegesSecteurModifies || passageHorsDept || passageDansDept) {
            eleve.setRoleMajCollegeSecteur(donneesCalcul.getRoleUtilisateur());
        }
    }

    @Override
    public List<Eleve> listerElevesAvecDerogSansEditionAccuseReceptionEcole(String codeUai, String codeDept) {
        return eleveDao.listerElevesAvecDerogSansEditionAccuseReceptionEcole(codeUai, codeDept);
    }

    @Override
    public void restaurerEleve(Eleve eleve, SauvegardeEleve sauvegardeEleve) throws ApplicationException {
        if (sauvegardeEleve != null) {
            sauvegardeEleveManager.appliquerSauvegardeEleve(sauvegardeEleve, eleve);
            eleveDao.maj(eleve);
        }
    }

    @Override
    public boolean isImportEcolePubliqueOnde(Eleve eleve) {

        if (eleve == null || eleve.getTypeOrigineDossier() == null) {
            return false;
        }

        return Arrays.asList(TypeOrigineDossier.ORIGINE_DOSSIER_IMPORT_ONDE_ECOLE_PUBLIQUE)
                .contains(eleve.getTypeOrigineDossier().getCode());
    }

    @Override
    public boolean isEtabOrigineDsden(Eleve eleve) {
        if (eleve == null || eleve.getEcoleOrigine() == null) {
            return false;
        }

        return EtablissementOrigine.CODE_NATURE_DSDEN.equals(eleve.getEcoleOrigine().getCodNtr());
    }

    /**
     * Créer une copie de l'adresse dans le département.
     * TODO : à déplacer dans AdresseManager, au lieu d'injecter adresseDao il faudrait injecter adresseManager
     * 
     * @param adresseSource
     *            l'adresse à copier : peut-être nulle
     * @param deptMen
     *            le département du MEN où il faut faire la copie
     * @return l'adresse copiée et crée en base si l'adresse source est non nulle sion nulle
     * @throws ApplicationException
     *             en cas d'erreur
     */
    public Adresse creerCopieAdresse(Adresse adresseSource, DeptMen deptMen) throws ApplicationException {

        if (adresseSource == null) {
            return null;
        }

        Adresse adresseCopie = AdresseHelper.copieAdresse(adresseSource, deptMen);
        adresseDao.creer(adresseCopie);

        // Copie des propositions, codes retour et messages retour de l'adresse
        Set<PropositionAdresse> propositionsCopie = new LinkedHashSet<>();
        if (!adresseSource.getPropositions().isEmpty()) {
            for (PropositionAdresse propositionSource : adresseSource.getPropositions()) {
                PropositionAdresse propositionCopie = AdresseHelper.copieProposition(propositionSource,
                        adresseCopie);
                propositionAdresseDao.creer(propositionCopie);
                propositionsCopie.add(propositionCopie);
            }
        }
        adresseCopie.setPropositions(propositionsCopie);

        Set<RetourAdresseRNVP> codesRetourCopie = new HashSet<>();
        if (!adresseSource.getRetoursRNVP().isEmpty()) {
            for (RetourAdresseRNVP retourSource : adresseSource.getRetoursRNVP()) {
                RetourAdresseRNVP retourCopie = AdresseHelper.copieRetour(retourSource, adresseCopie);
                retourRNVPDao.creer(retourCopie);
                codesRetourCopie.add(retourCopie);
            }
        }
        adresseCopie.setRetoursRNVP(codesRetourCopie);

        return adresseCopie;
    }

    /**
     * Sélecteur d'élèves affectés dans le département.
     *
     * @param codeDeptMen
     *            le code MEN du département
     * @return le sélecteur d'élèves affectés dans le département
     */
    private Selector<Eleve> selectorElevesAffectes(String codeDeptMen) {
        Selector<Eleve> selector = eleveDao.creerSelector();
        selector.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        selector.addFiltre(filtreElevesAffectes());
        selector.addFiltre(filtreElevesDept(codeDeptMen));
        return selector;
    }

    /**
     * Sélecteur d'élèves affectés dans le département avec une décision de passage en 6ème.
     *
     * @param codeDeptMen
     *            le code MEN du département
     * @return le sélecteur d'élèves affectés dans le département avec une décision de passage en 6ème
     */
    private Selector<Eleve> selectorElevesAffectesPassageEn6eme(String codeDeptMen) {
        Selector<Eleve> selector = selectorElevesAffectes(codeDeptMen);
        selector.addFiltre(filtreElevesPassageEn6eme());
        return selector;
    }
}
