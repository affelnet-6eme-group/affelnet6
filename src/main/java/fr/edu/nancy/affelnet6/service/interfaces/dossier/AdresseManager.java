/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.service.interfaces.dossier;

import java.util.List;

import fr.edu.nancy.affelnet6.domain.calcul.DonneesCalculCollegeSecteur;
import fr.edu.nancy.affelnet6.domain.dossier.Adresse;
import fr.edu.nancy.affelnet6.domain.dossier.Eleve;
import fr.edu.nancy.affelnet6.domain.dossier.Responsable;
import fr.edu.nancy.commun.dao.interfaces.Pagination;
import fr.edu.nancy.commun.dao.interfaces.Tri;
import fr.edu.nancy.commun.dao.interfaces.filtres.Filtre;
import fr.edu.nancy.commun.exceptions.ApplicationException;
import fr.edu.nancy.commun.exceptions.BusinessException;
import fr.edu.nancy.commun.exceptions.DaoException;
import fr.edu.nancy.commun.exceptions.TechnicalException;
import fr.edu.nancy.commun.web.session.InfoListe;

/**
 * Interface du gestionnaire de Adresse.
 */
public interface AdresseManager {

    /** Filtre sur le flag "Adresse Française" de l'adresse. */
    String[] FILTRE_ADRESSE_FR = { "0", "adresseEnFrance", "Adresse en France" };
    /** Filtre sur le statut d'une adresse. */
    String[] FILTRE_STATUT_ADRESSE = { "1", "statutAdresse.code", "Statut de l'adresse" };
    /** Filtre sur l'état d'une adresse. */
    String[] FILTRE_ETAT_ADRESSE = { "2", "statutAdresse.codeEtat", "Etat de l'adresse" };
    /** Filtre sur le département. */
    String[] FILTRE_CODE_DEPARTEMENT = { "3", "dept.deptCode", "Département" };
    /** Filtre sur le code uai du l'école d'origine des élèves. */
    String[] FILTRE_UAI_ECOLE_ELEVE = { "4", "eleves.codUaiOri", "UAI de l'école" };

    /** Filtre sur le code uai du collège d'affectation finale. */
    String[] FILTRE_UAI_COLLEGE = { "5", "eleves.affectationFinale.offreformation.etablissementSconet.etabId",
            "UAI du collège" };
    /** Filtre sur le statut du calcul de collège de l'élève. */
    String[] FILTRE_ETA_COL = { "6", "eleves.etatCollegeSecteur.code", "Code de l'état du collège de secteur" };

    /** Filtre sur le type de voie. */
    String[] FILTRE_TYPE_VOIE = { "7", "typeVoie", "Type de voie" };

    /** Filtre sur le libellé de la voie. */
    String[] FILTRE_LIBELLE_VOIE = { "8", "libelleVoie", "Libellé de voie" };

    /** Filtre sur le code postal. */
    String[] FILTRE_CODE_POSTAL = { "9", "codePostal.codePostal", "Code Postal" };

    /** Filtre sur le code commune INSEE. */
    String[] FILTRE_CODE_COMMUNE_INSEE = { "10", "communeInsee.codeCommuneInsee", "Code commune INSEE" };

    /** Filtre sur le libellé de la commune INSEE. */
    String[] FILTRE_LIBELLE_COMMUNE_INSEE = { "11", "communeInsee.llInseeCommune", "Libellé de la commune" };

    /** Filtre sur le code de la cause du calcul de collège de secteur non abouti. */
    String[] FILTRE_CODE_CAUSE_CALCUL_COLLEGE_SECTEUR = { "12", "elevesSimule.causeCalculCollegeSecteur.code",
            "Cause du calcul non abouti" };

    /** Filtre sur la ligne 3 (concaténation du numéro de voie, type et libellé de voie). */
    String[] FILTRE_LIGNE3 = { "13", "ligne3Adr", "Type et libellé de voie" };

    /** Filtre sur le numéro de dossier. */
    String[] FILTRE_ELEVE_NUM_DOS = { "14", "eleves.numDossier", "Numéro de dossier de l'élève" };

    /** Filtre sur l'id des responsables. */
    String[] FILTRE_RESP_ID = { "15", "responsables.id", "Id des responsables" };

    /** Filtre sur le numéro de dossier pour l'adresse secondaire. */
    String[] FILTRE_ELEVE_SECONDAIRE_NUM_DOS = { "16", "elevesSecondaire.numDossier",
            "Numéro de dossier de l'élève (adresse secondaire)" };

    /** Numéro de l'adresse 1. */
    String ADRESSE_1 = "1";

    /** Numéro de l'adresse 2. */
    String ADRESSE_2 = "2";

    /**
     * Efface les adresses du département en vue d'un nouvel import.
     * 
     * @param dptMenId
     *            Identifiant du département MEN Concené
     * @return nombre de suppressions effectuées
     * @throws ApplicationException
     *             en cas de problème
     */
    int supprimerPourRemplacerDepartement(int dptMenId) throws ApplicationException;

    /**
     * Met à jour et raffraîchit l'adresse passée en paramètre dans la session hibernate.
     * 
     * @param adresse
     *            l'adresse à sauvegarder
     * @throws DaoException
     *             en cas d'erreur
     */
    void creerOuMajEtRefresh(Adresse adresse) throws DaoException;

    /**
     * Liste l'ensemble des Adresses.
     * 
     * @param paginationSupport
     *            les infos de pagination (ex.:numéro de la page courante
     *            (commence à 0))
     * @param listTri
     *            la liste des tris à effectuer
     * @param filtre
     *            le filtre à appliquer
     * @return liste des adresses.
     * @throws DaoException
     *             En cas de problème pour accéder aux données
     */
    List<Adresse> lister(Pagination<Adresse> paginationSupport, List<Tri> listTri, Filtre filtre)
            throws DaoException;

    /**
     * Liste les adresses avec un distinct sur le type et libellé de voie, le code postal et la commune.
     * 
     * @param infoListe
     *            les informations de pagination, tri, filtre
     * @param avecDistinctNumeroVoie
     *            indique s'il faut aussi faire un distinct sur le numéro de voie et l'indice de répétition
     * @return la liste
     * @throws DaoException
     *             en cas d'erreur
     */
    List<Adresse> listerDistinctLibelleVoieCommune(InfoListe<Adresse> infoListe, boolean avecDistinctNumeroVoie)
            throws DaoException;

    /**
     * Liste les adresses avec un distinct sur le type et libellé de voie, le code postal et la commune.
     * 
     * @param paginationSupport
     *            les paramètres de pagination et filtrage
     * @param listTri
     *            les paramètres de tri
     * @param filtre
     *            les paramètres de filtrage
     * @param avecDistinctNumeroVoie
     *            indique s'il faut aussi faire un distinct sur le numéro de voie et l'indice de répétition
     * @return la liste
     * @throws DaoException
     *             en cas d'erreur
     */
    List<Adresse> listerDistinctLibelleVoieCommune(Pagination<Adresse> paginationSupport, List<Tri> listTri,
            Filtre filtre, boolean avecDistinctNumeroVoie) throws DaoException;

    /**
     * Liste l'ensemble des adresses.
     * 
     * @return la liste des adresses
     * @throws TechnicalException
     *             En cas d'erreur
     * @throws BusinessException
     *             En cas d'erreur
     */
    List<Adresse> lister() throws TechnicalException, BusinessException;

    /**
     * Teste s'il existe une adresse correspondant à l'identifiant.
     * 
     * @param id
     *            l'id de l'adresse
     * @return Vrai si l'adresse existe.<br>
     *         Faux si l'adresse n'existe.<br>
     *         Faux si <code>key</code> est <code>null</code>.
     * @throws ApplicationException
     *             Exception lancée en cas de problème
     */
    boolean existe(Long id) throws ApplicationException;

    /**
     * Charge une adresse à partir de son id.
     * 
     * @param id
     *            l'id de l'adresse
     * @return l'adresse chargée
     * @throws ApplicationException
     *             Exception lancée en cas de problème
     */
    Adresse charger(Long id) throws ApplicationException;

    /**
     * Supprimer une adresse.
     * 
     * @param adresse
     *            l'adresse à supprimer
     * @throws ApplicationException
     *             Exception lancée en cas de problème à la mise à jour
     */
    void supprimer(Adresse adresse) throws ApplicationException;

    /**
     * Supprimer une adresse.
     * 
     * @param adresseId
     *            l'identifiant de l'adresse à supprimer
     * @throws ApplicationException
     *             Exception lancée en cas de problème à la mise à jour
     */
    void supprimer(Long adresseId) throws ApplicationException;

    /**
     * 
     * @param donneesCalcul
     *            les données utilisées pour le calcul du / des collèges de secteur
     * @param adresse
     *            l'adresse
     * @return vrai si l'adresse a été modifiée
     * @throws ApplicationException
     *             Exception lancée en cas de problème à la mise à jour
     */
    boolean modifierUneAdresse(DonneesCalculCollegeSecteur donneesCalcul, Adresse adresse)
            throws ApplicationException;

    /**
     * Donne le nombre de voies manquantes dans la carte scolaire à partir de la simulation du calcul de collège de
     * secteur.
     * 
     * @param codeDept
     *            code département
     * @return le nombre de voies manquantes dans la carte scolaire à partir de la simulation du calcul de collège
     *         de secteur
     * @throws DaoException
     *             en cas d'erreur
     */
    int compteNbVoiesManquantesCarteScoalire(String codeDept) throws DaoException;

    /**
     * Donne le nombre de numéros de voie manquants dans la carte scolaire à partir de la simulation du calcul de
     * collège de secteur.
     * 
     * @param codeDept
     *            code département
     * @return le nombre de numéros de voie manquants dans la carte scolaire à partir de la simulation du calcul de
     *         collège de secteur
     * @throws DaoException
     *             en cas d'erreur
     */
    int compteNbNumerosVoieManquantsCarteScoalire(String codeDept) throws DaoException;

    /**
     * Donne le nombre de chevauchement de voies dans la carte scolaire à partir de la simulation du calcul de
     * collège de secteur.
     * 
     * @param codeDept
     *            code département
     * @return le nombre de chevauchements de voies dans la carte scolaire à partir de la simulation du calcul de
     *         collège de secteur
     * @throws DaoException
     *             en cas d'erreur
     */
    int compteNbChevauchementsVoiesCarteScoalire(String codeDept) throws DaoException;

    /**
     * Créer ou met à jour l'adresse d'un élève.
     * 
     * @param adresse
     *            l'adresse
     * @param eleve
     *            l'élève
     * @throws ApplicationException
     *             en cas d'erreur
     */
    void creerOuMajAdresseEleve(Adresse adresse, Eleve eleve) throws ApplicationException;

    /**
     * Créer ou met à jour l'adresse secondaire d'un élève.
     * 
     * @param adresse
     *            l'adresse secondaire de l'élève
     * @param eleve
     *            l'élève
     * @throws ApplicationException
     *             en cas d'erreur
     */
    void creerOuMajAdresseSecondaireEleve(Adresse adresse, Eleve eleve) throws ApplicationException;

    /**
     * Créer ou met à jour l'adresse d'un responsable.
     * 
     * @param adresse
     *            l'adresse
     * @param responsable
     *            le responsable
     * @throws ApplicationException
     *             en cas d'erreur
     */
    void creerOuMajAdresseResponsable(Adresse adresse, Responsable responsable) throws ApplicationException;

    /**
     * Supprime une adresse si elle est liée à personne.
     * 
     * @param adresse
     *            l'adresse à supprimée
     */
    void supprimerSiAdresseInutilisee(Adresse adresse);
}
