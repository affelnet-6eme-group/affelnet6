/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.service.interfaces.dossier;

import fr.edu.nancy.affelnet6.domain.dossier.PropositionAdresse;
import fr.edu.nancy.commun.exceptions.DaoException;

/**
 * Interface du gestionnaire de PropositionAdresse.
 */
public interface PropositionAdresseManager {
    /** Filtre sur le flag "Adresse Française" de l'adresse. */
    String[] FILTRE_ADRESSE_FR = { "0", "adresse.adresseEnFrance", "Adresse en France" };
    /** Filtre sur le statut d'une adresse. */
    String[] FILTRE_STATUT_ADRESSE = { "1", "adresse.statutAdresse.code", "Statut de l'adresse" };
    /** Filtre sur le département. */
    String[] FILTRE_CODE_DEPARTEMENT = { "2", "adresse.dept.deptCode", "Département" };

    /**
     * Efface les propositions d'adresse du département en vue d'un nouvel import.
     * 
     * @param dptMenId
     *            Identifiant du département MEN Concené
     * @return nombre de suppressions effectuées
     * @throws DaoException
     *             en cas de problème
     */
    int supprimerPourRemplacerDepartement(int dptMenId) throws DaoException;

    /**
     * Recherche une proposition à partir de son identifiant.
     * 
     * @param id
     *            l'identifiant de la proposition
     * @return la proposition d'adresse
     * @throws DaoException
     *             en cas de problème
     */
    PropositionAdresse charger(Long id) throws DaoException;

    /**
     * Insertion d'une proposition d'adresse en base.
     * 
     * @param propositionAdresse
     *            la proposition à insérer
     * @return l'identifiant de la proposition
     * @throws DaoException
     *             en cas de problème
     */
    Long creer(PropositionAdresse propositionAdresse) throws DaoException;

    /**
     * Efface les propositions d'adresse d'une adresse.
     * 
     * @param idAdresse
     *            identifiant de l'adresse pour laquelle il faut supprimer les propositions
     * @return nombre de suppressions effectuées
     * @throws DaoException
     *             en cas de problème
     */
    int supprimerPourAdresse(Long idAdresse) throws DaoException;
}
