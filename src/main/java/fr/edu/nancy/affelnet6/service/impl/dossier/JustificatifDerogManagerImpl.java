/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.service.impl.dossier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.edu.nancy.affelnet6.dao.interfaces.dossier.JustificatifDerogDao;
import fr.edu.nancy.affelnet6.domain.dossier.JustificatifDerog;
import fr.edu.nancy.affelnet6.domain.nomenclature.MotifDerog;
import fr.edu.nancy.affelnet6.service.dossier.JustificatifDerogManager;
import fr.edu.nancy.commun.dao.interfaces.Pagination;
import fr.edu.nancy.commun.dao.interfaces.Tri;
import fr.edu.nancy.commun.dao.interfaces.filtres.Filtre;
import fr.edu.nancy.commun.exceptions.ApplicationException;
import fr.edu.nancy.commun.exceptions.BusinessException;
import fr.edu.nancy.commun.exceptions.TechnicalException;
import fr.edu.nancy.commun.web.session.InfoListe;

/**
 * Gestionnaire de JustificatifDerog.
 * 
* @see fr.edu.nancy.affelnet6.domain.dossier.JustificatifDerog
 */
@Service("justificatifDerogManager")
public class JustificatifDerogManagerImpl implements JustificatifDerogManager {

    /** Objet d'accès aux données des JustificatifDerog. */
    @Autowired
    private JustificatifDerogDao justificatifderogDao;

    @Override
    public List<JustificatifDerog> lister(Pagination<JustificatifDerog> paginationSupport, List<Tri> listTri,
            Filtre filtre) throws TechnicalException, BusinessException {
        return justificatifderogDao.lister(paginationSupport, listTri, filtre);
    }

    @Override
    public List<JustificatifDerog> lister(InfoListe<JustificatifDerog> inf) throws TechnicalException,
            BusinessException {
        return justificatifderogDao.lister(inf.getPaginationSupport(), inf.getListTri(), inf.composeTousFiltres());
    }

    @Override
    public List<JustificatifDerog> lister() throws TechnicalException, BusinessException {
        return justificatifderogDao.lister();
    }

    @Override
    public JustificatifDerog charger(Long id) throws ApplicationException {
        return justificatifderogDao.charger(id);
    }

    @Override
    public void maj(JustificatifDerog p) throws ApplicationException {
        justificatifderogDao.maj(p);
    }

    @Override
    public void supprimer(JustificatifDerog p) throws ApplicationException {
        justificatifderogDao.supprimer(p);
    }

    @Override
    public Long creer(JustificatifDerog p) throws ApplicationException {
        return justificatifderogDao.creer(p);
    }

    @Override
    public boolean existe(Long id) throws ApplicationException {
        return justificatifderogDao.existe(id);
    }

    @Override
    public int compteDemandesDerog(String codeOffre, String codeMotif) {
        return justificatifderogDao.compteDemandesDerog(codeOffre, codeMotif);
    }

    @Override
    public int compteDemandesDerogMotif(MotifDerog motif) {
        return justificatifderogDao.compteDemandesDerogMotif(motif);
    }

    @Override
    public int compteAffectationsDerog(String codeOffre, String codeMotif) {
        return justificatifderogDao.compteAffectationsDerog(codeOffre, codeMotif);
    }

}
