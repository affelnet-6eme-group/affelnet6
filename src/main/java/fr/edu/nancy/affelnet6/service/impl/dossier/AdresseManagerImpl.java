/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.service.impl.dossier;

import java.util.List;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.edu.nancy.affelnet6.dao.interfaces.dossier.AdresseDao;
import fr.edu.nancy.affelnet6.domain.calcul.DonneesCalculCollegeSecteur;
import fr.edu.nancy.affelnet6.domain.dossier.Adresse;
import fr.edu.nancy.affelnet6.domain.dossier.Eleve;
import fr.edu.nancy.affelnet6.domain.dossier.Responsable;
import fr.edu.nancy.affelnet6.domain.nomenclature.StatutAdresse;
import fr.edu.nancy.affelnet6.exceptions.RNVPException;
import fr.edu.nancy.affelnet6.service.dossier.EleveManager;
import fr.edu.nancy.affelnet6.service.interfaces.DeterminationAutomatiqueManager;
import fr.edu.nancy.affelnet6.service.interfaces.ValidationAdresseRNVPManager;
import fr.edu.nancy.affelnet6.service.interfaces.dossier.AdresseManager;
import fr.edu.nancy.commun.dao.interfaces.Pagination;
import fr.edu.nancy.commun.dao.interfaces.Tri;
import fr.edu.nancy.commun.dao.interfaces.filtres.Filtre;
import fr.edu.nancy.commun.exceptions.ApplicationException;
import fr.edu.nancy.commun.exceptions.BusinessException;
import fr.edu.nancy.commun.exceptions.DaoException;
import fr.edu.nancy.commun.exceptions.TechnicalException;
import fr.edu.nancy.commun.web.session.InfoListe;

/**
 * Gestionnaire de Adresse.
 */
@Service("adresseManager")
public class AdresseManagerImpl implements AdresseManager {

    /** LOG. */
    private static final Logger LOG = LoggerFactory.getLogger(AdresseManagerImpl.class);

    /** DAO Adresse. */
    @Autowired
    private AdresseDao adresseDao;

    /** Gestionnaire pour les élèves. */
    @Autowired
    private EleveManager eleveManager;

    /** Gestionnaire pour la détermination automatique des collèges de secteur. */
    @Autowired
    private DeterminationAutomatiqueManager determinationAutomatiqueManager;

    /** Gestionnaire de la validation par RNVP. */
    @Autowired
    private ValidationAdresseRNVPManager validationAdresseRNVPManager;

    @Override
    public int supprimerPourRemplacerDepartement(int dptMenId) throws ApplicationException {
        return adresseDao.supprimerPourRemplacerDepartement(dptMenId);
    }

    @Override
    public void creerOuMajEtRefresh(Adresse adresse) throws DaoException {
        adresseDao.creerOuMaj(adresse);
        adresseDao.rafraichit(adresse);
    }

    @Override
    public List<Adresse> lister() throws TechnicalException, BusinessException {
        return adresseDao.lister();
    }

    @Override
    public List<Adresse> lister(Pagination<Adresse> paginationSupport, List<Tri> listTri, Filtre filtre)
            throws DaoException {
        return adresseDao.lister(paginationSupport, listTri, filtre);
    }

    @Override
    public List<Adresse> listerDistinctLibelleVoieCommune(InfoListe<Adresse> infoListe,
            boolean avecDistinctNumeroVoie) throws DaoException {
        return adresseDao.listerDistinctLibelleVoieCommune(infoListe, avecDistinctNumeroVoie);
    }

    @Override
    public List<Adresse> listerDistinctLibelleVoieCommune(Pagination<Adresse> paginationSupport, List<Tri> listTri,
            Filtre filtre, boolean avecDistinctNumeroVoie) throws DaoException {
        return adresseDao.listerDistinctLibelleVoieCommune(paginationSupport, listTri, filtre,
                avecDistinctNumeroVoie);
    }

    @Override
    public boolean existe(Long id) throws ApplicationException {
        return adresseDao.existe(id);
    }

    @Override
    public Adresse charger(Long id) throws ApplicationException {
        return adresseDao.charger(id);
    }

    @Override
    public void supprimer(Adresse adresse) throws ApplicationException {
        adresseDao.supprimer(adresse);
    }

    @Override
    public void supprimer(Long adresseId) throws ApplicationException {
        adresseDao.supprimer(adresseId);
    }

    @Override
    public boolean modifierUneAdresse(DonneesCalculCollegeSecteur donneesCalcul, Adresse adresse)
            throws ApplicationException {
        boolean serviceDispo = true;

        if (adresse.getAdresseEnFrance()
                && !StatutAdresse.ADR_VALIDEE.equals(adresse.getStatutAdresse().getCodeEtat())) {
            // redressement de l'adresse
            try {
                validationAdresseRNVPManager.controlerUneAdresse(adresse);
            } catch (RNVPException e) {
                LOG.warn(RNVPException.ERREUR_MESSAGE, e);
                serviceDispo = false;
            }
        }

        if (donneesCalcul != null) {
            determinationAutomatiqueManager.calculCollegeSecteur(donneesCalcul);
        }

        return serviceDispo;
    }

    @Override
    public int compteNbVoiesManquantesCarteScoalire(String codeDept) throws DaoException {
        return adresseDao.compteNbVoiesManquantesCarteScoalire(codeDept);
    }

    @Override
    public int compteNbNumerosVoieManquantsCarteScoalire(String codeDept) throws DaoException {
        return adresseDao.compteNbNumerosVoieManquantsCarteScoalire(codeDept);
    }

    @Override
    public int compteNbChevauchementsVoiesCarteScoalire(String codeDept) throws DaoException {
        return adresseDao.compteNbChevauchementsVoiesCarteScoalire(codeDept);
    }

    @Override
    public void creerOuMajAdresseEleve(Adresse adresse, Eleve eleve) throws ApplicationException {

        if (eleve == null || adresse == null) {
            return;
        }

        Adresse adresseEleve = eleve.getAdresseResidence();

        if (adresse.getAdrId() == null) {
            creerOuMajEtRefresh(adresse);
        }

        eleve.setAdresseResidence(adresse);

        if (adresseEleve != null && !adresse.getAdrId().equals(adresseEleve.getAdrId())) {
            Set<Eleve> eleves = adresseEleve.getEleves();
            if (CollectionUtils.isNotEmpty(eleves) && eleves.contains(eleve)) {
                eleves.remove(eleve);
            }
            supprimerSiAdresseInutilisee(adresseEleve);
        }
    }

    @Override
    public void creerOuMajAdresseSecondaireEleve(Adresse adresse, Eleve eleve) throws ApplicationException {

        if (eleve == null || adresse == null) {
            return;
        }

        Adresse adresseEleve = eleve.getAdresseSecondaire();

        if (adresse.getAdrId() == null) {
            creerOuMajEtRefresh(adresse);
        }

        eleve.setAdresseSecondaire(adresse);

        if (adresseEleve != null && !adresse.getAdrId().equals(adresseEleve.getAdrId())) {
            Set<Eleve> eleves = adresseEleve.getElevesSecondaire();
            if (CollectionUtils.isNotEmpty(eleves) && eleves.contains(eleve)) {
                eleves.remove(eleve);
            }
            supprimerSiAdresseInutilisee(adresseEleve);
        }
    }

    @Override
    public void creerOuMajAdresseResponsable(Adresse adresse, Responsable responsable)
            throws ApplicationException {

        if (responsable == null || adresse == null) {
            return;
        }

        Adresse adresseResp = responsable.getAdresseResponsable();

        if (adresse.getAdrId() == null) {
            creerOuMajEtRefresh(adresse);
        }

        responsable.setAdresseResponsable(adresse);

        if (adresseResp != null && !adresse.getAdrId().equals(adresseResp.getAdrId())) {
            Set<Responsable> responsables = adresseResp.getResponsables();
            if (CollectionUtils.isNotEmpty(responsables) && responsables.contains(responsable)) {
                responsables.remove(responsable);
            }
            supprimerSiAdresseInutilisee(adresseResp);
        }
    }

    @Override
    public void supprimerSiAdresseInutilisee(Adresse adresse) {
        if (CollectionUtils.isEmpty(adresse.getResponsables()) && CollectionUtils.isEmpty(adresse.getEleves())
                && CollectionUtils.isEmpty(adresse.getElevesSecondaire())
                && CollectionUtils.isEmpty(adresse.getElevesSimule())) {
            try {
                supprimer(adresse);
            } catch (ApplicationException e) {
                LOG.error("Erreur lors de la suppression de l'adresse : " + e.getMessage(), e);
            }
        }
    }
}
