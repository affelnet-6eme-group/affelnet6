/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.service.interfaces;

import java.util.List;
import java.util.Map;

import fr.edu.nancy.affelnet6.utils.Message;
import fr.edu.nancy.commun.exceptions.ApplicationException;
import fr.edu.nancy.commun.exceptions.BusinessException;
import fr.edu.nancy.commun.utils.BatchProgressMonitor;

/** Interface définissant les fonctions du gestionnaire d'affectation. */
public interface AffectationManager {

    /** Clé de résultat pour le département en pré-affectation. */
    String RES_KEY_DEPT_MEN = "RES_KEY_DEPT_MEN";

    /** Clé de résultat pour le nombre d'offres traitées en pré-affectation. */
    String RES_KEY_NB_OFFRES = "RES_KEY_NB_OFFRES";

    /** Clé de résultat pour le nombre de demandes traitées en pré-affectation. */
    String RES_KEY_NB_DEMANDES = "RES_KEY_NB_DEMANDES";

    /** Clé de résultat pour le nombre d'itérations en pré-affectation. */
    String RES_KEY_ITERATION_COUNT = "RES_KEY_ITERATION_COUNT";

    /** Clé de résultat - Liste des ids d'élèves sans affectation finale. */
    String RES_KEY_ELEVES_SANS_AFFECTATION_FINALE = "RES_KEY_ELEVES_SANS_AFFECTATION_FINALE";

    /** Clé de résultat - Nombre d'élèves sans demande. */
    String KEY_NB_ELEVES_SANS_DEMANDE = "KEY_NB_ELEVES_SANS_DEMANDE";

    /**
     * Vérification avant de lancer le batch de pré-affectation.
     * 
     * @param codeDeptMen
     *            code du département MEN concerné
     * @param bpm
     *            gestionnaire de progression
     * @throws BusinessException
     *             en cas d'erreur bloquante
     */
    void verifierPreaffectationPossible(String codeDeptMen, BatchProgressMonitor bpm) throws BusinessException;

    /**
     * Vérification pour savoir si on a le droit de lancer le batch de validation d'affectation.
     * Concrètement on vérifie que l'état fonctionnel est en période d'affectation.
     * 
     * @param codeDeptMen
     *            code du département MEN concerné
     * @param bpm
     *            gestionnaire de progression
     * @throws ApplicationException
     *             en cas d'erreur bloquante
     */
    void verifieValidationAffectationPossible(String codeDeptMen, BatchProgressMonitor bpm)
            throws ApplicationException;

    /**
     * Saisie d'un code de décision manuelle sur une demande d'affectation.
     * 
     * @param idDemande
     *            id demande d'affectation
     * @param codeDecisionManuelle
     *            code de la décision manuelle
     * @param msgAvertissement
     *            Fournir une liste vide (mais pas <code>null</code>)
     *            que la méthode pourra remplir avec des messages
     *            d'avertissement.
     * @throws ApplicationException
     *             Renvoie <code>DejaAffecteException</code> si
     *             l'élève est déjà affecté manuellement sur une
     *             autre demande.
     */
    void saisieDecisionManuelle(Long idDemande, String codeDecisionManuelle, List<Message> msgAvertissement)
            throws ApplicationException;

    /**
     * Saisie d'un code de décision manuelle sur une demande d'affectation. Propagation REQUIRES_NEW.
     * 
     * @param idDemande
     *            id demande d'affectation
     * @param codeDecisionManuelle
     *            code de la décision manuelle
     * @param msgAvertissement
     *            Fournir une liste vide (mais pas <code>null</code>)
     *            que la méthode pourra remplir avec des messages
     *            d'avertissement.
     * @throws ApplicationException
     *             Renvoie <code>DejaAffecteException</code> si
     *             l'élève est déjà affecté manuellement sur une
     *             autre demande.
     */
    void saisieDecisionManuelleNouvelleTransaction(Long idDemande, String codeDecisionManuelle,
            List<Message> msgAvertissement) throws ApplicationException;

    /**
     * Réinitialise l'affectation pour un département (uniquement les demandes PUBDEP,
     * pas les demandes 6SEGPA, ni RETIRE).
     * 
     * @param codeDeptMen
     *            code du département MEN concerné
     * @throws ApplicationException
     *             en cas de problème
     */
    void reinitialiseAffectation(String codeDeptMen) throws ApplicationException;

    /**
     * Préaffectation automatique.
     * 
     * @param progMon
     *            Moniteur de progresssion
     * @param resultats
     *            Table de stockage des résultats pour construire le rapport
     * @param codeDeptMen
     *            code du département MEN concerné
     * @throws ApplicationException
     *             en cas de problème
     */
    void affectationAuto(BatchProgressMonitor progMon, Map<String, Object> resultats, String codeDeptMen)
            throws ApplicationException;

    /**
     * Compose le compte rendu de la pré-affectation automatique.
     * 
     * @param resultats
     *            table des résultats
     * @return compte rendu
     */
    String composeCompteRenduAffAuto(Map<String, Object> resultats);

    /**
     * Contrôle que tous les élèves aient des demandes dans un état final.
     * 
     * @param progMon
     *            Moniteur de progresssion
     * @param codeDeptMen
     *            code du département MEN concerné
     * @return vrai si le contrôle est correct
     * @throws ApplicationException
     *             en cas de problème
     */
    boolean controleDemandesToutesFinales(BatchProgressMonitor progMon, String codeDeptMen)
            throws ApplicationException;

    /**
     * Valide l'affectation.
     * 
     * @param progMon
     *            Moniteur de progresssion
     * @param resultats
     *            Table de stockage des résultats pour construire le rapport
     * @param codeDeptMen
     *            code du département MEN concerné
     * @throws ApplicationException
     *             en cas de problème
     */
    void validationAffectation(BatchProgressMonitor progMon, Map<String, Object> resultats, String codeDeptMen)
            throws ApplicationException;

    /**
     * Compose le compte rendu de la validation de l'affectation.
     * 
     * @param resultats
     *            table des résultats
     * @return compte rendu
     */
    String composeCompteRenduValidationAff(Map<String, Object> resultats);

}
