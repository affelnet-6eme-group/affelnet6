/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.service.impl.dossier;

/**
 * États possibles lors de la création d'une demande d'affectation.
 */
public enum EtatDemandeAffectation {

    /**
     * La demande d'affectation a été créée.
     */
    CREEE,

    /**
     * La demande d'affectation est impossible car l'offre de formation 6ème est absente dans le collège de secteur
     * désigné.
     */
    IMPOSSIBLE_OFFRE_FORMATION_6EME_ABSENTE_COLLEGE_DESIGNE,

    /**
     * La demande d'affectation est impossible car l'offre de formation 6ème est absente dans tous les collèges de
     * secteur.
     */
    IMPOSSIBLE_OFFRE_FORMATION_6EME_ABSENTE_TOUS_LES_COLLEGES,

    /**
     * La demande d'affectation est impossible car la décision de passage de l'élève est maintenu.
     */
    IMPOSSIBLE_DECISION_PASSAGE_MAINTENU,

    /**
     * La demande d'affectation /*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

est impossible car il y a eu une erreur technique.
     */
    IMPOSSIBLE_ERREUR_TECHNIQUE,

    /**
     * Il n y a eu aucun traitement.
     */
    RIEN
}
