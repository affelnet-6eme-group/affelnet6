/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.service.impl.dossier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.edu.nancy.affelnet6.dao.interfaces.dossier.PropositionAdresseDao;
import fr.edu.nancy.affelnet6.domain.dossier.PropositionAdresse;
import fr.edu.nancy.affelnet6.service.interfaces.dossier.PropositionAdresseManager;
import fr.edu.nancy.commun.exceptions.DaoException;

/**
 * Gestionnaire de PropositionAdresse.
 */
@Service("propositionAdresseManager")
public class PropositionAdresseManagerImpl implements PropositionAdresseManager {

    /** DAO Adresse. */
    @Autowired
    private PropositionAdresseDao propositionAdresseDao;

    @Override
    public int supprimerPourRemplacerDepartement(int dptMenId) throws DaoException {
        return propositionAdresseDao.supprimerPourRemplacerDepartement(dptMenId);
    }

    @Override
    public PropositionAdresse charger(Long id) throws DaoException {
        return propositionAdresseDao.charger(id);
    }

    @Override
    public Long creer(PropositionAdresse propositionAdresse) throws DaoException {
        return propositionAdresseDao.creer(propositionAdresse);
    }

    @Override
    public int supprimerPourAdresse(Long idAdresse) throws DaoException {
        return propositionAdresseDao.supprimerPourAdresse(idAdresse);
    }
}
