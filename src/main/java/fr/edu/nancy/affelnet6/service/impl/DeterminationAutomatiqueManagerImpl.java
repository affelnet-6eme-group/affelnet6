/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.edu.nancy.affelnet6.domain.calcul.DonneesCalculCollegeSecteur;
import fr.edu.nancy.affelnet6.domain.dossier.Adresse;
import fr.edu.nancy.affelnet6.domain.dossier.Eleve;
import fr.edu.nancy.affelnet6.domain.nomenclature.CommuneInseePtt;
import fr.edu.nancy.affelnet6.domain.nomenclature.DeptMen;
import fr.edu.nancy.affelnet6.domain.nomenclature.EtablissementSconet;
import fr.edu.nancy.affelnet6.domain.nomenclature.EtatCollegeSecteur;
import fr.edu.nancy.affelnet6.domain.nomenclature.TronconCarteScolaire;
import fr.edu.nancy.affelnet6.helper.AccesHelper;
import fr.edu.nancy.affelnet6.helper.EleveHelper;
import fr.edu.nancy.affelnet6.helper.ParametreHelper;
import fr.edu.nancy.affelnet6.service.Af6ParametreApplicationManager;
import fr.edu.nancy.affelnet6.service.PreControleDetermination;
import fr.edu.nancy.affelnet6.service.dossier.DemandeAffectationManager;
import fr.edu.nancy.affelnet6.service.dossier.EleveManager;
import fr.edu.nancy.affelnet6.service.interfaces.AppAutomateManager;
import fr.edu.nancy.affelnet6.service.interfaces.DeterminationAutomatiqueManager;
import fr.edu.nancy.affelnet6.service.interfaces.nomenclature.StatutAdresseManager;
import fr.edu.nancy.affelnet6.service.interfaces.nomenclature.TronconCarteScolaireManager;
import fr.edu.nancy.affelnet6.service.nomenclature.CauseCalculCollegeSecteurManager;
import fr.edu.nancy.affelnet6.service.nomenclature.DeptMenManager;
import fr.edu.nancy.commun.domain.Role;
import fr.edu.nancy.commun.exceptions.ApplicationException;
import fr.edu.nancy.commun.exceptions.BusinessException;
import fr.edu.nancy.commun.exceptions.DaoException;
import fr.edu.nancy.commun.exceptions.TechnicalException;
import fr.edu.nancy.commun.utils.BatchProgressMonitor;

/** Gestionnaire pour la détermination automatique du ou des collège(s) de secteur. */
@Service("determinationAutomatiqueManager")
public class DeterminationAutomatiqueManagerImpl implements DeterminationAutomatiqueManager {

    /** L'URL du résultat de la simulation de la détermination automatique du collège de secteur. */
    private static final String URL_RESULTAT_SIMU_DETERMINATION = "/affelnet6/action/dossier/resultatSimulationDetermination?action=Chercher&codeEtatSimulation=";

    /**
     * Libellé pour le compte rendu des élèves pour lesquels le calcul des collèges de secteur n'a pas pu être
     * lancé.
     */
    private static final String NB_ELEVE_CALCUL_IMPOSSIBLE = "Nombre d'élève(s) dont le(s) collège(s) de secteur n'ont pas pu être déterminé(s) automatiquement car soit l'adresse est vide, n'a pas été traitée ou est non contrôlée, soit la commune n'est pas renseignée : ";

    /** Libellé pour le compte rendu des élèves pour qui les collèges ont été déterminés automatiquement. */
    private static final String NB_ELEVE_CALCUL_ABOUTI = "Nombre d'élève(s) dont le(s) collège(s) ont été déterminé(s) automatiquement : ";

    /**
     * Libellé pour le compte rendu des élèves pour qui les collèges n'ont pas pu être déterminés automatiquement.
     */
    private static final String NB_ELEVE_CALCUL_NON_ABOUTI = "Nombre d'élève(s) dont le(s) collège(s) de secteur n'ont pas pu être déterminé(s) automatiquement : ";

    /** Libellé pour le compte rendu des élèves pour qui les collèges ont été déterminés hors du département. */
    private static final String NB_ELEVE_CALCUL_ABOUTI_HORS_DEPARTEMENT = "Nombre d'élève(s) dont le(s) collège(s) ont été déterminé(s) hors du département : ";

    /** Libellé pour le compte rendu du nombre de communes manquantes dans la carte scolaire. */
    private static final String NB_COMMUNES_MANQUANTES_CARTE_SCOLAIRE = "Nombre de communes manquantes : ";

    /** Libellé pour le compte rendu du nombre de voies manquantes dans la carte scolaire. */
    private static final String NB_VOIES_MANQUANTES_CARTE_SCOLAIRE = "Nombre de voies manquantes : ";

    /** Libellé pour le compte rendu du nombre de numéros de voie manquants dans la carte scolaire. */
    private static final String NB_NUMEROS_VOIE_MANQUANTS_CARTE_SCOLAIRE = "Nombre de numéros de voie manquants : ";

    /** Libellé pour le compte rendu du nombre de chevauchement de voie dans la carte scolaire. */
    private static final String NB_CHEVAUCHEMENT_VOIES_CARTE_SCOLAIRE = "Nombre de chevauchements de voies : ";

    /** Clé pour la map des résultats à utiliser pour stocker un warning. */
    private static final String KEY_WARNING = "KEY_WARNING";
    /** Logger de la classe. */
    private static final Logger LOG = LoggerFactory.getLogger(DeterminationAutomatiqueManager.class);

    /** Taille des paquets d'élèves qui sont commités dans la méthode determinationAuto. */
    private static final int TAILLE_PAQUET_ELEVES = 100;

    /**
     * Gestionnaire de la détermination de(s) collège(s) automatique (cette classe),. permet de placer les
     * transactions (en passant par un proxy)
     */
    @Resource(name = "determinationAutomatiqueManager")
    private DeterminationAutomatiqueManager determinationAutomatiqueManager;

    /** Gestionnaire de département. */
    @Autowired
    private DeptMenManager deptMenManager;

    /** Gestionnaire pour la carte scolaire. */
    @Autowired
    private TronconCarteScolaireManager tronconCarteScolaireManager;

    /** Gestionnaire pour les élèves. */
    @Autowired
    private EleveManager eleveManager;

    /** Gestionnaire demande d'affectation. */
    @Autowired
    private DemandeAffectationManager demandeAffectationManager;

    /** Gestionnaire de l'état applicatif. */
    @Autowired
    private AppAutomateManager appAutomateManager;

    /** Gestionnaire des paramètres applicatifs. */
    @Autowired
    private Af6ParametreApplicationManager parametreService;

    /** Gestionnaire pour les déterminations automatiques non abouties.. */
    @Autowired
    private CauseCalculCollegeSecteurManager causeCalCulCollegeSecteurManager;

    @Override
    public boolean verifierDeterminationAutomatiquePossible(String codeDeptMen, boolean simulation)
            throws ApplicationException, TechnicalException {

        return
        // La carte scolaire doit avoir été importée
        tronconCarteScolaireManager.isCarteScolaireDejaImporteePourDepartement(codeDeptMen) &&
        // il s'agit d'une simulation de la détermination
                (simulation || (
                // Le département n'est pas à l'état résultat
                !appAutomateManager.isEtatFonctionnelDepartement(codeDeptMen,
                        AppAutomateManager.ETAT_DEPT_RESULTATS, true) &&
                // Le département a choisi le mode automatique
                        ParametreHelper.isModeCalculClgSecteurAutomatique(codeDeptMen)));
    }

    @Override
    public void determinationAuto(BatchProgressMonitor progMon, Map<String, Object> resultats, String codeDeptMen,
            String codeRoleUtilisateur, boolean simulation)
            throws ApplicationException, TechnicalException, BusinessException {

        LOG.info("Lancement du traitement de détermination automatique du collège de secteur");

        // on calcule les tronçon de la carte scolaire à secteur unique qu'une seule fois.
        LOG.info("Chargement des tronçons de la carte scolaire en secteur unique du département");
        Map<Integer, TronconCarteScolaire> tronconCarteScolaireSecteurUnique = chargeTronconSecteurUnique(
                codeDeptMen);

        Map<CommuneInseePtt, Map<String, List<TronconCarteScolaire>>> listTronconsCarteScolaireSecteurNonUnique = new HashMap<>();

        if (simulation) {
            // on charge les tronçons secteur non unique qu'une seule fois
            // cette liste est utile pour l'aide à la fiabilisation de la carte scolaire (uniquement en simulation)
            LOG.info("Chargement des tronçons de la carte scolaire en secteur non unique du département");
            listTronconsCarteScolaireSecteurNonUnique = causeCalCulCollegeSecteurManager
                    .chargeTronconSecteurNonUnique(codeDeptMen);
        }

        if (simulation && appAutomateManager.isEtatFonctionnelDepartement(codeDeptMen,
                AppAutomateManager.ETAT_DEPT_PARAMERTAGE, true)) {
            resultats.put(KEY_WARNING,
                    "Simulation effectuée sur un nombre réduit d'élèves (les candidatures individuelles) car l'import BE1D n'a pas été lancé.");
        }

        // on compte le nombre des élèves du département sans collège de secteur potentiel
        // les élèves dont l'adresse est au statut A TRAITER ou NON CONTROLEE ne sont pas pris en compte sauf pour
        // les communes à secteur unique

        int nbElevesSansCollege = eleveManager.compterElevesPourDetermination(codeDeptMen, !simulation);
        int nbElevesSansCollegeOrigine = nbElevesSansCollege;

        // Initialisation des compteurs pour le compte rendu
        resultats.put(KEY_CPT_ELEVE, nbElevesSansCollege);
        resultats.put(KEY_CPT_ELEVE_NON_ABOUTI, 0);
        resultats.put(KEY_CPT_ELEVE_CLG_AUTO, 0);
        resultats.put(KEY_CPT_ELEVE_DETERMINER_MANUEL, 0);
        resultats.put(KEY_CPT_ELEVE_ADR_INCORRECTE, 0);
        resultats.put(KEY_CPT_ELEVE_ABOUTI_HORS_DEPARTEMENT, 0);
        int numDossier = 0;

        // chaque itération correspond à 100 élèves
        int nbIteration = 0;
        int pourMille = 0;
        while (nbElevesSansCollege > 0) {
            progMon.setProgression((nbElevesSansCollegeOrigine - nbElevesSansCollege), nbElevesSansCollegeOrigine);
            String message;
            // pour plus de lisibilité on affiche l'avancement du traitement tous les milles élèves.
            if ((nbIteration >= 10 && nbIteration % 10 == 0) || nbIteration == 0) {

                int nbPlusMille = (nbElevesSansCollege > TAILLE_PAQUET_ELEVES * 10)
                        ? (TAILLE_PAQUET_ELEVES * 10 - 100) + TAILLE_PAQUET_ELEVES
                        : nbElevesSansCollege;

                message = "Calcul des collèges de secteur pour les élèves "
                        + (nbElevesSansCollegeOrigine - nbElevesSansCollege) + " à "
                        + (nbElevesSansCollegeOrigine - nbElevesSansCollege + nbPlusMille);
                LOG.info(message);
                progMon.beginTask(message);

            }
            // // calcul par paquet de 100 (q.setMaxResults(100) dans la requête HQL)pour gérer les transactions et
            // la tailles de la session
            // hibernate
            numDossier = determinationAutomatiqueManager.elevesCalculCollegeSecteur(progMon, resultats,
                    codeDeptMen, codeRoleUtilisateur, numDossier, tronconCarteScolaireSecteurUnique,
                    listTronconsCarteScolaireSecteurNonUnique, simulation);

            nbElevesSansCollege -= TAILLE_PAQUET_ELEVES;
            nbIteration += 1;

            pourMille += TAILLE_PAQUET_ELEVES;

            // on affiche le temps du traitement tous les milles
            if (pourMille % 1000 == 0 || (nbElevesSansCollege <= 0)) {
                progMon.endTask();
                pourMille = 0;
            }

        }

        if (simulation) {
            LOG.info("Mise à jour des compteurs pour les causes du calcul de collège de secteur non abouti");
            causeCalCulCollegeSecteurManager.updateCompteursResultats(codeDeptMen, resultats);
        }

        LOG.info("Fin du traitement de détermination automatique du collège de secteur");
    }

    @Override
    public int elevesCalculCollegeSecteur(BatchProgressMonitor progMon, Map<String, Object> resultats,
            String codeDeptMen, String codeRoleUtilisateur, int numDossier,
            Map<Integer, TronconCarteScolaire> tronconCarteScolaireSecteurUnique,
            Map<CommuneInseePtt, Map<String, List<TronconCarteScolaire>>> listTronconsCarteScolaireSecteurNonUnique,
            boolean simulation) throws DaoException, ApplicationException {

        LOG.info(
                "Recherche les 100 prochains élèves sans collège de secteur potentiel à partir du numéro de dossier "
                        + numDossier);
        List<Eleve> elevesSansCollege = eleveManager.listerElevesPourDetermination(codeDeptMen, numDossier,
                !simulation);

        int nbEleveSansCollege = CollectionUtils.size(elevesSansCollege);

        LOG.info("Traitement des élèves sans collège de secteur : " + nbEleveSansCollege);

        // Si la liste des élèves sans collège de secteur est vide
        // on retourne le numéro de dossier courant.
        if (nbEleveSansCollege <= 0) {
            return numDossier;
        }

        // /!\ Attention on n'appelle pas la méthode
        // calculCollegeSecteur() de manière interne
        // (exemple : calculCollegeSecteur()) mais à travers le proxy de Spring
        // (exemple : determinationAutomatiqueManager.calculCollegeSecteur())
        // afin de bénéficier des transactions. On peut faire
        // cela car le proxy n'a pas d'état (pas d'attribut).
        // Si on faisait un appel interne, il n'y aurait pas
        // de point de coupe et donc pas de transaction sur cette méthode !
        return determinationAutomatiqueManager.calculCollegeSecteur(resultats, elevesSansCollege, codeDeptMen,
                codeRoleUtilisateur, tronconCarteScolaireSecteurUnique, listTronconsCarteScolaireSecteurNonUnique,
                simulation);
    }

    @Override
    public int calculCollegeSecteur(Map<String, Object> resultats, List<Eleve> elevesSansCollege,
            String codeDeptMen, String codeRoleUtilisateur,
            Map<Integer, TronconCarteScolaire> tronconCarteScolaireSecteurUnique,
            Map<CommuneInseePtt, Map<String, List<TronconCarteScolaire>>> listTronconsCarteScolaireSecteurNonUnique,
            boolean simulation) throws ApplicationException {

        int nbElevesSansCollege = CollectionUtils.size(elevesSansCollege);

        // On récupère le dernier numéro de dossier traité.
        // Si la liste des élèves est vide on retourne un numéro de dossier max
        // pour éviter de tomber dans un cas de boucle.
        int numDernierDossier = Integer.MAX_VALUE;
        if (nbElevesSansCollege > 0) {
            numDernierDossier = elevesSansCollege.get(nbElevesSansCollege - 1).getNumDossier().intValue();
        }

        // parcours des élèves
        for (Eleve eleve : elevesSansCollege) {
            // calcul et mise à jour des collèges de secteur de l'élève
            DonneesCalculCollegeSecteur donneesCalcul = new DonneesCalculCollegeSecteur(eleve, null, codeDeptMen,
                    tronconCarteScolaireSecteurUnique);
            donneesCalcul.setDeclenchementManuel(true);
            donneesCalcul.setSimulation(simulation);
            if (codeRoleUtilisateur != null) {
                donneesCalcul.setRoleUtilisateur(new Role(codeRoleUtilisateur));
            }
            determinationAutomatiqueManager.calculCollegeSecteur(donneesCalcul);

            if (simulation) {
                incrementCompteurs(resultats, donneesCalcul);
                eleveManager.updateCollegeSecteurSimule(donneesCalcul);

                // détermination de la cause du calcul de collège de secteur non abouti
                causeCalCulCollegeSecteurManager.determinerCauseNonAbouti(resultats, donneesCalcul,
                        listTronconsCarteScolaireSecteurNonUnique);
            } else {
                incrementCompteurs(resultats, donneesCalcul);
                eleveManager.updateCollegeSecteur(donneesCalcul);
                // création des voeux de secteur ou demande hors département
                demandeAffectationManager.genererDemandesAutomatique(eleve, codeDeptMen);
            }
        }

        return numDernierDossier;
    }

    /**
     * Incrémente les compteurs du batch en fonction du résultat de la détermination.
     * 
     * @param resultats
     *            la map des compteurs
     * @param donneesCalcul
     *            les données de la détermination
     */
    private void incrementCompteurs(Map<String, Object> resultats, DonneesCalculCollegeSecteur donneesCalcul) {

        EtatCollegeSecteur etatCollegeSecteur = donneesCalcul.getEtatCalcul();
        String identiteEleve = EleveHelper.getIdentite(donneesCalcul.getEleve());

        if (DeterminationAutomatiqueManager.CALCUL_AUTO_ABOUTI.equals(etatCollegeSecteur)) {
            LOG.info("Le calcul du collège de secteur a abouti pour l'élève " + identiteEleve);
            incrementeCompteur(resultats, KEY_CPT_ELEVE_CLG_AUTO);
        } else if (DeterminationAutomatiqueManager.CALCUL_ABOUTI_HORS_DEPARTEMENT.equals(etatCollegeSecteur)) {
            LOG.info("Le calcul du collège de secteur a abouti hors du département pour l'élève " + identiteEleve);
            incrementeCompteur(resultats, KEY_CPT_ELEVE_ABOUTI_HORS_DEPARTEMENT);
        } else if (DeterminationAutomatiqueManager.CALCUL_AUTO_NON_ABOUTI.equals(etatCollegeSecteur)) {
            LOG.info("Le calcul du collège de secteur n'a pas abouti pour l'élève " + identiteEleve);
            incrementeCompteur(resultats, KEY_CPT_ELEVE_NON_ABOUTI);
        } else if (DeterminationAutomatiqueManager.SAISIE_MANUELLE.equals(etatCollegeSecteur)) {
            LOG.info("Le collège de secteur a été saisi manuellement pour l'élève " + identiteEleve);
            incrementeCompteur(resultats, KEY_CPT_ELEVE_DETERMINER_MANUEL);
        } else {
            LOG.info("L'adresse de l'élève n'a pas permis de calculer le collège de secteur pour l'élève "
                    + identiteEleve);
            this.incrementeCompteur(resultats, KEY_CPT_ELEVE_ADR_INCORRECTE);
        }
    }

    @Override
    public void calculCollegeSecteur(DonneesCalculCollegeSecteur donneesCalcul) throws ApplicationException {

        Eleve eleve = donneesCalcul.getEleve();
        String identiteEleve = EleveHelper.getIdentite(eleve);

        LOG.debug("Calcul du collège de secteur pour l'élève : " + identiteEleve);

        switch (preControleDetermination(donneesCalcul, eleve, identiteEleve)) {
            case DETERMINATION_A_EFFECTUER:
                break;
            case SAUVEGARDE_COLLEGES_SAISIS:
                return;
            default:
                donneesCalcul.setEtatCalcul(CALCUL_AVORTE);
                return;
        }

        Adresse adresseEleve = donneesCalcul.getEleve().getAdresseResidence();

        // on écrase les éventuelles saisies manuelles pour le calcul
        donneesCalcul.setCollegesSecteur(null);
        donneesCalcul.setCodeRneCollegeChoisiParIA(null);
        donneesCalcul.setCollegeDansDepartement(true);

        // adresse de l'élève à l'étranger --> on met à jour les collèges de secteur à null et hors
        // département
        if (!adresseEleve.getAdresseEnFrance()) {
            LOG.debug("Impossible de déterminer le collège de secteur d'une adresse étrangère pour l'élève "
                    + identiteEleve);

            donneesCalcul.setCollegeDansDepartement(false);
            donneesCalcul.setEtatCalcul(CALCUL_ABOUTI_HORS_DEPARTEMENT);
            return;
        }

        if (adresseEleve.getVide() || adresseEleve.getCommuneInsee() == null) {
            LOG.debug(
                    "Impossible de déterminer le collège de secteur d'une adresse vide ou sans commune pour l'élève "
                            + identiteEleve);
            donneesCalcul.setEtatCalcul(CALCUL_IMPOSSIBLE);
            return;
        }

        // quand le calcul est appelé autrement que par le batch, la map n'est pas initialisée
        if (MapUtils.isEmpty(donneesCalcul.getTronconCarteScolaireSecteurUnique())) {
            donneesCalcul.setTronconCarteScolaireSecteurUnique(
                    chargeTronconSecteurUnique(donneesCalcul.getCodeDeptMen()));
        }

        // adresse non vide ET commune renseignée
        int idCommuneInsee = adresseEleve.getCommuneInsee().getCommuneInseeId();
        TronconCarteScolaire tronconAdresseEleve = donneesCalcul.getTronconCarteScolaireSecteurUnique()
                .get(idCommuneInsee);
        // rechercher si la commune de l'élève est dans un secteur unique de la carte scolaire
        if (tronconAdresseEleve != null) {
            // l'élève vit dans une commune en secteur unique dans la carte scolaire
            LOG.debug("La commune de  l'élève " + identiteEleve + " est un secteur unique de la carte scolaire");
            // sinon on vérifie le code état du statut de l'adresse de l'élève
        } else if (StatutAdresseManager.ADR_VALIDEE.equals(adresseEleve.getStatutAdresse().getCodeEtat())) {

            // si adresse validée on cherche une correspondance entre l'adresse de résidence à la rentrée de
            // l'élève et la carte scolaire
            if (adresseEleve.getNumeroVoie() == null) {
                LOG.debug("Recherche du tronçon de la carte scolaire correspondant à l'adresse "
                        + "sans numéro de voie de l'élève " + identiteEleve);
                tronconAdresseEleve = tronconCarteScolaireManager
                        .chercherTronconPourEleveSansNumeroVoie(adresseEleve, donneesCalcul.getCodeDeptMen());
            } else {
                LOG.debug("Recherche du tronçon de la carte scolaire correspondant à l'adresse de l'élève "
                        + identiteEleve);
                tronconAdresseEleve = tronconCarteScolaireManager.chercherTronconPourEleve(adresseEleve,
                        donneesCalcul.getCodeDeptMen());
            }
            // si non validé, on ne peut pas déterminer le(s) collège(s)
        } else {
            LOG.debug("Impossible de déterminer le collège de secteur d'une adresse à traiter pour l'élève "
                    + identiteEleve);

            traiterAdresseEleveHorsDepartement(donneesCalcul, eleve);

            if (donneesCalcul.getEstCollegeDansDepartement()) {
                donneesCalcul.setEtatCalcul(CALCUL_IMPOSSIBLE);
            } else {
                donneesCalcul.setEtatCalcul(CALCUL_ABOUTI_HORS_DEPARTEMENT);
            }

            return;
        }

        // si le tronçon est renseignée, le calcul a abouti
        if (tronconAdresseEleve != null) {
            LOG.debug("Une correspondance a été trouvée dans la carte scolaire pour l'adresse de l'élève "
                    + identiteEleve);

            donneesCalcul.setCollegesSecteur(tronconAdresseEleve.getColleges());
            boolean nouveauxCollegesDansDepartement = true;
            DeptMen departement = deptMenManager.chargerParDepartement(donneesCalcul.getCodeDeptMen());

            for (EtablissementSconet college : tronconAdresseEleve.getColleges()) {
                nouveauxCollegesDansDepartement &= college.getCommuneInsee().getDept().getDeptInseeId()
                        .equals(departement.getDeptMenId());
            }
            donneesCalcul.setCollegeDansDepartement(nouveauxCollegesDansDepartement);
            donneesCalcul.setEtatCalcul(CALCUL_AUTO_ABOUTI);
        } else {
            LOG.debug("Aucune correspondance trouvée dans la carte scolaire pour l'élève " + identiteEleve);

            traiterAdresseEleveHorsDepartement(donneesCalcul, eleve);

            if (donneesCalcul.getEstCollegeDansDepartement()) {
                // si le calcul n'a pas abouti
                donneesCalcul.setEtatCalcul(CALCUL_AUTO_NON_ABOUTI);
            } else {
                donneesCalcul.setEtatCalcul(CALCUL_ABOUTI_HORS_DEPARTEMENT);
            }
        }
    }

    /**
     * Traite le cas où l'adresse de l'élève est hors du département.
     * 
     * @param donneesCalcul
     *            Les données pour le positionnement du/des collèges de secteur
     * @param eleve
     *            L'élève
     */
    private void traiterAdresseEleveHorsDepartement(DonneesCalculCollegeSecteur donneesCalcul, Eleve eleve) {

        // si l'adresse de l'élève est hors département
        // alors le collège est positionné sur hors département
        if (!eleveManager.estAdresseDansDepartement(eleve)) {
            donneesCalcul.setCollegeDansDepartement(false);
        }
    }

    /**
     * Détermine le type de détermination.
     * 
     * @param donneesCalcul
     *            les données pour le calcul
     * @param eleve
     *            l'élève
     * @param identiteEleve
     *            l'indentité de l'élève
     * @return le type de détermination
     * @throws BusinessException
     *             en cas d'erreur
     */
    protected PreControleDetermination preControleDetermination(DonneesCalculCollegeSecteur donneesCalcul,
            Eleve eleve, String identiteEleve) throws BusinessException {

        // en cas de simulation la détermination est effectuée (que se soit en mode manuel ou automatique)
        if (donneesCalcul.isSimulation()) {
            return PreControleDetermination.DETERMINATION_A_EFFECTUER;
        }

        // en paramétrage "mode manuel" la détermination automatique n'est pas effectué
        // seule la sauvegarde des données est réalisée
        if (!parametreService.isModeCalculClgSecteurAutomatique(donneesCalcul.getCodeDeptMen())) {
            if (donneesCalcul.isActionAvecSaisieCollege() && collegeSecteurModifie(donneesCalcul, eleve, false)) {
                return PreControleDetermination.SAUVEGARDE_COLLEGES_SAISIS;
            } else {
                return PreControleDetermination.RIEN;
            }
        }

        // En profil Directeur, la saisie des collèges de secteur n'est possible que si l'édition du volet 1 a
        // été effectuée
        if (eleve.getEditionVolet1() == null && donneesCalcul.getProfilUtilisateur() != null
                && AccesHelper.isDirec(donneesCalcul.getProfilUtilisateur())) {
            LOG.debug("Impossible de déterminer le collège de secteur avant l'édition du volet 1 de l'élève "
                    + identiteEleve);
            return PreControleDetermination.RIEN;
        }

        if (collegeSecteurModifie(donneesCalcul, eleve, true)) {
            return PreControleDetermination.SAUVEGARDE_COLLEGES_SAISIS;
        }

        EtatCollegeSecteur etatCollegeSecteurEleve = eleve.getEtatCollegeSecteur();

        // si déclenchement manuel
        if (donneesCalcul.isDeclenchementManuel()) {

            // si état collège secteur a été saisi manuellement ou a abouti lors d'un calcul
            // ou a été déterminé hors département alors le calcul ne doit pas être effectué
            if (SAISIE_MANUELLE.equals(etatCollegeSecteurEleve)
                    || CALCUL_AUTO_ABOUTI.equals(etatCollegeSecteurEleve)
                    || CALCUL_ABOUTI_HORS_DEPARTEMENT.equals(etatCollegeSecteurEleve)) {
                return PreControleDetermination.RIEN;
            }

            // sinon si l'adresse n'a pas été modifiée
            // ou qu'il n y a jamais eu de calcul ou de saisie manuelle
            // le calcul ne doit pas être effectué
        } else if (!donneesCalcul.isAdresseEleveModifiee() || etatCollegeSecteurEleve == null) {
            return PreControleDetermination.RIEN;
        }

        return PreControleDetermination.DETERMINATION_A_EFFECTUER;
    }

    /**
     * Détermine si le collège de secteur est modifié.
     * 
     * @param donneesCalcul
     *            les données pour le calcul
     * @param eleve
     *            l'élève
     * @param verifierProfilUtilisateur
     *            indique s'il faut vérifier le profil de l'utilisateur
     * @return vrai si le collège de secteur est modifié
     * @throws BusinessException
     *             en cas d'erreur
     */
    protected boolean collegeSecteurModifie(DonneesCalculCollegeSecteur donneesCalcul, Eleve eleve,
            boolean verifierProfilUtilisateur) throws BusinessException {

        // si l'action ne permet pas de modifier le collège de secteur
        if (!donneesCalcul.isActionAvecSaisieCollege()) {
            return false;
        }

        // seul l'IA-DASEN peut modifier manuellement le ou les collège(s) de secteur
        if (verifierProfilUtilisateur && donneesCalcul.getProfilUtilisateur() != null
                && !AccesHelper.isDsden(donneesCalcul.getProfilUtilisateur())) {
            return false;
        }

        String collegeSecteurIA = null;
        if (eleve.getCollegeSecteur() != null) {
            collegeSecteurIA = eleve.getCollegeSecteur().getCodeRne();
        }

        String collegeSecteurIAsaisi = null;
        if (StringUtils.isNotEmpty(donneesCalcul.getCodeRneCollegeChoisiParIA())) {
            collegeSecteurIAsaisi = donneesCalcul.getCodeRneCollegeChoisiParIA();
        }

        // Si une saisie manuelle a été effectuée par le DSDEN, il ne faut rien modifier (les saisies manuelles
        // sont prioritaires dans l'application)
        if (EleveHelper.estCollegesSecteurModifies(eleve,
                donneesCalcul.getCodeCollegesSecteur().toArray(new String[] {}))
                // Flag collège dans département modifié
                || (donneesCalcul.getEstCollegeDansDepartement() != null && !donneesCalcul
                        .getEstCollegeDansDepartement().equals(eleve.getEstCollegeSecteurDansDepartement()))
                // ou collège désigné par l'IA modifié
                || (collegeSecteurIAsaisi != null
                        && !StringUtils.equals(collegeSecteurIA, collegeSecteurIAsaisi))) {

            return true;
        }

        return false;
    }

    @Override
    public String composeCompteRendu(Map<String, Object> resultats, boolean simulation) {
        if (simulation) {
            return composeCompteRenduSimulation(resultats);
        } else {
            return composeCompteRendu(resultats);
        }
    }

    /**
     * Construit le compte rendu de résultat pour une détermination des collèges de secteur.
     * 
     * @param resultats
     *            la map des résultats
     * @return le résultat
     */
    private String composeCompteRendu(Map<String, Object> resultats) {
        StringBuffer cr = new StringBuffer();

        int nbElevesTraites = (Integer) resultats.get(KEY_CPT_ELEVE);
        int nbElevesDetermines = (Integer) resultats.get(KEY_CPT_ELEVE_CLG_AUTO);
        int nbElevesNonDetermines = (Integer) resultats.get(KEY_CPT_ELEVE_NON_ABOUTI);
        int nbElevesManuel = (Integer) resultats.get(KEY_CPT_ELEVE_DETERMINER_MANUEL);
        int nbElevesAdrInconrrecte = (Integer) resultats.get(KEY_CPT_ELEVE_ADR_INCORRECTE);
        int nbElevesAboutiHorsDep = (Integer) resultats.get(KEY_CPT_ELEVE_ABOUTI_HORS_DEPARTEMENT);

        cr.append("<h3>").append("Calcul automatique des collèges de secteur pour les élèves du département")
                .append("</h3>");

        cr.append("Nombre d'élève(s) concerné(s) : ").append(nbElevesTraites);
        cr.append("<br/>");

        if (nbElevesDetermines > 0) {
            lienCompteRendu(cr,
                    "/affelnet6/action/dossier/listeEleveCollegeSecteur?action=Chercher&etatCollegeSecteur="
                            + EtatCollegeSecteur.CALCUL_ABOUTI,
                    NB_ELEVE_CALCUL_ABOUTI, nbElevesDetermines);
        } else {
            cr.append(NB_ELEVE_CALCUL_ABOUTI).append(nbElevesDetermines);
        }

        if (nbElevesAboutiHorsDep > 0) {
            cr.append("<br/>");
            lienCompteRendu(cr,
                    "/affelnet6/action/dossier/listeEleveCollegeSecteur?action=Chercher&etatCollegeSecteur="
                            + EtatCollegeSecteur.CALCUL_ABOUTI_HORS_DEPARTEMENT,
                    NB_ELEVE_CALCUL_ABOUTI_HORS_DEPARTEMENT, nbElevesAboutiHorsDep);
        }

        if (nbElevesNonDetermines > 0) {
            cr.append("<br/>");
            lienCompteRendu(cr,
                    "/affelnet6/action/dossier/listeElevesSansCollege?action=Chercher&etatCollegeSecteur="
                            + EtatCollegeSecteur.CALCUL_NON_ABOUTI,
                    NB_ELEVE_CALCUL_NON_ABOUTI, nbElevesNonDetermines);
        }

        if (nbElevesManuel > 0) {
            cr.append("<br/>");
            cr.append("Nombre d'élève(s) dont le(s) collège(s) de secteur ont été déterminé(s) manuellement : ")
                    .append(nbElevesManuel);
        }

        if (nbElevesAdrInconrrecte > 0) {
            cr.append("<br/>");
            lienCompteRendu(cr,
                    "/affelnet6/action/dossier/listeElevesSansCollege?action=Chercher&etatCollegeSecteur="
                            + EtatCollegeSecteur.CALCUL_IMPOSSIBLE,
                    NB_ELEVE_CALCUL_IMPOSSIBLE, nbElevesAdrInconrrecte);
        }

        return cr.toString();
    }

    /**
     * Construit le compte rendu de résultat pour une simulation de détermination des collèges de secteur.
     * 
     * @param resultats
     *            la map des résultats
     * @return le résultat
     */
    private String composeCompteRenduSimulation(Map<String, Object> resultats) {
        StringBuffer cr = new StringBuffer();

        int nbElevesTraites = (Integer) resultats.get(KEY_CPT_ELEVE);
        int nbElevesDetermines = (Integer) resultats.get(KEY_CPT_ELEVE_CLG_AUTO);
        int nbElevesNonDetermines = (Integer) resultats.get(KEY_CPT_ELEVE_NON_ABOUTI);
        int nbElevesAdrInconrrecte = (Integer) resultats.get(KEY_CPT_ELEVE_ADR_INCORRECTE);
        int nbElevesAboutiHorsDep = (Integer) resultats.get(KEY_CPT_ELEVE_ABOUTI_HORS_DEPARTEMENT);

        int nbCommunesManquantesCarteScolaire = (Integer) resultats
                .get(KEY_CPT_COMMUNES_MANQUANTES_CARTE_SCOLAIRE);
        int nbVoiesManquantesCarteScolaire = (Integer) resultats.get(KEY_CPT_VOIES_MANQUANTES_CARTE_SCOLAIRE);
        int nbNumerosVoieManquantsCarteScolaire = (Integer) resultats
                .get(KEY_CPT_NUMEROS_VOIE_MANQUANTS_CARTE_SCOLAIRE);
        int nbChevauchementVoiesCarteScolaire = (Integer) resultats
                .get(KEY_CPT_CHEVAUCHEMENT_VOIES_CARTE_SCOLAIRE);

        String warn = (String) resultats.get(KEY_WARNING);
        if (!StringUtils.isEmpty(warn)) {
            cr.append("<div class='messages_avertissement'>Avertissement</div>");
            cr.append("<ul class='messages_avertissement'>");
            cr.append("<li>").append(warn).append("</li>");
            cr.append("</ul>");
        }

        cr.append("Nombre d'élève(s) concerné(s) : ").append(nbElevesTraites);
        cr.append("<br/>");
        lienCompteRendu(cr, URL_RESULTAT_SIMU_DETERMINATION + EtatCollegeSecteur.CALCUL_ABOUTI,
                NB_ELEVE_CALCUL_ABOUTI, nbElevesDetermines);

        if (nbElevesAboutiHorsDep > 0) {
            cr.append("<br/>");
            lienCompteRendu(cr,
                    URL_RESULTAT_SIMU_DETERMINATION + EtatCollegeSecteur.CALCUL_ABOUTI_HORS_DEPARTEMENT,
                    NB_ELEVE_CALCUL_ABOUTI_HORS_DEPARTEMENT, nbElevesAboutiHorsDep);
        }

        if (nbElevesNonDetermines > 0) {
            cr.append("<br/>");
            lienCompteRendu(cr, URL_RESULTAT_SIMU_DETERMINATION + EtatCollegeSecteur.CALCUL_NON_ABOUTI,
                    NB_ELEVE_CALCUL_NON_ABOUTI, nbElevesNonDetermines);
        }

        if (nbElevesAdrInconrrecte > 0) {
            cr.append("<br/>");
            lienCompteRendu(cr, URL_RESULTAT_SIMU_DETERMINATION + EtatCollegeSecteur.CALCUL_IMPOSSIBLE,
                    NB_ELEVE_CALCUL_IMPOSSIBLE, nbElevesAdrInconrrecte);
        }

        cr.append("<h3>Aide à la fiabilisation de la carte scolaire</h3>");

        if (nbCommunesManquantesCarteScolaire > 0) {
            lienCompteRendu(cr, "/affelnet6/action/dossier/communesManquantesCarteScolaireListe",
                    NB_COMMUNES_MANQUANTES_CARTE_SCOLAIRE, nbCommunesManquantesCarteScolaire);
        }

        if (nbVoiesManquantesCarteScolaire > 0) {
            cr.append("<br/>");
            lienCompteRendu(cr, "/affelnet6/action/dossier/voiesManquantesCarteScolaireListe",
                    NB_VOIES_MANQUANTES_CARTE_SCOLAIRE, nbVoiesManquantesCarteScolaire);
        }

        if (nbNumerosVoieManquantsCarteScolaire > 0) {
            cr.append("<br/>");
            lienCompteRendu(cr, "/affelnet6/action/dossier/numerosVoieManquantsCarteScolaireListe",
                    NB_NUMEROS_VOIE_MANQUANTS_CARTE_SCOLAIRE, nbNumerosVoieManquantsCarteScolaire);
        }

        if (nbChevauchementVoiesCarteScolaire > 0) {
            cr.append("<br/>");
            lienCompteRendu(cr, "/affelnet6/action/dossier/chevauchementVoiesCarteScolaireListe",
                    NB_CHEVAUCHEMENT_VOIES_CARTE_SCOLAIRE, nbChevauchementVoiesCarteScolaire);
        }

        return cr.toString();
    }

    /**
     * Construit un lien HTML pour impression dans le compte rendu.
     * 
     * @param sb
     *            le StringBuffer
     * @param lien
     *            le lien
     * @param titre
     *            les éléments à concaténer pour construire le libellé du lien
     */
    private void lienCompteRendu(StringBuffer sb, String lien, Object... titre) {
        sb.append("<a href=\"");
        sb.append(lien);
        sb.append("\">");
        for (Object t : titre) {
            sb.append(t);
        }
        sb.append("</a>");
    }

    /**
     * Incrémente un compteur.
     * 
     * @param resultats
     *            table des données contenant le résultat du traitement
     * @param cle
     *            clé du compteur concerné
     */
    private void incrementeCompteur(Map<String, Object> resultats, String cle) {
        Integer cpt = 0;
        if (resultats.get(cle) != null) {
            cpt = (Integer) resultats.get(cle);
        }
        resultats.put(cle, cpt + 1);
    }

    @Override
    public Map<Integer, TronconCarteScolaire> chargeTronconSecteurUnique(String codeDeptMen)
            throws ApplicationException {
        return tronconCarteScolaireManager.listerTronconCarteScolaireDepartementSecteurUnique(codeDeptMen);
    }

    @Override
    public boolean isEleveAvecCalculCollegeSecteurAbouti(String codeDeptMen) throws DaoException {
        return eleveManager.compteElevesAvecCalculCollegeSecteurAbouti(codeDeptMen) > 0;
    }

    @Override
    public boolean isEleveAvecCalculCollegeSecteurDejaEffectue(String codeDeptMen) throws DaoException {
        return eleveManager.compteElevesAvecCalculCollegeSecteurDejaEffectue(codeDeptMen) > 0;
    }
}
