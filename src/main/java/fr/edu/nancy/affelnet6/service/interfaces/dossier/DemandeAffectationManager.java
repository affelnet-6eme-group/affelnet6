/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.service.interfaces.dossier;

import java.util.List;
import java.util.Map;

import fr.edu.nancy.affelnet6.domain.dossier.DemandeAffectation;
import fr.edu.nancy.affelnet6.domain.dossier.DemandeAffectationVue;
import fr.edu.nancy.affelnet6.domain.dossier.Eleve;
import fr.edu.nancy.affelnet6.domain.dossier.Log;
import fr.edu.nancy.affelnet6.domain.dossier.TypeDemandeSpecifique;
import fr.edu.nancy.affelnet6.domain.nomenclature.DeptMen;
import fr.edu.nancy.affelnet6.domain.nomenclature.EtablissementSconet;
import fr.edu.nancy.affelnet6.domain.nomenclature.MotifDerog;
import fr.edu.nancy.affelnet6.domain.nomenclature.OffreFormation;
import fr.edu.nancy.affelnet6.service.impl.dossier.EtatDemandeAffectation;
import fr.edu.nancy.commun.dao.interfaces.EntityFetcher;
import fr.edu.nancy.commun.dao.interfaces.Pagination;
import fr.edu.nancy.commun.dao.interfaces.Tri;
import fr.edu.nancy.commun.dao.interfaces.filtres.Filtre;
import fr.edu.nancy.commun.exceptions.ApplicationException;
import fr.edu.nancy.commun.exceptions.BusinessException;
import fr.edu.nancy.commun.exceptions.DaoException;
import fr.edu.nancy.commun.exceptions.TechnicalException;
import fr.edu.nancy.commun.utils.BatchProgressMonitor;
import fr.edu.nancy.commun.web.session.InfoListe;

/**
 * Gestionnaire de demande d'affectation.
 */
public interface DemandeAffectationManager {

    /** Filtre sur la valeur du departement. */
    String[] FILTRE_DEPT = { "0", "dept.deptCode", "Code département MEN" };
    /** Filtre sur l'identifiant du département. */
    String[] FILTRE_DEPT_ID = { "0", "dept.deptMenId", "Département" };
    /** Filtre sur le numero de dossier de l'éleve. */
    String[] FILTRE_NUMDOSS_ELEVE = { "0", "numDossier.numDossier", "numero dossier eleve" };
    /** Filtre sur le code UAI (anciennement RNE) de l'établissement demandé. */
    String[] FILTRE_UAI_ETAB_DEMANDE = { "0", "offreformation.etablissementSconet.codeRne", "Collège demandé" };
    /** Filtre sur les justificatifs de dérogation. */
    String[] FILTRE_JUSTIF_DEROGS = { "0", "justificatifDerogs", "Justificatifs de dérogation" };
    /** Filtre sur le nom de l'élève qui a fait la demande. */
    String[] FILTRE_NOM_ELEVE = { "0", "numDossier.nom", "Nom de l'élève" };
    /** Filtre sur le prénom de l'élève qui a fait la demande. */
    String[] FILTRE_PRENOM_ELEVE = { "0", "numDossier.prenom", "Prénom de l'élève" };
    /** Filtre sur le code postal de l'adresse de l'élève qui a fait la demande. */
    String[] FILTRE_LIBELLE_CODE_POSTAL_ELEVE = { "0", "numDossier.adresseResidence.codePostal.codePostal",
            "Code Postal" };
    /** Filtre sur la commune postale de l'adresse de l'élève qui a fait la demande. */
    String[] FILTRE_LIBELLE_COMMUNE_POSTAL_ELEVE = { "0", "numDossier.adresseResidence.communePostal.llPostal",
            "Commune" };
    /** Filtre sur la commune INSEE de l'adresse de l'élève qui a fait la demande. */
    String[] FILTRE_LIBELLE_COMMUNE_INSEE_ELEVE = { "0", "numDossier.adresseResidence.communeInsee.llInseeCommune",
            "Commune" };
    /** Filtre sur l'INE de l'élève qui a fait la demande. */
    String[] FILTRE_INE_ELEVE = { "0", "numDossier.ine", "INE de l'élève" };
    /** Filtre sur le code UAI de l'établissement d'origine de l'élève qui a fait la demande. */
    String[] FILTRE_UAI_ECOLE_ORIGINE_ELEVE = { "0", "numDossier.codUaiOri", "École d'origine" };
    /** Filtre sur le code de l'offre de formation de la demande. */
    String[] FILTRE_COD_OFFRE_FORMATION = { "0", "offreformation.cod", "Code de l'offre de formation" };
    /** Filtre sur le code décision finale. */
    String[] FILTRE_DEC_FIN = { "0", "numDossier.codeDecisionFinale.codeEtatFinal", "Code décision finale" };
    /** Filtre sur l'état de la demande. */
    String[] FILTRE_COD_ETAT_DEMANDE = { "0", "etatDemande.code", "Etat de la demande" };
    /** Filtre sur l'état de l'état de la demande. */
    String[] FILTRE_CODE_ETAT_ETAT_DECISION = { "0", "etatDemande.codeEtatFinal", "Etat de la demande" };
    /** Filtre sur le type de la demande. */
    String[] FILTRE_TYPE_DEMANDE = { "0", "typeDemande.code", "Type de la demande" };
    /** Filtre sur la décision de passage. */
    String[] FILTRE_DECISION_PASSAGE = { "0", "numDossier.decisionPassage.cod", "Décision de passage" };
    /** Filtre sur le code de l'offre de formation de la demande. */
    String[] FILTRE_COD_OFFRE_SEGPA = { "0", "offreformation.formation.code", "Offre de formation segpa" };
    /** Filtre sur le code de l'offre de formation de la demande. */
    String[] FILTRE_RANG = { "0", "rang", "Rang de la demande" };
    /** Filtre sur l'identifiant de la demande. */
    String[] FILTRE_ID = { "0", "id", "Identifiant de la demande" };

    /** Etat de la demande d'affecation. */
    String[] FILTRE_DEMANDE_HORS_COLLEGE_DEPARTEMENT = { "", "etatDemande.code",
            "Etat de la demande d'affectation" };

    /** Le code du motif de dérogation. */
    String[] FILTRE_MOTIF_DEROGATION = { "", "justificatifDerogs.idMotifDerog.code", "Motif de dérogation" };

    /** Le département du motif de dérogation. */
    String[] FILTRE_DEPT_MOTIF_DEROGATION = { "", "justificatifDerogs.idMotifDerog.dept.deptCode",
            "Département du motif de dérogation" };

    /** La formation demandée. */
    String[] FILTRE_FORMATION_DEMANDEE = { "", "offreformation.formation.mnemonique", "Formation demandée" };

    /** Le collège de secteur. */
    String[] FILTRE_COLLEGE_SECTEUR = { "", "numDossier.collegeSecteur.codeRne", "Collège secteur" };

    /** Le collège d'affectation. */
    String[] FILTRE_COLLEGE_AFFECTATION_FINALE = { "",
            "numDossier.affectationFinale.offreformation.etablissementSconet.codeRne", "Collège affecté" };

    /** Le rang de la demande affectée. */
    String[] FILTRE_RANG_AFFECTATION_FINALE = { "", "numDossier.affectationFinale.rang",
            "Rang affectation finale" };

    /** La formation de la demande affectée. */
    String[] FILTRE_FORMATION_AFFECTATION_FINALE = { "",
            "numDossier.affectationFinale.offreformation.formation.code", "Formation affectation finale" };

    /** Le code de la formation liée à la demande. */
    String[] FILTRE_FORMATION_DEMANDEE_CODE = { "", "formation.code", "Code de la formation demandée" };

    /** Le nombre de dérogations d'un élève. */
    String[] FILTRE_NOMBRE_DEROGATIONS = { "", "numDossier.nombreDeDerogation",
            "Nombre de dérogations demandées" };

    /**
     * Le filtre sur le type de demande spécifique.
     * 
     * @see TypeDemandeSpecifique
     */
    String[] FILTRE_TYPE_DEMANDE_SPEC = { "", "indicateurTypeDemandeSpecifique", "Type demande spécifique" };

    /** Collège de secteur dans le département. */
    String[] FILTRE_COL_SCT_DANS_DEPT = { "", "numDossier.estCollegeSecteurDansDepartement",
            "Collège de secteur dans département" };

    /**
     * Liste l'ensemble des DemandeAffectation.
     * 
     * @param paginationSupport
     *            les infos de pagination (ex.:numéro de la page courante (commence à 0))
     * @param listTri
     *            la liste des tris à effectuer
     * @param filtre
     *            Filtre à appliquer à la liste
     * @return liste des demandes d'affectation
     * @throws TechnicalException
     *             En cas de problème lors de la constitution de la liste
     * @throws BusinessException
     *             En cas de problème métier
     */
    List<DemandeAffectation> lister(Pagination<DemandeAffectation> paginationSupport, List<Tri> listTri,
            Filtre filtre) throws TechnicalException, BusinessException;

    /**
     * Liste l'ensemble des DemandeAffectation.
     * 
     * @param paginationSupport
     *            les infos de pagination (ex.:numéro de la page courante (commence à 0))
     * @param listTri
     *            la liste des tris à effectuer
     * @param filtre
     *            Filtre à appliquer à la liste
     * @param entityFetchers
     *            la liste des stratégies de chargement à appliquer à la liste
     * @return liste des demandes d'affectation
     * @throws TechnicalException
     *             En cas de problème lors de la constitution de la liste
     * @throws BusinessException
     *             En cas de problème métier
     */
    List<DemandeAffectation> lister(Pagination<DemandeAffectation> paginationSupport, List<Tri> listTri,
            Filtre filtre, List<EntityFetcher> entityFetchers) throws TechnicalException, BusinessException;
    /**
     * Liste l'ensemble des DemandeAffectation.
     * 
     * @param inf
     *            informations pour constituer la liste
     * @return liste des demandes d'affectation
     * @throws TechnicalException
     *             En cas de problème lors de la constitution de la liste
     * @throws BusinessException
     *             En cas de problème métier
     */
    List<DemandeAffectation> lister(InfoListe<DemandeAffectation> inf)
            throws TechnicalException, BusinessException;

    /**
     * Liste l'ensemble des DemandeAffectation.
     * 
     * @return liste des demandes d'affectation
     * @throws TechnicalException
     *             En cas de problème lors de la constitution de la liste
     * @throws BusinessException
     *             En cas de problème métier
     */
    List<DemandeAffectation> lister() throws TechnicalException, BusinessException;

    /**
     * Charge une DemandeAffectation à partir de son identifiant.
     * 
     * @param id
     *            Identifiant de la demande à charger
     * @return matiere chargé
     * @throws ApplicationException
     *             En ca de problème au chargement de DemandeAffectation.
     */
    DemandeAffectation charger(Long id) throws ApplicationException;

    /**
     * Mettre à jour une demande d'affectation.
     * 
     * @param demande
     *            la DemandeAffectation à mettre à jour
     * @throws ApplicationException
     *             Exception lancée en cas de problème à la mise à jour
     */
    void maj(DemandeAffectation demande) throws ApplicationException;

    /**
     * Mettre à jour une demande d'affectation et ne lance pas la méthode flush() d'Hibernate.
     * 
     * @param demande
     *            la DemandeAffectation à mettre à jour
     * @throws ApplicationException
     *             Exception lancée en cas de problème à la mise à jour
     */
    void majSansFlush(DemandeAffectation demande) throws ApplicationException;

    /**
     * Mettre à jour une demande d'affectation, ne lance pas la méthode flush() d'Hibernate et fait un evict de
     * l'instance.
     * 
     * @param demande
     *            la DemandeAffectation à mettre à jour
     * @throws ApplicationException
     *             Exception lancée en cas de problème à la mise à jour
     */
    void majSansFlushAvecEvict(DemandeAffectation demande) throws ApplicationException;

    /**
     * Supprime une <code>DemandeAffectation</code>.<br>
     * Annule la décision d'affectation sur la demande.<br>
     * Met à jour les compteurs de l'offre de formation liée à la demande.
     *
     * @param idDemande
     *            l'identifiant de la demande d'affectation à supprimer
     * @throws ApplicationException
     *             Exception lancée en cas de problème
     */
    void supprimer(Long idDemande) throws ApplicationException;

    /**
     * Supprime une <code>DemandeAffectation</code> et ses justificatifs de dérog.<br>
     * Annule la décision d'affectation sur la demande.<br>
     * Met à jour les compteurs de l'offre de formation liée à la demande.
     *
     * @param demande
     *            le DemandeAffectation à supprimer
     * @throws ApplicationException
     *             Exception lancée en cas de problème
     */
    void supprimer(DemandeAffectation demande) throws ApplicationException;

    /**
     * Créer une demande d'affectation.
     * Met à jour les compteurs de l'offre de formation liées à la demande.
     *
     * @param demande
     *            le DemandeAffectation à créer
     * @return identifiant de la demande créée
     * @throws ApplicationException
     *             Exception lancée en cas de problème
     */
    Long creer(DemandeAffectation demande) throws ApplicationException;

    /**
     * Teste si un demande d'affectation existe.
     * 
     * @param id
     *            l'identifiant de la demande d'affectation à tester
     * @return vrai si la demande existe
     * @throws ApplicationException
     *             Exception lancée en cas de problème
     */
    boolean existe(Long id) throws ApplicationException;

    /**
     * Compte le nombre total de demandes pour une offre.
     * 
     * @param codeOffre
     *            code de l'offre de formation
     * @return nombre de demandes totales
     * @throws ApplicationException
     *             en cas de problème
     */
    int compteDemandesTotales(String codeOffre) throws ApplicationException;

    /**
     * Compte les élèves relevant du secteur.
     * 
     * @param codeOffre
     *            code de l'offre de formation
     * @return nombre d'élèves relevant du secteur
     * @throws ApplicationException
     *             en cas de problème
     */
    int compteDemandesRelevantSecteur(String codeOffre) throws ApplicationException;

    /**
     * Compte les demandes de derogation.
     * 
     * @param codeOffre
     *            code de l'offre de formation
     * @return nombre de demandes de derogation
     * @throws ApplicationException
     *             en cas de problème
     */
    int compteDemandesDerogation(String codeOffre) throws ApplicationException;

    /**
     * Donne le niveau du motif de dérogation le plus prioritaire d'une demande de dérogation.
     * Retourne 0 dans le cas d'une demande d'affectation dans le collège de secteur.
     *
     * @param demande
     *            à examiner
     * @return niveau le plus prioritaire pour la demande
     */
    int niveauPrioritePlusHaute(DemandeAffectation demande);

    /**
     * Met à jour tous les compteurs d'une offre de formation
     * si on est en phase d'affectation.
     *
     * @param offre
     *            Offre de formation
     * @throws ApplicationException
     *             en cas de problème
     */
    void majCompteursOffre(OffreFormation offre) throws ApplicationException;

    /**
     * Met à jour les compteurs d'une offre de formation
     * si on est en phase d'affectation.
     *
     * @param offre
     *            Offre de formation concernée
     * @param majDemandes
     *            vrai si on doit mettre à jour les compteurs des demandes
     * @param majAffectation
     *            vrai si on doit mettre à jour les compteurs d'état d'affectation
     * @throws ApplicationException
     *             exception remontant en cas de problème
     */
    void majCompteursOffre(OffreFormation offre, boolean majDemandes, boolean majAffectation)
            throws ApplicationException;

    /**
     * Met à jour les compteurs d'une offre de formation.<br>
     * Ne vérifie pas que l'on est en phase d'affectation. Cette
     * vérification DOIT être faite avant l'appel à cette méthode !
     *
     * @param codeOffre
     *            Code de l'offre de formation concernée
     * @param majDemandes
     *            vrai si on doit mettre à jour les compteurs des demandes
     * @param majAffectation
     *            vrai si on doit mettre à jour les compteurs d'état d'affectation
     * @throws ApplicationException
     *             exception remontant en cas de problème
     */
    void majCompteursOffreSansVerifPhaseAffectation(String codeOffre, boolean majDemandes, boolean majAffectation)
            throws ApplicationException;

    /**
     * Met à jour les compteurs sur chacune des offres de formation
     * si l'état applicatif est correct (batch validation de la saisie ou affectation).
     * 
     * @param codeDeptMen
     *            code de département MEN
     * @param majDemandes
     *            vrai si on doit mettre à jour les compteurs des demandes
     * @param majAffectation
     *            vrai si on doit mettre à jour les compteurs d'état d'affectation
     * @param progMon
     *            Le surveilleur de progression. Mettre <code>null</code> si on n'est pas dans un batch.
     * @throws ApplicationException
     *             exception remontant en cas de problème
     */
    void majCompteursOffres(String codeDeptMen, boolean majDemandes, boolean majAffectation,
            BatchProgressMonitor progMon) throws ApplicationException;

    /**
     * Donne la liste des motifs de dérogation pour d'une demande de dérogation.
     *
     * @param demande
     *            à examiner
     * @return la liste des motifs pour la demande
     */
    List<MotifDerog> listeMotifsDerogation(DemandeAffectation demande);

    /**
     * Liste les demandes dans un collège public ou de Segpa du département.
     * 
     * @param codeDeptMen
     *            code MEN du département
     * @return liste des demandes
     * @throws ApplicationException
     *             en cas de problème
     */
    List<DemandeAffectation> listerDemandesDepartement(String codeDeptMen) throws ApplicationException;

    /**
     * Donne un filtre sur les demandes de collège public (mais sans les demandes SEGPA).
     * 
     * @return filtre de demandes de collège public
     */
    Filtre getFiltreTypeDemandeCollegePublic();

    /**
     * Compte le nombre de demandes de secteur concernant une offre donnée.
     * 
     * @param codeOffre
     *            Code de l'offre de formation
     * @return nombre de demandes de secteur
     * @throws ApplicationException
     *             en cas de problème
     */
    int compteDemandesSecteur(String codeOffre) throws ApplicationException;

    /**
     * Liste les demandes de dérogation concernant les élèves pour lesquels l'établissement
     * est collège de secteur et dont ce n'est pas une demande de secteur.
     * Les demandes en 6EME SEGPA et en ULIS ne sont pas retournées.
     * 
     * @param codeEtab
     *            code de l'etab
     * @return liste des demandes.
     */
    List<DemandeAffectation> listeDemandesDerogation(String codeEtab);

    /**
     * Liste les affectations en dérogation des élèves pour lesquels l'établissement
     * passé en paramètre est collège de secteur et qui sont affectés
     * dans un autre établissement.
     * Les affectations en 6EME SEGPA et en ULIS ne sont pas retournées.
     * 
     * @param codeEtab
     *            code de l'etab
     * @return liste des affectations.
     */
    List<DemandeAffectation> listeAffectationsDerogation(String codeEtab);

    /**
     * Compose un filtre des demandes du département dans un état non final.
     * 
     * @param codeDeptMen
     *            code du département MEN
     * @return filtre filtre de sélection
     */
    Filtre getFiltreDemandesDeptEtatNonFinal(String codeDeptMen);

    /**
     * Compose un filtre des demandes du département dans un état non final.
     * 
     * @param codeDeptMen
     *            code du département MEN
     * @return filtre filtre de sélection
     * @throws TechnicalException
     *             en cas de problème
     * @throws BusinessException
     *             en cas de problème
     */
    List<DemandeAffectation> listerDemandesDeptEtatNonFinal(String codeDeptMen)
            throws TechnicalException, BusinessException;

    /**
     * Compte le nombre total d'affectations finales sur une offre.
     * 
     * @param codeOffre
     *            code de l'offre de formation
     * @return nombre d'affectations totales
     * @throws ApplicationException
     *             en cas de problème
     */
    int compteAffectationsTotales(String codeOffre) throws ApplicationException;

    /**
     * Compte les affectations finales en dérogation sur une offre.
     * 
     * @param codeOffre
     *            code de l'offre de formation
     * @return nombre d'affectations finales en dérogation
     * @throws ApplicationException
     *             en cas de problème
     */
    int compteAffectationsDerogation(String codeOffre) throws ApplicationException;

    /**
     * Compte les affectations finales sur l'offre de secteur.
     * 
     * @param codeOffre
     *            code de l'offre de formation
     * @return nombre d'affectations finales de secteur
     * @throws ApplicationException
     *             en cas de problème
     */
    int compteAffectationsSecteur(String codeOffre) throws ApplicationException;

    /**
     * Compose un filtre des demandes SEGPA du département.
     * 
     * @param codeDeptMen
     *            code du département MEN
     * @return filtre filtre de sélection
     */
    Filtre getFiltreDemandesDeptSegpa(String codeDeptMen);

    /**
     * Nombre de demandes de formations SEGPA banalisée pour le département.
     * 
     * @param codDeptMen
     *            code du département MEN
     * @return nombre de demandes SEGPA banalisées.
     */
    int compteDemandesSegpaBanalisees(String codDeptMen);

    /**
     * Nombre de demandes de formations SEGPA banalisée prises pour le département.
     * 
     * @param codDeptMen
     *            code du département MEN
     * @return nombre de demandes SEGPA banalisées prises.
     */
    int compteDemandesSegpaBanaliseesPrises(String codDeptMen);

    /**
     * Nombre de demandes de formations SEGPA sans décision finale pour le département.
     * 
     * @param codDeptMen
     *            code du département MEN
     * @return nombre de demandes SEGPA sans décision finale.
     */
    int compteDemandesSegpaSansDecisionFinale(String codDeptMen);

    /**
     * Donne si un département a au moins une demande dont le rang est strictement
     * supérieur au rang passé en paramètre.
     *
     * @param codeDeptMen
     *            code du département MEN
     * @param rang
     *            Le rang
     * @return Vrai si le département au moins une demande dont le rang est strictement
     *         supérieur à <code>rang</code>
     * @throws DaoException
     *             En cas de problème d'accès aux données
     */
    boolean hasDemandesRangSuperieur(String codeDeptMen, short rang) throws DaoException;

    /**
     * Donne si une demande est une demande qui porte sur l'offre de secteur (collège de secteur et formation
     * 6EME).
     * 
     * @param demande
     *            La demande à vérifier
     * @return Vrai si la demande porte sur l'offre de secteur
     */
    boolean estDemandeOffreSecteur(DemandeAffectation demande);

    /**
     * Donne si une demande est une demande SEGPA.
     * 
     * @param demande
     *            La demande à vérifier
     * @return Vrai si c'est une demande SEGPA
     */
    boolean estDemandeSegpa(DemandeAffectation demande);

    /**
     * Donne si une demande est une proposition d'affectation.
     * 
     * @param demande
     *            La demande à vérifier
     * @return Vrai si une demande est une proposition d'affectation.
     */
    boolean estProposition(DemandeAffectation demande);

    /**
     * Donne si une demande est banalisée.
     * 
     * @param demande
     *            La demande à vérifier
     * @return Vrai si c'est une demande banalisée
     */
    boolean estDemandeBanalisee(DemandeAffectation demande);

    /**
     * Donne si une demande est banalisée ET sans collège défini.
     * 
     * @param demande
     *            La demande à vérifier
     * @return Vrai si c'est une demande banalisée ET sans collège défini.
     */
    boolean estDemandeBanaliseeSansCollege(DemandeAffectation demande);

    /**
     * @param codeDeptMen
     *            Le code département
     * @return Le rang maximum pour le département
     */
    int getRangMax(String codeDeptMen);

    /**
     * @param codeDeptMen
     *            Le code du département (par ex '054')
     * @return le Nombre de dérogation maximum (Nombre de voeux max -1)
     *         du département.
     */
    Integer getNbMaxDerogations(String codeDeptMen);

    /**
     * Liste l'ensemble des DemandeAffectation de la vue.
     * 
     * @param paginationSupport
     *            les infos de pagination (ex.:numéro de la page courante (commence à 0))
     * @param listTri
     *            la liste des tris à effectuer
     * @param filtreFormulaire
     *            filtre du formulaire
     * @param codOffreFormation
     *            le code de l'offre de formation
     * @return liste des demandes d'affectation
     * @throws ApplicationException
     *             en cas de problème
     */
    List<DemandeAffectationVue> listerDemandeAffectationVue(Pagination<DemandeAffectationVue> paginationSupport,
            List<Tri> listTri, Filtre filtreFormulaire, String codOffreFormation) throws ApplicationException;

    /**
     * Donne la liste des ids des demandes du département ayant comme justificatif de dérogation le motif passé en
     * paramètre.
     * 
     * @param codeDeptMen
     *            Le code MEN du département.
     * @param codeMotif
     *            Le code du motif de dérogation.
     * @return La liste des ids des demandes du département
     *         ayant comme justificatif de dérogation le motif passé en paramètre.
     */
    List<Long> getDemandeAffectationSelonMotif(String codeDeptMen, String codeMotif);

    /**
     * @param demande
     *            La demande qui sera modifié.
     * @param mode
     *            Indicateur pour savoir si on est en création ou en mis à jour.
     * @return
     *         <ul>
     *         <li>6EME dans le cas de la mise à jour d'une ULIS</li>
     *         <li>6EME SEGPA sinon</li>
     *         </ul>
     */
    String getFormationPropositionAffectation(DemandeAffectation demande, String mode);

    /**
     * @param codeOffre
     *            L'offre de formation pour laquelle on veut le nombre de demande ULIS.
     * @return Le nombre de demande en ULIS pour une offre de formation donnée.
     * @throws ApplicationException
     *             Un problème lors de l'accès aux données.
     */
    int compteDemandeUlis(String codeOffre) throws ApplicationException;

    /**
     * @param codeDeptMen
     *            Le code département MEN de l'utilisateur.
     * @return Map&lt;codeRne, Map&lt;codeMotif, compteur&gt;&gt; : Une map contenant tous les totaux par
     *         établissement
     *         des dérogations faites pour les formations de l'établissement.
     */
    Map<String, Map<String, Integer>> getCompteurDerogEtab(String codeDeptMen);

    /**
     * 
     * @param codeDeptMen
     *            Le code département MEN de l'utilisateur.
     * @return Map&lt;codeRne, Map&lt;codeMotif, compteur&gt;&gt; : Une map contenant les totaux par établissement
     *         des dérogations faites pour ne pas aller dans l'établissement de secteur.
     */
    Map<String, Map<String, Integer>> getCompteurDerogExterieureEtab(String codeDeptMen);

    /**
     * @param codeDeptMen
     *            Le code département MEN de l'utilisateur.
     * @return Map&lt;codeRne, Map&lt;codeOffre, Map&lt;codeMotif, compteur&gt;&gt;&gt; : Une map contenant les
     *         totaux par
     *         offre de formation des dérogations faites pour les formations de l'établissement.
     */
    Map<String, Map<String, Map<String, Integer>>> getCompteurDerogOffre(String codeDeptMen);

    /**
     * @param codeDeptMen
     *            Le code département MEN de l'utilisateur.
     * @return Map&lt;codeRne, compteur&gt; : Le compteur correspond aux nombres de demandes faites
     *         sur un autre collège que le collège de secteur.
     */
    Map<String, Integer> getCompteurTotalDemandeExterneEtab(String codeDeptMen);

    /**
     * @param codeDeptMen
     *            Le code département MEN de l'utilisateur.
     * @return Le nombre de demandes ULIS banalisées du département.
     */
    int compteDemandesUlisBanalisees(String codeDeptMen);

    /**
     * @param codeDeptMen
     *            Le code département MEN de l'utilisateur.
     * @return Le nombre de demandes SEGPA qui sont à la fois banalisées et ULIS dans le département.
     */
    int compteDemandesSegpaUlisBanalisees(String codeDeptMen);

    /**
     * @param codeDeptMen
     *            Le code département MEN de l'utilisateur.
     * @return Map&lt;codeRne, Map&lt;codeMotif, compteur&gt;&gt; : Une map contenant tous les totaux par
     *         établissement
     *         du motif de dérogation ayant prévalu à l'affectation.
     */
    Map<String, Map<String, Integer>> getCompteurDerogEtabaffectationDetaillee(String codeDeptMen);

    /**
     * @param codeDeptMen
     *            Le code département MEN de l'utilisateur.
     * @return Map&lt;codeRne, Map&lt;codeOffre, Map&lt;codeMotif, compteur&gt;&gt;&gt; : Une map contenant les
     *         totaux par
     *         offre de formation des dérogations ayant prévalu à l'affectation dans l'établissement.
     */
    Map<String, Map<String, Map<String, Integer>>> getCompteurDerogOffreAffectationDetaillee(String codeDeptMen);

    /**
     * @param codeDeptMen
     *            Le code département MEN de l'utilisateur.
     * @return Map&lt;codeRne, Map&lt;codeMotif, compteur&gt;&gt; : Une map contenant les totaux par établissement
     *         des dérogations ayant prévalues à l'affectation pour ne pas aller dans l'établissement de secteur.
     */
    Map<String, Map<String, Integer>> getCompteurDerogExterieureEtabAffectationDetaillee(String codeDeptMen);

    /**
     * @param codeDeptMen
     *            Le code département MEN de l'utilisateur.
     * @return Map&lt;codeRne, compteur&gt; : Le compteur correspond aux nombres de demandes acceptées
     *         sur un autre collège que le collège de secteur.
     */
    Map<String, Integer> getCompteurTotalDemandeExterneEtabAffectationDetaillee(String codeDeptMen);

    /**
     * @param codeDeptMen
     *            Le code département MEN de l'utilisateur.
     * @return Le nombre de derogation accepte dans le departement.
     */
    int compteDemandeDerogAffectees(String codeDeptMen);

    /**
     * @param infoListe
     *            L'infoListe de l'action.
     * @param afficherTout
     *            booléen indiquant si on pagine ou non.
     * @return La liste de toutes les demandes refusées dont le dossier est pris sur le voeu de secteur.
     * @throws ApplicationException
     *             Une erreur d'accès aux données.
     */
    List<DemandeAffectation> listerElevesRefusesDerog(InfoListe<DemandeAffectation> infoListe,
            boolean afficherTout) throws ApplicationException;

    /**
     * @param eleve
     *            L'élève pour lequel on veut récupérer le collège servant à la pré-affectation.
     * @return L'établissement qui sera utilisé qui sera définitivement (ou temporairement si la DSDEN le modifie)
     *         utilisé pour la pré-affectation.
     */
    EtablissementSconet getCollegeSecteurPreAffectation(Eleve eleve);

    /**
     * Méthode qui crée les demandes d'affectation en fonction du choix émis par la famille.
     * 
     * @param eleve
     *            L'élève pour lequel on veut créer les demandes d'affectation.
     * @throws ApplicationException
     *             Une erreur lors de la mise en place des demandes d'affectation.
     */
    void gereDemandeAffectation(Eleve eleve) throws ApplicationException;

    /**
     * Méthode qui crée automatiquement un voeu de secteur ou la demande hors département d'un élève.
     *
     * @param eleve
     *            L'élève pour lequel on veut créer les demandes d'affectation.
     * @param codeDeptMen
     *            Le code département MEN de l'utilisateur.
     * @return si la demande d'affectation par défaut est créée ou pas
     * @throws ApplicationException
     *             Une erreur lors de la mise en place des demandes d'affectation.
     */
    EtatDemandeAffectation genererDemandesAutomatique(Eleve eleve, String codeDeptMen) throws ApplicationException;

    /**
     * Méthode qui crée automatiquement une demande hors département d'un élève.
     * 
     * @param eleve
     *            L'élève pour lequel on veut créer les demandes d'affectation.
     * @param deptMen
     *            Le code département MEN de l'utilisateur.
     * @return si la demande d'affectation est créée ou pas
     * @throws ApplicationException
     *             Une erreur lors de la mise en place des demandes d'affectation hors département.
     */
    EtatDemandeAffectation creerDemandeHorsCollegePublicDepartement(Eleve eleve, DeptMen deptMen)
            throws ApplicationException;

    /**
     * Méthode qui recherche la proposition de SEGPA/ULIS parmi les demandes d'affectation d'un élève.
     * 
     * @param eleve
     *            l'élève
     * @return la proposition de SEGPA/ULIS si elle existe, null sinon
     */
    DemandeAffectation rechercheProposition(Eleve eleve);
    
    /**
     * Restaure les demandes d'affectations d'un élève.
     * 
     * @param eleve
     *            élève
     * @param log
     *            log associée à la sauvegarde
     * @throws ApplicationException
     *             en cas d'erreur lors de l'accès aux données
     */
    void restaurerDemandes(Eleve eleve, Log log) throws ApplicationException;
}
