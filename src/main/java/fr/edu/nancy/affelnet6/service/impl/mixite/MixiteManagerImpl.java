/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.service.impl.mixite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.edu.nancy.affelnet6.domain.affectation.IndicateurPcsEnum;
import fr.edu.nancy.affelnet6.domain.affectation.SuiviMixite;
import fr.edu.nancy.affelnet6.domain.dossier.CollegeSecteur;
import fr.edu.nancy.affelnet6.domain.dossier.Eleve;
import fr.edu.nancy.affelnet6.domain.dossier.SecteurAffectation;
import fr.edu.nancy.affelnet6.domain.nomenclature.EtablissementSconet;
import fr.edu.nancy.affelnet6.domain.nomenclature.IndicateurPCS;
import fr.edu.nancy.affelnet6.service.dossier.EleveManager;
import fr.edu.nancy.affelnet6.service.interfaces.mixite.MixiteManager;
import fr.edu.nancy.commun.dao.interfaces.filtres.Filtre;
import fr.edu.nancy.commun.exceptions.DaoException;
import fr.edu.nancy.commun.exceptions.TechnicalException;

/**
 * Service de gestion de la mixité des secteurs multi-collèges.
 * 
 * 
 *
 */
@Service("mixiteManager")
public class MixiteManagerImpl implements MixiteManager {

    /** Gestionnaire des élèves. */
    @Autowired
    private EleveManager eleveManager;

    @Override
    public List<SuiviMixite> suiviMixite(Filtre filtreEleves) throws TechnicalException {
        List<Eleve> eleves;
        try {
            eleves = eleveManager.listerDistinct(null, null, filtreEleves);
            List<SecteurAffectation> secteurAffectations = eleveManager.regrouperElevesParCollegeSecteur(eleves);
            List<SuiviMixite> suiviMixites = new ArrayList<SuiviMixite>(secteurAffectations.size());
            for (SecteurAffectation secteur : secteurAffectations) {
                suiviMixites.add(computeSuiviMixite(secteur));
            }
            Collections.sort(suiviMixites);
            return suiviMixites;
        } catch (DaoException e) {
            throw new TechnicalException(e);
        }
    }

    /**
     * Construit l'objet de suivi de la mixité.
     * 
     * @param secteur
     *            le secteur d'affectation (établissements et élèves)
     * @return l'objet de suivi de la mixité sociale
     */
    private SuiviMixite computeSuiviMixite(SecteurAffectation secteur) {
        SuiviMixite ret = new SuiviMixite();

        for (Eleve e : secteur.getEleves()) {

            IndicateurPcsEnum indic = IndicateurPcsEnum.NP;
            if (e.getIndicateurPcs() != null) {
                indic = IndicateurPcsEnum.fromCode(e.getIndicateurPcs().getCode());
            }
            ret.getCompositionSociale().ajouteEleve(indic);
        }

        for (EtablissementSconet etab : secteur.getEtablissementSconets()) {
            ret.addEtablissement(etab);

            // Conversion des données en base pour l'object 'CompositionSociale'
            int[] compteurParIndicateur = new int[IndicateurPcsEnum.values().length];
            for (Map.Entry<IndicateurPCS, Integer> entry : etab.getObjectifsMixite().entrySet()) {
                compteurParIndicateur[entry.getKey().getIndicateurEnum().ordinal()] = entry.getValue();
            }
            ret.getCompositionObjectifs().get(etab).setCompteurParIndicateur(compteurParIndicateur);

            for (Eleve e : secteur.getEleves()) {
                IndicateurPcsEnum indic = IndicateurPcsEnum.NP;
                if (e.getIndicateurPcs() != null) {
                    indic = IndicateurPcsEnum.fromCode(e.getIndicateurPcs().getCode());
                }
                // Composition sociale après désignation
                EtablissementSconet etabChoisiParIa = e.getCollegeSecteur();
                if (etabChoisiParIa != null && etabChoisiParIa.equals(etab)) {
                    ret.getCompositionsApresDesignation().get(etab).ajouteEleve(indic);
                }
                // Composition sociale après classement
                EtablissementSconet etabChoisiParFamille = etabChoisiParFamille(e);
                if (etabChoisiParFamille != null && etabChoisiParFamille.equals(etab)) {
                    ret.getCompositionsApresClassement().get(etab).ajouteEleve(indic);
                }
            }
        }
        return ret;
    }

    /**
     * @param e
     *            l'élève
     * @return retourne le collège de secteur choisi par la famille (celui avec le numéro d'ordre le plus petit)
     */
    private EtablissementSconet etabChoisiParFamille(Eleve e) {
        EtablissementSconet premierChoix = null;
        int meilleurRang = Integer.MAX_VALUE;
        for (CollegeSecteur c : e.getCollegeSecteurs()) {
            if (c.getNumeroOrdre() != null && c.getNumeroOrdre() < meilleurRang) {
                premierChoix = c.getEtablissementSconet();
                meilleurRang = c.getNumeroOrdre();
            }
        }
        return premierChoix;
    }
}
