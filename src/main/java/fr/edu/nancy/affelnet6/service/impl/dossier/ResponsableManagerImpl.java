/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.service.impl.dossier;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.edu.nancy.affelnet6.dao.interfaces.dossier.AdresseDao;
import fr.edu.nancy.affelnet6.dao.interfaces.dossier.ResponsableDao;
import fr.edu.nancy.affelnet6.dao.interfaces.nomenclature.PcsDao;
import fr.edu.nancy.affelnet6.domain.dossier.Adresse;
import fr.edu.nancy.affelnet6.domain.dossier.Eleve;
import fr.edu.nancy.affelnet6.domain.dossier.Responsable;
import fr.edu.nancy.affelnet6.domain.dossier.ResponsableId;
import fr.edu.nancy.affelnet6.domain.nomenclature.NiveauResponsabilite;
import fr.edu.nancy.affelnet6.domain.nomenclature.PCS;
import fr.edu.nancy.affelnet6.helper.EleveHelper;
import fr.edu.nancy.affelnet6.service.dossier.EleveManager;
import fr.edu.nancy.affelnet6.service.dossier.ResponsableManager;
import fr.edu.nancy.affelnet6.service.nomenclature.LienParenteManager;
import fr.edu.nancy.affelnet6.utils.Message;
import fr.edu.nancy.commun.exceptions.ApplicationException;
import fr.edu.nancy.commun.service.impl.AbstractDaoManager;

/**
 * Gestionnaire de Responsable.
 *
 * @see fr.edu.nancy.affelnet6.domain.dossier.Responsable
 */
@Service("responsableManager")
public class ResponsableManagerImpl extends AbstractDaoManager<Responsable, ResponsableId, ResponsableDao>
        implements ResponsableManager {

    /** La longueur maximale dans BEE d'un nom, d'un prénom ou d'un intitulé d'un responsable. */
    private static final int MAX_LENGTH_NAME_RESPONSABLE_BEE = 50;

    /** Gestionnaire d'adresses. */
    @Autowired
    private AdresseDao adresseDao;

    /** Le gestionnaire des élèves. */
    @Autowired
    private EleveManager eleveManager;

    /** DAO des PCS. */
    @Autowired
    private PcsDao pcsDao;

    @Override
    public int supprimerPourRemplacerDepartement(int dptMenId) throws ApplicationException {
        return dao.supprimerPourRemplacerDepartement(dptMenId);
    }

    @Override
    public String nomResponsable(Responsable responsable) {
        return nomResponsable(responsable, false);
    }

    @Override
    public String nomResponsable(Responsable responsable, boolean truncateForRcpPer) {
        // si responsable moral
        if (Responsable.TYPE_RESPONSABLE_PERSONNE_MORALE.equals(responsable.getTypeResponsable())) {
            if (truncateForRcpPer) {
                return StringUtils.substring(responsable.getIntitule(), 0, MAX_LENGTH_NAME_RESPONSABLE_BEE);
            } else {
                return responsable.getIntitule();
            }
        }
        // sinon si responsable physique
        else {
            if (truncateForRcpPer) {
                return StringUtils.substring(responsable.getNom(), 0, MAX_LENGTH_NAME_RESPONSABLE_BEE);
            } else {
                return responsable.getNom();
            }
        }
    }

    @Override
    public String nomUsageResponsable(Responsable responsable) {
        // si responsable moral
        if (Responsable.TYPE_RESPONSABLE_PERSONNE_MORALE.equals(responsable.getTypeResponsable())) {
            return "";
        }
        // sinon si responsable physique
        else {
            if (StringUtils.isEmpty(responsable.getNomUsage())) {
                return "";
            } else {
                return responsable.getNomUsage();
            }

        }
    }

    @Override
    public String prenomResponsable(Responsable responsable) {
        // si responsable moral
        if (Responsable.TYPE_RESPONSABLE_PERSONNE_MORALE.equals(responsable.getTypeResponsable())) {
            return responsable.getIntitule();
        }
        // sinon si responsable physique
        else {
            return responsable.getPrenom();
        }
    }

    @Override
    public String civiliteResponsable(Responsable responsable) {
        // si responsable physique
        if (Responsable.TYPE_RESPONSABLE_PERSONNE_PHYSIQUE.equals(responsable.getTypeResponsable())) {
            return responsable.getCivilite().getCodeCivilite().toString();
        }
        // sinon si responsable moral
        else {
            return null;
        }
    }

    @Override
    public Message ajouteOuModifieResponsable(Eleve e, Responsable resp, short numResp)
            throws ApplicationException {
        int nbRepLegaux = 0;
        int nbEnCharge = 0;

        Responsable responsableModifie = null;
        short maxNum = 0;

        resp.setId(new ResponsableId(e, numResp));

        for (Responsable r : e.getResponsables()) {
            if (r.getId().getNumResponsable() > maxNum) {
                maxNum = r.getId().getNumResponsable();
            }
            if (r.getId().getNumResponsable() == resp.getId().getNumResponsable()) {
                responsableModifie = r;
            } else if (r.getNiveauResponsabilite() != null) {
                if (NiveauResponsabilite.CODE_REPRESENTANT_LEGAL.equals(r.getNiveauResponsabilite().getCode())) {
                    nbRepLegaux++;
                } else {
                    nbEnCharge++;
                }
            }
        }
        if (resp.getNiveauResponsabilite() != null) {
            if (NiveauResponsabilite.CODE_REPRESENTANT_LEGAL.equals(resp.getNiveauResponsabilite().getCode())) {
                if (nbRepLegaux >= MAX_REPRESENTANTS_LEGAUX) {
                    return new Message("error.dossier.responsable.maxRepresentants");
                }
            } else {
                if (nbEnCharge >= MAX_EN_CHARGE) {
                    return new Message("error.dossier.responsable.maxPersonneEnCharge");
                }
                // on vérifie que l'on ne perd pas le dernier responsable légal
                if (responsableModifie != null
                        && NiveauResponsabilite.CODE_REPRESENTANT_LEGAL
                                .equals(responsableModifie.getNiveauResponsabilite().getCode())
                        && nbRepLegaux == 0) {
                    return new Message("error.dossier.responsable.minRepresentants");
                }
            }
        }

        if (responsableModifie != null) {
            resp.setPcs(responsableModifie.getPcs());
            e.getResponsables().remove(responsableModifie);
            supprimer(responsableModifie);
        } else {
            resp.getId().setNumResponsable((short) (maxNum + 1));
        }
        // La PCS est positionnée par défaut à 'Non renseigné'
        if (resp.getPcs() == null) {
            resp.setPcs(pcsDao.chargerParCode(PCS.CODE_NON_RENSEIGNEE));
        }

        e.getResponsables().add(resp);
        e.setExisteAdresseATraiter(EleveHelper.calculFlagExisteAdresseATraiter(e));
        if (e.getNumDossier() != null) {
            eleveManager.maj(e, true);
        }
        return null;
    }

    @Override
    public List<Message> verifieValiditeLiens(Eleve e) throws ApplicationException {
        List<Message> warnings = new LinkedList<>();
        int nbPeres = 0;
        int nbMeres = 0;
        int nbTuteurs = 0;

        for (Responsable r : e.getResponsables()) {
            if (r.getLienParente() != null) {
                switch (r.getLienParente().getCodeParente()) {
                    case LienParenteManager.CODE_PERE:
                        nbPeres++;
                        break;
                    case LienParenteManager.CODE_MERE:
                        nbMeres++;
                        break;
                    case LienParenteManager.CODE_TUTEUR:
                        nbTuteurs++;
                        break;
                    default:
                }
            }
        }

        if (nbPeres + nbMeres > 2) {
            warnings.add(new Message("error.dossier.responsable.deuxParents"));
        }
        if (nbTuteurs > 2) {
            warnings.add(new Message("error.dossier.responsable.deuxTuteurs"));
        }

        return warnings;
    }

    @Override
    public Message supprimer(Eleve eleve, short numResp) throws ApplicationException {
        ResponsableId respId = new ResponsableId(eleve, numResp);
        Responsable resp = charger(respId);
        boolean autreRepresentant = false;
        for (Responsable r : eleve.getResponsables()) {
            if (r.getId().getNumResponsable() != numResp && r.getNiveauResponsabilite() != null
                    && NiveauResponsabilite.CODE_REPRESENTANT_LEGAL
                            .equals(r.getNiveauResponsabilite().getCode())) {
                autreRepresentant = true;
            }
        }

        if (resp.getNiveauResponsabilite() != null
                && NiveauResponsabilite.CODE_REPRESENTANT_LEGAL.equals(resp.getNiveauResponsabilite().getCode())
                && !autreRepresentant) {
            return new Message("error.dossier.responsable.minRepresentants");
        }
        eleve.getResponsables().remove(resp);
        // Mise à jour du flag d'existence d'une adresse à traiter pour l'élève
        eleve.setExisteAdresseATraiter(EleveHelper.calculFlagExisteAdresseATraiter(eleve));

        // sauvegarde BDD
        eleveManager.maj(eleve, true);

        // suppression de l'adresse
        Adresse adresseResp = resp.getAdresseResponsable();
        if (adresseResp != null && CollectionUtils.isEmpty(adresseResp.getResponsables())
                && CollectionUtils.isEmpty(adresseResp.getEleves())
                && CollectionUtils.isEmpty(adresseResp.getElevesSecondaire())) {
            adresseDao.supprimer(adresseResp);
        }

        return null;
    }

    @Autowired
    @Override
    public void setDao(ResponsableDao dao) {
        super.setDao(dao);
    }
}
