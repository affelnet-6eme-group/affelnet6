/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.service.interfaces.dossier;

import fr.edu.nancy.affelnet6.domain.dossier.Eleve;
import fr.edu.nancy.affelnet6.domain.dossier.Log;
import fr.edu.nancy.affelnet6.web.struts.forms.dossier.EleveChoixFamilleForm;
import fr.edu.nancy.commun.exceptions.ApplicationException;

/**
 * Gestionnaire pour la saisie des choix de la famille.
 * 
 * 
 *
 */
public interface SaisieChoixFamilleManager {

    /**
     * Permet de persister les demandes d'affectations émises par la famille.
     * Cette action met en place les données pour calculer les demandes d'affectations.
     * 
     * @param eleve
     *            L'élève sur lequel on travaille.
     * @param form
     *            Le formulaire de saisie du choix de la famille.
     * @param codeDeptMen
     *            le code département de l'utilisateur.
     * @throws ApplicationException
     *             Une erreur lors de l'accès aux données.
     * @see DemandeAffectationManager#gereDemandeAffectation(Eleve)
     */
    void gereSaisieChoixFamille(Eleve eleve, EleveChoixFamilleForm form, String codeDeptMen)
            throws ApplicationException;

    /**
     * 
     * @param eleve
     *            L'élève sur lequel on travaille.
     * @param log
     *            Les données de sauvegarde
     * @throws ApplicationException
     *             en cas d'erreur
     */
    void gereSaisieChoixFamille(Eleve eleve, Log log) throws ApplicationException;

    /**
     * Méthode réinitialisant les choix de la famille pour l'élève.
     * 
     * @param eleve
     *            l'élève à réinitialiser
     * @param avecTypeDemande
     *            booléen indiquant le fait si on souhaite
     *            aussi réinitialiser la réponse à la question
     *            "Affectation demandée dans un collège public du département"
     */
    void razSaisieChoixFamille(Eleve eleve, boolean avecTypeDemande);

    /**
     * Restaure les choix de la famille pour un élève.
     * 
     * @param eleve
     *            élève
     * @param log
     *            log associée à la sauvegarde
     * @throws ApplicationException
     *             en cas d'erreur lors de l'accès aux données
     */
    void restaurerChoixFamille(Eleve eleve, Log log) throws ApplicationException;
}
