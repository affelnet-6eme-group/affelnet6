/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.service.interfaces.dossier;

import java.util.List;

import fr.edu.nancy.affelnet6.domain.dossier.JustificatifDerog;
import fr.edu.nancy.affelnet6.domain.nomenclature.MotifDerog;
import fr.edu.nancy.commun.dao.interfaces.Pagination;
import fr.edu.nancy.commun.dao.interfaces.Tri;
import fr.edu.nancy.commun.dao.interfaces.filtres.Filtre;
import fr.edu.nancy.commun.exceptions.ApplicationException;
import fr.edu.nancy.commun.exceptions.BusinessException;
import fr.edu.nancy.commun.exceptions.TechnicalException;
import fr.edu.nancy.commun.web.session.InfoListe;

/**
 * Gestionnaire de JustificatifDerog.
 * 
* @see fr.edu.nancy.affelnet6.domain.dossier.JustificatifDerog
 */
public interface JustificatifDerogManager {

    /** Filtre par département. */
    String[] FILTRE_DEPARTEMENT = { "1", "idDemandeAffec.numDossier.dept.deptCode", "Département" };
    /** Filtre par motif de dérogation (code). */
    String[] FILTRE_MOTIF_DEROGATION = { "2", "idMotifDerog.code", "Motif de dérogation" };
    /** Filtre par département de motif de dérogation. */
    String[] FILTRE_DEPT_MOTIF_DEROGATION = { "3", "idMotifDerog.dept.deptCode",
            "Département du motif de dérogation" };
    /** Filtre par Code RNE d'établissement demandé. */
    String[] FILTRE_ETABLISSEMENT_DEMANDEE = { "4", "idDemandeAffec.offreformation.etablissementSconet.codeRne",
            "Etablissement demandée" };
    /** Filtre par formation demandée. */
    String[] FILTRE_FORMATION_DEMANDEE = { "5", "idDemandeAffec.offreformation.formation.mnemonique",
            "Formation demandée" };
    /** Filtre par code RNE du collège de secteur demandée. */
    String[] FILTRE_COLLEGE_SECTEUR = { "6", "idDemandeAffec.numDossier.collegeSecteur.codeRne", "Collège secteur" };
    /** Filtre par code UAI de l'école d'origine. */
    String[] FILTRE_ECOLE_ORIGINE = { "7", "idDemandeAffec.numDossier.codUaiOri", "Etablissement" };
    /** Filtre par rang. */
    String[] FILTRE_RANG_DEMANDE_AFFECTATION = { "8", "idDemandeAffec.rang" };
    /** Filtre par nombre de justificatif de dérogation. */
    String[] FILTRE_NB_JUSTIFICATIF_DEROG = { "9", "idDemandeAffec.numDossier.nombreDeDerogation",
            "Nombres de justificatifs de dérogation" };
    /** Filtre par code de décision d'affectation finale. */
    String[] FILTRE_CODE_DECISION_AFFECTATION_FINALE = { "10", "idDemandeAffec.numDossier.codeDecisionFinale",
            "Décision d'affectation" };

    /**
     * Liste l'ensemble des JustificatifDerog.
     * 
     * @param paginationSupport
     *            les infos de pagination (ex.:numéro de la page courante
     *            (commence à 0))
     * @param listTri
     *            la liste des tris à effectuer
     * @param filtre
     *            la liste des filtres à appliquer
     * @return liste des JustificatifDerog.
     * @throws TechnicalException
     *             En cas de problème lors de la constitution de la liste
     * @throws BusinessException
     *             en cas d'erreur fonctionnelle
     */
    List<JustificatifDerog> lister(Pagination<JustificatifDerog> paginationSupport, List<Tri> listTri,
            Filtre filtre) throws TechnicalException, BusinessException;

    /**
     * Liste l'ensemble des JustificatifDerog.
     * 
     * @param inf
     *            les infos de pagination, tri, filtre
     * @return liste des JustificatifDerog.
     * @throws TechnicalException
     *             En cas de problème lors de la constitution de la liste
     * @throws BusinessException
     *             en cas d'erreur fonctionnelle
     */
    List<JustificatifDerog> lister(InfoListe<JustificatifDerog> inf) throws TechnicalException, BusinessException;

    /**
     * Liste l'ensemble des JustificatifDerog.
     * 
     * @return liste des JustificatifDerog.
     * @throws TechnicalException
     *             En cas de problème lors de la constitution de la liste
     * @throws BusinessException
     *             en cas d'erreur fonctionnelle
     */
    List<JustificatifDerog> lister() throws TechnicalException, BusinessException;

    /**
     * Charge une JustificatifDerog à partir de son code.
     * 
     * @param id
     *            identifiant du Justificatif de dérogation à charger
     * @return Justificatif de dérogation chargé
     * @throws ApplicationException
     *             En cas de problème au chargement de JustificatifDerog.
     */
    JustificatifDerog charger(Long id) throws ApplicationException;

    /**
     * Mettre à jour un JustificatifDerog.
     * 
     * @param p
     *            le JustificatifDerog à mettre à jour
     * @throws ApplicationException
     *             Exception lancée en cas de problème à la mise à jour
     */
    void maj(JustificatifDerog p) throws ApplicationException;

    /**
     * Mettre à jour un JustificatifDerog.
     * 
     * @param p
     *            le JustificatifDerog à mettre à jour
     * @throws ApplicationException
     *             Exception lancée en cas de problème à la mise à jour
     */
    void supprimer(JustificatifDerog p) throws ApplicationException;

    /**
     * Créer un JustificatifDerog.
     * 
     * @param p
     *            le JustificatifDerog à créer
     * @return l'identifiant du justificatif de dérog créé
     * @throws ApplicationException
     *             Exception lancée en cas de problème à la mise à jour
     */
    Long creer(JustificatifDerog p) throws ApplicationException;

    /**
     * Test si un JustificatifDerog est présent en base.
     * 
     * @param id
     *            l'id de l'JustificatifDerog à tester
     * @return vrai si un Justificatif avec cet identifiant existe
     * @throws ApplicationException
     *             Exception lancée en cas de problème
     */
    boolean existe(Long id) throws ApplicationException;

    /**
     * Compte les justificatifs par motif concernant une offre de formation .
     * 
     * @param codeOffre
     *            code de l'offre
     * @param codeMotif
     *            code du motif
     * @return Le nombre de demandes avec cette dérogation sur cette offre
     */
    int compteDemandesDerog(String codeOffre, String codeMotif);

    /**
     * Compte les justificatifs par motif .
     * 
     * @param motif
     *            motif de dérog
     * @return Le nombre de demandes avec cette dérogation
     */
    int compteDemandesDerogMotif(MotifDerog motif);

    /**
     * Compte les justificatifs par motif concernant une offre de formation pour les élèves affectés.
     * 
     * @param codeOffre
     *            code de l'offre
     * @param codeMotif
     *            code du motif
     * @return Le nombre d'affectations avec cette dérogation sur cette offre
     * 
     */
    int compteAffectationsDerog(String codeOffre, String codeMotif);

}
