/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.service.impl.dossier;

import java.util.SortedSet;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.edu.nancy.affelnet6.domain.dossier.DemandeFamille;
import fr.edu.nancy.affelnet6.domain.dossier.Eleve;
import fr.edu.nancy.affelnet6.domain.dossier.Log;
import fr.edu.nancy.affelnet6.domain.dossier.SauvegardeDemandeFamille;
import fr.edu.nancy.affelnet6.domain.dossier.SauvegardeEleve;
import fr.edu.nancy.affelnet6.domain.nomenclature.EtablissementSconet;
import fr.edu.nancy.affelnet6.domain.nomenclature.Formation;
import fr.edu.nancy.affelnet6.domain.nomenclature.Matiere;
import fr.edu.nancy.affelnet6.domain.nomenclature.MotifDerog;
import fr.edu.nancy.affelnet6.domain.nomenclature.TypeDemandeAffec;
import fr.edu.nancy.affelnet6.helper.DemandeAffectationHelper;
import fr.edu.nancy.affelnet6.helper.EleveHelper;
import fr.edu.nancy.affelnet6.helper.EtablissementHelper;
import fr.edu.nancy.affelnet6.service.dossier.DemandeAffectationManager;
import fr.edu.nancy.affelnet6.service.dossier.SaisieChoixFamilleManager;
import fr.edu.nancy.affelnet6.service.nomenclature.EtablissementSconetManager;
import fr.edu.nancy.affelnet6.service.nomenclature.FormationManager;
import fr.edu.nancy.affelnet6.service.nomenclature.MatiereManager;
import fr.edu.nancy.affelnet6.service.nomenclature.MotifDerogManager;
import fr.edu.nancy.affelnet6.web.struts.forms.dossier.EleveChoixFamilleForm;
import fr.edu.nancy.commun.exceptions.ApplicationException;

/**
 * Gestionnaire pour la saisie des choix de la famille.
 * 
 * 
 *
 */
@Service("saisieChoixFamilleManager")
public class SaisieChoixFamilleManagerImpl implements SaisieChoixFamilleManager {

    /** Logger de la classe. **/
    private static final Logger LOG = LoggerFactory.getLogger(SaisieChoixFamilleManagerImpl.class);

    /** Gestionnaire des formations. */
    @Autowired
    private FormationManager formationManager;

    /** Gestionnaire des matières. */
    @Autowired
    private MatiereManager matiereManager;

    /** Gestionnaire des établissements. */
    @Autowired
    private EtablissementSconetManager etablissementSconetManager;

    /** Gestionnaire des motifs de dérogation . */
    @Autowired
    private MotifDerogManager motifDerogManager;

    /** Gestionnaire des demandes d'affectation . */
    @Autowired
    protected DemandeAffectationManager demandeAffectationManager;

    @Override
    public void gereSaisieChoixFamille(Eleve eleve, EleveChoixFamilleForm form, String codeDeptMen)
            throws ApplicationException {
    	
        Boolean estScolarisationDansCollegeSecteur = Boolean.FALSE;
        Formation formationDemandeSecteur = null;

        // On traduit le choix de la demande d'affectation dans un collège public du département en booléen.
        Boolean estAffectationDemandeeCollegePublicDept = StringUtils.isEmpty(form.getTypeDemande()) ? null
                : StringUtils.equals(TypeDemandeAffec.CODE_TYPE_DEMANDE_DANS_COLLEGE_PUBLIC_DEPT,
                        form.getTypeDemande());
        
        if (BooleanUtils.isTrue(estAffectationDemandeeCollegePublicDept)) {
        	estScolarisationDansCollegeSecteur = StringUtils.equals(form.getDemandeCollegeSecteur(),
                EleveChoixFamilleForm.DEMANDE_COLLEGE_SECTEUR);
        	
            if (estScolarisationDansCollegeSecteur) {
                EtablissementSconet etab = demandeAffectationManager.getCollegeSecteurPreAffectation(eleve);
                if (etab != null) {
                    form.setDerogation1EtablissementCodeRne(etab.getCodeRne());
                    form.setDerogation1LibelleEtab(EtablissementHelper.getLibelleSimpleEtablissement(etab));
                } else {
                    form.setDerogation1EtablissementCodeRne(StringUtils.EMPTY);
                    form.setDerogation1LibelleEtab(StringUtils.EMPTY);
                }
                form.initDerog(2);
                form.initDerog(4);
                form.initDerog(3);
            }
            
            formationDemandeSecteur = formationManager.charger(form.getCodeMefFormation());

        } else {
            form.initDemandeHorsCollegePublicDept();
        }    	
        
        Matiere lv1 = matiereManager.chargerByCleGestion(form.getLangueVivante1CleGestion());
        Matiere lv2 = matiereManager.chargerByCleGestion(form.getLangueVivante2CleGestion());
        Matiere lv3 = matiereManager.chargerByCleGestion(form.getLangueVivante3CleGestion());

        gereSaisieChoixFamille(eleve, codeDeptMen,
        		estAffectationDemandeeCollegePublicDept, estScolarisationDansCollegeSecteur,
        		formationDemandeSecteur, lv1, lv2, lv3,
        		form.getDerogation1EtablissementCodeRne(), form.getDerogation1MotifsSelectedSubmitted(),
        		form.getDerogation2EtablissementCodeRne(), form.getDerogation2MotifsSelectedSubmitted(),
        		form.getDerogation3EtablissementCodeRne(), form.getDerogation3MotifsSelectedSubmitted(),
        		form.getDerogation4EtablissementCodeRne(), form.getDerogation4MotifsSelectedSubmitted());
    	
    }
    
    @Override
    public void gereSaisieChoixFamille(Eleve eleve, Log log)
            throws ApplicationException {

		SauvegardeEleve sauvegardeEleve = log.getEleve();
		SortedSet<SauvegardeDemandeFamille> demandesFamille = log.getDemandesFamille();
		
        gereSaisieChoixFamille(eleve, eleve.getDept().getDeptCode(),
        		sauvegardeEleve.getEstAffectationDemandeeCollegePublicDept(), sauvegardeEleve.getEstScolarisationDansCollegeSecteur(), 
        		sauvegardeEleve.getFormationDemandeSecteur(), sauvegardeEleve.getLv1(), sauvegardeEleve.getLv2(), sauvegardeEleve.getLv3(),
        		getDerogationEtablissementCodeRne(demandesFamille, 1), getDerogationMotifs(demandesFamille, 1),
        		getDerogationEtablissementCodeRne(demandesFamille, 2), getDerogationMotifs(demandesFamille, 2),
        		getDerogationEtablissementCodeRne(demandesFamille, 3), getDerogationMotifs(demandesFamille, 3),
        		getDerogationEtablissementCodeRne(demandesFamille, 4), getDerogationMotifs(demandesFamille, 4));
    	
    }    
    
    private void gereSaisieChoixFamille(Eleve eleve, String codeDeptMen,
    		Boolean estAffectationDemandeeCollegePublicDept, Boolean estScolarisationDansCollegeSecteur, 
    		Formation formationDemandeSecteur, Matiere lv1, Matiere lv2, Matiere lv3,
    		String derogation1EtablissementCodeRne, String[] derogation1Motifs,
    		String derogation2EtablissementCodeRne, String[] derogation2Motifs,
    		String derogation3EtablissementCodeRne, String[] derogation3Motifs,
    		String derogation4EtablissementCodeRne, String[] derogation4Motifs)
            throws ApplicationException {
    	
    	eleve.setEstAffectationDemandeeCollegePublicDept(estAffectationDemandeeCollegePublicDept);
    	
        razSaisieChoixFamille(eleve, false);
        if (BooleanUtils.isTrue(estAffectationDemandeeCollegePublicDept)) {
            eleve.setEstScolarisationDansCollegeSecteur(estScolarisationDansCollegeSecteur);
            eleve.setFormationDemandeSecteur(formationDemandeSecteur);
            eleve.setLv1(lv1);
            eleve.setLv2(lv2);
            eleve.setLv3(lv3);

            eleve.getDemandeFamilles().clear(); // TODO supprimer également les demandes familles en base ou au
                                                // moins supprimer le lien bidirectionnel ie mettre eleve à null
                                                // dans les demandes

            short rang = 0;
            LOG.debug("Création des demandes de la famille pour l'élève " + EleveHelper.getIdentite(eleve));
            ajouteDemandeFamille(eleve, derogation1EtablissementCodeRne,
                    derogation1Motifs, ++rang, codeDeptMen);

            ajouteDemandeFamille(eleve, derogation2EtablissementCodeRne,
                    derogation2Motifs, ++rang, codeDeptMen);

            ajouteDemandeFamille(eleve, derogation3EtablissementCodeRne,
                    derogation3Motifs, ++rang, codeDeptMen);

            ajouteDemandeFamille(eleve, derogation4EtablissementCodeRne,
                    derogation4Motifs, ++rang, codeDeptMen);

        }
    }

    /**
     * Récupère le code établissement de dérogation de la demande famille correspondant au rang indiqué
     * 
     * @param demandesFamille
     * @param rang
     * @return
     */
    private String getDerogationEtablissementCodeRne(SortedSet<SauvegardeDemandeFamille> demandesFamille, int rang) {
    	SauvegardeDemandeFamille demandeFamilleRang = getSauvegardeFamilleRang(demandesFamille, rang);
    	if (demandeFamilleRang == null) {
    		return null;
    	}
    	
    	EtablissementSconet etablissement = demandeFamilleRang.getEtablissement();
    	if (etablissement == null) {
    		return null;
    	}
    	
    	return etablissement.getCodeRne();
    }
    
    /**
     * Récupère les motifs de dérogation de la demande famille correspondant au rang indiqué
     * 
     * @param demandesFamille
     * @param rang
     * @return
     */
    private String[] getDerogationMotifs(SortedSet<SauvegardeDemandeFamille> demandesFamille, int rang) {
    	SauvegardeDemandeFamille demandeFamilleRang = getSauvegardeFamilleRang(demandesFamille, rang);
    	if (demandeFamilleRang == null) {
    		return null;
    	}
    	
    	SortedSet<MotifDerog> motifs = demandeFamilleRang.getMotifs();
    	if (motifs == null) {
    		return null;
    	}
    	
    	return motifs.stream()
				.map(MotifDerog::getCode)
				.toArray(String[]::new);
    }   
    
    /**
     * Récupère la demande famille correspondant au rang indiqué
     * 
     * @param demandesFamille
     * @param rang
     * @return
     */
    private SauvegardeDemandeFamille getSauvegardeFamilleRang(SortedSet<SauvegardeDemandeFamille> demandesFamille, int rang) {
    	if (demandesFamille == null) {
    		return null;
    	}
    	
    	return demandesFamille.stream()
    			.filter(curDemandeFamille -> curDemandeFamille.getRang() == rang)
    			.findFirst().orElse(null);
    }

    /**
     * Ajoute une demande de famille à un élève.
     * 
     * @param eleve
     *            l'élève
     * @param codeUai
     *            Code rne de l'établissement
     * @param codeMotifs
     *            code des motifs de dérogation
     * @param rang
     *            rang de la demande
     * @param codeDeptMen
     *            code du département
     * @throws ApplicationException
     *             en cas d'erreur
     */
    private void ajouteDemandeFamille(Eleve eleve, String codeUai, String[] codeMotifs, short rang,
            String codeDeptMen) throws ApplicationException {
        if (StringUtils.isNotEmpty(codeUai) || (StringUtils.isEmpty(codeUai) && rang == 1)) {
            DemandeFamille demande = new DemandeFamille();
            demande.setRang(rang);

            String codeMef = eleve.getFormationDemandeSecteur().getCode();

            if (!ArrayUtils.isEmpty(codeMotifs) && !DemandeAffectationHelper.estFormationSegpaOuUlis(codeMef)
                    && !(eleve.getEstScolarisationDansCollegeSecteur() && Formation.CODE_MEF_6EME.equals(codeMef))) {
                for (String codeMotif : codeMotifs) {
                    // ajout du motif de dérogation
                    demande.ajouteMotifDerog(motifDerogManager.chargerByCode(codeMotif, codeDeptMen));
                }
            }

            if (StringUtils.isNotEmpty(codeUai)) {
                demande.setEtablissement(etablissementSconetManager.chargerParCodeRne(codeUai));
            } else {
                demande.setEtablissement(null);
            }

            demande.setEleve(eleve);
            eleve.addDemandeFamille(demande);
        }
    }

    @Override
    public void razSaisieChoixFamille(Eleve eleve, boolean avecTypeDemande) {
        LOG.debug("Remise à zéro des choix de la famille pour l'élève " + EleveHelper.getIdentite(eleve));
        if (avecTypeDemande) {
            eleve.setEstAffectationDemandeeCollegePublicDept(null);
        }
        eleve.setEstScolarisationDansCollegeSecteur(null);
        eleve.getDemandeFamilles().clear();
        eleve.setFormationDemandeSecteur(null);
        eleve.setLv1(null);
        eleve.setLv2(null);
        eleve.setLv3(null);
    }

    @Override
    public void restaurerChoixFamille(Eleve eleve, Log log) throws ApplicationException {
    	gereSaisieChoixFamille(eleve, log);
    }
}
