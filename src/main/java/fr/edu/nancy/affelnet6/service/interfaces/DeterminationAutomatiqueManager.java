/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.service.interfaces;

import java.util.List;
import java.util.Map;

import fr.edu.nancy.affelnet6.domain.calcul.DonneesCalculCollegeSecteur;
import fr.edu.nancy.affelnet6.domain.dossier.Eleve;
import fr.edu.nancy.affelnet6.domain.nomenclature.CommuneInseePtt;
import fr.edu.nancy.affelnet6.domain.nomenclature.EtatCollegeSecteur;
import fr.edu.nancy.affelnet6.domain.nomenclature.TronconCarteScolaire;
import fr.edu.nancy.commun.exceptions.ApplicationException;
import fr.edu.nancy.commun.exceptions.DaoException;
import fr.edu.nancy.commun.exceptions.TechnicalException;
import fr.edu.nancy.commun.utils.BatchProgressMonitor;

/**
 * Gestionnaire de détermination automatique du ou des collège(s) de secteur.
 */
public interface DeterminationAutomatiqueManager {
    /** Clé pour les résultats - Compteur d'élèves traités. */
    String KEY_CPT_ELEVE = "KEY_CPT_ELEVE";

    /** Clé pour les résultats - Compteur d'élèves dont le(s) collège(s) ont été déterminé(s) automatiquement. */
    String KEY_CPT_ELEVE_CLG_AUTO = "KEY_CPT_ELEVE_CLG_AUTO";

    /**
     * Clé pour les résultats - Compteur d'élèves dont le(s) collège(s) de secteur n'ont pas pu être déterminé(s)
     * automatiquement pour défaut de correspondance dans la carte scolaire.
     */
    String KEY_CPT_ELEVE_NON_ABOUTI = "KEY_CPT_ELEVE_NON_ABOUTI";

    /**
     * Clé pour les résultats - Compteur d'élèves dont le(s) collège(s) de secteur n'ont pas pu être déterminé(s)
     * automatiquement car soit l'adresse est vide ou n'a pas été traitée ou est non contrôlée,
     * soit la commune n'est pas renseignée.
     */
    String KEY_CPT_ELEVE_ADR_INCORRECTE = "KEY_CPT_ELEVE_ADR_INCORRECTE";

    /**
     * Clé pour les résultats - Compteur d'élèves dont le(s) collège(s) ont été déterminé(s) manuellement.
     */
    String KEY_CPT_ELEVE_DETERMINER_MANUEL = "KEY_CPT_ELEVE_DETERMINER_MANUEL";

    /**
     * Clé pour les résultats - Compteur d'élèves dont le(s) collège(s) de secteur a abouti hors du département.
     */
    String KEY_CPT_ELEVE_ABOUTI_HORS_DEPARTEMENT = "KEY_CPT_ELEVE_ABOUTI_HORS_DEPARTEMENT";

    /**
     * Objet indiquant le succès de la détermination automatique des collèges de secteur.
     */
    EtatCollegeSecteur CALCUL_AUTO_ABOUTI = new EtatCollegeSecteur(EtatCollegeSecteur.CALCUL_ABOUTI);
    /**
     * Objet indiquant l'échec de la détermination automatique des collèges de secteur.
     */
    EtatCollegeSecteur CALCUL_AUTO_NON_ABOUTI = new EtatCollegeSecteur(EtatCollegeSecteur.CALCUL_NON_ABOUTI);
    /**
     * Objet indiquant que le/les collège(s) de secteur a été positionné manuellement.
     */
    EtatCollegeSecteur SAISIE_MANUELLE = new EtatCollegeSecteur(EtatCollegeSecteur.SAISIE_MANUELLE);
    /**
     * Objet indiquant que la détermination automatique de collège de secteur est impossible.
     */
    EtatCollegeSecteur CALCUL_IMPOSSIBLE = new EtatCollegeSecteur(EtatCollegeSecteur.CALCUL_IMPOSSIBLE);
    /**
     * Objet indiquant que la détermination automatique de collège de secteur a abouti hors du département.
     */
    EtatCollegeSecteur CALCUL_ABOUTI_HORS_DEPARTEMENT = new EtatCollegeSecteur(
            EtatCollegeSecteur.CALCUL_ABOUTI_HORS_DEPARTEMENT);

    /**
     * Objet indiquant que la détermination automatique de collège de secteur est avortée.
     */
    EtatCollegeSecteur CALCUL_AVORTE = new EtatCollegeSecteur("AVORTE");

    /** Clé pour les résultats - Compteur communes manquantes de la carte scolaire. */
    String KEY_CPT_COMMUNES_MANQUANTES_CARTE_SCOLAIRE = "KEY_CPT_COMMUNES_MANQUANTES_CARTE_SCOLAIRE";

    /** Clé pour les résultats - Compteur voies manquantes de la carte scolaire. */
    String KEY_CPT_VOIES_MANQUANTES_CARTE_SCOLAIRE = "KEY_CPT_VOIES_MANQUANTES_CARTE_SCOLAIRE";

    /** Clé pour les résultats - Numéros de voies manquants de la carte scolaire. */
    String KEY_CPT_NUMEROS_VOIE_MANQUANTS_CARTE_SCOLAIRE = "KEY_CPT_NUMEROS_VOIE_MANQUANTS_CARTE_SCOLAIRE";

    /** Clé pour les résultats - Chevauchement de voies de la carte scolaire. */
    String KEY_CPT_CHEVAUCHEMENT_VOIES_CARTE_SCOLAIRE = "KEY_CPT_CHEVAUCHEMENT_VOIES_CARTE_SCOLAIRE";

    /**
     * Vérifie si on peut lancer le batch de détermination des collèges de secteur pour un département.
     * 
     * @param codeDeptMen
     *            Code MEN du département
     * @param simulation
     *            s'il s'agit d'une simulation de la determination
     * @throws ApplicationException
     *             En cas de problème
     * @throws TechnicalException
     *             En cas de problème
     * @return true si la détermination est possible
     */
    boolean verifierDeterminationAutomatiquePossible(String codeDeptMen, boolean simulation)
            throws ApplicationException, TechnicalException;

    /**
     * Méthode qui calcule le(s) collège(s) de secteur des élèves d'un département.
     * 
     * @param progMon
     *            Moniteur de progression du traitement
     * @param resultats
     *            table des données contenant le résultat du traitement
     * @param codeDeptMen
     *            Code MEN du département
     * @param codeRoleUtilisateur
     *            Code du rôle de l'utilisateur
     * @param simulation
     *            s'il s'agit d'une simulation
     * @throws ApplicationException
     *             En cas de problème
     * @throws TechnicalException
     *             En cas de problème
     */
    void determinationAuto(BatchProgressMonitor progMon, Map<String, Object> resultats, String codeDeptMen,
            String codeRoleUtilisateur, boolean simulation) throws ApplicationException, TechnicalException;

    /**
     * Détermine le collège de secteur par paquet de 100 élèves et renvoi le dernier numéro de dossier d'élève
     * traité.
     * 
     * @param resultats
     *            table des données contenant le résultat du traitement
     * @param elevesSansCollege
     *            liste des élèves sans collège de secteur pour lesquels on veut les calculer
     * @param codeDeptMen
     *            Code MEN du département
     * @param codeRoleUtilisateur
     *            Code du rôle de l'utilisateur
     * @param tronconCarteScolaireSecteurUnique
     *            table des tronçons de carte scolaire en secteur unique du département
     * @param listTronconsCarteScolaireSecteurNonUnique
     *            liste des tronçons de carte scolaire en secteur non unique du département
     * @param simulation
     *            si il s'agit d'une simulation
     * @return le numéro de dossier du dernier élève traité
     * @throws ApplicationException
     *             En cas de problème
     */
    int calculCollegeSecteur(
            Map<String, Object> resultats,
            List<Eleve> elevesSansCollege,
            String codeDeptMen,
            String codeRoleUtilisateur,
            Map<Integer, TronconCarteScolaire> tronconCarteScolaireSecteurUnique,
            Map<CommuneInseePtt, Map<String, List<TronconCarteScolaire>>> listTronconsCarteScolaireSecteurNonUnique,
            boolean simulation) throws ApplicationException;

    /**
     * Calcule le/les collèges de secteur d'un élève (résultat sauvegardé dans l'objet donné en paramètre).
     * 
     * @param donneesCalcul
     *            les données pour le calcul
     * @throws ApplicationException
     *             en cas d'erreur applicative
     */
    void calculCollegeSecteur(DonneesCalculCollegeSecteur donneesCalcul) throws ApplicationException;

    /**
     * Construit le compte rendu.
     *
     * @param resultats
     *            table des données contenant le résultat du traitement
     * @param simulation
     *            s'il s'agit d'une simulation
     * @return Compte rendu (HTML)
     */
    String composeCompteRendu(Map<String, Object> resultats, boolean simulation);

    /**
     * Extraction de la boucle de calcul du collège de secteur pour placer un pointcut REQUIRES_NEW.
     * 
     * @param progMon
     *            Moniteur de progression du traitement.
     * @param resultats
     *            table des données contenant le résultat du traitement.
     * @param codeDeptMen
     *            Code MEN du département.
     * @param codeRoleUtilisateur
     *            Code du rôle de l'utilisateur
     * @param numDossier
     *            le numéro de dossier du dernier élève traité lors de l'itération précédente
     * @param tronconCarteScolaireSecteurUnique
     *            table des tronçons de carte scolaire en secteur unique du département
     * @param listTronconsCarteScolaireSecteurNonUnique
     *            liste des tronçons de carte scolaire en secteur non unique du département
     * @param simulation
     *            s'il s'agit d'une simulation
     * @return le numéro de dossier du dernier élève traité dans cette itération
     * @throws DaoException
     *             En cas de problème.
     * @throws ApplicationException
     *             En cas de problème.
     */
    int elevesCalculCollegeSecteur(
            BatchProgressMonitor progMon,
            Map<String, Object> resultats,
            String codeDeptMen,
            String codeRoleUtilisateur,
            int numDossier,
            Map<Integer, TronconCarteScolaire> tronconCarteScolaireSecteurUnique,
            Map<CommuneInseePtt, Map<String, List<TronconCarteScolaire>>> listTronconsCarteScolaireSecteurNonUnique,
            boolean simulation) throws DaoException, ApplicationException;

    /**
     * Méthode qui récupère la liste des tronçons de carte scolaire en secteur unique d'un département.
     * 
     * @param codeDeptMen
     *            Code MEN du département.
     * @return la table des tronçons de carte scolaire en secteur unique du département
     * @throws ApplicationException
     *             En cas de problème.
     */
    Map<Integer, TronconCarteScolaire> chargeTronconSecteurUnique(String codeDeptMen) throws ApplicationException;

    /**
     * Vérifie si au moins un élève du département a un calcul de collège de secteur abouti.
     * 
     * @param codeDeptMen
     *            Code MEN du département
     * @throws DaoException
     *             En cas de problème
     * @return Vrai si au moins un élève du département a un calcul de collège de secteur abouti
     */
    boolean isEleveAvecCalculCollegeSecteurAbouti(String codeDeptMen) throws DaoException;

    /**
     * Vérifie si au moins un élève du département a un calcul de collège de secteur déjà effectué.
     * soit abouti ou abouti hors département ou non abouti ou impossible
     * 
     * @param codeDeptMen
     *            Code MEN du département
     * @throws DaoException
     *             En cas de problème
     * @return Vrai si au moins un élève du département a un calcul de collège de secteur abouti
     */
    boolean isEleveAvecCalculCollegeSecteurDejaEffectue(String codeDeptMen) throws DaoException;
}
