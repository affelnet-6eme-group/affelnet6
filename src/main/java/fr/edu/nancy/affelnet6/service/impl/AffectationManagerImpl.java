/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.edu.nancy.affelnet6.domain.dossier.DemandeAffectation;
import fr.edu.nancy.affelnet6.domain.dossier.Eleve;
import fr.edu.nancy.affelnet6.domain.nomenclature.EtatDecision;
import fr.edu.nancy.affelnet6.domain.nomenclature.MotifDerog;
import fr.edu.nancy.affelnet6.domain.nomenclature.OffreFormation;
import fr.edu.nancy.affelnet6.exceptions.DejaAffecteException;
import fr.edu.nancy.affelnet6.exceptions.LancementPreaffectationException;
import fr.edu.nancy.affelnet6.exceptions.ValidationAffectationException;
import fr.edu.nancy.affelnet6.helper.EleveHelper;
import fr.edu.nancy.affelnet6.service.dossier.DemandeAffectationManager;
import fr.edu.nancy.affelnet6.service.dossier.EleveManager;
import fr.edu.nancy.affelnet6.service.interfaces.AffectationManager;
import fr.edu.nancy.affelnet6.service.interfaces.AppAutomateManager;
import fr.edu.nancy.affelnet6.service.nomenclature.EtatDecisionManager;
import fr.edu.nancy.affelnet6.service.nomenclature.OffreFormationManager;
import fr.edu.nancy.affelnet6.utils.Message;
import fr.edu.nancy.commun.exceptions.ApplicationException;
import fr.edu.nancy.commun.exceptions.BusinessException;
import fr.edu.nancy.commun.utils.BatchProgressMonitor;

/** Gestionnaire d'affectation. */
@Service("affectationManager")
public class AffectationManagerImpl implements AffectationManager {

    /** Loggueur de la classe. */
    private static final Logger LOG = LoggerFactory.getLogger(AffectationManagerImpl.class);

    /** Le code de décision indéterminé, valable seulement pour un groupe de demandes. */
    private static final String DECISION_GROUPE_INDETERMINE = "ID";

    /** Gestionnaire d'automate. */
    @Autowired
    private AppAutomateManager automateManager;

    /** Gestionnaire des offres de formations (pour les informations d'arrêt de traitement). */
    @Autowired
    private OffreFormationManager offreFormationManager;

    /** Gestionnaire des états de décision. */
    @Autowired
    private EtatDecisionManager etatDecisionManager;

    /** Gestionnaire des demandes d'affectations. */
    @Autowired
    private DemandeAffectationManager demandeAffectationManager;

    /** Gestionnaire d'élèves. */
    @Autowired
    private EleveManager eleveManager;

    /**
     * Vérifie que l'état fonctionnel est en période d'affectation.
     * 
     * @param codeDeptMen
     *            code du département MEN
     * @return vrai si on est en affectation
     */
    private boolean verifieEtatAffectation(String codeDeptMen) {

        boolean etatAutomateOk = automateManager.isEtatFonctionnelDepartement(codeDeptMen,
                AppAutomateManager.ETAT_DEPT_AFFECTATION, true);

        if (!etatAutomateOk) {
            LOG.error("L'automate du département " + codeDeptMen + " n'est pas dans l'état d'affectation.");
            return false;
        }

        return true;
    }

    @Override
    public void verifierPreaffectationPossible(String codeDeptMen, BatchProgressMonitor bpm)
            throws BusinessException {

        /*
         * Pour pouvoir lancer une pré-affectation, il faut que :
         * - la saisie départementale soit validée par la DSDEN (=> état Affectation pour le département)
         * - les élèves habitants dans un secteur multi-collèges aient un collège de secteur désigné par l'IA-DASEN
         * - les demandes SEGPA prises aient un collège associé
         * - toutes les demandes de SEGPA aient une décision finale (pris ou refusé)
         */

        if (!verifieEtatAffectation(codeDeptMen)) {
            throw new LancementPreaffectationException(codeDeptMen,
                    "L'application n'est pas dans l'état 'Affectation'.");
        }

        if (existenceElevesSansCollegeSecteurDept(codeDeptMen)) {
            throw new LancementPreaffectationException(codeDeptMen,
                    "Il reste des élèves sans collège de secteur désigné. Utilisez la page "
                            + "\"Secteurs multi-collèges\" pour définir les collèges de secteur des élèves.");
        }

        if (existenceDemandesSegpaBanaliseesDept(codeDeptMen)) {
            throw new LancementPreaffectationException(codeDeptMen,
                    "Il reste des demandes SEGPA prises sans collège associé.");
        }

        if (existenceDemandesSegpaSansDecisionFinale(codeDeptMen)) {
            throw new LancementPreaffectationException(codeDeptMen, "Il reste des demandes SEGPA non traitées.");
        }
    }

    /**
     * Vérifie s'il y a des élèves sans collège de secteur final désigné par l'IA-DASEN .
     * 
     * @param codeDeptMen
     *            code du département MEN
     * @return vrai s'il existe des élèves sans collège de secteur final désigné par l'IA-DASEN
     */
    private Boolean existenceElevesSansCollegeSecteurDept(String codeDeptMen) {
        int nb = eleveManager.compteElevesSansCollegeSecteurFinalAvecCollegesSecteursPotentiels(codeDeptMen);
        LOG.debug("Nombre d'élèves sans collège de secteur désigné par l'IA-DASEN et "
                + "avec au moins un collège de secteur potentiel pour le département " + codeDeptMen + " : " + nb);
        return (nb != 0);
    }

    /**
     * Vérifie s'il y a des demandes d'affectation en 6ème Segpa sans offre associee.
     * 
     * @param codeDeptMen
     *            code du département MEN
     * @return vrai s'il existe des demande SEGPA sans offre
     */
    private boolean existenceDemandesSegpaBanaliseesDept(String codeDeptMen) {

        int nb = demandeAffectationManager.compteDemandesSegpaBanaliseesPrises(codeDeptMen);
        LOG.debug("Nombre de demandes de SEGPA banalisées sans offre pour le département " + codeDeptMen + " : "
                + nb);
        return (nb != 0);
    }

    /**
     * Vérifie si des demandes d'affectation en 6ème Segpa n'ont pas de décision finale.
     * 
     * @param codeDeptMen
     *            code du département MEN
     * @return vrai s'il existe des demande SEGPA sans décision finale
     */
    private boolean existenceDemandesSegpaSansDecisionFinale(String codeDeptMen) {

        int nb = demandeAffectationManager.compteDemandesSegpaSansDecisionFinale(codeDeptMen);
        LOG.debug("Nombre de demandes de SEGPA sans décision finale pour le département " + codeDeptMen + " : "
                + nb);
        return (nb != 0);
    }

    @Override
    public void verifieValidationAffectationPossible(String codeDeptMen, BatchProgressMonitor bpm)
            throws ApplicationException {

        // Audit avant de pouvoir lancer une validation d'affectation

        // Il faut être en période d'affectation
        bpm.beginTask("Contrôle de l'état applicatif");
        if (!verifieEtatAffectation(codeDeptMen)) {

            throw new ValidationAffectationException(codeDeptMen,
                    "L'application n'est pas dans l'état 'Affectation'.");
        }
        bpm.endTask();

        // Vérifie qu'il y a des décisions finales sur toutes les demandes
        bpm.beginTask("Contrôle des décisions sur les demandes d'affectation");
        if (!controleDemandesToutesFinales(bpm, codeDeptMen)) {

            throw new ValidationAffectationException(codeDeptMen,
                    "Il reste des demandes non traitées ou en attente de décision.");
        }
        bpm.endTask();
    }

    @Override
    public void reinitialiseAffectation(String codeDeptMen) throws ApplicationException {

        List<DemandeAffectation> demandes = demandeAffectationManager.listerDemandesDepartement(codeDeptMen);

        EtatDecision decNT = etatDecisionManager.charger(EtatDecision.CODE_NON_TRAITE);

        for (DemandeAffectation demande : demandes) {
            demande.setEtatDemande(decNT);
            demandeAffectationManager.maj(demande);
        }

        demandeAffectationManager.majCompteursOffres(codeDeptMen, true, true, null);
    }

    /**
     * @throws ApplicationException
     *             en cas d'erreur
     */
    @Override
    public void saisieDecisionManuelle(Long idDemande, String codeDecisionManuelle, List<Message> msgAvertissement)
            throws ApplicationException {

        traitementSaisieDecisionManuelle(idDemande, codeDecisionManuelle, msgAvertissement);
    }

    /**
     * @throws ApplicationException
     *             en cas d'erreur
     */
    @Override
    public void saisieDecisionManuelleNouvelleTransaction(Long idDemande, String codeDecisionManuelle,
            List<Message> msgAvertissement) throws ApplicationException {

        traitementSaisieDecisionManuelle(idDemande, codeDecisionManuelle, msgAvertissement);
    }

    /**
     * Traitement de la saisie manuelle de décision.
     * 
     * @param codeDecisionManuelle
     *            code de la décision manuelle
     * @param msgAvertissement
     *            Fournir une liste vide (mais pas <code>null</code>)
     *            que la méthode pourra remplir avec des messages
     *            d'avertissement
     * @param idDemande
     *            demande d'affectation
     * @throws ApplicationException
     *             Exception levée en cas de problème
     */
    private void traitementSaisieDecisionManuelle(long idDemande, String codeDecisionManuelle,
            List<Message> msgAvertissement) throws ApplicationException {

        DemandeAffectation demande = demandeAffectationManager.charger(idDemande);
        Eleve eleve = demande.getNumDossier();

        // TODO on pourrait ne faire le contrôle ci-dessous que quand on veut "prendre manuellement"
        // (cad qu'il faut déplacer ce contrôle dans saisieDecisionManuelleAffecter()
        // et dans les saisieDecisionManuelle*() remplacer isRefuseManuellement() par un
        // hasDecisionManuelle()

        // dans tous les cas on vérifie si l'élève n'est pas déjà
        // affecté manuellement sur une autre demande
        // Si c'est le cas, il n'est pas possible de modifier la
        // décision courante tant que l'autre demande n'a pas changé.
        if (isDejaAffecteManuellementAutresDemandes(eleve, demande)) {
            throw new DejaAffecteException();
        }

        // Annuler la décision
        if (EtatDecision.CODE_NON_TRAITE.equals(codeDecisionManuelle)) {
            saisieDecisionManuelleAnnuler(demande);
        }
        // Affecter
        else if (EtatDecision.CODE_PRIS_MANUEL.equals(codeDecisionManuelle)) {
            saisieDecisionManuelleAffecter(demande);
        }
        // Refuser
        else if (EtatDecision.CODE_REFUSE_MANUEL.equals(codeDecisionManuelle)) {
            saisieDecisionManuelleRefuser(demande, msgAvertissement);
        } else {
            throw new IllegalArgumentException("La valeur '" + codeDecisionManuelle
                    + "' de l'argument codeDecisionManuelle est incorrecte.");
        }
    }

    /**
     * Saisie manuelle de l'annulation de la décision sur une demande d'affectation.
     * 
     * @param demande
     *            Demande d'affectation concernée
     * @throws ApplicationException
     *             Exception levée en cas de problème
     */
    private void saisieDecisionManuelleAnnuler(DemandeAffectation demande) throws ApplicationException {

        Eleve eleve = demande.getNumDossier();

        EtatDecision etatNonTraite = etatDecisionManager.charger(EtatDecision.CODE_NON_TRAITE);
        EtatDecision etatHorsAffectation = etatDecisionManager.charger(EtatDecision.CODE_HORS_AFFECTATION);

        // la demande passe à l'état Non traité
        demande.setEtatDemande(etatNonTraite);
        demandeAffectationManager.maj(demande);
        demandeAffectationManager.majCompteursOffre(demande.getOffreformation(), false, true);

        // parcours des demandes de l'élève
        Set<DemandeAffectation> demandesEleve = eleve.getDemandeAffectation();
        for (DemandeAffectation d : demandesEleve) {
            // si la demande courante n'est pas la demande
            // que l'on passe à Non traité
            // et n'est pas à l'état Refusé manuellement
            // alors on passe son état à Non traité
            if (!demande.equals(d) && !isRefuseManuellement(d)) {
                // traitement du cas particulier d'un élève qui a fait
                // initialement une demande hors collège public du département,
                // à qui on a proposé une SEGPA, qu'il refuse
                // => il faut que la demande hors collège public du département
                // passe à l'état HA et non NT
                if (d.isHorsPublicDepartmental()) {
                    d.setEtatDemande(etatHorsAffectation);
                }
                // cas général => non traité
                else {
                    d.setEtatDemande(etatNonTraite);
                }

                demandeAffectationManager.maj(d);
                demandeAffectationManager.majCompteursOffre(d.getOffreformation(), false, true);
            }
        }
    }

    /**
     * Saisie de l'affectation manuelle sur une demande.
     * 
     * @param demande
     *            Demande d'affectation concernée
     * @throws ApplicationException
     *             Exception levée en cas de problème
     */
    private void saisieDecisionManuelleAffecter(DemandeAffectation demande) throws ApplicationException {

        Eleve eleve = demande.getNumDossier();

        EtatDecision etatPrisManuel = etatDecisionManager.charger(EtatDecision.CODE_PRIS_MANUEL);
        EtatDecision etatQuitteOffre = etatDecisionManager.charger(EtatDecision.CODE_QUITTE_OFFRE);

        // la demande passe à l'état Pris manuellement
        demande.setEtatDemande(etatPrisManuel);
        demandeAffectationManager.maj(demande);
        demandeAffectationManager.majCompteursOffre(demande.getOffreformation(), false, true);

        // parcours des demandes de l'élève
        Set<DemandeAffectation> demandesEleve = eleve.getDemandeAffectation();
        for (DemandeAffectation d : demandesEleve) {
            // si la demande courante n'est pas la demande prise
            // et n'est pas à l'état Refusé manuellement
            // alors on passe l'état à Quitte le secteur / Pris ailleurs
            if (!demande.equals(d) && !isRefuseManuellement(d)) {
                d.setEtatDemande(etatQuitteOffre);
                demandeAffectationManager.maj(d);
                demandeAffectationManager.majCompteursOffre(d.getOffreformation(), false, true);
            }
        }
    }

    /**
     * Saisie du refus manuel sur une demande.
     * 
     * @param demande
     *            Demande d'affectation concernée
     * @param msgAvertissement
     *            Ajoute un message d'avertissement si, à l'issu de
     *            la modification, toutes les demandes de l'élève
     *            sont à l'état refusé.
     * @throws ApplicationException
     *             Exception levée en cas de problème
     */
    private void saisieDecisionManuelleRefuser(DemandeAffectation demande, List<Message> msgAvertissement)
            throws ApplicationException {

        Eleve eleve = demande.getNumDossier();

        EtatDecision etatRefuseManuel = etatDecisionManager.charger(EtatDecision.CODE_REFUSE_MANUEL);
        EtatDecision etatNonTraite = etatDecisionManager.charger(EtatDecision.CODE_NON_TRAITE);
        EtatDecision etatHorsAffectation = etatDecisionManager.charger(EtatDecision.CODE_HORS_AFFECTATION);

        // la demande passe à l'état Refusé manuellement
        demande.setEtatDemande(etatRefuseManuel);
        demandeAffectationManager.maj(demande);
        demandeAffectationManager.majCompteursOffre(demande.getOffreformation(), false, true);

        // parcours des demandes de l'élève
        Set<DemandeAffectation> demandesEleve = eleve.getDemandeAffectation();
        for (DemandeAffectation d : demandesEleve) {
            // si demande courante n'est pas la demande refusée
            // et n'est pas à l'état Refusé manuellement
            // alors on la passe en Non traité
            if (!demande.equals(d) && !isRefuseManuellement(d)) {
                // traitement du cas particulier d'un élève qui a fait
                // initialement une demande hors collège public du département,
                // à qui on a proposé une SEGPA, qu'il refuse
                // => il faut que la demande hors collège public du département
                // passe à l'état HA et non NT
                if (d.isHorsPublicDepartmental()) {
                    d.setEtatDemande(etatHorsAffectation);
                }
                // cas général => non traité
                else {
                    d.setEtatDemande(etatNonTraite);
                }

                demandeAffectationManager.maj(d);
                demandeAffectationManager.majCompteursOffre(d.getOffreformation(), false, true);
            }
        }

        if (isRefusePartout(eleve)) {

            String msg = "Toutes les demandes de l'élève " + EleveHelper.getIdentite(eleve)
                    + ") sont refusées. Si " + "ses demandes restent dans l'état actuel, l'élève n'aura pas "
                    + "d'affectation dans un collège.";
            LOG.warn(msg);

            if (msgAvertissement != null) {
                Message message = new Message("warning.affectation.refusePartout", eleve.getPrenom(),
                        eleve.getNom(), eleve.getIne());
                msgAvertissement.add(message);
            }
        }
    }

    /**
     * Donne si l'élève a une demande distincte de <code>demande</code>)
     * qui est dans l'état Affecté manuellement.
     * 
     * @param eleve
     *            Élève concerné
     * @param demande
     *            demande exclue (Ne doit pas être <code>null</code>).
     * @return vrai si l'élève a une autre affectation manuelle
     */
    private static boolean isDejaAffecteManuellementAutresDemandes(Eleve eleve, DemandeAffectation demande) {

        Set<DemandeAffectation> demandes = eleve.getDemandeAffectation();

        // parcours des demandes
        if (demandes != null && demandes.size() > 0) {
            for (DemandeAffectation d : demandes) {
                // si demande courante n'est pas la demande exclue
                if (!demande.equals(d)) {
                    EtatDecision etat = d.getEtatDemande();
                    // si code état à Affecté
                    // alors on renvoie vrai
                    if (etat != null && EtatDecision.CODE_PRIS_MANUEL.equals(etat.getCode())) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Donne si toutes les demandes d'un élève sont refusées.
     * 
     * @param eleve
     *            Doit avoir une ou plusieurs demandes (sinon renvoie
     *            une <code>IllegalArgumentException</code>).
     * @return vrai si l'élève est refusé sur toutes ses demandes
     */
    private static boolean isRefusePartout(Eleve eleve) {
        Set<DemandeAffectation> demandes = eleve.getDemandeAffectation();

        if (demandes == null || demandes.size() == 0) {
            throw new IllegalArgumentException("L'élève doit avoir au moins une demande.");
        }

        // parcours des demandes
        for (DemandeAffectation demande : demandes) {
            EtatDecision etat = demande.getEtatDemande();

            // si on trouve une demande dont l'état n'est pas "refusé"
            // alors toutes les demandes ne sont pas à l'état "refusé"
            if (etat == null || !EtatDecision.CODE_ETAT_FINAL_REFUSE.equals(etat.getCodeEtatFinal())) {
                return false;
            }
        }

        return true;
    }

    /**
     * Donne si l'état de décision d'une demande est Refusé manuellement.
     * 
     * @param demande
     *            Demande concernée
     * @return vrai si la demande est refusée manuellement
     */
    private static boolean isRefuseManuellement(DemandeAffectation demande) {
        EtatDecision etat = demande.getEtatDemande();

        if (etat == null) {
            return false;
        }

        return EtatDecision.CODE_REFUSE_MANUEL.equals(etat.getCode());
    }

    // MARQUEUR DE DEBUT DE SYNCHRO AFFELNET6EME - AFFECT6

    @Override
    public void affectationAuto(BatchProgressMonitor progMon, Map<String, Object> resultats, String codeDeptMen)
            throws ApplicationException {

        if (LOG.isDebugEnabled()) {
            LOG.debug("************************************************************");
            LOG.debug("* Pré-affectation automatique sur le département " + codeDeptMen);
            LOG.debug("************************************************************");
        }

        // On construit une table donnant la liste des demandes pour chaque offre

        progMon.beginTask("Chargement des demandes du département");

        Map<OffreFormation, List<DemandeAffectation>> demandesParOffre = chargeTableDemandesParOffre(codeDeptMen,
                resultats, progMon);

        int nbOffres = demandesParOffre.keySet().size();
        int nbOffresTraitees = 0;

        progMon.endTask();

        // Table associant deux compteurs à chaque offre de formation
        // * Nombre de de places manquantes pour affecter les demandes qui pourraient être prises
        // * Nombre de demandes qui pourraient être prises

        Map<OffreFormation, int[]> cptDemandesRestantes = new HashMap<OffreFormation, int[]>();

        // Table donnant le dernier bloc (rang/priorité) pris par offre de formation
        Map<OffreFormation, int[]> infoBlocPris = new HashMap<OffreFormation, int[]>();

        boolean uneDecisionAChange = true;
        int iteration = 1;

        // Préparation des variables utilisées
        DemandeAffectation premiereDemandeGroupe = null;
        DemandeAffectation demandeExaminee = null;
        DemandeAffectation demandeSuivante = null;
        int priGroupe = -1;
        int rangGroupe = -1;

        // On itère tant que des demandes on changé d'état de décision
        while (uneDecisionAChange) {

            LOG.debug("**** Itération " + iteration + " ****************************");
            progMon.beginTask("Itération " + iteration);

            uneDecisionAChange = false;

            nbOffresTraitees = 0;

            // On itère sur les offres de formation de la table demandesParOffre
            for (Entry<OffreFormation, List<DemandeAffectation>> demande : demandesParOffre.entrySet()) {

                OffreFormation of = demande.getKey();
                // Récupérer la liste de travail des demandes liées à l'offre
                List<DemandeAffectation> listDemandeOffre = demande.getValue();

                LOG.debug("** Offre examinée " + of.getCod() + " **");

                int nbDemandesOffre = listDemandeOffre.size();

                // Trace des demandes de taille réduite en debug
                if (LOG.isDebugEnabled() && (nbDemandesOffre <= 10)) {
                    for (DemandeAffectation demPrint : listDemandeOffre) {
                        LOG.debug(" - " + demPrint.toGuruString());
                    }
                }

                // Initialisation du nombre de places restantes à partir de la capacité d'accueil
                int nbrPlacesRestantes = of.getCapAcc();

                // On retire le nombre de demandes prises manuellement
                nbrPlacesRestantes -= getNbrDemandesForceesPris(listDemandeOffre);

                LOG.debug("Demandes " + nbDemandesOffre + ", placesRestantes " + nbrPlacesRestantes);

                // Indice de la demande courante
                int indiceDemandeCourant = 0;

                // La liste des demandes du groupe courant.
                // Elle constitue le groupe de même (priorité, rang) qu'on essaye d'affecter.
                List<DemandeAffectation> listDemandesGpeCourant = new ArrayList<DemandeAffectation>();

                // Mémorise la première demande du groupe courant
                premiereDemandeGroupe = null;

                // On parcourt la liste des demandes
                // tant qu'il y des demandes et de la place
                boolean plusDePlace = false;

                while ((indiceDemandeCourant < nbDemandesOffre) && !plusDePlace) {

                    // Récupérer la demande courante
                    demandeExaminee = listDemandeOffre.get(indiceDemandeCourant);

                    // Si la demande est la première du groupe
                    if (premiereDemandeGroupe == null) {
                        // On la mémorise ainsi que sa priorité et son rang
                        premiereDemandeGroupe = demandeExaminee;
                        priGroupe = demandeAffectationManager.niveauPrioritePlusHaute(demandeExaminee);
                        rangGroupe = demandeExaminee.getRang();

                        LOG.debug("Nouveau groupe pri = " + priGroupe + ", rang = " + rangGroupe);
                    }

                    // On ajoute la demande à la liste de demandes du groupe courant
                    listDemandesGpeCourant.add(demandeExaminee);

                    // Récupère la demande suivante afin de déterminer si le groupe est complet,
                    // si il a la même priorité.
                    demandeSuivante = null;
                    if ((indiceDemandeCourant + 1) < nbDemandesOffre) {
                        demandeSuivante = listDemandeOffre.get(indiceDemandeCourant + 1);
                    }

                    // Le groupe est complet si la demande suivante n'existe pas, change priorité ou de rang.
                    boolean groupeComplet = (demandeSuivante == null)
                            || (demandeAffectationManager.niveauPrioritePlusHaute(demandeSuivante) != priGroupe)
                            || (demandeSuivante.getRang() != rangGroupe);

                    // Si le groupe est complet
                    if (groupeComplet) {

                        // On va regarder si on peut affecter le groupe dans sa globalité

                        // Regarde le nombre de demandes pouvant être prises automatiquement
                        int nbrDemPrisePossibleAuto = getNbrDemandePriseAutoPossible(listDemandesGpeCourant);

                        LOG.debug("On tente de prendre en auto " + nbrDemPrisePossibleAuto + " demandes sur "
                                + nbrPlacesRestantes + " places");

                        // Teste s'il reste assez de place pour toutes les demandes
                        if (nbrDemPrisePossibleAuto <= nbrPlacesRestantes) {

                            // Oui, il y a assez de place pour affecter le groupe
                            LOG.debug("Affectation du groupe");

                            // On change la décision en pris pour toutes les demandes que l'on peut
                            for (DemandeAffectation demandeAPrendre : listDemandesGpeCourant) {

                                if (estDemandePriseAutoPossible(demandeAPrendre)) {

                                    if (changeDecisionAuto(demandeAPrendre, EtatDecision.CODE_PRIS_AUTO)) {
                                        uneDecisionAChange = true;
                                    }
                                    nbrPlacesRestantes--;

                                } // if -- demande prise possible
                            } // for -- demandes du groupe

                            // Remet à null la première demande du groupe
                            premiereDemandeGroupe = null;

                            // Toutes les demandes sont affectés, donc on
                            // supprime le compteur des demandes restantes pour cette offre
                            cptDemandesRestantes.remove(of);

                            infoBlocPris.put(of, new int[] { priGroupe, rangGroupe });

                        } else {

                            // Il n'y a plus de place pour affecter le groupe
                            plusDePlace = true;

                            LOG.debug("Groupe non affectable : " + nbrDemPrisePossibleAuto
                                    + " demandes décidables auto pour " + nbrPlacesRestantes + " places");

                            // On calcule les compteurs pour l'offre courante
                            int[] tab = new int[2];

                            // Nombre de de places manquantes pour affecter les demandes
                            // qui pourraient être prises en auto
                            tab[0] = -(nbrPlacesRestantes - nbrDemPrisePossibleAuto);

                            // nombre de possibilités de prises en auto
                            tab[1] = nbrDemPrisePossibleAuto;

                            cptDemandesRestantes.put(of, tab);

                        } // if -- test place suffisante pour tout le groupe

                        // On vide la liste correspondant au groupe de demandes
                        listDemandesGpeCourant.clear();

                    } // if -- groupe complet

                    // Avance l'indice sur les demandes en cours
                    indiceDemandeCourant++;

                } // while -- il y a de la place sur cette offre

                // Compte le nombre de pris sur l'offre
                int nbrPris = 0;
                for (DemandeAffectation demTmp : listDemandeOffre) {
                    if (demTmp.getEtatDemande().isDecisionPris()) {
                        nbrPris++;
                    }
                } // for -- demandes sur l'offre courante

                // Reste-t-il des demandes à traiter ?
                if (premiereDemandeGroupe != null) {

                    // Tente de prendre une décision globale pour tous les restants
                    String decisionDemandesRestantes = DECISION_GROUPE_INDETERMINE;

                    // Si toutes les places sont prises alors
                    // la préaffectation est terminée pour cette offre
                    if (nbrPris == of.getCapAcc()) {

                        // On essayera de mettre les demandes restantes à refusé
                        decisionDemandesRestantes = EtatDecision.CODE_REFUSE_AUTO;

                    } else if (plusDePlace) {

                        // On ne peut pas choisir qui affecter au sein du groupe courant
                        // => Mise en attente des demandes s'il n'y a pas assez de place
                        decisionDemandesRestantes = EtatDecision.CODE_ATTENTE;
                    }

                    // Applique la décision aux demandes restantes
                    // (à partir de premiereDemandeGroupe)
                    if (!decisionDemandesRestantes.equals(DECISION_GROUPE_INDETERMINE)) {

                        for (int k = listDemandeOffre.indexOf(premiereDemandeGroupe); k < nbDemandesOffre; ++k) {
                            DemandeAffectation demTmp = listDemandeOffre.get(k);

                            // Si la demande est modifiable de façon automatique
                            if (estDemandePriseAutoPossible(demTmp)
                                    && changeDecisionAuto(demTmp, decisionDemandesRestantes)) {
                                uneDecisionAChange = true;

                            } // if -- estDemandePriseAutoPossible
                        } // for -- demandes restantes

                    } // if -- codeDecisionRestant

                } // if -- il reste des demandes

                progMon.setProgression(nbOffresTraitees++, nbOffres);

            } // for -- offres de formations

            if (uneDecisionAChange) {
                iteration++;
            }

            progMon.endTask();

        } // while -- uneDecisionAChange

        resultats.put(RES_KEY_ITERATION_COUNT, Integer.valueOf(iteration));

        // Changer la décision en pris sur chaque offre
        // pour toutes les demandes de secteur restantes sans autre possiblité
        LOG.debug("Traitement des demandes restantes sur les offres");
        progMon.beginTask("Traitement des demandes restantes sur les offres.");
        nbOffresTraitees = 0;

        EtatDecision etatDem;

        for (Entry<OffreFormation, List<DemandeAffectation>> demande : demandesParOffre.entrySet()) {

            for (DemandeAffectation demandeRestante : demande.getValue()) {

                // S'il s'agit-il de la demande de secteur restante ?
                if (isDemandeSecteurRestante(demandeRestante.getNumDossier(), demandeRestante)) {

                    // Regarde l'état de la demande
                    etatDem = demandeRestante.getEtatDemande();

                    if (etatDem.isDecisionPrisAilleurs()) {

                        // l'élève est pris ailleurs
                        LOG.debug("L'élève est pris sur une autre demande");

                    } else if (etatDem.isDecisionManuelleRefuse()) {

                        // l'élève est refusé manuellement sur son offre de secteur
                        LOG.debug("Impossible de prendre la demande de secteur restante "
                                + demandeRestante.toGuruString());

                    } else if (etatDem.isDecisionManuellePris()) {

                        // l'élève est explicitement pris sur son offre de secteur
                        LOG.debug("L'élève est déjà pris sur sa demande de secteur restante "
                                + demandeRestante.toGuruString());

                    } else {
                        // Change la décision en prise s'il s'agit de la demande de secteur restante
                        changeDecisionAuto(demandeRestante, EtatDecision.CODE_PRIS_AUTO);

                    } // if -- test forcé refusé

                }
            } // for -- demandes de l'offre courante

            // Maj progression
            progMon.setProgression(nbOffresTraitees++, nbOffres);

        } // for -- offres de formations

        progMon.endTask();

        // Mise à jour des infos de dernier bloc pris
        LOG.debug("Mise à jour des infos de dernier bloc pris");
        progMon.beginTask("Mise à jour des infos de dernier bloc pris");

        nbOffresTraitees = 0;
        int[] stopInfos;
        for (Map.Entry<OffreFormation, int[]> ent : infoBlocPris.entrySet()) {
            stopInfos = ent.getValue();
            offreFormationManager.changerInformationsArretPreaffectationOffre(ent.getKey(), stopInfos[0],
                    stopInfos[1]);
        }
        progMon.endTask();
    }

    /**
     * Charge une table des demandes d'affectation par offre.
     * 
     * @param codeDeptMen
     *            code du département MEN
     * @param resultats
     *            table des résultats du traitement à compléter
     * @param progMon
     *            progress monitor pour le batch
     * @return table des demandes d'affectation
     * @throws ApplicationException
     *             en cas de problème
     */
    private Map<OffreFormation, List<DemandeAffectation>> chargeTableDemandesParOffre(String codeDeptMen,
            Map<String, Object> resultats, BatchProgressMonitor progMon) throws ApplicationException {

        LOG.debug("Constitution des tables de demandes par offre");
        Map<OffreFormation, List<DemandeAffectation>> demandesParOffre;
        demandesParOffre = new HashMap<OffreFormation, List<DemandeAffectation>>();

        // Liste toutes les demandes
        progMon.beginTask("Chargement depuis la base de données");
        List<DemandeAffectation> toutesDemandes = demandeAffectationManager.listerDemandesDepartement(codeDeptMen);
        progMon.endTask();

        List<DemandeAffectation> lst;

        // Ventile sur les offres
        for (DemandeAffectation de : toutesDemandes) {

            OffreFormation of = de.getOffreformation();
            lst = demandesParOffre.get(of);

            if (lst == null) {
                // L'offre n'existe pas encore dans la table => on la crée
                lst = new LinkedList<DemandeAffectation>();
                demandesParOffre.put(of, lst);
            }

            lst.add(de);

        }

        progMon.beginTask("Tri des demandes par offre de formation");
        LOG.debug("Tri des demandes sur chaque offre");
        int nbTraite = 0;
        // On trie les listes de demandes selon le comparateur
        for (List<DemandeAffectation> lst2 : demandesParOffre.values()) {
            Collections.sort(lst2, new DemandeAffectationComparatorPrioriteRang());
            progMon.setProgression(nbTraite++, demandesParOffre.size());
        }
        progMon.endTask();

        resultats.put(RES_KEY_NB_OFFRES, Integer.valueOf(demandesParOffre.keySet().size()));
        resultats.put(RES_KEY_NB_DEMANDES, Integer.valueOf(toutesDemandes.size()));

        return demandesParOffre;

    }

    /**
     * Fixe la décision liée à une demande d'affectation et met à jour les autres demandes de l'élève.
     * 
     * @param demande
     *            la demande d'affectation à modifier
     * @param decisionDemandee
     *            le code de la décision à appliquer
     * @return vrai si une décision a été modifiée, sinon faux
     * @throws ApplicationException
     *             en cas de problème
     */
    public boolean changeDecisionAuto(DemandeAffectation demande, String decisionDemandee)
            throws ApplicationException {

        // Teste si la demande peut être prise/refusée de façon automatique
        if (!estDemandePriseAutoPossible(demande)) {
            LOG.error("Impossible d'attribuer automatiquement la décision " + decisionDemandee + " à la demande "
                    + demande.toGuruString());
            throw new BusinessException("La demande " + demande.toPrettyString()
                    + " ne peut pas de recevoir la décision '" + decisionDemandee + "'");
        }

        // Si la décision ne change pas, on ne fait rien
        if (demande.getEtatDemande().getCode().equals(decisionDemandee)) {
            LOG.debug("La décision de la demande " + demande.getId() + " est déjà en " + decisionDemandee);
            return false;
        }

        // On ne touche pas aux décisions manuelles
        if (demande.getEtatDemande().isDecisionManuelle()) {
            LOG.debug("Il a déjà une décision manuelle sur la demande " + demande);
            return false;
        }

        // Récupère le dossier élève
        Eleve elv = demande.getNumDossier();

        // Si l'on cherche à prendre l'élève sur une décision et qu'une demande
        // de rang inférieur n'est pas fixée, alors cette demande doit être mise en attente
        // au lieu d'être prise
        if ((decisionDemandee.equals(EtatDecision.CODE_PRIS_AUTO)) && isDecisionIndetRangInferieur(elv, demande)) {
            return changeDecisionAuto(demande, EtatDecision.CODE_ATTENTE);
        }

        // On fixe la décision
        changerEtatDemande(demande, decisionDemandee);

        // Prépare la décision à prendre sur les autre demandes restantes
        String decisionAutres;
        if (decisionDemandee.equals(EtatDecision.CODE_PRIS_AUTO)) {
            decisionAutres = EtatDecision.CODE_QUITTE_OFFRE;
        } else if (decisionDemandee.equals(EtatDecision.CODE_ATTENTE)) {
            decisionAutres = EtatDecision.CODE_ATTENTE;
        } else if (decisionDemandee.equals(EtatDecision.CODE_REFUSE_AUTO)) {
            decisionAutres = EtatDecision.CODE_NON_TRAITE;
        } else {
            // Par sécurité, on ne fait rien pour les autres décisions
            // mais le cas ne doit pas se produire ...
            return true;
        }

        // Mise à jour des décisions pour les demandes restantes de l'élève de rang supérieur
        for (DemandeAffectation autreDemande : elv.getDemandeAffectation()) {

            EtatDecision etatAutreDemande = autreDemande.getEtatDemande();

            // On ne touche pas aux décisions manuelles
            // ni aux demandes de rang inférieur
            if ((autreDemande.getRang() > demande.getRang()) && !etatAutreDemande.isDecisionManuelle()) {

                changerEtatDemande(autreDemande, decisionAutres);

            }
        } // for -- maj autres demandes

        // Au moins une décision a changé
        return true;
    }

    /**
     * Change l'état d'une demande.
     * 
     * @param demande
     *            demande concernée
     * @param decision
     *            code de la nouvelle décision à appliquer
     * @return vrai si la décision a été modifiée
     * @throws ApplicationException
     *             en cas de problème
     */
    public boolean changerEtatDemande(DemandeAffectation demande, String decision) throws ApplicationException {

        // Regarde s'il y a une décision sur la demande
        EtatDecision etatCourant = demande.getEtatDemande();

        // Si une décision existe et a le même code, alors on ne change rien
        if ((etatCourant != null) && (etatCourant.getCode().equals(decision))) {
            return false;
        }

        LOG.debug("Attribution de la décision '" + decision + "' à la demande " + demande.toGuruString());

        // Charge la nouvelle décision
        EtatDecision nouvelEtat = etatDecisionManager.charger(decision);
        demande.setEtatDemande(nouvelEtat);
        demandeAffectationManager.majSansFlush(demande);
        return true;
    }

    /**
     * Cette méthode indique si la demande spécifiée est la seule possibilité d'affectation.
     * 
     * @param eleve
     *            Élève concerné
     * @param demande
     *            demande examinée
     * @return vrai si la demande est la seule possiblité d'affectation
     */
    public boolean isDemandeSecteurRestante(Eleve eleve, DemandeAffectation demande) {

        // Teste si la demande est de secteur
        if (!estDemandeSecteur(demande)) {
            // Ce n'est pas une demande de secteur
            return false;
        }

        // On examine les demandes de l'élève
        Set<DemandeAffectation> demandes = eleve.getDemandeAffectation();

        // Parcours des demandes
        for (DemandeAffectation autreDemande : demandes) {

            // Considère les demandes de rang inférieur
            if (autreDemande.getRang() < demande.getRang()) {

                // Regarde l'état de la demande
                String codeEtatAutreDemande = autreDemande.getEtatDemande().getCode();

                boolean autreDemandeRefusee = codeEtatAutreDemande.equals(EtatDecision.CODE_REFUSE_MANUEL)
                        || codeEtatAutreDemande.equals(EtatDecision.CODE_REFUSE_AUTO);

                if (!autreDemandeRefusee) {
                    // L'autre demande est de rang inférieur et n'est pas refusée
                    return false;
                }

            }// if -- rang inférieur

        } // for -- demandes
        return true;
    }

    /**
     * Teste si l'élève a une demande dans un état indéterminé pour un rang inférieur à celle fournie.
     * 
     * @param demande
     *            demande pour laquelle on en recherche une autre indéterminée de rang inférieure
     * @param elv
     *            élève concerné
     * @return vrai si une demande de rang inférieur est non fixée
     * 
     *         Note : sont non fixées les demandes non traitées (NT) et en attente (NT)
     */
    public static boolean isDecisionIndetRangInferieur(Eleve elv, DemandeAffectation demande) {

        EtatDecision edAutre = null;

        // Parcours des demandes
        for (DemandeAffectation demandeTmp : elv.getDemandeAffectation()) {

            edAutre = demandeTmp.getEtatDemande();

            // Teste si la demande a un rang inférieur a un état non déterminé
            if ((demandeTmp.getRang() < demande.getRang()) && (edAutre.isDecisionNonDeterminee())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Teste si un élève est pris sur une demande de rang inférieur à celle fournie.
     * 
     * @param eleve
     *            élève concerné
     * @param demande
     *            demande pour laquelle on en recherche une autre prise de rang inférieure
     * @return vrai si élève est pris sur une demande de rang inférieure
     */
    public static boolean estPrisRangInferieur(Eleve eleve, DemandeAffectation demande) {

        Iterator<DemandeAffectation> it = eleve.getDemandeAffectation().iterator();
        while (it.hasNext()) {
            DemandeAffectation autreDemande = it.next();

            boolean autreDecisionPris = autreDemande.getEtatDemande().isDecisionPris();

            // Il est pris sur une autre demande plus satisfaisante
            if (autreDemande.getRang() < demande.getRang() && autreDecisionPris) {
                return true;
            }
        }
        return false;

    }

    /**
     * Teste si la demande donnée peut être prise.
     * Une demande peut être prise en auto s'il n'y a pas de décision manuelle,
     * et si l'élève n'est pas pris ailleurs.
     * 
     * @param eleve
     *            Élève concerné
     * @param demande
     *            Demande d'affectationl
     * @return vrai s'il n'y a pas de décision
     */
    public static boolean estDemandePriseAutoPossible(Eleve eleve, DemandeAffectation demande) {

        // Regarde l'état actuel de la demande
        EtatDecision etd = demande.getEtatDemande();
        String codeEtat = etd.getCode();

        // Est-ce une décision manuelle
        boolean isForceManuel = etd.isDecisionManuelle();

        // L'élève est-il refusé manuellement ou pris ailleurs (quitte l'offre)
        boolean isPrisAilleurs = codeEtat.equals(EtatDecision.CODE_QUITTE_OFFRE);

        return (!isForceManuel) && (!isPrisAilleurs) && (!isForcePrisManuelAutreDemande(eleve, demande))
                && (!estPrisRangInferieur(eleve, demande));
    }

    /**
     * Teste si l'élève est 'pris manuellement' sur une autre demande.
     * 
     * @param eleve
     *            Élève concerné par le test
     * @param demande
     *            la demande de l'élève
     * @return vrai si l'élève est pris manuel sur une autre demande, sinon faux
     */
    private static boolean isForcePrisManuelAutreDemande(Eleve eleve, DemandeAffectation demande) {

        int rangDemande = demande.getRang();
        EtatDecision etatAutre;

        for (DemandeAffectation autreDemande : eleve.getDemandeAffectation()) {

            etatAutre = autreDemande.getEtatDemande();
            if ((autreDemande.getRang() != rangDemande) && (etatAutre != null)
                    && (etatAutre.isDecisionManuellePris())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Teste si la demande est une demande de secteur.
     * 
     * @param demande
     *            demande à tester
     * @return vrai si la demande est de secteur
     */
    public boolean estDemandeSecteur(DemandeAffectation demande) {

        // HACK Pour éviter d'aller rechercher le collège de secteur dans la demande de l'élève
        // le comparer à celui de l'offre de formation,
        // On considère que la demande de secteur est celle dont la priorité est de secteur
        // Il ne faut toutefois pas que ce soit une demande SEGPA (qui n'a pas de justificatifs
        // et se retrouve avec une priorité de secteur ...)
        return (!demande.isDemandeSegpa())
                && demandeAffectationManager.niveauPrioritePlusHaute(demande) == MotifDerog.PRIORITE_SECTEUR;
    }

    /**
     * Calcule, pour une liste de demandes le nombre de places pouvant être prises automatiquement.
     * 
     * @param listDemandesEleve
     *            la liste de demandes
     * @return nombre de places pouvant être prises automatiquement par les demandes passés en paramètre
     */
    private int getNbrDemandePriseAutoPossible(List<DemandeAffectation> listDemandesEleve) {
        int nbrDemande = 0;
        for (DemandeAffectation demandeTmp : listDemandesEleve) {
            if (estDemandePriseAutoPossible(demandeTmp)) {
                nbrDemande++;
            }
        }
        return nbrDemande;
    }

    /**
     * Calcule, pour une liste de demandes le nombre de celles pouvant être prises.
     * 
     * @param listDemandesEleve
     *            la liste de demandes
     * @return le nombre de places susceptibles d'être prises par les demandes passés en paramètre
     */
    private static int getNbrDemandesForceesPris(List<DemandeAffectation> listDemandesEleve) {
        int nbrDemande = 0;

        for (DemandeAffectation demandeTmp : listDemandesEleve) {
            if (demandeTmp.getEtatDemande().isDecisionManuellePris()) {
                nbrDemande++;
            }
        }
        return nbrDemande;
    }

    /**
     * Teste si cette demande est susceptible d'être prise en automatique.
     * 
     * @param demande
     *            demande à tester
     * @return vrai si la demande est susceptible d'être prise automatiquement
     */
    public static boolean estDemandePriseAutoPossible(DemandeAffectation demande) {

        Eleve eleve = demande.getEleve();
        return estDemandePriseAutoPossible(eleve, demande);
    }

    /** Comparateur de demandes d'affectation selon leur priorité asc puis selon leur rang asc. */
    class DemandeAffectationComparatorPrioriteRang implements Comparator<DemandeAffectation> {

        @Override
        public int compare(DemandeAffectation o1, DemandeAffectation o2) {

            // Récupère les niveaux de priorité les plus fortes de chaque demande
            int p1 = demandeAffectationManager.niveauPrioritePlusHaute(o1);
            int p2 = demandeAffectationManager.niveauPrioritePlusHaute(o2);

            // A priorité égale, on compare les rangs
            if (p1 - p2 == 0) {
                return (o1.getRang() - o2.getRang());
            }

            // On compare les priorités
            return (p1 - p2);
        }
    }

    @Override
    public String composeCompteRenduAffAuto(Map<String, Object> resultats) {

        StringBuffer cr = new StringBuffer();
        cr.append("Traitement de proposition automatique d'affectation");
        cr.append("<br/>");
        cr.append("Département MEN concerné : ");
        cr.append(resultats.get(RES_KEY_DEPT_MEN));
        cr.append("<br/>");
        cr.append("Nombre d'offres de formation traitées : ");
        cr.append(resultats.get(RES_KEY_NB_OFFRES));
        cr.append("<br/>");
        cr.append("Nombre de demandes traitées : ");
        cr.append(resultats.get(RES_KEY_NB_DEMANDES));
        cr.append("<br/>");
        cr.append("Nombre d'itérations effectuées : ");
        cr.append(resultats.get(RES_KEY_ITERATION_COUNT));
        cr.append("<br/>");

        return cr.toString();
    }

    @Override
    public void validationAffectation(BatchProgressMonitor progMon, Map<String, Object> resultats,
            String codeDeptMen) throws ApplicationException {

        // Comptage indicatif des élèves sans demande
        progMon.beginTask("Comptage des élèves sans demande");
        compteElevesSansDemande(codeDeptMen, resultats);
        progMon.endTask();

        // On récupère l'ensemble des élèves affectables du département
        progMon.beginTask("Constitution de la liste des élèves à affecter");
        List<Eleve> eleves = eleveManager.listerElevesAffectablesDepartement(codeDeptMen);
        progMon.endTask();

        // Identifiants d'élèves sans affectation
        List<Long> listeIdElvSansAff = new ArrayList<Long>();

        // Préparation
        DemandeAffectation demandeCourante;
        DemandeAffectation demandeFinale;
        EtatDecision etatDemandeCourante;
        EtatDecision etatDecisionFinale;
        SortedSet<DemandeAffectation> demandesEleve;
        Iterator<DemandeAffectation> itDemandesEleve;
        boolean rechercheDecisionFinale;

        int nbTraite = 0;
        int nbEleves = eleves.size();

        progMon.beginTask("Attribution des décisions finales aux élèves");

        // Les décisions prises localement à chaque demande d'affectation
        // sont remontées au niveau de l'élève.
        for (Eleve eleveCourant : eleves) {

            demandeFinale = null;
            etatDecisionFinale = null;

            // Les demandes de l'élève sont examinées dans l'ordre des rangs croissant.
            demandesEleve = eleveCourant.getDemandeAffectation();

            // L'élève a des demandes
            if (demandesEleve.size() == 0) {

                LOG.info("Eleve " + eleveCourant.toShortString() + " sans demande => non traité");

            } else {

                itDemandesEleve = demandesEleve.iterator();

                // On commence la recherche
                rechercheDecisionFinale = true;

                while (rechercheDecisionFinale && itDemandesEleve.hasNext()) {

                    // Examine la demande suivante de l'élève
                    demandeCourante = itDemandesEleve.next();
                    etatDemandeCourante = demandeCourante.getEtatDemande();

                    if (demandeCourante.isHorsPublicDepartmental() || etatDemandeCourante.isDecisionPris()) {
                        // décision prise manuellement (PM) ou prise automatiquement (PA)
                        // ou hors de l'affectation dans le public du département (HA)

                        // L'élève reçoit le code de la décision pour cette demande
                        etatDecisionFinale = etatDemandeCourante;

                        // L'élève reçoit cette demande comme finale
                        demandeFinale = demandeCourante;

                        // La recherche de décision finale se termine.
                        rechercheDecisionFinale = false;

                    } else if (etatDemandeCourante.isDecisionNonPris()) {

                        // décision refusé manuellement (RM)
                        // ou refusé automatiquement (RA)
                        // ou encore s'il quitte l'offre (QO)

                        // L'élève reçoit temporairement le code de la décision
                        // pour cette demande
                        etatDecisionFinale = etatDemandeCourante;

                        // L'élève reçoit temporairement cette demande comme finale
                        demandeFinale = demandeCourante;

                        // La recherche d'une décision finale se poursuit.

                    } else {
                        // Etat de décision non final
                        LOG.error("La demande " + demandeCourante.toGuruString()
                                + " n'est pas finale, la validation de l'affectation" + " n'est pas possible.");
                        throw new BusinessException("Le traitement de validation d'affectation a rencontré "
                                + "une demande (" + demandeCourante + " ) qui n'est pas dans un état final.");
                    }

                } // while -- examen des demandes

                if (rechercheDecisionFinale) {
                    // Ici, la recherche d'une décision finale d'affectation pour l'élève n'a pas abouti
                    // alors que l'on a parcouru toutes ses demandes
                    // => l'élève n'est affecté nulle part (refusé partout)
                    // demandeFinale vaut sa dernière demande examinée (celle de secteur)
                    // etatDecisionFinale vaut la décision correspondante (refusé)

                    // Le cas de l'élève sera notifié à l'issue de la validation.
                    LOG.info("Eleve " + eleveCourant.toShortString() + " sans affectation finale ");
                    listeIdElvSansAff.add(eleveCourant.getNumDossier());
                }

                // Enregistre la décision finale
                eleveManager.positionneDecisionFinale(eleveCourant, demandeFinale, etatDecisionFinale);

            } // if -- nb demandes

            // Progression
            nbTraite++;

            if (nbTraite % 100 == 0) {
                progMon.setProgression(nbTraite, nbEleves);
            }

        } // for -- eleves

        progMon.setProgression(nbTraite, nbEleves);

        progMon.endTask();

        resultats.put(RES_KEY_ELEVES_SANS_AFFECTATION_FINALE, listeIdElvSansAff);
    }

    @Override
    public boolean controleDemandesToutesFinales(BatchProgressMonitor progMon, String codeDeptMen)
            throws ApplicationException {

        // Vérification que toutes les demandes soient dans un état final
        List<DemandeAffectation> lDemNonFinales = demandeAffectationManager
                .listerDemandesDeptEtatNonFinal(codeDeptMen);

        int nbDemNonFinales = lDemNonFinales.size();

        LOG.debug("Nombre de demandes sans décision finale " + nbDemNonFinales);
        return (nbDemNonFinales == 0);
    }

    /**
     * Compte le nombre d'élèves sans demande.
     * 
     * @param codeDeptMen
     *            Code du département MEN concerné
     * @param resultats
     *            table des résultats de traitement
     * @throws ApplicationException
     *             en cas de problème
     */
    private void compteElevesSansDemande(String codeDeptMen, Map<String, Object> resultats)
            throws ApplicationException {

        // On laisse passer les élèves sans demande ...
        // il ne seront pas traités mais simplement comptabilisés

        List<Eleve> listeElevesSansDemande = eleveManager.listerElevesSansDemande(null, null, codeDeptMen, null);

        int nbElevesSansDemande = listeElevesSansDemande.size();

        LOG.debug("Nombre d'élèves sans demande " + nbElevesSansDemande);
        resultats.put(KEY_NB_ELEVES_SANS_DEMANDE, Integer.valueOf(nbElevesSansDemande));
    }

    @SuppressWarnings("unchecked")
    @Override
    public String composeCompteRenduValidationAff(Map<String, Object> resultats) {

        StringBuffer cr = new StringBuffer();
        cr.append("<p>Traitement de validation des décisions d'affectation</p>");
        cr.append("<p>Département MEN concerné : " + resultats.get(RES_KEY_DEPT_MEN) + "</p>");

        int nbElevesSansDemande = ((Integer) resultats.get(KEY_NB_ELEVES_SANS_DEMANDE)).intValue();

        cr.append("<h3>Les affectations finales ont été attribuées aux élèves.</h3>");

        if (nbElevesSansDemande == 1) {

            cr.append("<h3>un(e) élève sans demande n'a pas été traité(e).</h3>");
        } else if (nbElevesSansDemande > 1) {
            cr.append("<h3>" + nbElevesSansDemande + " élèves sans demande n'ont pas été traités.</h3>");
        }

        List<Long> listeIdElvSansAff = (List<Long>) resultats.get(RES_KEY_ELEVES_SANS_AFFECTATION_FINALE);

        if (listeIdElvSansAff.size() > 0) {

            cr.append("<h3>Il reste des élèves sans affectation finale</h3>");
            cr.append("<p>La liste ci-dessous recense les élèves sans affectation finale"
                    + " (refusés sur toutes leurs demandes)" + " qui ont été trouvés au cours du traitement.</p>");

            eleveManager.listerElevesCompteRendu(cr, listeIdElvSansAff);

        }

        return cr.toString();
    }
}
