/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.service.interfaces.dossier;

import java.util.List;

import fr.edu.nancy.affelnet6.dao.interfaces.dossier.ResponsableDao;
import fr.edu.nancy.affelnet6.domain.dossier.Eleve;
import fr.edu.nancy.affelnet6.domain.dossier.Responsable;
import fr.edu.nancy.affelnet6.domain.dossier.ResponsableId;
import fr.edu.nancy.affelnet6.utils.Message;
import fr.edu.nancy.commun.exceptions.ApplicationException;
import fr.edu.nancy.commun.service.interfaces.DaoManager;

/**
 * Gestionnaire de Responsable.
 *
* @see fr.edu.nancy.affelnet6.domain.dossier.Responsable
 */
public interface ResponsableManager extends DaoManager<Responsable, ResponsableId, ResponsableDao> {

    /** Nombre maximum de responsables. */
    int MAX_RESPONSABLES = 5;

    /** Nombre maximum de représentants légaux pour un élève. */
    int MAX_REPRESENTANTS_LEGAUX = 3;
    /** Nombre maximum de personnes en charge pour un élève. */
    int MAX_EN_CHARGE = 2;
    /** Nombre maximum de personnes en charge pour un élève. */
    int MAX_LIEN_PARENTE_SIMILAIRE = 2;
    /** Filtre sur la valeur du nom de famille. */
    String[] FILTRE_NOM = { "0", "nom", "Nom" };
    /** Filtre sur la valeur du prénom. */
    String[] FILTRE_PRENOM = { "1", "prenom", "Prénom" };
    /** Filtre sur la valeur d'identifiant de l'adresse. */
    String[] FILTRE_STATUT_ADRESSE = { "2", "adresseResponsable.adrId", "Adresse" };
    /** Filtre sur le département du responsable. */
    String[] FILTRE_CODE_DEPT = { "3", "dept.deptCode", "Département" };

    /** Filtre sur le code postal de l'adresse du responsable. */
    String[] FILTRE_CODE_POSTAL = { "4", "adresseResponsable.codePostal", "Code Postal" };

    /** Filtre sur la commune INSEE concernée de l'adresse du responsable. */
    String[] FILTRE_COMMUNE_INSEE = { "5", "adresseResponsable.communeInsee", "Commune INSEE" };

    /** Filtre sur Libellé de commune étrangère de l'adresse du responsable. */
    String[] FILTRE_LIB_COMMUNE_ETRANGERE = { "6", "adresseResponsable.libCommuneEtrangere",
            "Libellé de commune étrangère" };

    /** Filtre sur Ligne 1 de l'adresse du responsable. */
    String[] FILTRE_LIGNE_1 = { "7", "adresseResponsable.ligne1Adr", "Ligne d'adresse 1" };

    /** Filtre sur Ligne 2 de l'adresse du responsable. */
    String[] FILTRE_LIGNE_2 = { "8", "adresseResponsable.ligne2Adr", "Ligne d'adresse 2" };

    /** Filtre sur Ligne 3 de l'adresse du responsable. */
    String[] FILTRE_LIGNE_3 = { "9", "adresseResponsable.ligne3Adr", "Ligne d'adresse 3" };

    /** Filtre sur Ligne 4 de l'adresse du responsable. */
    String[] FILTRE_LIGNE_4 = { "10", "adresseResponsable.ligne4Adr", "Ligne d'adresse 4" };

    /** Filtre sur Le pays du responsable. */
    String[] FILTRE_PAYS = { "11", "adresseResponsable.pays.codePays", "Pays" };

    /** Filtre sur Le pays du responsable. */
    String[] FILTRE_NOTIF_EDITEE = { "12", "dateEditNotif", "Date Notification Editée" };

    /** Filtre sur le numéro de dossier de l'élève du responsable. */
    String[] FILTRE_NUM_DOSSIER = { "13", "numDossierEleve", "Numéro de dossier de l'élève" };

    /** Filtre sur la valeur du nom d'usage. */
    String[] FILTRE_NOM_USA = { "14", "nomUsage", "Nom d'usage" };

    /**
     * Efface les responsables du département en vue d'un nouvel import.
     *
     * @param dptMenId
     *            Identifiant du département MEN Concené
     * @return nombre de suppressions effectuées
     * @throws ApplicationException
     *             en cas de problème
     */
    int supprimerPourRemplacerDepartement(int dptMenId) throws ApplicationException;

    /**
     * Donne le nom du responsable.
     * Si c'est un responsable moral, retourne son intitulé.
     * Si c'est un responsable physique, retourne son nom d'usage s'il est non blanc,
     * sinon retourne son nom de famille.
     *
     * @param responsable
     *            Un responsable
     * @return Le nom du responsable
     */
    String nomResponsable(Responsable responsable);

    /**
     * Donne le nom du responsable.
     * Si c'est un responsable moral, retourne son intitulé.
     * Si c'est un responsable physique, retourne son nom d'usage s'il est non blanc,
     * sinon retourne son nom de famille.
     *
     * @param responsable
     *            Un responsable
     * @param truncateForRcpPer
     *            Vrai si le nom doit être tronqué au-delà
     *            de 25 caractères afin de pouvoir être inséré dans RCP_PER.NOM_PERSONNE
     * @return Le nom du responsable
     */
    String nomResponsable(Responsable responsable, boolean truncateForRcpPer);

    /**
     * Donne le nom d'usage du responsable.
     * Si c'est un responsable moral, retourne son intitulé.
     * Si c'est un responsable physique, retourne son nom d'usage.
     *
     * @param responsable
     *            Un responsable
     * @return Le nom d'usage du responsable
     */
    String nomUsageResponsable(Responsable responsable);

    /**
     * Donne le prénom du responsable à insérer dans RCP_PER.PRENOM_PERSONNE.
     *
     * @param responsable
     *            Un responsable
     * @return Le prénom du responsable ou son intitulé si c'est un responsable moral
     */
    String prenomResponsable(Responsable responsable);

    /**
     * Donne le code de la civilité du responsable à insérer dans RCP_PER.CODE_CIVILITE.
     *
     * @param responsable
     *            Un responsable
     * @return La civilité du responsable
     */
    String civiliteResponsable(Responsable responsable);

    /**
     * Supprime le responsable avec le numéro donné avec contrôle des règles de gestion.
     *
     * @param eleve
     *            l'élève
     * @param numResp
     *            le numéro du responsable
     * @return le code d'erreur si une règle de gestion n'est pas respectée
     * @throws ApplicationException
     *             en cas d'erreur lors de l'accès aux données du responsable
     */
    Message supprimer(Eleve eleve, short numResp) throws ApplicationException;

    /**
     * Ajoute ou modifie un responsable.
     *
     * @param eleve
     *            l'élève
     * @param responsable
     *            le responsable à ajouter
     * @param numResp
     *            le numéro du responsable modifié (0 si ajout)
     * @return le code d'erreur si une règle de gestion n'est pas respectée
     * @throws ApplicationException
     *             en cas d'erreur lors de l'écriture des données
     */
    Message ajouteOuModifieResponsable(Eleve eleve, Responsable responsable, short numResp)
            throws ApplicationException;

    /**
     * Vérifie la validité des liens entre l'élève et ses responsables.
     *
     * @param eleve
     *            l'élève
     * @return les éventuels messages de warning
     * @throws ApplicationException
     *             en cas d'erreur
     */
    List<Message> verifieValiditeLiens(Eleve eleve) throws ApplicationException;

}
