/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.service.interfaces.dossier;

import fr.edu.nancy.commun.exceptions.TechnicalException;

/**
 * Gestionnaire des collèges de secteur issus de la simulation de détermination.
 * 
 * 
 *
 */
public interface CollegeSecteurSimuleManager {
    /**
     * Efface les collèges issus de la simulation de détermination du département en vue d'un nouvel import.
     * 
     * @param dptMenId
     *            Identifiant du département MEN Concerné
     * @return le nombre de collèges supprimés
     * @throws TechnicalException
     *             en cas d'erreur lors de l'accès aux données
     */
    int supprimerPourRemplacerDepartement(int dptMenId) throws TechnicalException;
}
