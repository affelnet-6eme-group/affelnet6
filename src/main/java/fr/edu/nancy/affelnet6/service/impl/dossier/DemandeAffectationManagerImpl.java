/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.service.impl.dossier;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts.Globals;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import fr.edu.nancy.affelnet6.dao.interfaces.dossier.DemandeAffectationDao;
import fr.edu.nancy.affelnet6.domain.dossier.CollegeSecteur;
import fr.edu.nancy.affelnet6.domain.dossier.DemandeAffectation;
import fr.edu.nancy.affelnet6.domain.dossier.DemandeAffectationVue;
import fr.edu.nancy.affelnet6.domain.dossier.DemandeFamille;
import fr.edu.nancy.affelnet6.domain.dossier.Eleve;
import fr.edu.nancy.affelnet6.domain.dossier.JustificatifDerog;
import fr.edu.nancy.affelnet6.domain.dossier.Log;
import fr.edu.nancy.affelnet6.domain.dossier.SauvegardeDemandeAffectation;
import fr.edu.nancy.affelnet6.domain.dossier.SauvegardeJustificatifDerog;
import fr.edu.nancy.affelnet6.domain.dossier.TypeDemandeSpecifique;
import fr.edu.nancy.affelnet6.domain.nomenclature.DecisionPassage;
import fr.edu.nancy.affelnet6.domain.nomenclature.DecisionPassageEnum;
import fr.edu.nancy.affelnet6.domain.nomenclature.DeptMen;
import fr.edu.nancy.affelnet6.domain.nomenclature.EtablissementSconet;
import fr.edu.nancy.affelnet6.domain.nomenclature.EtatDecision;
import fr.edu.nancy.affelnet6.domain.nomenclature.Formation;
import fr.edu.nancy.affelnet6.domain.nomenclature.MotifDerog;
import fr.edu.nancy.affelnet6.domain.nomenclature.OffreFormation;
import fr.edu.nancy.affelnet6.domain.nomenclature.TypeDemandeAffec;
import fr.edu.nancy.affelnet6.helper.DemandeAffectationHelper;
import fr.edu.nancy.affelnet6.helper.EleveHelper;
import fr.edu.nancy.affelnet6.helper.ParametreHelper;
import fr.edu.nancy.affelnet6.service.dossier.DemandeAffectationManager;
import fr.edu.nancy.affelnet6.service.dossier.EleveManager;
import fr.edu.nancy.affelnet6.service.dossier.JustificatifDerogManager;
import fr.edu.nancy.affelnet6.service.interfaces.AffectationManager;
import fr.edu.nancy.affelnet6.service.interfaces.AppAutomateManager;
import fr.edu.nancy.affelnet6.service.nomenclature.DeptMenManager;
import fr.edu.nancy.affelnet6.service.nomenclature.EtatDecisionManager;
import fr.edu.nancy.affelnet6.service.nomenclature.FormationManager;
import fr.edu.nancy.affelnet6.service.nomenclature.OffreFormationManager;
import fr.edu.nancy.affelnet6.service.nomenclature.TypeDemandeAffecManager;
import fr.edu.nancy.affelnet6.utils.Message;
import fr.edu.nancy.commun.dao.impl.filtres.FiltreFactory;
import fr.edu.nancy.commun.dao.impl.jointures.JointureFactory;
import fr.edu.nancy.commun.dao.interfaces.BaseDao;
import fr.edu.nancy.commun.dao.interfaces.EntityFetcher;
import fr.edu.nancy.commun.dao.interfaces.Pagination;
import fr.edu.nancy.commun.dao.interfaces.Selector;
import fr.edu.nancy.commun.dao.interfaces.Tri;
import fr.edu.nancy.commun.dao.interfaces.filtres.Filtre;
import fr.edu.nancy.commun.dao.interfaces.filtres.operateurs.FiltreOpChaines;
import fr.edu.nancy.commun.dao.interfaces.filtres.operateurs.FiltreOpCollection;
import fr.edu.nancy.commun.dao.interfaces.filtres.operateurs.FiltreOpComparaison;
import fr.edu.nancy.commun.dao.interfaces.filtres.operateurs.FiltreOpLogique;
import fr.edu.nancy.commun.dao.interfaces.filtres.operateurs.FiltreOpNull;
import fr.edu.nancy.commun.dao.interfaces.filtres.operateurs.FiltreOpProprietes;
import fr.edu.nancy.commun.exceptions.ApplicationException;
import fr.edu.nancy.commun.exceptions.BusinessException;
import fr.edu.nancy.commun.exceptions.DaoException;
import fr.edu.nancy.commun.exceptions.TechnicalException;
import fr.edu.nancy.commun.exceptions.ValidationException;
import fr.edu.nancy.commun.utils.BatchProgressMonitor;
import fr.edu.nancy.commun.web.session.InfoListe;
import fr.edu.nancy.commun.web.utils.Constantes;

/**
 * Gestionnaire de DemandeAffectation.
 * 
 * @see fr.edu.nancy.affelnet6.domain.dossier.DemandeAffectation
 */
@Service("demandeAffectationManager")
public class DemandeAffectationManagerImpl implements DemandeAffectationManager {

    /** Logger de la classe. **/
    private static final Logger LOG = LoggerFactory.getLogger(DemandeAffectationManagerImpl.class);

    /** Gestionnaire des élèves. */
    @Autowired
    protected EleveManager eleveManager;

    /**
     * Gestionnaire de demandes d'affectation (cette classe).
     * Permet d'utiliser des transactions (en passant par un proxy)
     */
    @Resource(name = "demandeAffectationManager")
    protected DemandeAffectationManager demandeAffectationManager;

    /** Objet d'accès aux données des DemandeAffectation. */
    @Autowired
    private DemandeAffectationDao demandeAffectationDao;

    /** Objet d'accès à la vue des DemandeAffectation. */
    @Autowired
    @Qualifier("demandeAffectationVueDao")
    private BaseDao<DemandeAffectationVue, Long> demandeAffectationVueDao;

    /** Gestionnaire d'offres de formation. */
    @Autowired
    private OffreFormationManager offreFormationManager;

    /** Gestionnaire d'automate pour l'état applicatif. */
    @Autowired
    private AppAutomateManager appAutomateManager;

    /** Gestionnaire de l'affectation. */
    @Autowired
    private AffectationManager affectationManager;

    /** Gestionnaire de justificatif de dérogation. */
    @Autowired
    private JustificatifDerogManager justificatifDerogManager;

    /** Gestionnaire des types de demande. */
    @Autowired
    private TypeDemandeAffecManager typeDemandeAffecManager;

    /** Gestionnaire de l'état de la demande. */
    @Autowired
    private EtatDecisionManager etatDecisionManager;

    /** Gestionnaire des formations. */
    @Autowired
    private FormationManager formationManager;

    /** Gestionnaire des départements. */
    @Autowired
    private DeptMenManager deptMenManager;

    @Override
    public List<DemandeAffectation> lister(Pagination<DemandeAffectation> paginationSupport, List<Tri> listTri,
            Filtre filtre) throws TechnicalException, BusinessException {
        return lister(paginationSupport, listTri, filtre, null);
    }

    @Override
    public List<DemandeAffectation> lister(Pagination<DemandeAffectation> paginationSupport, List<Tri> listTri,
            Filtre filtre, List<EntityFetcher> entityFetchers) throws TechnicalException, BusinessException {
        return demandeAffectationDao.lister(paginationSupport, listTri, filtre, entityFetchers);

    }

    @Override
    public List<DemandeAffectation> lister(InfoListe<DemandeAffectation> inf)
            throws TechnicalException, BusinessException {
        return demandeAffectationDao.lister(inf.getPaginationSupport(), inf.getListTri(),
                inf.composeTousFiltres());
    }

    @Override
    public List<DemandeAffectation> lister() throws TechnicalException, BusinessException {
        return demandeAffectationDao.lister();
    }

    @Override
    public DemandeAffectation charger(Long id) throws ApplicationException {
        return demandeAffectationDao.charger(id);
    }

    @Override
    public void maj(DemandeAffectation demande) throws ApplicationException {
        demandeAffectationDao.maj(demande);
    }

    @Override
    public void majSansFlush(DemandeAffectation demande) throws ApplicationException {
        demandeAffectationDao.maj(demande, false);
    }

    @Override
    public void majSansFlushAvecEvict(DemandeAffectation demande) throws ApplicationException {
        demandeAffectationDao.maj(demande, false, true);
    }

    @Override
    public void supprimer(Long idDemande) throws ApplicationException {
        DemandeAffectation demande = charger(idDemande);
        supprimer(demande);
    }

    @Override
    public void supprimer(DemandeAffectation demande) throws ApplicationException {

        // supprime chaque justificatif de dérogation de la demande
        Set<JustificatifDerog> justifsDerog = demande.getJustificatifDerogs();
        if (justifsDerog != null && justifsDerog.size() > 0) {
            for (JustificatifDerog justifDerog : justifsDerog) {
                justificatifDerogManager.supprimer(justifDerog);
            }
        }

        // mémorise l'offre de formation liée à la demande
        OffreFormation offre = demande.getOffreformation();

        // annule la décision d'affectation
        List<Message> messagesAvertissement = new ArrayList<Message>();
        affectationManager.saisieDecisionManuelle(demande.getId(), EtatDecision.CODE_NON_TRAITE,
                messagesAvertissement);

        // supprime la demande
        demandeAffectationDao.supprimer(demande);

        // met à jour les compteurs de l'offre de formation liée à la demande
        majCompteursOffre(offre);
    }

    @Override
    public Long creer(DemandeAffectation demande) throws ApplicationException {
        // enregistrement en base
        Long id = demandeAffectationDao.creer(demande);

        // met à jour les compteurs de l'offre de formation liée à la demande
        OffreFormation offre = demande.getOffreformation();
        majCompteursOffre(offre);

        return id;
    }

    @Override
    public boolean existe(Long id) throws ApplicationException {
        return demandeAffectationDao.existe(id);
    }

    @Override
    public int compteDemandesTotales(String codeOffre) throws ApplicationException {

        OffreFormation of = offreFormationManager.charger(codeOffre);

        // Le nombre de demandes totales correspond à la somme
        // du nombre de des demandes de secteur
        // du nombre de des demandes de dérogations
        int tot = 0;
        if (of.getNbrDemSct() != null) {
            tot += of.getNbrDemSct();
        }
        if (of.getNbrDemDrg() != null) {
            tot += of.getNbrDemDrg();
        }
        return tot;
    }

    @Override
    public int compteDemandesRelevantSecteur(String codeOffre) throws ApplicationException {

        OffreFormation of = offreFormationManager.charger(codeOffre);
        if (of.getNbrDemMaxSct() != null) {
            return of.getNbrDemMaxSct();
        }
        return 0;
    }

    @Override
    public int compteDemandesDerogation(String codeOffre) throws ApplicationException {
        OffreFormation of = offreFormationManager.charger(codeOffre);
        if (of.getNbrDemDrg() != null) {
            return of.getNbrDemDrg();
        }
        return 0;

    }

    @Override
    public int niveauPrioritePlusHaute(DemandeAffectation demande) {

        Set<JustificatifDerog> justificatifs = demande.getJustificatifDerogs();

        // si pas de motif de dérogation
        // alors on considère que c'est la demande de secteur
        if (justificatifs == null || justificatifs.size() == 0) {
            return MotifDerog.PRIORITE_SECTEUR;
        }

        // on part de la priorité minimale
        int nivPrioMin = MotifDerog.PRIORITE_MINIMALE;
        for (JustificatifDerog justifDerof : justificatifs) {
            if (justifDerof.getIdMotifDerog().getNivPrio() < nivPrioMin) {
                nivPrioMin = justifDerof.getIdMotifDerog().getNivPrio();
            }
        }

        return nivPrioMin;
    }

    @Override
    public void majCompteursOffre(OffreFormation offre) throws ApplicationException {
        majCompteursOffre(offre, true, true);
    }

    @Override
    public void majCompteursOffre(OffreFormation offre, boolean majDemandes, boolean majAffectation)
            throws ApplicationException {

        if (offre != null) {
            majCompteursOffreSansVerifPhaseAffectation(offre, majDemandes, majAffectation);
        }
    }

    @Override
    public void majCompteursOffreSansVerifPhaseAffectation(String codeOffre, boolean majDemandes,
            boolean majAffectation) throws ApplicationException {

        OffreFormation offre = offreFormationManager.charger(codeOffre);
        majCompteursOffreSansVerifPhaseAffectation(offre, majDemandes, majAffectation);
    }

    /**
     * Met à jour les compteurs d'une offre de formation.<br>
     * Ne vérifie pas que l'on est en phase d'affectation. Cette
     * vérification DOIT être faite avant l'appel à cette méthode !
     * 
     * @param offre
     *            Offre de formation concernée
     * @param majDemandes
     *            vrai si on doit mettre à jour les compteurs des demandes
     * @param majAffectation
     *            vrai si on doit mettre à jour les compteurs d'état d'affectation
     * @throws ApplicationException
     *             exception remontant en cas de problème
     */
    private void majCompteursOffreSansVerifPhaseAffectation(OffreFormation offre, boolean majDemandes,
            boolean majAffectation) throws ApplicationException {

        if (!majDemandes && !majAffectation) {
            // Aucune mise à jour réelle ...
            return;
        }

        String codeOffre = offre.getCod();
        String codeFormation = offre.getFormation().getCode();
        LOG.debug("Mise à jour de l'offre de formation " + codeOffre);

        if (majDemandes) {

            // Nombre de demandes relevant du secteur (NBR_DEM_MAX_SCT)
            offre.setNbrDemMaxSct(demandeAffectationDao.compteDemandesMaxSecteur(codeOffre));

            // Nombre de demandes du secteur (NBR_DEM_SCT)
            offre.setNbrDemSct(demandeAffectationDao.compteDemandesSecteur(codeOffre));

            // Nombre de demandes de dérogations (NBR_DEM_DRG)
            // Pour les offres SEGPA et ULIS, on compte aussi les demandes dans la
            // colonne NBR_DEM_DRG
            if (!DemandeAffectationHelper.estFormationSegpaOuUlis(codeFormation)) {
                offre.setNbrDemDrg(demandeAffectationDao.compteDemandesDerogation(codeOffre));
            } else {
                offre.setNbrDemDrg(demandeAffectationDao.compteDemandes(codeOffre));
            }

        }

        if (majAffectation) {

            // Nombre d'élèves affectés sur leur demande de secteur (NBR_AFF_DEM_SCT)
            offre.setNbrAffDemSct(demandeAffectationDao.compteAffectesSecteur(codeOffre, true));

            // Nombre d'élèves affectés en dérogation (NBR_AFF_DRG)
            if (DemandeAffectationHelper.estFormationSegpaOuUlis(codeFormation)) {
                offre.setNbrAffDrg(demandeAffectationDao.compteDemandesAffecteesSegpaOuUlis(codeOffre));
            } else {
                offre.setNbrAffDrg(demandeAffectationDao.compteAffectesDerog(codeOffre));
            }
            // Nombre d'élèves affectés manuellement (NBR_AFF_MAN)
            offre.setNbrAffMan(demandeAffectationDao.compteAffectesManuellement(codeOffre));

            // Nombre d'élèves affectés sur le secteur (NBR_AFF_SCT)
            offre.setNbrAffSct(demandeAffectationDao.compteAffectesSecteur(codeOffre, false));

            // Nombre de demandes de dérogations en attente (NBR_ATT_DRG) pour les formations différentes de SEGPA
            // ou nombre de demandes de formation SEGPA
            if (DemandeAffectationHelper.estFormationSegpaOuUlis(codeFormation)) {
                offre.setNbrAttDrg(demandeAffectationDao.compteDemandesAttentesSegpaOuUlis(codeOffre));
            } else {
                offre.setNbrAttDrg(demandeAffectationDao.compteDemandesAttenteDerog(codeOffre));
            }

            // Nombre d'élèves en attente sur le secteur (NBR_ATT_SCT)
            offre.setNbrAttSct(demandeAffectationDao.compteAttenteSecteur(codeOffre));

            // Nombre de demandes non traitées (NBR_DEM_NON_TRT)
            offre.setNbrDemNonTrt(demandeAffectationDao.compteDemandesNotTraitees(codeOffre));

        }
        offreFormationManager.majSansFlush(offre);
    }

    @Override
    public void majCompteursOffres(String codeDeptMen, boolean majDemandes, boolean majAffectation,
            BatchProgressMonitor progMon) throws ApplicationException {

        LOG.info("Tentative de mise à jour des compteurs de demandes"
                + " sur les offres de formation du département " + codeDeptMen + ".");

        // si on est en phase d'affectation
        // ou si on est en phase saisie que le batch de validation est lancé
        // alors on met à jour les compteurs
        if (appAutomateManager.isEtatFonctionnelDepartement(codeDeptMen, AppAutomateManager.ETAT_DEPT_AFFECTATION,
                true)
                || (appAutomateManager.isEtatFonctionnelDepartement(codeDeptMen,
                        AppAutomateManager.ETAT_DEPT_SAISIE, true)
                        && appAutomateManager.isBatchDepartementalBloquant(codeDeptMen))) {

            List<OffreFormation> offreList = offreFormationManager.listerOffresDepartement(codeDeptMen);

            int nbTotal = offreList.size();
            int nbTraite = 0;

            for (OffreFormation offre : offreList) {

                // /!\ Attention on n'appelle pas la méthode
                // majCompteursOffreSansVerifPhaseAffectation() de manière interne
                // mais à travers le proxy de Spring
                // (en passant par le bean demandeAffectationManager)
                // afin de bénéficier des transactions. On peut faire
                // cela car le proxy n'a pas d'état (pas d'attribut).
                // Si on faisait un appel interne, il n'y aurait pas
                // de point de coupe et donc pas de transaction sur
                // cette méthode !
                demandeAffectationManager.majCompteursOffreSansVerifPhaseAffectation(offre.getCod(), majDemandes,
                        majAffectation);

                if (progMon != null && nbTraite % 10 == 0) {
                    progMon.setProgression(nbTraite, nbTotal);
                }

                nbTraite++;
            }

            LOG.info("Mise à jour des compteurs de demandes effectuée pour les " + offreList.size() + " offres.");

        }
    }

    @Override
    public List<MotifDerog> listeMotifsDerogation(DemandeAffectation demande) {
        List<MotifDerog> lDerog = new ArrayList<MotifDerog>();

        Set<JustificatifDerog> justificatifs = demande.getJustificatifDerogs();

        // si pas de motif de dérogation
        // alors on considère que c'est la demande de secteur
        if (justificatifs == null || justificatifs.size() == 0) {
            return null;
        }

        for (JustificatifDerog justifDerof : justificatifs) {
            lDerog.add(justifDerof.getIdMotifDerog());
        }
        return lDerog;
    }

    @Override
    public List<DemandeAffectation> listerDemandesDepartement(String codeDeptMen) throws ApplicationException {

        // Filtre départemental
        Filtre filtreDep = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                DemandeAffectationManager.FILTRE_DEPT[Constantes.FILTRE_COLONNE], codeDeptMen);

        // Filtre retirant les élèves maintenus à l'école
        // Remarque : on garde les élèves sans décision de passage (null)

        Filtre filtreElevesNonMaintenusCycle = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.DIFFERENT,
                DemandeAffectationManager.FILTRE_DECISION_PASSAGE[Constantes.FILTRE_COLONNE],
                DecisionPassageEnum.MAINTENU.getCode());

        Filtre filtreElevesDecisionPassageNulle = FiltreFactory.createFiltreNull(FiltreOpNull.EST_NULL,
                DemandeAffectationManager.FILTRE_DECISION_PASSAGE[Constantes.FILTRE_COLONNE]);

        Filtre filtreDecisionPassage = FiltreFactory.createFiltreLogique(FiltreOpLogique.OU,
                filtreElevesDecisionPassageNulle, filtreElevesNonMaintenusCycle);

        // On retire les demandes d'orientation en ULIS
        Filtre filtrePasUlis = FiltreFactory.createFiltreLogique(FiltreOpLogique.NON,
                FiltreFactory.createFiltreChaines(FiltreOpChaines.FINI_PAR,
                        DemandeAffectationManager.FILTRE_FORMATION_DEMANDEE_CODE[Constantes.FILTRE_COLONNE],
                        Formation.TYPE_MEF_ULIS));

        // Sélecteur
        Selector<DemandeAffectation> selector = demandeAffectationDao.creerSelector();
        selector.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        selector.addFiltre(filtreDep);
        selector.addFiltre(getFiltreTypeDemandeCollegePublic());
        selector.addFiltre(filtreDecisionPassage);
        selector.addFiltre(filtrePasUlis);
        selector.addJointure(JointureFactory.creerJointureExterneGauche("offreformation"));
        selector.addJointure(JointureFactory.creerJointureExterneGauche("etatDemande"));
        selector.setFetchMode(DemandeAffectationManager.FILTRE_JUSTIF_DEROGS[Constantes.FILTRE_COLONNE],
                FetchMode.JOIN);

        return demandeAffectationDao.listerAvecSelector(selector);
    }

    @Override
    public Filtre getFiltreTypeDemandeCollegePublic() {
        // Filtre sur les demandes de collège public
        Filtre filtreTypeDemColPub = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                DemandeAffectationManager.FILTRE_TYPE_DEMANDE[Constantes.FILTRE_COLONNE],
                TypeDemandeAffec.CODE_TYPE_DEMANDE_DANS_COLLEGE_PUBLIC_DEPT);

        return filtreTypeDemColPub;
    }

    @Override
    public int compteDemandesSecteur(String codeOffre) throws ApplicationException {

        OffreFormation of = offreFormationManager.charger(codeOffre);

        int tot = 0;
        if (of.getNbrDemSct() != null) {
            tot += of.getNbrDemSct();
        }
        return tot;
    }

    @Override
    public List<DemandeAffectation> listeDemandesDerogation(String codeEtab) {
        return demandeAffectationDao.listeDemandesDerogation(codeEtab);
    }

    @Override
    public List<DemandeAffectation> listeAffectationsDerogation(String codeEtab) {
        return demandeAffectationDao.listeAffectationsDerogation(codeEtab);
    }

    @Override
    public Filtre getFiltreDemandesDeptEtatNonFinal(String codeDeptMen) {

        List<Filtre> filtreRes = new ArrayList<Filtre>();

        // Filtre sur le dept
        Filtre filtredept = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                DemandeAffectationManager.FILTRE_DEPT[Constantes.FILTRE_COLONNE], codeDeptMen);

        filtreRes.add(filtredept);

        // Filtre sur les élèves non affectés
        Filtre filtreNonAffectes = FiltreFactory.createFiltreNull(FiltreOpNull.EST_NULL,
                DemandeAffectationManager.FILTRE_CODE_ETAT_ETAT_DECISION[Constantes.FILTRE_COLONNE]);
        // code état à null == en attente ou non traité
        filtreRes.add(filtreNonAffectes);

        return FiltreFactory.createFiltreLogique(FiltreOpLogique.ET, filtreRes);
    }

    @Override
    public List<DemandeAffectation> listerDemandesDeptEtatNonFinal(String codeDeptMen)
            throws TechnicalException, BusinessException {

        Selector<DemandeAffectation> selector = demandeAffectationDao.creerSelector();
        selector.addFiltre(getFiltreDemandesDeptEtatNonFinal(codeDeptMen));
        return demandeAffectationDao.listerAvecSelector(selector);
    }

    @Override
    public int compteAffectationsTotales(String codeOffre) throws ApplicationException {

        OffreFormation of = offreFormationManager.charger(codeOffre);

        // Le nombre d'affectations totales correspond à la somme
        // du nombre d'affectation de secteur
        // du nombre d'affectation de dérogations
        int tot = 0;
        if (of.getNbrAffSct() != null) {
            tot += of.getNbrAffSct();
        }
        if (of.getNbrAffDrg() != null) {
            tot += of.getNbrAffDrg();
        }
        return tot;
    }

    @Override
    public int compteAffectationsDerogation(String codeOffre) throws ApplicationException {
        OffreFormation of = offreFormationManager.charger(codeOffre);
        if (of.getNbrAffDrg() != null) {
            return of.getNbrAffDrg();
        }
        return 0;
    }

    @Override
    public int compteAffectationsSecteur(String codeOffre) throws ApplicationException {

        OffreFormation of = offreFormationManager.charger(codeOffre);

        int tot = 0;
        if (of.getNbrAffSct() != null) {
            tot += of.getNbrAffSct();
        }
        return tot;
    }

    @Override
    public Filtre getFiltreDemandesDeptSegpa(String codeDeptMen) {

        List<Filtre> filtreRes = new ArrayList<Filtre>();

        // Filtre sur le dept
        Filtre filtredept = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                DemandeAffectationManager.FILTRE_DEPT[Constantes.FILTRE_COLONNE], codeDeptMen);

        filtreRes.add(filtredept);

        // Filtre sur les demandes SEGPA
        Filtre filtreSegpa = FiltreFactory.createFiltreChaines(FiltreOpChaines.COMMENCE_PAR,
                DemandeAffectationManager.FILTRE_FORMATION_DEMANDEE_CODE[Constantes.FILTRE_COLONNE],
                Formation.CODE_DISPOSITIF_SEGPA, true, true);
        filtreRes.add(filtreSegpa);

        return FiltreFactory.createFiltreLogique(FiltreOpLogique.ET, filtreRes);
    }

    @Override
    public int compteDemandesSegpaBanalisees(String codDeptMen) {
        return demandeAffectationDao.compteDemandesSegpaBanalisees(codDeptMen);
    }

    @Override
    public int compteDemandesSegpaBanaliseesPrises(String codDeptMen) {
        return demandeAffectationDao.compteDemandesSegpaBanaliseesPrises(codDeptMen);
    }

    @Override
    public int compteDemandesSegpaSansDecisionFinale(String codDeptMen) {
        return demandeAffectationDao.compteDemandesSegpaSansDecisionFinale(codDeptMen);
    }

    @Override
    public boolean hasDemandesRangSuperieur(String codeDeptMen, short rang) throws DaoException {
        // Filtre sur le dept
        Filtre filtreDept = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                FILTRE_DEPT[Constantes.FILTRE_COLONNE], codeDeptMen);

        // Filtre sur le rang
        Filtre filtreRang = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.SUPERIEUR,
                FILTRE_RANG[Constantes.FILTRE_COLONNE], rang);

        // On ne prend pas en compte les propositions de Segpa / Ulis
        Filtre filtrePasProposition = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.DIFFERENT,
                FILTRE_TYPE_DEMANDE_SPEC[Constantes.FILTRE_COLONNE], TypeDemandeSpecifique.PROPOSITION.getCode());

        // Filtre final
        Filtre filtreFinal = FiltreFactory.createFiltreLogique(FiltreOpLogique.ET, filtreDept, filtreRang,
                filtrePasProposition);

        return (demandeAffectationDao.count(filtreFinal) != 0);
    }

    @Override
    public boolean estDemandeOffreSecteur(DemandeAffectation demande) {
        if (demande == null) {
            return false;
        }

        Eleve eleve = demande.getEleve();
        EtablissementSconet collegeSecteur = eleve.getCollegeSecteur();

        OffreFormation offre = demande.getOffreformation();

        if (collegeSecteur == null || offre == null) {
            return false;
        }

        EtablissementSconet etablissementAccueil = offre.getEtablissementSconet();

        return (collegeSecteur.getCodeRne().equals(etablissementAccueil.getCodeRne())
                && Formation.CODE_MEF_6EME.equals(offre.getFormation().getCode()));
    }

    @Override
    public boolean estDemandeSegpa(DemandeAffectation demande) {
        TypeDemandeAffec typeDemande = demande.getTypeDemande();
        String codeTypeDemande = typeDemande.getCode();

        return TypeDemandeAffec.CODE_TYPE_DEMANDE_SEGPA.equals(codeTypeDemande);
    }

    @Override
    public boolean estProposition(DemandeAffectation demande) {
        return StringUtils.equals(demande.getIndicateurTypeDemandeSpecifique(),
                TypeDemandeSpecifique.PROPOSITION.getCode());
    }

    @Override
    public boolean estDemandeBanalisee(DemandeAffectation demande) {
        return StringUtils.equals(demande.getIndicateurTypeDemandeSpecifique(),
                TypeDemandeSpecifique.BANALISEE.getCode());
    }

    @Override
    public boolean estDemandeBanaliseeSansCollege(DemandeAffectation demande) {
        boolean isBanalise = estDemandeBanalisee(demande);
        boolean isSansCollege = demande.getOffreformation() == null;

        return isSansCollege && isBanalise;
    }

    @Override
    public int getRangMax(String codeDeptMen) {
        return demandeAffectationDao.getRangMax(codeDeptMen);
    }

    @Override
    public Integer getNbMaxDerogations(String codeDeptMen) {
        return ParametreHelper.getNbMaxVoeux(codeDeptMen) - 1;
    }

    @Override
    public List<DemandeAffectationVue> listerDemandeAffectationVue(
            Pagination<DemandeAffectationVue> paginationSupport, List<Tri> listTri, Filtre filtreFormulaire,
            String codOffreFormation) throws ApplicationException {

        // Filtre des demandes sur l'offre de formation courante
        Filtre filtreOffre = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                FILTRE_COD_OFFRE_FORMATION[Constantes.FILTRE_COLONNE], codOffreFormation);

        // Agrégation des filtres
        List<Filtre> filtres = new ArrayList<>();
        filtres.add(filtreOffre);
        filtres.add(filtreFormulaire);

        Filtre filtre = FiltreFactory.createFiltreLogique(FiltreOpLogique.ET, filtres);

        return demandeAffectationVueDao.lister(paginationSupport, listTri, filtre);
    }

    @Override
    public List<Long> getDemandeAffectationSelonMotif(String codeDeptMen, String codeMotif) {
        return demandeAffectationDao.getDemandeAffectationSelonMotif(codeDeptMen, codeMotif);
    }

    @Override
    public String getFormationPropositionAffectation(DemandeAffectation demande, String mode) {
        if (StringUtils.isEmpty(mode) && demande.getFormation() != null) {
            return demande.getFormation().getCode();
        }
        return Formation.CODE_MEF_6SEGPA;
    }

    @Override
    public int compteDemandeUlis(String codeOffre) throws ApplicationException {
        Filtre fUlis = FiltreFactory.createFiltreChaines(FiltreOpChaines.FINI_PAR,
                DemandeAffectationManager.FILTRE_FORMATION_DEMANDEE_CODE[Constantes.FILTRE_COLONNE],
                Formation.TYPE_MEF_ULIS);
        Filtre fOffre = FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                DemandeAffectationManager.FILTRE_COD_OFFRE_FORMATION[Constantes.FILTRE_COLONNE], codeOffre);

        Selector<DemandeAffectation> selector = demandeAffectationDao.creerSelector();
        selector.addFiltre(fUlis);
        selector.addFiltre(fOffre);

        return selector.getNombreElements();
    }

    @Override
    public Map<String, Map<String, Integer>> getCompteurDerogEtab(String codeDeptMen) {
        return demandeAffectationDao.getCompteurDerogEtab(codeDeptMen, "sql.select.compteur.derog.etab");
    }

    @Override
    public Map<String, Map<String, Map<String, Integer>>> getCompteurDerogOffre(String codeDeptMen) {
        return demandeAffectationDao.getCompteurDerogOffre(codeDeptMen, "sql.select.compteur.derog.offre");
    }

    @Override
    public Map<String, Map<String, Integer>> getCompteurDerogExterieureEtab(String codeDeptMen) {
        return demandeAffectationDao.getCompteurDerogEtab(codeDeptMen,
                "sql.select.compteur.derog.exterieure.etab");
    }

    @Override
    public Map<String, Integer> getCompteurTotalDemandeExterneEtab(String codeDeptMen) {
        return demandeAffectationDao.getCompteurTotalDemandeExterneEtab(codeDeptMen,
                "sql.select.compteur.total.demande.externe.etab");
    }

    @Override
    public int compteDemandesUlisBanalisees(String codeDeptMen) {
        return demandeAffectationDao.compteDemandesUlisBanalisees(codeDeptMen);
    }

    @Override
    public int compteDemandesSegpaUlisBanalisees(String codeDeptMen) {
        return demandeAffectationDao.compteDemandesSegpaUlisBanalisees(codeDeptMen);
    }

    @Override
    public Map<String, Map<String, Integer>> getCompteurDerogEtabaffectationDetaillee(String codeDeptMen) {
        return demandeAffectationDao.getCompteurDerogEtab(codeDeptMen,
                "sql.select.compteur.derog.etab.affectation.detaillee");
    }

    @Override
    public Map<String, Map<String, Map<String, Integer>>> getCompteurDerogOffreAffectationDetaillee(
            String codeDeptMen) {
        return demandeAffectationDao.getCompteurDerogOffre(codeDeptMen,
                "sql.select.compteur.derog.offre.affectation.detaillee");
    }

    @Override
    public Map<String, Map<String, Integer>> getCompteurDerogExterieureEtabAffectationDetaillee(
            String codeDeptMen) {
        return demandeAffectationDao.getCompteurDerogEtab(codeDeptMen,
                "sql.select.compteur.derog.exterieure.etab.affectation.detaillee");
    }

    @Override
    public Map<String, Integer> getCompteurTotalDemandeExterneEtabAffectationDetaillee(String codeDeptMen) {
        return demandeAffectationDao.getCompteurTotalDemandeExterneEtab(codeDeptMen,
                "sql.select.compteur.total.demande.externe.etab.affectation.detaillee");
    }

    @Override
    public int compteDemandeDerogAffectees(String codeDeptMen) {
        return demandeAffectationDao.compteDemandeDerogAffectees(codeDeptMen);
    }

    @Override
    public List<DemandeAffectation> listerElevesRefusesDerog(InfoListe<DemandeAffectation> infoListe,
            boolean afficherTout) throws ApplicationException {

        List<Filtre> filtreGlobal = new ArrayList<Filtre>();

        filtreGlobal.add(infoListe.composeTousFiltres());

        // Elève affecté à son collège de secteur
        filtreGlobal.add(FiltreFactory.createFiltreProprietes(FiltreOpProprietes.EGAL,
                DemandeAffectationManager.FILTRE_COLLEGE_SECTEUR[Constantes.FILTRE_COLONNE],
                DemandeAffectationManager.FILTRE_COLLEGE_AFFECTATION_FINALE[Constantes.FILTRE_COLONNE]));

        // Affectation finale différente de la demande de rang 1
        filtreGlobal.add(FiltreFactory.createFiltreComparaison(FiltreOpComparaison.DIFFERENT,
                DemandeAffectationManager.FILTRE_RANG_AFFECTATION_FINALE[Constantes.FILTRE_COLONNE],
                Short.valueOf("1")));

        // Affectation finale en 6ème
        filtreGlobal.add(FiltreFactory.createFiltreComparaison(FiltreOpComparaison.EGAL,
                DemandeAffectationManager.FILTRE_FORMATION_AFFECTATION_FINALE[Constantes.FILTRE_COLONNE],
                Formation.CODE_MEF_6EME));

        // Demande de dérogation
        filtreGlobal.add(FiltreFactory.createFiltreCollection(FiltreOpCollection.EST_NON_VIDE,
                DemandeAffectationManager.FILTRE_JUSTIF_DEROGS[Constantes.FILTRE_COLONNE]));

        Pagination<DemandeAffectation> pagination = afficherTout ? null : infoListe.getPaginationSupport();
        return lister(pagination, infoListe.getListTri(),
                FiltreFactory.createFiltreLogique(FiltreOpLogique.ET, filtreGlobal));
    }

    @Override
    public EtablissementSconet getCollegeSecteurPreAffectation(Eleve eleve) {
        if (eleve.getEstCollegeSecteurDansDepartement()) {
            // si le gestionnaire a choisi le collège de pré affectation, on renvoie celui ci
            if (eleve.getCollegeSecteur() != null && eleve.getEstCollegeSecteurSaisiParDsden()) {
                return eleve.getCollegeSecteur();
            }

            // Récupération du collège de secteur le plus approprié pour le voeu de secteur
            CollegeSecteur collegeSecteur = DemandeAffectationHelper.getCollegeSecteurSansDecisionDsden(eleve);

            if (collegeSecteur != null) {
                return collegeSecteur.getEtablissementSconet();
            }
        } else if (!eleve.getEstCollegeSecteurDansDepartement() && eleve.getCollegeSecteur() != null) {
            // si le gestionnaire a choisi le collège de pré affectation, on renvoie celui ci
            return eleve.getCollegeSecteur();
        }

        return null;
    }

    @Override
    public void gereDemandeAffectation(Eleve eleve) throws ApplicationException {

        LOG.debug("Création des demandes d'affectation pour l'élève " + EleveHelper.getIdentite(eleve));

        // désignation du collège de pré affectation
        EtablissementSconet etabSecteur = getCollegeSecteurPreAffectation(eleve);
        eleve.setCollegeSecteur(etabSecteur);
        // chargement des données nécessaires pour le remplissage des demandes d'affectation
        DeptMen deptMen = eleve.getDept();

        if (eleve.getEstAffectationDemandeeCollegePublicDept() != null) {
            eleveManager.videDemandesAffecSansPropositionSegpa(eleve);
            // recherche une proposition SEGPA / ULIS dans la liste des demandes d'affectation d'un élève au lieu
            // de prendre la première demande de cette liste car suite à l'evol du college de secteur on ne
            // supprime plus systématiquement les demandes avant de les recréer
            DemandeAffectation proposition = rechercheProposition(eleve);

            // Demande hors collège public du département
            if (!eleve.getEstAffectationDemandeeCollegePublicDept()) {
                creerDemandeHorsCollegePublicDepartement(eleve, deptMen);
            } else {
                creerDemandeCollegePublicDepartement(eleve, deptMen, etabSecteur, proposition);
            }
        }
    }

    @Override
    public EtatDemandeAffectation creerDemandeHorsCollegePublicDepartement(Eleve eleve, DeptMen deptMen)
            throws ApplicationException {

        // si l'élève est maintenu à l'école, alors on ne crée pas de demande par défaut
        DecisionPassage decisionPassage = eleve.getDecisionPassage();

        if (decisionPassage == null || !DecisionPassageEnum.MAINTENU.getCode().equals(decisionPassage.getCod())) {
            LOG.debug(
                    "Création d'une demande d'affectation dans un collège public hors du département pour l'élève "
                            + EleveHelper.getIdentite(eleve));

            TypeDemandeAffec typeDemandeAffecHorsCollegePublicDept = typeDemandeAffecManager
                    .charger(TypeDemandeAffec.CODE_TYPE_DEMANDE_HORS_COLLEGE_PUBLIC_DEPT);

            DemandeAffectation demandeHorsCollegePublicDept = new DemandeAffectation();
            demandeHorsCollegePublicDept.setDept(deptMen);
            demandeHorsCollegePublicDept
                    .setEtatDemande(etatDecisionManager.charger(EtatDecision.CODE_HORS_AFFECTATION));
            demandeHorsCollegePublicDept.setTypeDemande(typeDemandeAffecHorsCollegePublicDept);
            demandeHorsCollegePublicDept
                    .setIndicateurTypeDemandeSpecifique(TypeDemandeSpecifique.NORMALE.getCode());
            demandeHorsCollegePublicDept.setRang(1);
            demandeHorsCollegePublicDept.setNumDossier(eleve);

            // ajout de la demande à l'élève
            eleve.addDemandeAffectation(demandeHorsCollegePublicDept);

            demandeAffectationManager.creer(demandeHorsCollegePublicDept);

            return EtatDemandeAffectation.CREEE;
        } else {
            LOG.debug("La demande d'affectation dans un collège public hors du département pour l'élève "
                    + EleveHelper.getIdentite(eleve)
                    + ") n'a pas été créé car il est maintenu à l'école primaire");
            return EtatDemandeAffectation.IMPOSSIBLE_DECISION_PASSAGE_MAINTENU;
        }
    }

    /**
     * Créée une demande dans un collège public du département.
     * 
     * @param eleve
     *            L'élève
     * @param deptMen
     *            le département
     * @param etabSecteur
     *            le collège de secteur
     * @param propositionSegpa
     *            la dmeande d'affectation
     * @throws ApplicationException
     *             en cas d'erreur
     */
    private void creerDemandeCollegePublicDepartement(Eleve eleve, DeptMen deptMen,
            EtablissementSconet etabSecteur, DemandeAffectation propositionSegpa) throws ApplicationException {
        // chargement des données nécessaires pour le remplissage des demandes d'affectation
        EtatDecision etatDecisionNonTraite = etatDecisionManager.charger(EtatDecision.CODE_NON_TRAITE);
        TypeDemandeAffec typeDemandeAffecDansCollegePublicDept = typeDemandeAffecManager
                .charger(TypeDemandeAffec.CODE_TYPE_DEMANDE_DANS_COLLEGE_PUBLIC_DEPT);
        TypeDemandeAffec typeDemandeSegpa = typeDemandeAffecManager
                .charger(TypeDemandeAffec.CODE_TYPE_DEMANDE_SEGPA);
        int rang = 0;
        Formation formation = eleve.getFormationDemandeSecteur();

        if (eleve.getEstScolarisationDansCollegeSecteur() != null) {
            if (eleve.getEstScolarisationDansCollegeSecteur()) {
                // élève scolarisé dans le collège de secteur
                if (formation != null && !StringUtils.equals(Formation.CODE_MEF_6EME, formation.getCode())
                        && etabSecteur != null) {
                    EtablissementSconet etabDerog = null;
                    if (!offreFormationManager.existeByFormationAndEtab(formation.getCode(),
                            etabSecteur.getCodeRne())) {
                        if (CollectionUtils.isNotEmpty(eleve.getCollegeSecteurs())) {
                            // On cherche le premier collège de la liste (autre que celui de secteur) qui
                            // propose la formation.
                            for (CollegeSecteur collegeSecteur : eleve.getCollegeSecteurs()) {
                                EtablissementSconet etab = collegeSecteur.getEtablissementSconet();
                                if (!etab.equals(etabSecteur) && offreFormationManager.existeByFormationAndEtab(
                                        formation.getCode(), etab.getCodeRne()) && etabDerog == null) {
                                    etabDerog = etab;
                                }
                            }
                        }
                    } else {
                        etabDerog = etabSecteur;
                    }

                    DemandeAffectation demandeDerogDansCollegeSecteur = new DemandeAffectation();
                    demandeDerogDansCollegeSecteur
                            .setIndicateurTypeDemandeSpecifique(TypeDemandeSpecifique.NORMALE.getCode());
                    if (etabDerog == null) {
                        if (DemandeAffectationHelper.estFormationSegpaOuUlis(formation.getCode())) {
                            demandeDerogDansCollegeSecteur
                                    .setIndicateurTypeDemandeSpecifique(TypeDemandeSpecifique.BANALISEE.getCode());
                        } else {
                            throw new ValidationException(Globals.ERROR_KEY,
                                    "error.dossier.aucuneOffreFormationExistante", formation.getLibelle());
                        }
                    }

                    demandeDerogDansCollegeSecteur.setRang(++rang);
                    if (formation.getEstFormationSegpa()) {
                        demandeDerogDansCollegeSecteur.setTypeDemande(typeDemandeSegpa);
                    } else {
                        demandeDerogDansCollegeSecteur.setTypeDemande(typeDemandeAffecDansCollegePublicDept);
                    }
                    demandeDerogDansCollegeSecteur
                            .setIndicateurTypeDemandeSpecifique(TypeDemandeSpecifique.NORMALE.getCode());
                    demandeDerogDansCollegeSecteur.setDept(deptMen);
                    demandeDerogDansCollegeSecteur.setEtatDemande(etatDecisionNonTraite);
                    demandeDerogDansCollegeSecteur.setNumDossier(eleve);
                    demandeDerogDansCollegeSecteur.setFormation(formation);
                    if (etabDerog != null) {
                        demandeDerogDansCollegeSecteur.setOffreformation(
                                offreFormationManager.chargerByFormationAndEtab(formation.getCode(),
                                        etabDerog.getCodeRne(), deptMen.getDeptCode()));
                    }
                    // création des demandes d'affectaion et MAJ des compteurs
                    demandeAffectationManager.creer(demandeDerogDansCollegeSecteur);
                    if (!formation.getEstFormationSegpa() && !formation.getEstFormationUlis()) {
                        DemandeFamille demandeFamille = eleve.getDemandeFamilles().iterator().next();
                        for (MotifDerog motif : demandeFamille.getMotifs()) {
                            JustificatifDerog justificatifDerog = new JustificatifDerog(motif, eleve.getDept(),
                                    demandeDerogDansCollegeSecteur);
                            justificatifDerogManager.creer(justificatifDerog);
                        }
                    }
                    OffreFormation offre = demandeDerogDansCollegeSecteur.getOffreformation();
                    // On met à jour une deuxième fois les compteurs des offres pour les élèves
                    // ayant au moins une dérogation
                    // car NBR_DEM_DRG est erroné dans la première MAJ des compteurs des offres
                    demandeAffectationManager.majCompteursOffre(offre);

                }
            } else if (CollectionUtils.isNotEmpty(eleve.getDemandeFamilles())) {
                LOG.debug("Création des demandes d'affectations pour l'élève " + EleveHelper.getIdentite(eleve));
                for (DemandeFamille demandeFamille : eleve.getDemandeFamilles()) {
                    DemandeAffectation demande = new DemandeAffectation();
                    demande.setRang(++rang);
                    demande.setDept(deptMen);
                    demande.setNumDossier(eleve);
                    demande.setFormation(formation);
                    demande.setEtatDemande(etatDecisionNonTraite);

                    if (formation.getEstFormationSegpa()) {
                        demande.setTypeDemande(typeDemandeSegpa);
                    } else {
                        demande.setTypeDemande(typeDemandeAffecDansCollegePublicDept);
                    }
                    if (demandeFamille.getEtablissement() != null) {
                        demande.setOffreformation(
                                offreFormationManager.chargerByFormationAndEtab(formation.getCode(),
                                        demandeFamille.getEtablissement().getCodeRne(), deptMen.getDeptCode()));
                        demande.setIndicateurTypeDemandeSpecifique(TypeDemandeSpecifique.NORMALE.getCode());
                    } else {
                        demande.setIndicateurTypeDemandeSpecifique(TypeDemandeSpecifique.BANALISEE.getCode());
                    }

                    // création des demandes d'affectations et MAJ des compteurs des offres de formation
                    // Attention : pour les élèves ayant au moins une dérogation(Ni SEGPA ni ULIS), le
                    // nombre des demandes de dérogation NBR_DEM_DRG est faux
                    demandeAffectationManager.creer(demande);
                    if (CollectionUtils.isNotEmpty(demandeFamille.getMotifs())) {
                        for (MotifDerog motif : demandeFamille.getMotifs()) {
                            JustificatifDerog justificatifDerog = new JustificatifDerog(motif, demande.getDept(),
                                    demande);
                            justificatifDerogManager.creer(justificatifDerog);
                        }
                    }
                    OffreFormation offre = demande.getOffreformation();
                    // On met à jour une deuxième fois les compteurs des offres pour les élèves
                    // ayant au moins une dérogation
                    // car NBR_DEM_DRG est erroné dans la première MAJ des compteurs des offres
                    demandeAffectationManager.majCompteursOffre(offre);
                }

            }

            // si le collège de secteur est dans le département et qu'un collège existe
            if (BooleanUtils.isTrue(eleve.getEstCollegeSecteurDansDepartement()) && etabSecteur != null) {
                rang = creerDemandeAffectationSecteur(rang, deptMen, eleve, etatDecisionNonTraite,
                        typeDemandeAffecDansCollegePublicDept, etabSecteur);
            }

            if (propositionSegpa != null) {
                propositionSegpa.setRang(++rang);
                demandeAffectationManager.maj(propositionSegpa);
            }
        }
    }

    /**
     * Méthode qui créé la demande d'affecation dans le collège de secteur de pré affectation d'un élève.
     * 
     * @param rang
     *            Le rang de la demande précédente
     * @param deptMen
     *            le département
     * @param eleve
     *            l'élève
     * @param etatDecisionNonTraite
     *            état de la décision non traité
     * @param typeDemandeAffecDansCollegePublicDept
     *            type de demande
     * @param etabSecteur
     *            le collège de secteur pré affecté
     * @return le rang de la demande créée
     * @throws ApplicationException
     *             en cas d'erreur
     */
    private int creerDemandeAffectationSecteur(int rang, DeptMen deptMen, Eleve eleve,
            EtatDecision etatDecisionNonTraite, TypeDemandeAffec typeDemandeAffecDansCollegePublicDept,
            EtablissementSconet etabSecteur) throws ApplicationException {

        LOG.debug("Création d'une demande d'affectations dans le collège de secteur de pré affectation de l'élève "
                + EleveHelper.getIdentite(eleve));

        DemandeAffectation demandeSecteur = new DemandeAffectation();
        demandeSecteur.setRang(++rang);
        demandeSecteur.setDept(deptMen);
        demandeSecteur.setNumDossier(eleve);
        demandeSecteur.setEtatDemande(etatDecisionNonTraite);
        demandeSecteur.setTypeDemande(typeDemandeAffecDansCollegePublicDept);
        demandeSecteur.setFormation(formationManager.charger(Formation.CODE_MEF_6EME));
        demandeSecteur.setOffreformation(offreFormationManager.chargerByFormationAndEtab(Formation.CODE_MEF_6EME,
                etabSecteur.getCodeRne(), deptMen.getDeptCode()));
        demandeSecteur.setIndicateurTypeDemandeSpecifique(TypeDemandeSpecifique.NORMALE.getCode());
        // ajout de la demande d'affectation à l'élève
        eleve.addDemandeAffectation(demandeSecteur);
        demandeAffectationManager.creer(demandeSecteur);

        return rang;
    }

    @Override
    public EtatDemandeAffectation genererDemandesAutomatique(Eleve eleve, String codeDeptMen)
            throws ApplicationException {
        if (eleve.getEstAffectationDemandeeCollegePublicDept() == null) {
            if (eleve.getEstCollegeSecteurDansDepartement()
                    && CollectionUtils.isNotEmpty(eleve.getCollegeSecteurs())) {
                // création du voeu de secteur
                LOG.debug("Création automatique des demandes d'affectaion pour l'élève "
                        + EleveHelper.getIdentite(eleve));
                return eleveManager.creerDemandesDefautEleve(eleve);

            } else if (!eleve.getEstCollegeSecteurDansDepartement()) {
                // creation de la demande hors département
                LOG.debug("Création automatique d'une demande hors département pour l'élève "
                        + EleveHelper.getIdentite(eleve));
                EtatDemandeAffectation etatDemandeAffectation = demandeAffectationManager
                        .creerDemandeHorsCollegePublicDepartement(eleve,
                                deptMenManager.chargerParDepartement(codeDeptMen));
                eleve.setEstAffectationDemandeeCollegePublicDept(false);
                return etatDemandeAffectation;
            }
        }
        return EtatDemandeAffectation.RIEN;
    }

    @Override
    public DemandeAffectation rechercheProposition(Eleve eleve) {
        Set<DemandeAffectation> demandesAffectationEleve = eleve.getDemandeAffectation();

        if (CollectionUtils.isNotEmpty(demandesAffectationEleve)) {
            // recherche si l'élève a une demande d'affectation en SEGPA
            for (DemandeAffectation demandeAffectation : demandesAffectationEleve) {
                if (estProposition(demandeAffectation)) {
                    return demandeAffectation;
                }
            }
        }
        return null;
    }

    @Override
    public void restaurerDemandes(Eleve eleve, Log log) throws ApplicationException {
        SortedSet<SauvegardeDemandeAffectation> sauvegardeDemandesAffectation = log.getDemandesAffectation();

        // Réinjecte les demandes d'affectation
        EtatDecision etatNonTraite = etatDecisionManager.charger(EtatDecision.CODE_NON_TRAITE);
        for (SauvegardeDemandeAffectation curSauvegardeDemandeAffectation : sauvegardeDemandesAffectation) {
            // Part de la demande d'affectation sauvegardée en forçant à l'état non traité la décision
            // Cette action va forcer l'élève à repartir en pré-affectation
            DemandeAffectation demandeAffectation = copierSauvegardeDemandeAffectation(
                    curSauvegardeDemandeAffectation);
            demandeAffectation.setEtatDemande(etatNonTraite);

            // lors de la création de la demande les compteurs des offres seront mis à jour une première fois
            demandeAffectationManager.creer(demandeAffectation);

            // on créé les justificatifs de dérogation restaurés
            if (CollectionUtils.isNotEmpty(demandeAffectation.getJustificatifDerogs())) {
                for (JustificatifDerog justificatifDerog : demandeAffectation.getJustificatifDerogs()) {
                    justificatifDerogManager.creer(justificatifDerog);
                }
            }

            // on met à jour une deuxième fois les compteurs des offres pour les élèves ayant au moins une
            // dérogation car NBR_DEM_DRG est erroné dans la première MAJ des compteurs des offres
            demandeAffectationManager.majCompteursOffre(demandeAffectation.getOffreformation());

            eleve.addDemandeAffectation(demandeAffectation);
        }
    }

    /**
     * Copie la sauvegarde de demande d'affectation pour la restaurer
     * 
     * @param sauvegardeDemandeAffectation
     *            sauvegarde de la demande d'affection
     * @return la demande restaurée
     */
    private DemandeAffectation copierSauvegardeDemandeAffectation(
            SauvegardeDemandeAffectation sauvegardeDemandeAffectation) {
        DemandeAffectation demandeAffectation = new DemandeAffectation();
        demandeAffectation.setDept(sauvegardeDemandeAffectation.getDept());
        demandeAffectation.setNumDossier(sauvegardeDemandeAffectation.getNumDossier());
        demandeAffectation.setTypeDemande(sauvegardeDemandeAffectation.getTypeDemande());
        demandeAffectation.setRang(sauvegardeDemandeAffectation.getRang());
        demandeAffectation.setOffreformation(sauvegardeDemandeAffectation.getOffreformation());
        demandeAffectation.setEtatDemande(sauvegardeDemandeAffectation.getEtatDemande());
        demandeAffectation.setIndicateurTypeDemandeSpecifique(
                sauvegardeDemandeAffectation.getIndicateurTypeDemandeSpecifique());
        demandeAffectation.setFormation(sauvegardeDemandeAffectation.getFormation());

        SortedSet<SauvegardeJustificatifDerog> sauvegardeJustificatifDerogs = sauvegardeDemandeAffectation
                .getSauvegardeJustificatifDerogs();

        for (SauvegardeJustificatifDerog curSauvegardeJustificatifDerog : sauvegardeJustificatifDerogs) {
            JustificatifDerog justificatifDerog = copierSauvegardeJustificatifDero(curSauvegardeJustificatifDerog);
            justificatifDerog.setIdDemandeAffec(demandeAffectation);
            demandeAffectation.addJustificatifDerog(justificatifDerog);
        }

        return demandeAffectation;
    }

    /**
     * Copie la sauvegarde de justificatif de dérogation pour la restaurer.
     * 
     * @param sauvegardeJustificatifDerog
     *            sauvegarde du justificatif de dérogation
     * @return le justificatif restauré
     */
    private JustificatifDerog copierSauvegardeJustificatifDero(
            SauvegardeJustificatifDerog sauvegardeJustificatifDerog) {
        JustificatifDerog justificatifDerog = new JustificatifDerog();
        justificatifDerog.setIdMotifDerog(sauvegardeJustificatifDerog.getIdMotifDerog());
        justificatifDerog.setDept(sauvegardeJustificatifDerog.getDept());
        return justificatifDerog;
    }
}
