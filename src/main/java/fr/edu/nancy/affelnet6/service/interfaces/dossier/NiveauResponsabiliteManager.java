/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.service.interfaces.dossier;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import fr.edu.nancy.affelnet6.domain.nomenclature.NiveauResponsabilite;
import fr.edu.nancy.commun.dao.interfaces.BaseDao;
import fr.edu.nancy.commun.dao.interfaces.Tri;
import fr.edu.nancy.commun.service.interfaces.DaoManager;

/** Gestionnaire des niveaux de responsabilité. */
public interface NiveauResponsabiliteManager extends
        DaoManager<NiveauResponsabilite, String, BaseDao<NiveauResponsabilite, String>> {

    /** Tri par défaut pour les niveaux de responsabilité (par ordre). */
    List<Tri> TRI_DEFAUT = Collections.unmodifiableList(Arrays.asList(new Tri("ordre")));
}
