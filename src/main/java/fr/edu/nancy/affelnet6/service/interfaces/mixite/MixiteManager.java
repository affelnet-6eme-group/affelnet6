/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.service.interfaces.mixite;

import java.util.List;

import fr.edu.nancy.affelnet6.domain.affectation.SuiviMixite;
import fr.edu.nancy.commun.dao.interfaces.filtres.Filtre;
import fr.edu.nancy.commun.exceptions.TechnicalException;

/**
 * Interface du service de gestion de la mixité des secteurs multi-collèges.
 * 
 * 
 *
 */
public interface MixiteManager {

    /**
     * 
     * @param filtreEleves
     *            un filtre pour restreindre les élèves
     * @return la liste des Suivi de mixité
     * @throws TechnicalException
     *             en cas d'erreur technique
     */
    List<SuiviMixite> suiviMixite(Filtre filtreEleves) throws TechnicalException;
}
