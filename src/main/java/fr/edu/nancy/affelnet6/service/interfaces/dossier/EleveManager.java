/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.service.interfaces.dossier;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import fr.edu.nancy.affelnet6.domain.affectation.DecisionDepartement;
import fr.edu.nancy.affelnet6.domain.calcul.DonneesCalculCollegeSecteur;
import fr.edu.nancy.affelnet6.domain.dossier.CollegeSecteur;
import fr.edu.nancy.affelnet6.domain.dossier.DemandeAffectation;
import fr.edu.nancy.affelnet6.domain.dossier.Eleve;
import fr.edu.nancy.affelnet6.domain.dossier.Responsable;
import fr.edu.nancy.affelnet6.domain.dossier.SauvegardeEleve;
import fr.edu.nancy.affelnet6.domain.dossier.SecteurAffectation;
import fr.edu.nancy.affelnet6.domain.dossier.SuiviEcole;
import fr.edu.nancy.affelnet6.domain.nomenclature.EtablissementSconet;
import fr.edu.nancy.affelnet6.domain.nomenclature.EtatDecision;
import fr.edu.nancy.affelnet6.domain.nomenclature.Matiere;
import fr.edu.nancy.affelnet6.service.impl.dossier.EtatDemandeAffectation;
import fr.edu.nancy.commun.dao.interfaces.Pagination;
import fr.edu.nancy.commun.dao.interfaces.Tri;
import fr.edu.nancy.commun.dao.interfaces.filtres.Filtre;
import fr.edu.nancy.commun.exceptions.ApplicationException;
import fr.edu.nancy.commun.exceptions.BusinessException;
import fr.edu.nancy.commun.exceptions.DaoException;
import fr.edu.nancy.commun.exceptions.TechnicalException;
import fr.edu.nancy.commun.service.securite.engine.ProfilUtilisateur;
import fr.edu.nancy.commun.utils.BatchProgressMonitor;
import fr.edu.nancy.commun.web.session.InfoListe;

/**
 * Gestionnaire de Eleve.
 * 
 * @see fr.edu.nancy.affelnet6.domain.dossier.Eleve
 */
public interface EleveManager {

    /** Filtre sur la valeur du nom. */
    String[] FILTRE_NOM = { "0", "nom", "Nom" };
    /** Filtre sur la valeur du prénom 1. */
    String[] FILTRE_PRENOM = { "1", "prenom", "Prénom" };
    /** Filtre sur la valeur de l'édition du volet1. */
    String[] FILTRE_EDITION_VOLET1 = { "2", "editionVolet1", "Volet 1 édité" };
    /** Filtre sur la valeur du numéro de dossier. */
    String[] FILTRE_NUM_DOSSIER = { "3", "numDossier", "Numéro de dossier" };
    /** Filtre sur la valeur de l'édition du volet2. */
    String[] FILTRE_EDITION_VOLET2 = { "4", "editionVolet2", "Volet 2 édité" };
    /** Filtre sur la valeur du code de l'établissement d'origine. */
    String[] FILTRE_ECOLE_ORIGINE = { "5", "codUaiOri", "Etablissement d'origine" };
    /** Filtre sur la valeur de la décision de passage. */
    String[] FILTRE_DECISION_PASSAGE = { "7", "decisionPassage.cod", "Decision passage" };
    /** Filtre sur la valeur du code de la langue étrangère obligatoire étudiée à l'école. */
    String[] FILTRE_LANGUE_ECOLE_OBL = { "", "langueEtrangereOblEcole.code", "Langue étrangère obligatoire" };
    /** Filtre sur la valeur du code de la langue étrangère facultative étudiée à l'école. */
    String[] FILTRE_LANGUE_ECOLE_FAC = { "", "langueEtrangereFacEcole.code", "Langue étrangère facultative" };
    /** Filtre sur la valeur du code de la langue régionale étudiée à l'école. */
    String[] FILTRE_LANGUE_ECOLE_REG = { "", "langueRegionaleEcole.code", "Langue régionale" };
    /** Filtre sur la valeur du code du collège de secteur. */
    String[] FILTRE_COLLEGE_SECTEUR = { "9", "collegeSecteur.codeRne", "Collège secteur" };
    /** Filtre sur la valeur du type de saisie (origine du dossier). */
    String[] FILTRE_TYPE_SAISIE = { "10", "origineDossier", "type de saisie" };
    /** Filtre sur la valeur du département. */
    String[] FILTRE_DEPT = { "11", "dept.deptMenId", "Département" };
    /** Filtre sur la date de mise a jour. */
    String[] FILTRE_DT_MAJ = { "12", "dateMaj", "Date mise à jour" };
    /** Filtre sur la date de mise a jour. */
    String[] FILTRE_INE = { "13", "ine", "INE" };
    /** Filtre sur la langue de l'a demande d'affectation. */
    String[] FILTRE_LANGUE_AFFECT = { "14", "demandeAffectation.lv1.code", "Langue" };
    /** Filtre sur la langue de l'a demande d'affectation. */
    String[] FILTRE_FORMATION_AFFECT = { "15", "demandeAffectation.offreformation.formation.code", "Formation" };
    /** Filtre sur le code MEN du département. */
    String[] FILTRE_CODE_DEPT = { "16", "dept.deptCode", "Département" };
    /** Filtre sur l'origine du dossier. */
    String[] FILTRE_ORIGINE_SAISIE = { "18", "origineDossier", "Origine de la saisie" };
    /** Filtre sur le code du collège de saisie. */
    String[] FILTRE_COLLEGE_SAISIE = { "19", "collegeSaisie.codeRne", "Collège de saisie" };
    /** Filtre sur les demandes d'affectation. */
    String[] FILTRE_DEMANDES = { "22", "demandeAffectation", "Demandes d'affectation" };
    /** Filtre sur l'affectation finale. */
    String[] FILTRE_AFFECTATION_FINALE = { "23", "affectationFinale", "Affectation finale" };
    /** Filtre sur le code RNE du collège d'affectation finale. */
    String[] FILTRE_RNE_CLG_AFFECT_FINALE = { "24", "affectationFinale.offreformation.etablissementSconet.codeRne",
            "Code RNE du collège d'affectation finale" };
    /** Filtre sur le code MEF de la formation d'affection finale. */
    String[] FILTRE_MEF_FORMATION_AFFECT_FINALE = { "25", "affectationFinale.offreformation.formation.code",
            "Code Mef de la formation d'affectation finale" };
    /** Filtre sur le rang de l'affectation finale. */
    String[] FILTRE_RANG_AFFECTATION_FINALE = { "26", "affectationFinale.rang", "Rang de l'affectation finale" };
    /** Filtre sur le code état de la décision finale. */
    String[] FILTRE_CODE_ETAT_FINALE = { "27", "codeDecisionFinale.codeEtatFinal",
            "Code état de la décision finale" };
    /** Filtre sur l'indicateur de type de demande. */
    String[] FILTRE_BANAL = { "28", "demandeAffectation.indicateurTypeDemandeSpecifique",
            "Indicateur de type de demande" };
    /** Filtre sur le type de demande. */
    String[] FILTRE_TYPE_DEMANDE = { "29", "demandeAffectation.typeDemande.code", "Type de demande" };
    /** Filtre sur la liste des responsables légaux. */
    String[] FILTRE_EXISTENCE_RESP = { "30", "responsables", "Responsables légaux" };
    /** Filtre sur l'adresse de résidence de l'élève à la rentrée. */
    String[] FILTRE_ADR = { "", "adresseResidence", "Adresse de résidence de l'élève à la rentrée" };
    /** Filtre sur le sexe de l'élève. */
    String[] FILTRE_SEXE = { "", "sexe", "Sexe de l'élève" };
    /** Filtre sur le collège d'affectation finale. */
    String[] FILTRE_CLG_AFF_FINALE = { "", "affectationFinale.offreformation.etablissementSconet",
            "Collège d'affectation finale" };
    /** Filtre sur la formation d'affectation finale. */
    String[] FILTRE_FORMATION_AFF_FINALE = { "", "affectationFinale.offreformation.formation",
            "Formation d'affectation finale" };
    /** Filtre sur l'adresse des responsables. */
    String[] FILTRE_RESP_ADR = { "", "responsables.adresseResponsable", "Adresse des responsables" };
    /** Filtre sur l'etabId du collège de secteur. */
    String[] FILTRE_ID_COLLEGE_SECTEUR = { "", "collegeSecteur.etabId", "Collège de secteur" };
    /** Filtre sur l'etabId d'un collège demandé par l'élève. */
    String[] FILTRE_ID_COLLEGE_DEMANDE = { "", "demandeAffectation.offreformation.etablissementSconet.etabId",
            "Collège demandé" };
    /** Filtre sur le(s) justificatif(s) de dérogation d'une demande. */
    String[] FILTRE_JUSTIF_DEROG = { "", "demandeAffectation.justificatifDerogs",
            "Justificatif(s) de dérogation" };
    /** Filtre sur l'etabId du collège d'affectation finale. */
    String[] FILTRE_ID_CLG_AFF_FINALE = { "", "affectationFinale.offreformation.etablissementSconet.etabId",
            "Collège d'affectation finale" };
    // String[] FILTRE_CODE_RNE_CLG_AFF_FINALE = { "",
    // "affectationFinale.offreformation.etablissementSconet.etabId.codeRne", "Collège d'affectation finale" };
    /** Filtre sur le(s) justificatif(s) de l'affectation finale. */
    String[] FILTRE_JUSTIF_DEROG_AFF_FINALE = { "", "affectationFinale.justificatifDerogs",
            "Justificatif(s) de dérogation de l'affectation finale" };
    /** Filtre sur le rang de la demande d'affectation. */
    String[] FILTRE_RANG_DEMANDE = { "", "demandeAffectation.rang",
            "Justificatif(s) de dérogation de l'affectation finale" };

    /** Filtre sur le code RNE d'un collège demandé. */
    String[] FILTRE_CODE_RNE_CLG_AFFECTION = { "24",
            "demandeAffectation.offreformation.etablissementSconet.codeRne", "Code RNE d'un collège demandé" };

    /** Filtre sur l'état d'une demande. */
    String[] FILTRE_ETAT_DEMANDE = { "", "demandeAffectation.etatDemande.codeEtatFinal", "Etat de demande" };

    /** Filtre sur le secteur de l'école d'origine : privée ou publique. */
    String[] FILTRE_SECTEUR_ECOLE_ORIGINE = { "", "ecoleOrigine.secteur.secteurCode", "Secteur école origine" };

    /** */
    String[] FILTRE_COLLEGE_SECTEUR_DANS_DEPARTEMENT = { "", "estCollegeSecteurDansDepartement",
            "Collège de secteur dans le département" };

    /** Filtre sur le code postal de l'adresse de l élève : code postal. */
    String[] FILTRE_LIBELLE_CODE_POSTAL = { "", "adresseResidence.codePostal.codePostal", "Code Postal" };

    /** Filtre sur la commune postale de l'adresse de l élève. */
    String[] FILTRE_LIBELLE_COMMUNE_POSTAL = { "", "adresseResidence.communePostal.llPostal", "Commune" };

    /** Filtre sur la commune INSEE de l'adresse de l élève. */
    String[] FILTRE_LIBELLE_COMMUNE_INSEE = { "", "adresseResidence.communeInsee.llInseeCommune", "Commune" };

    /** Filtre sur l'orientation en Ulis. */
    String[] FILTRE_ORIENTATION_ULIS = { "", "demandeAffectation.ulis", "Orientation en Ulis" };

    /** Filtre sur le type de demande d'affectation finale. */
    String[] FILTRE_AFFECTATION_FINALE_TYPE_DEMANDE = { "", "affectationFinale.typeDemande.code",
            "Type de demande d'affectation finale" };

    /** Filtre sur l'affectation finale en Ulis. */
    String[] FILTRE_AFFECTATION_FINALE_ULIS = { "", "affectationFinale.formation.code",
            "Affectation finale en Ulis" };

    /** Filtre sur le code du département d'origine de l'élève importé. */
    String[] FILTRE_DOSSIER_SOURCE_CODE_DEPT = { "", "numDossierOrigine.dept.deptCode",
            "Code du département d'origine de l'élève importé" };

    /** Filtre sur l'état de l'export de l'élève vers SIECLE BEE. */
    String[] FILTRE_ETAT_EXPORT_BEE = { "", "etatExportBee.code", "État de l'export vers BEE" };

    /** Filtre sur le code de la formation demandée. */
    String[] FILTRE_FORMATION_DEMANDEE = { "", "demandeAffectation.formation.code",
            "Code de la formation demandée" };

    /** Filtre sur le(s) collège(s) de secteur potentiels. */
    String[] FILTRE_COLLEGE_POTENTIEL = { "", "collegeSecteurs", "Collège(s) de secteur potentiels" };

    /** Filtre sur le code UAI d'un des collèges de secteur potentiels de l'élève. */
    String[] FILTRE_CODE_UAI_COLLEGE_POTENTIEL = { "", "collegeSecteurs.etablissementSconet.codeRne",
            "Collège(s) de secteur" };

    /** Filtre sur le code UAI d'un des collèges de secteur issu de la simulation. */
    String[] FILTRE_CODE_UAI_COLLEGE_SIMULE = { "", "collegeSecteursSimules.etablissementSconet.codeRne",
            "Collège(s) de secteur simulé(s)" };

    /** Filtre sur les demandes de l'élève si elles sont dans un collège public du département. */
    String[] FILTRE_AFFECTATION_DEMANDEE_CLG_PUB_DEP = { "", "estAffectationDemandeeCollegePublicDept",
            "Affectation demandée dans un collège publique du département" };

    /** Filtre sur le code de la formation demandée par la famille. */
    String[] FILTRE_FORMATION_CHOIX_FAMILLE = { "", "formationDemandeSecteur.code",
            "Code de la formation demandée par la famille" };

    /** Filtre sur le flag "Adresse Française" de l'adresse. */
    String[] FILTRE_EXISTE_ADR_A_TRAITER = { "36", "existeAdresseATraiter",
            "Flag indiquant si l'élève ou un de ses responsables à une adresse à vérifier" };

    /** Filtre sur les adresses des responsables françaises ou non. */
    String[] FILTRE_RESP_ADR_IS_FRA = { "37", "responsables.adresseResponsable.adresseEnFrance",
            "Adresse en France" };

    /** Filtre sur le flag "Saisie automatique du collège de secteur" de l'adresse. */
    String[] FILTRE_SAISIE_COLLEGE_SECTEUR_AUTO = { "38", "estSaisieCollegeSecteurAuto",
            "Flag indiquant si le(s) collège(s) de secteur ont été déterminés automatiquement" };

    /** Filtre sur le flag "Aucun collège de secteur n'a de correspondance pour l'adresse de l'élève. */
    String[] FILTRE_NON_CORRESPONDANCE_COLLEGE_ADRESSE = { "39", "nonCorrespondanceCollegeSecteurPourAdresse",
            "Flag indiquant si le calcul automatique du ou des collège(s) de secteur n'a pas été déterminé pour défaut de correspondance de l'adresse de l'élève." };

    /** Filtre sur le flag "transfert automatique vers un autre département. */
    String[] FILTRE_EST_TRANSFERE_AUTRE_DEPARTEMENT_AUTO = { "40", "estTransfereAutreDeptAuto",
            "Flag indiquant si un élève a été transféré automatiquement vers un autre département de l'académie." };

    /**
     * Filtre sur la manière dont a été valorisé le collège de secteur : déterminé automatiquement = autoAbouti,
     * calcul abouti automatiquement hors du département, calcul automatique n''a pas abouti = autoNonAbouti,
     * les collèges ont été saisi manuellement = manuel ou pas de collèges = NULL.
     */
    String[] FILTRE_ETAT_VALORISE_COLLEGE_SECTEUR = { "41", "etatCollegeSecteur.code",
            "Calcul du collège de secteur" };
    /**
     * Filtre sur la manière dont a été valorisé le collège de secteur lors de la simulation : déterminé
     * automatiquement = autoAbouti, la
     * calcul automatique n''a pas abouti = autoNonAbouti, les collèges ont été saisi manuellement = manuel ou pas
     * de collèges = NULL.
     */
    String[] FILTRE_ETAT_SIMU_COLLEGE_SECTEUR = { "41", "etatCollegeSecteurSimule.code",
            "Simulation du calcul du collège de secteur" };

    /**
     * Filtre sur la manière dont a été valorisé le collège de secteur : déterminé automatiquement = autoAbouti, la
     * calcul automatique n''a pas abouti = autoNonAbouti, les collèges ont été saisi manuellement = manuel ou pas
     * de collèges = NULL.
     */
    String[] FILTRE_COD_UAI_ORIGIN = { "42", "ecoleOrigine.codUai", "École d'origine" };
    /**
     * Filtre sur la manière dont a été valorisé le collège de secteur : déterminé automatiquement = autoAbouti, la
     * calcul automatique n''a pas abouti = autoNonAbouti, les collèges ont été saisi manuellement = manuel ou pas
     * de collèges = NULL.
     */
    String[] FILTRE_INDICATEUR_PCS = { "43", "indicateurPcs.id",
            "Indicateur de catégorie socioprofessionnelle des responsables de l'élève" };

    /** Filtre sur le code état du statut d'adresse de l'élève. */
    String[] FILTRE_STATUT_ADR_CODE_ETAT = { "44", "adresseResidence.statutAdresse.codeEtat",
            "Code état du statut d'adresse principale" };

    /** Filtre sur le code état du statut d'adresse des responsables. */
    String[] FILTRE_RESP_STATUT_ADR_CODE_ETAT = { "45", "responsables.adresseResponsable.statutAdresse.codeEtat",
            "Code état du staut d'adresse des responsables" };

    /** Filtre sur la date d'édition des notifications de l'élève. */
    String[] FILTRE_DATE_EDIT_NOTIF = { "46", "dateEditNotif",
            "Date à laquelle toutes les notifications d'affectation ont été éditées" };

    /** Filtre sur le code du rôle de l'utilisateur qui a fait la dernière modification du collège de secteur. */
    String[] FILTRE_ROLE_MAJ_COLLEGE_SECTEUR = { "47", "roleMajCollegeSecteur.cod",
            "Code rôle de l'uitlisateur qui modifie le collège de secteur" };

    /** Filtre sur la cause du calcul de collège de secteur non abouti. */
    String[] FILTRE_CAUSE_CALCUL_COLLEGE_SECTEUR = { "48", "causeCalculCollegeSecteur.code",
            "Cause du calcul non abouti" };

    /** Filtre sur le code du niveau. */
    String[] FILTRE_CODE_NIVEAU = { "49", "niveau.codeNiveau", "Niveau" };

    /** Filtre sur l'identifiant d'échange SIECLE Synchro BEE. */
    String[] FILTRE_ECHANGE_ID_SIECLE_SYNCHRO_BEE = { "50", "echangeIdSiecleSynchroBee",
            "Identifiant d'échange SIECLE Synchro BEE" };

    /** Filtre sur l'édition de l'accusé de réception. */
    String[] FILTRE_EDITION_ACCUSE_RECEPTION = { "51", "editionAccuseReception",
            "Édition de l'accusé de réception" };

    /** Filtre sur le flag parents séparés - garantie de la confidentialité. */
    String[] FILTRE_CONFIDENTIALITE_RESP = { "52", "confidentialiteResp",
            "Parents séparés - Garantie de la confidentialité" };

    /** Filtre sur le code état du statut d'adresse de l'élève. */
    String[] FILTRE_STATUT_ADR2_CODE_ETAT = { "53", "adresseSecondaire.statutAdresse.codeEtat",
            "Code état du statut d'adresse secondaire" };

    /** Filtre sur l'identifiant de l'adresse principale de l'élève. */
    String[] FILTRE_STATUT_ADR_ID = { "54", "adresseResidence.adrId", "Code état du statut d'adresse secondaire" };

    /** Filtre sur l'identifiant de l'adresse secondaire de l'élève. */
    String[] FILTRE_STATUT_ADR2_ID = { "55", "adresseSecondaire.adrId",
            "Code état du statut d'adresse secondaire" };

    /** Filtre sur l'identifiant de l'adresse des responsables. */
    String[] FILTRE_RESP_STATUT_ADR_ID = { "56", "responsables.adresseResponsable.adrId",
            "Code état du statut d'adresse secondaire" };

    /** Filtre sur la valeur de l'édition du volet1 bis. */
    String[] FILTRE_EDITION_VOLET1BIS = { "2", "editionVolet1Bis", "Volet 1 bis édité" };

    /** Filtre sur la date d'envoi de la notification d'affectation par courrier électronique. */
    String[] FILTRE_RESP_DATE_ENV_NTF = { "57", "responsables.dateEnvoiNotif",
            "Date d''envoi de la notification d''affectation par courrier électronique" };

    /** Filtre sur la date d'édition des notifications de l'élève. */
    String[] FILTRE_DATE_ENVOI_NOTIF = { "58", "dateEnvoiNotif",
            "Date  d'envoi des notifications d'affectation par courrier électronique" };

    /**
     * L'âge maximum des élèves que l'on importe. Utile pour calculer la date de
     * naissance maximale que l'on accepte.
     */
    int AGE_MAX = 55;
    /**
     * L'âge maximum des élèves que l'on saisit. Utile pour calculer la date de
     * naissance maximale à partir duquel on lance un avertissement.
     */
    int AGE_MAX_AV = 18;
    /**
     * L'âge minimum des élèves que l'on importe. Utile pour calculer la date de
     * naissance minimale que l'on accepte.
     */
    int AGE_MIN = 5;

    /**
     * Liste l'ensemble des Eleve.
     * 
     * @param paginationSupport
     *            les infos de pagination (ex.:numéro de la page courante
     *            (commence à 0))
     * @param listTri
     *            la liste des tris à effectuer
     * @param filtres
     *            la liste des filtres à appliquer (filtre logique ET)
     * @return liste des élèves.
     * @throws DaoException
     *             En cas de problème pour accéder aux données
     */
    List<Eleve> lister(Pagination<Eleve> paginationSupport, List<Tri> listTri, List<Filtre> filtres)
            throws DaoException;

    /**
     * Liste l'ensemble des Eleve.
     * 
     * @param paginationSupport
     *            les infos de pagination (ex.:numéro de la page courante
     *            (commence à 0))
     * @param listTri
     *            la liste des tris à effectuer
     * @param filtre
     *            le filtre à appliquer
     * @return liste des élèves.
     * @throws DaoException
     *             En cas de problème pour accéder aux données
     */
    List<Eleve> lister(Pagination<Eleve> paginationSupport, List<Tri> listTri, Filtre filtre) throws DaoException;

    /**
     * Lister distinct.
     * 
     * @param inf
     *            les informations de pagination, tri, filtre
     * @return la liste des élèves correspondant
     * @throws DaoException
     *             en cas d'erreur d'accès aux données
     */
    List<Eleve> listerDistinct(InfoListe<Eleve> inf) throws DaoException;

    /**
     * Liste paginée sans doublon des élèves correspondants au tri et filtre.
     * 
     * @param pagination
     *            les paramètres de pagination et filtrage
     * @param tris
     *            les paramètres de tri
     * @param filtre
     *            les paramètres de filtrage
     * @return la liste
     * @throws DaoException
     *             une erreur d'accès aux données.
     */
    List<Eleve> listerDistinct(Pagination<Eleve> pagination, List<Tri> tris, Filtre filtre) throws DaoException;

    List<Eleve> lister(InfoListe<Eleve> inf) throws TechnicalException, BusinessException;

    List<Eleve> listerDemandeAffectation(InfoListe<Eleve> inf) throws TechnicalException, BusinessException;

    List<Eleve> listerCollegeSecteur(InfoListe<Eleve> inf) throws TechnicalException, BusinessException;

    List<Eleve> listerCollegeVoeux(InfoListe<Eleve> inf) throws TechnicalException, BusinessException;

    List<Eleve> lister() throws TechnicalException, BusinessException;

    /**
     * Liste les élèves par leur numéro INE.
     *
     * @param ine
     *            Numéro INE
     * @return liste des élèves par INE
     * @throws DaoException
     *             En cas de problème au chargement des Eleves.
     */
    List<Eleve> listerParIne(String ine) throws DaoException;

    /**
     * Charge une Eleve à partir de son code.
     * 
     * @param id
     *            Code de la matiere à charger
     * @return matiere chargé
     * @throws ApplicationException
     *             En ca de problème au chargement de Eleve.
     */
    Eleve charger(Long id) throws ApplicationException;

    Eleve chargerParIne(String ine) throws DaoException;

    /**
     * Mettre à jour un Eleve et n'appelle pas la méthode flush() d'Hibernate.
     * 
     * @param e
     *            le Eleve à mettre à jour
     * @throws ApplicationException
     *             Exception lancée en cas de problème à la mise à jour
     */
    void majSansFlush(Eleve e) throws ApplicationException;

    /**
     * Mettre à jour un Eleve.
     * 
     * @param e
     *            le Eleve à mettre à jour
     * @throws ApplicationException
     *             Exception lancée en cas de problème à la mise à jour
     */
    void maj(Eleve e) throws ApplicationException;

    /**
     * Mettre à jour un Eleve.
     * 
     * @param e
     *            le Eleve à mettre à jour
     * @param majDateMaj
     *            S'il faut mettre à jour le champ DateMaj de l'Eleve avec la
     *            date courante
     * @throws ApplicationException
     *             Exception lancée en cas de problème à la mise à jour
     */
    void maj(Eleve e, boolean majDateMaj) throws ApplicationException;

    /**
     * Supprime un <code>Eleve</code> et les objets liés (demandes
     * d'affectations...).
     * 
     * @param eleve
     *            l'Eleve à mettre à jour
     * @throws ApplicationException
     *             Exception lancée en cas de problème à la mise à jour
     */
    void supprimer(Eleve eleve) throws ApplicationException;

    /**
     * Créer un Eleve.
     * 
     * @param p
     *            le Eleve à créer
     * @return la clé de l'objet créé
     * @throws ApplicationException
     *             Exception lancée en cas de problème à la mise à jour
     */
    Long creer(Eleve p) throws ApplicationException;

    /**
     * Test si un Eleve est présent en base.
     * 
     * @param id
     *            l'id de l'Eleve à tester
     * @return vrai si l'objet existe
     * @throws ApplicationException
     *             Exception lancée en cas de problème
     */
    boolean existe(Long id) throws ApplicationException;

    boolean existeParIne(String ine) throws DaoException;

    /**
     * Donne si plusieurs élèves existent avec le même INE.
     * 
     * @param ine
     *            le code INE
     * @return Vrai si plusieurs élèves existent dans la BDD avec l'INE <code>ine</code>
     * @throws DaoException
     *             en cas d'erreur d'accès aux données
     */
    boolean existePlusieursParIne(String ine) throws DaoException;

    /**
     * Efface les dossiers élève du département en vue d'un nouvel import.
     * 
     * @param dptMenId
     *            Identifiant du département MEN Concené
     * @return nombre de suppressions effectuées
     * @throws ApplicationException
     *             en cas de problème
     */
    int supprimerPourRemplacerDepartement(int dptMenId) throws ApplicationException;

    /**
     * @param dept
     *            le code MEN du département
     * @return compte les élèves dans des écoles pubs pour le dept.
     * @throws ApplicationException
     *             en cas de problème
     */
    int compteElevesEcolesPubDept(String dept) throws ApplicationException;

    /**
     * compte les élèves en appel pour le dept.
     * 
     * @param dept
     *            le code MEN du département
     * @param typeSaisie
     *            origine de la saisie
     * @return le nombre d'élèves en appel pour le dept
     * @throws ApplicationException
     *             en cas de problème
     */
    int compteElevesAppelDept(String dept, String typeSaisie) throws ApplicationException;

    /**
     * compte les élèves extraits pour la circo.
     * 
     * @param codeUaiCirco
     *            code UAI de la circonscription
     * @param typeSaisie
     *            origine de la saisie
     * @return le nombre d'élèves extraits pour la circo
     * @throws ApplicationException
     *             en cas de problème
     */
    int compteElevesCirco(String codeUaiCirco, String typeSaisie) throws ApplicationException;

    /**
     * compte les élèves en appel pour la circo.
     * 
     * @param codeUaiCirco
     *            code UAI de la circonscription
     * @param typeSaisie
     *            origine de la saisie
     * @return le nombre d'élèves en appel pour la circo
     * @throws ApplicationException
     *             en cas de problème
     */
    int compteElevesAppelCirco(String codeUaiCirco, String typeSaisie) throws ApplicationException;

    /**
     * @param codeDept
     *            le code MEN du département
     * @param uai
     *            code uai de l'établissement
     * @return compte les élèves qui n'ont pas de responsables pour une école.
     */
    int compteEleveSansRepLegal(String uai, String codeDept);

    /**
     * @param codeDept
     *            le code MEN du département
     * @param codeUaiOrigine
     *            code uai de l'établissement d'origine
     * @return compte les adresses à traiter des élèves et des responsables pour une école.
     */
    int compteAdressesATraiter(String codeUaiOrigine, String codeDept);

    /**
     * Compte le nombre d'adresses à traiter des élèves et des responsables.
     * 
     * @param infoListe
     *            les paramètres de Pagination, Filtre, Tri
     * 
     * @return le nombre d'adresses à traiter des élèves et des responsables
     * @throws DaoException
     *             en cas d'erreur
     */
    int compteAdressesATraiter(InfoListe<Eleve> infoListe) throws DaoException;

    /**
     * @param codeDept
     *            le code MEN du département
     * @param codeUaiOrigine
     *            code uai de l'établissement d'origine
     * @return compte les adresses non contrôlées des élèves et des responsables pour une école.
     */
    int compteAdressesNonControlees(String codeUaiOrigine, String codeDept);

    /**
     * Donne la liste des élèves sans demande d'affectation.
     * 
     * @param fEcole
     *            filtre sur l'école d'origine
     * @param tris
     *            liste des tris à appliquer
     * @return liste d'élèves
     * @throws DaoException
     *             erreur lors du chargement
     */
    List<Eleve> listerEleveSansDemande(Filtre fEcole, List<Tri> tris) throws DaoException;

    /**
     * Donne la liste des élèves n'ayant pas de représentant légal.
     * 
     * @param codeRne
     *            le code de l'école d'origine
     * @param codeDepMen
     *            le code du département
     * @return liste des élèves
     * @throws DaoException
     *             erreur lors du chargement
     */
    List<Eleve> listerEleveSansRepLegal(String codeRne, String codeDepMen) throws DaoException;

    /**
     * Donne la liste des élèves sans le volet 1 édité d'une école d'un département.
     * 
     * @param fEcole
     *            filtre sur l'école d'origine
     * @param tris
     *            liste des tris à appliquer
     * @return liste d'élèves
     * @throws DaoException
     *             erreur lors du chargement
     */
    List<Eleve> listerElevesSansVolet1Edite(Filtre fEcole, List<Tri> tris) throws DaoException;

    /**
     * Donne la liste des élèves sans le volet 2 édité d'une école d'un département.
     * 
     * @param fEcole
     *            filtre sur l'école d'origine
     * @param tris
     *            liste des tris à appliquer
     * @return liste d'élèves
     * @throws DaoException
     *             erreur lors du chargement
     */
    List<Eleve> listerElevesSansVolet2Edite(Filtre fEcole, List<Tri> tris) throws DaoException;

    /**
     * @param codeUai
     *            le code de l'établissement
     * @param typeSaisie
     *            origine de la saisie
     * @param codeDept
     *            le code MEN du département
     * @return compte les élèves d'une école.
     * @throws ApplicationException
     *             en cas de problème
     */
    int compteElevesEcole(String codeUai, String typeSaisie, String codeDept) throws ApplicationException;

    /**
     * Une map d'élève ayant au moins une dérogation et issu d'une école fourni
     * en paramètre.
     * 
     * @param codeUai
     *            le code de l'établissement.
     * @throws ApplicationException
     *             une exception
     * @return le nombre d'élève d'une école ayant au moins une dérogation
     */
    Map<String, Eleve> mapEleveAvecDerogation(String codeUai) throws ApplicationException;

    /**
     * Compte les élèves en appel pour l'école.
     * 
     * @param codeUai
     *            code UAI de l'établissement
     * @param typeSaisie
     *            l'origine du dossier
     * @param codeDept
     *            le code MEN du département
     * @return le nombre d'élèves en appel pour l'école
     * @throws ApplicationException
     *             en cas d'erreur
     */
    int compteElevesAppelEcole(String codeUai, String typeSaisie, String codeDept) throws ApplicationException;

    /**
     * @param codeDepMen
     *            le code MEN du département
     * @return compte les élèves du département
     */
    int compteElevesDept(String codeDepMen);

    /**
     * Donne le nombre d'élèves sans collège potentiel dans le département.
     * 
     * @param codeDepMen
     *            Le code MEN du département.
     * @return Le nombre d'élèves sans collège potentiel dans le département.
     */
    int compteElevesSansCollegeSecteurPotentiel(String codeDepMen);

    /**
     * Donne le nombre d'élèves sans collège potentiel dans pour l'école.
     * 
     * @param codeUai
     *            Le code UAI (RNE) de l'école dont on souhaite obtenir les
     *            indicateurs
     * @param codeDepMen
     *            Le code MEN du département.
     * @return Le nombre d'élèves sans collège potentiel dans l'école.
     */
    int compteElevesSansCollegeSecteurPotentielEcole(String codeUai, String codeDepMen);

    /**
     * @param codeDepMen
     *            le code MEN du département
     * @return Nombre d'élèves importés de Be1d.
     */
    int compteElevesDeptBe1d(String codeDepMen);

    /**
     * Nombre d'élèves du département sans décision de passage.
     *
     * @param codeDeptMen
     *            le code MEN du département
     * @return Nombre d'élèves du département sans décision de passage.
     * @throws DaoException
     *             En cas de problème d'accès à la base de données
     */
    int compteElevesSansDecisionPassage(String codeDeptMen) throws DaoException;

    /**
     * Nombre d'élèves du département en appel.
     *
     * @param codeDeptMen
     *            le code MEN du département
     * @return Nombre d'élèves du département en appel.
     * @throws DaoException
     *             En cas de problème d'accès à la base de données
     */
    int compteElevesEnAppel(String codeDeptMen) throws DaoException;

    /**
     * @param codeDepMen
     *            le code MEN du département
     * @return Nombre d'élèves maintenus à l'école.
     */
    int compteElevesMaintenus(String codeDepMen);

    /**
     * @param codeDepMen
     *            le code MEN du département
     * @return Nombre d'élèves saisis en candidatures individuelles.
     */
    int compteElevesDeptInd(String codeDepMen);

    /**
     * @param codeDepMen
     *            le code MEN du département
     * @return Nombre d'élèves affectés.
     */
    int compteElevesAffectes(String codeDepMen);

    /**
     * Nombre d'élèves affectés dans le département sans décision de passage.
     *
     * @param codeDeptMen
     *            le code MEN du département
     * @return Nombre d'élèves affectés dans le département sans décision de passage
     * @throws DaoException
     *             En cas de problème d'accès à la base de données
     */
    int compteElevesAffectesSansDecisionPassage(String codeDeptMen) throws DaoException;

    /**
     * Nombre d'élèves affectés dans le département avec une décision de passage en 6ème.
     *
     * @param codeDeptMen
     *            le code MEN du département
     * @return Nombre d'élèves affectés dans le département avec une décision de passage en 6ème
     * @throws DaoException
     *             En cas de problème d'accès à la base de données
     */
    int compteElevesAffectesPassageEn6eme(String codeDeptMen) throws DaoException;

    /**
     * Nombre d'élèves affectés dans le département avec une décision de passage en 6ème.
     *
     * @param codeDeptMen
     *            le code MEN du département
     * @param dejaExporte
     *            Booléen permettant de choisir si l'on veut le nombre d'élèves déjà exportés (true)
     *            ou le nombre d'élèves par encore exportés (false).
     * @return Nombre d'élèves affectés dans le département avec une décision de passage en 6ème
     * @throws DaoException
     *             En cas de problème d'accès à la base de données
     */
    int compteElevesAffectesPassageEn6eme(String codeDeptMen, boolean dejaExporte) throws DaoException;

    /**
     * Nombre d'élèves affectés dans le département avec une décision de passage Appel.
     *
     * @param codeDeptMen
     *            le code MEN du département
     * @return Nombre d'élèves affectés dans le département avec une décision de passage Appel
     * @throws DaoException
     *             En cas de problème d'accès à la base de données
     */
    int compteElevesAffectesEnAppel(String codeDeptMen) throws DaoException;

    /**
     * @param codeDepMen
     *            le code MEN du département
     * @return Nombre d'élèves refuses.
     */
    int compteElevesRefuses(String codeDepMen);

    /**
     * @param codeDepMen
     *            le code MEN du département
     * @return Nombre d'élèves sans demande non maintenus.
     */
    int compteElevesSansDemandePasMaintenus(String codeDepMen);

    /**
     * @param codeRne
     *            le code RNE de l'école
     * @param codeDept
     *            le code MEN du département
     * @return Nombre d'élèves sans demande non maintenus dans une école.
     */
    int compteElevesSansDemandePasMaintenusEcole(String codeRne, String codeDept);

    /**
     * @param codeDepMen
     *            le code MEN du département
     * @return Nombre d'élèves hors collège département.
     */
    int compteElevesHorsCPD(String codeDepMen);

    /**
     * Nombre d'élèves avec une demande de dérogation sans accusé réception édité.
     * 
     * @param codeUai
     *            le code UAI de l'école
     * @param codeDept
     *            le code MEN du département
     * @return nombre d'élèves trouvés
     */
    int compteElevesAvecDerogSansEditionAccuseReceptionEcole(String codeUai, String codeDept);

    /**
     * Donne le nombre d'élèves d'un département ayant demandé une dérogation.
     * 
     * @param codeDeptMen
     *            Le code d'un département MEN
     * @return Le nombre d'élèves d'un département ayant demandé une dérogation.
     * @throws DaoException
     *             En cas de problème d'accès à la base de données
     */
    int compteElevesAvecDerog(String codeDeptMen) throws DaoException;

    /**
     * Donne le nombre d'élèves avec une demande de dérogation pour entrer dans
     * l'établissement passé en paramètre.
     * 
     * @param etabId
     *            L'identifiant d'un établissement
     * @return Le nombre d'élèves avec une demande de dérogation pour entrer
     *         dans l'établissement passé en paramètre.
     * @throws DaoException
     *             En cas de problème d'accès à la base de données
     */
    int compteElevesAvecDerogEntrante(Integer etabId) throws DaoException;

    /**
     * Donne le nombre d'élèves avec une demande de dérogation pour aller dans
     * un autre établissement que l'établissement passé en paramètre.
     * 
     * @param etabId
     *            L'identifiant d'un établissement
     * @return Le nombre d'élèves avec une demande de dérogation pour aller
     *         ailleurs
     * @throws DaoException
     *             En cas de problème d'accès à la base de données
     */
    int compteElevesAvecDerogSortante(Integer etabId) throws DaoException;

    /**
     * Donne le nombre de dérogations accordées pour entrer dans l'établissement
     * passé en paramètre.
     * 
     * @param etabId
     *            L'identifiant d'un établissement
     * @return Le nombre de dérogations accordées pour entrer dans
     *         l'établissement passé en paramètre.
     * @throws DaoException
     *             En cas de problème d'accès à la base de données
     */
    int compteElevesAvecDerogEntranteAccordee(Integer etabId) throws DaoException;

    /**
     * Donne le nombre de dérogations accordées pour aller dans un autre
     * établissement que l'établissement passé en paramètre.
     * 
     * @param etabId
     *            L'identifiant d'un établissement
     * @return Le nombre de dérogations accordées pour aller dans un autre
     *         établissement que l'établissement passé en paramètre.
     * @throws DaoException
     *             En cas de problème d'accès à la base de données
     */
    int compteElevesAvecDerogSortanteAccordee(Integer etabId) throws DaoException;

    /**
     * Donne le nombre d'élèves d'un département dont le premier voeu est une
     * demande de dérogation dont le motif le plus prioritaire est celui passé
     * en paramètre.
     * 
     * @param codeDeptMen
     *            Le code d'un département MEN
     * @param idMtfDrg
     *            L'identifiant d'un motif de dérogation du département
     * @return Nombre d'élèves d'un département dont le premier voeu est une
     *         demande de dérogation dont le motif
     * @throws DaoException
     *             En cas de problème d'accès à la base de données
     */
    int compteElevesAvec1erVoeuDerogEtMotifLePlusPrioritaire(String codeDeptMen, Integer idMtfDrg)
            throws DaoException;

    /**
     * Donne le nombre de dérogations accordées d'un département dont le motif
     * le plus prioritaire est celui passé en paramètre.
     * 
     * @param codeDeptMen
     *            Le code d'un département MEN
     * @param idMtfDrg
     *            L'identifiant d'un motif de dérogation du département
     * @return Nombre de dérogations accordées d'un département dont le motif le
     *         plus prioritaire est celui passé en paramètre.
     * @throws DaoException
     *             En cas de problème d'accès à la base de données
     */
    int compteElevesAvecDerogAccordeeEtMotifLePlusPrioritaire(String codeDeptMen, Integer idMtfDrg)
            throws DaoException;

    /**
     * Verifie si le batch be1d peut etre lancé pour un departement.
     * 
     * @param dptMenId
     *            le departement concerné
     * @return true si le batch n'est pas verrouillé, false si il l'est
     * @throws ApplicationException
     *             exception
     */
    boolean verifieVerrouBatchImport(int dptMenId) throws ApplicationException;

    /**
     * Met à jour la date d'édition d'un volet de la fiche de liaison d'une liste d'élèves.
     * 
     * @param numVolet
     *            numéro du volet
     * @param numDossierEleves
     *            liste des numéros de dossier des élèves
     * @return le nombre de dossier mis à jour
     */
    int majDateEditionVolet(int numVolet, Long[] numDossierEleves);

    /**
     * Met à jour le flag parents séparés - garantie de la confidentialité d'une liste d'élèves.
     *
     * @param numDossierEleves
     *            liste des numéros de dossier des élèves dont on édite le volet 1
     * @param confidentialiteResp
     *            liste des numéros de dossier des élèves qui ont le flag parents séparés coché
     * @return le nombre de dossier mis à jour
     */
    int majConfidentialiteResponsables(Long[] numDossierEleves, Long[] confidentialiteResp);

    /**
     * Modifie le collège de secteur d'un élève.
     * 
     * @param eleve
     *            l'élève à mettre à jour
     * @param nouveauCollegeSecteur
     *            le collège de secteur à positionner
     * @throws ApplicationException
     *             en cas d'erreur
     */
    void majCollegeSecteur(Eleve eleve, EtablissementSconet nouveauCollegeSecteur) throws ApplicationException;

    /**
     * Supprime toutes les demandes d'affectation d'un élève et les
     * justificatifs de dérogation liés.
     * 
     * @param eleve
     *            l'élève dont on veut vider les demandes
     * @param avecPropositionSegpa
     *            booléen indiquant si l'on doit aussi supprimer les propositions SEGPA.
     * @throws ApplicationException
     *             en cas d'erreur
     */
    void videDemandesAffect(Eleve eleve, boolean avecPropositionSegpa) throws ApplicationException;

    /**
     * Supprime toutes les demandes d'affectation d'un élève et les
     * justificatifs de dérogation liés.
     * 
     * @param eleve
     *            l'élève
     * @throws ApplicationException
     *             en cas d'erreur
     */
    void videDemandesAffect(Eleve eleve) throws ApplicationException;

    /**
     * Supprime toutes les demandes d'affectation d'un élève ainsi que la décision finale et les
     * justificatifs de dérogation liés.
     *
     * @param eleve
     *            l'élève
     * @throws ApplicationException
     *             en cas d'erreur
     */
    void videDemandesAffectEtDecisionFinale(Eleve eleve) throws ApplicationException;

    /**
     * Vérifie s'il y a changement du collège de secteur d'un élève.
     * 
     * @param eleve
     *            L'élève avant le changement du collège
     * @param codeRneNewCollegeSecteur
     *            Le code RNE du nouveau collège de secteur
     * @return s'il y a changement du collège de secteur de l'élève
     */
    boolean isCollegeSecteurUpdated(Eleve eleve, String codeRneNewCollegeSecteur);

    /**
     * Donne si un élève a une ou plusieurs demandes d'affectation dont l'état
     * est différent de non traitées, en attente ou hors affectation.
     * 
     * @param eleve
     *            l'élève
     * @return Vrai si l'élève a une ou plusieurs demandes d'affectation ont
     *         l'état différent de non traitées, en attente ou hors affectation.
     */
    boolean hasDemandesDifferentesNonTraiteesEnAttenteHorsAffectation(Eleve eleve);

    /**
     * Donne si un élève a une ou plusieurs demandes d'affectation avec une décision finale.
     * 
     * @param eleve
     *            L'élève
     * @return Vrai si l'élève a une ou plusieurs demandes d'affectation avec une décision finale.
     */
    boolean hasDemandesAvecDecisionFinale(Eleve eleve);

    /**
     * Liste les demandes d'affecation pour un élève.
     * 
     * @param eleve
     *            L'élève
     * @return la liste des demandes
     * @throws DaoException
     *             En cas de problème d'accès aux données.
     */
    List<DemandeAffectation> listerDemandesEleve(Eleve eleve) throws DaoException;

    /**
     * @param codeDeptMen
     *            Code département
     * @return Cette fonction génère un identifiant provisoire d'immatriculation
     *         pour un élève composé comme suit : P + code département +
     *         incrément sur 6 + lettre modulo 23
     * @throws ApplicationException
     *             en cas d'erreur
     */
    String genereIneEleveProvisoire(String codeDeptMen) throws ApplicationException;

    /**
     * Crée la demande par défaut d'un élève si c'est possible.
     * 
     * @param eleve
     *            L'élève pour lequel il faut créer une demande par défaut
     * @return si la demande d"affectation pas défaut a été créée ou pas
     * @throws ApplicationException
     *             En cas de problème d'enregistrement de la demande par défaut
     */
    EtatDemandeAffectation creeDemandeDefaut(Eleve eleve) throws ApplicationException;

    /**
     * La méthode permettant de lister les élèves sans demande d'affectation.
     * 
     * @param pagination
     *            la pagination utilisée
     * @param listeDesTris
     *            le tri pour la liste de résultats
     * @param departement
     *            le département sélectionné (null si aucune sélection)
     * @param uaiEcole
     *            l'école d'origine sélectionnée (null si aucune sélection)
     * @return la liste des élèves sans demande d'affectation
     * @throws DaoException
     *             si la récupération des données échoue
     */
    List<Eleve> listerElevesSansDemande(Pagination<Eleve> pagination, List<Tri> listeDesTris, String departement,
            String uaiEcole) throws DaoException;

    /**
     * Liste des élèves du département participant à l'affectation.
     * 
     * @param codeDeptMen
     *            code du département
     * @return liste des demandes
     * @throws ApplicationException
     *             en cas de problème
     */
    List<Eleve> listerElevesAffectablesDepartement(String codeDeptMen) throws ApplicationException;

    /**
     * Positionne la décision finale d'affectation sur un élève.
     * 
     * @param eleve
     *            élève à modifier
     * @param demandeFinale
     *            demande concernée
     * @param etatDecisionFinale
     *            décision à attribuer
     * @throws ApplicationException
     *             En cas de problème à la mise à jour
     */
    void positionneDecisionFinale(Eleve eleve, DemandeAffectation demandeFinale, EtatDecision etatDecisionFinale)
            throws ApplicationException;

    /**
     * Génère une partie de compte rendu pour une liste d'élèves.
     * 
     * @param cr
     *            StringBuffer pour le compte rendu
     * @param listeIdElv
     *            Liste d'identifiants d'élèves concernés
     */
    void listerElevesCompteRendu(StringBuffer cr, List<Long> listeIdElv);

    /**
     * Permet de filtrer les élèves dont l'école d'origine est dans une
     * circonscription donnée.
     * 
     * @param codeUaiCirco
     *            code UAI de la circonscription
     * @return Filtre des élèves appartenant à la circonscription
     * @throws DaoException
     *             En cas de problème d'accès aux données
     */
    Filtre filtreElevesCirco(String codeUaiCirco) throws DaoException;

    /**
     * Liste tous les élèves affectés d'un département qui passent en 6ème.
     * 
     * @param codeDeptMen
     *            Le code d'un département
     * @param dejaExporte
     *            Booléen permettant de choisir si l'on veut les élèves déjà exportés (true)
     *            ou les élèves par encore exportés (false).
     * @return La liste des élèves affectés dans le département
     * @throws DaoException
     *             En cas de problème d'accès aux données
     */
    List<Eleve> listerElevesAffectesParDept(String codeDeptMen, boolean dejaExporte) throws DaoException;

    /**
     * Liste les élèves affectés qui passent en 6ème dont l'établissement d'origine est dans la
     * circonscription passée en paramètre.
     * 
     * @param codeDeptMen
     *            Le code d'un département
     * @param dejaExporte
     *            Booléen permettant de choisir si l'on veut les élèves déjà exportés (true)
     *            ou les élèves par encore exportés (false).
     * @param codeUaiCirco
     *            Le code UAI d'une circonscription
     * @return La liste des élèves affectés dans la circonscription
     * @throws DaoException
     *             En cas de problème d'accès aux données
     */
    List<Eleve> listerElevesAffectesParCirco(String codeDeptMen, String codeUaiCirco, boolean dejaExporte)
            throws DaoException;

    /**
     * Liste les élèves affectés dans un collège qui passent en 6ème.
     * 
     * @param codeDeptMen
     *            Le code d'un département
     * @param codeUaiEtabAccueil
     *            Le code Uai du collège d'affectation
     * @param dejaExporte
     *            Booléen permettant de choisir si l'on veut les élèves déjà exportés (true)
     *            ou les élèves par encore exportés (false).
     * @return La liste des élèves affectés dans le collège
     * @throws DaoException
     *             En cas de problème d'accès aux données
     */
    List<Eleve> listerElevesAffectesParEtabAccueil(String codeDeptMen, String codeUaiEtabAccueil,
            boolean dejaExporte) throws DaoException;

    /**
     * Liste les élèves affectés dans un collège avec un identifiant d'échange donné.
     * 
     * @param codeUaiEtabAccueil
     *            Le code Uai du collège d'affectation
     * @param echangeIdSiecleSynchroBee
     *            L'identifiant d'échange SIECLE Synchro
     * @return La liste des élèves affectés dans le collège avec l'identifiant d'échange en paramètre
     * @throws DaoException
     *             En cas de problème d'accès aux données
     */
    List<Eleve> listerElevesAffectesParEtabAccueilEtEchangeIdSiecleSynchroBee(String codeUaiEtabAccueil,
            String echangeIdSiecleSynchroBee) throws DaoException;

    /**
     * Renvoie l'élève s'il est affecté et qu'il passe en 6ème.
     * 
     * @param codeDeptMen
     *            Le code d'un département
     * @param ine
     *            Le code INE d'un élève
     * @param dejaExporte
     *            Booléen permettant de choisir si l'on veut les élèves déjà exportés (true)
     *            ou les élèves par encore exportés (false).
     * @return L'élève s'il est affecté dans le département. Sinon <code>null</code>.
     * @throws DaoException
     *             En cas de problème d'accès aux données
     */
    Eleve eleveAffecteParIne(String codeDeptMen, String ine, boolean dejaExporte) throws DaoException;

    /**
     * Liste les élèves refuses sur toutes leur demandes de dérogation.
     * 
     * @param filtreNom
     *            le nom d'élève utilisé pour le filtrage
     * @param filtreCollege
     *            le nom de collège pour le filtrage
     * @param tri
     *            valeur du tri (desc ou rien)
     * @param codeDeptMenUser
     *            code département men de l'utilisateur
     * @return Une liste des élèves refuses sur leurs derog.
     * @throws DaoException
     *             si la récupération des données échoue
     */
    List<Eleve> listerRefusesDerog(String filtreNom, String filtreCollege, String tri, String codeDeptMenUser)
            throws DaoException;

    /**
     * Donne le code UAI de l'établissement d'origine d'un élève.
     * 
     * @param eleve
     *            un élève
     * @return Le code UAI de l'établissement d'origine de l'élève.<br>
     *         <code>null</code> si <code>eleve.getEcoleOrigine() == null</code>
     */
    String codeUaiEtabOri(Eleve eleve);

    /**
     * Donne le code MefStat4 du niveau de l'élève dans le premier degré.
     * 
     * @param eleve
     *            Un élève
     * @return Le code MefStat4 de la formation d'origine de l'élève.<br>
     *         <code>null</code> si le niveau de l'élève n'est pas renseigné ou
     *         si une erreur s'est produite.
     */
    String codeMefNiveau1d(Eleve eleve);

    /**
     * Donne le code INE qui sera inséré dans le champ ID_ELEVE_ETAB de la table
     * RCP_ELE pour l'élève passé en paramètre.
     * 
     * @param eleve
     *            L'élève pour lequel il faut donner le code INE transmis à
     *            Sconet.
     * @return Comme on ne doit pas transmettre le code INE BE1D de l'élève, on
     *         renvoie la chaîne "Affelnet-6" quelque soit l'élève.
     */
    String ineExportSconet(Eleve eleve);

    /**
     * Donne l'identifiant national qui sera inséré dans le champs ID_NATIONAL
     * de la table RCP_ELE pour l'élève passé en paramètre.
     * 
     * @param eleve
     *            L'élève pour lequel il faut donner l'identifiant transmis à
     *            Sconet.
     * @return La chaine correspondant à l'identifiant national transmis à
     *         Sconet
     */
    String idNational(Eleve eleve);

    /**
     * Donne une liste de doublons d'élèves probables dans un département.
     * 
     * @param codeDeptMen
     *            Le code du département
     * @return La liste des doublons probables. Une liste vide si aucun doublon
     *         trouvé.
     * @throws DaoException
     *             En cas de problème d'accès aux données
     */
    List<Eleve> listerDoublonsProbables(String codeDeptMen) throws DaoException;

    /**
     * Donne la liste de tous les élèves d'un département.
     * 
     * @param codeDeptMen
     *            Le code du département
     * @return La liste des élèves d'un département
     * @throws DaoException
     *             En cas de problème d'accès aux données
     */
    List<Eleve> listerElevesDept(String codeDeptMen) throws DaoException;

    /**
     * Donne la liste de tous les élèves d'un département dans une map.
     * 
     * @param codeDeptMen
     *            Le code du département
     * @return La liste des élèves d'un département dans une map
     * @throws DaoException
     *             En cas de problème d'accès aux données
     */
    Map<String, Eleve> listerMapElevesDept(String codeDeptMen) throws DaoException;

    /**
     * vérifie la validité de la date de naissance.
     * 
     * @param datNais
     *            date de naissance
     * @return le message d'erreur.
     */
    String verifieDateNaissance(String datNais);

    /**
     * vérifie l'age minimum necessitant un avertissement.
     * 
     * @param datNais
     *            date de naissance
     * @return false si l'age est &gt; age max avertissement.
     */
    boolean verifieAge(String datNais);

    /**
     * Donne un filtre pour sélectionner les élèves dont la décision finale est
     * "affecté".
     * 
     * @return Filtre des élèves dont la décision finale est "affecté".
     */
    Filtre filtreElevesAffectes();

    /**
     * Donne un filtre pour sélectionner les élèves d'un département.
     * 
     * @param codeDeptMen
     *            Le code du département.
     * @return Filtre des élèves du département.
     */
    Filtre filtreElevesDept(String codeDeptMen);

    /**
     * Donne un filtre pour sélectionner les élèves dont le code UAI de l'école d'origine commence par le code uai
     * en paramètre.
     * 
     * @param uiaEcoleOrigine
     *            le début du code de l'école
     * @return Filtre des élèves
     */
    Filtre filtreEtabOrigine(String uiaEcoleOrigine);

    /**
     * Donne un filtre pour sélectionner les élèves dont le code de l'établissement d'accueil commence par le code
     * uai
     * en paramètre.
     * 
     * @param uidEtabAccueil
     *            le début du code de l'établissement d'accueil
     * @return Filtre des élèves
     */
    Filtre filtreEtabAccueil(String uidEtabAccueil);

    /**
     * Donne un filtre pour sélectionner les élèves dont l'identifiant d'échange est égal au paramètre.
     * 
     * @param echangeIdSiecleSynchroBee
     *            l'identifiant d'échange SIECLE Synchro BEE
     * @return Filtre des élèves
     */
    Filtre filtreEchangeIdSiecleSynchroBee(String echangeIdSiecleSynchroBee);

    /**
     * Donne un filtre pour sélectionner les élèves ayant l'INE donné.
     * 
     * @param ine
     *            l'INE de l'élève
     * @return le Filtre des élèves ayant cet INE
     */
    Filtre filtreIne(String ine);

    /**
     * Donne un filtre pour sélectionner les élèves sans décision de passage.
     *
     * @return Filtre des élèves sans décision de passage.
     */
    Filtre filtreElevesSansDecisionPassage();

    /**
     * Donne un filtre pour sélectionner les élèves qui passe en 6ème.
     *
     * @return Filtre des élèves qui passent en 6ème.
     */
    Filtre filtreElevesPassageEn6eme();

    /**
     * Donne un filtre pour sélectionner les élèves dont la décision de passage est "Appel".
     *
     * @return Filtre des élèves dont la décision de passage est "Appel".
     */
    Filtre filtreElevesEnAppel();

    /**
     * Donne un filtre pour sélectionner les élèves dont la décision de passage
     * n'est pas "Appel".
     *
     * @return Filtre des élèves dont la décision de passage n'est pas en appel.
     */
    Filtre filtreElevesPasEnAppel();

    /**
     * Donne un filtre pour sélectionner les élèves ayant une décision de passage "Passage en 6ème".
     *
     * @return le Filtre des élèves ayant une décision de passage à "Passage en 6ème"
     */
    Filtre filtrePassage6eme();

    /**
     * Donne une liste de filtre pour sélectionner les élèves affectés dans un collège.
     *
     * @param codeDeptMen
     *            Le code d'un département
     * @param codeUaiEtabAccueil
     *            Le code Uai du collège d'affectation
     * @return la liste de Filtre des élèves affectés
     */
    List<Filtre> filtresElevesAffectesParEtabAccueil(String codeDeptMen, String codeUaiEtabAccueil);

    /**
     * Donne une liste de filtre pour sélectionner les élèves affectés dans un collège ayant une décision de
     * passage "Passage en 6ème".
     *
     * @param codeDeptMen
     *            Le code d'un département
     * @param codeUaiEtabAccueil
     *            Le code Uai du collège d'affectation
     * @return la liste de Filtre des élèves affectés
     */
    List<Filtre> filtresElevesAffectesPassageParEtabAccueil(String codeDeptMen, String codeUaiEtabAccueil);

    /**
     * Donne un filtre pour sélectionner les élèves maintenus à l'école.
     * 
     * @return Filtre des élèves maintenus à l'école.
     */
    Filtre filtreMaintenu();

    /**
     * Donne un filtre pour sélectionner les élèves qui ne sont pas maintenus à l'école.
     * 
     * @return Filtre des élèves pas maintenus à l'école.
     */
    Filtre filtrePasMaintenu();

    /**
     * Donne un filtre pour sélectionner les élèves du département refusées sur toutes leur demandes d'affectation.
     * Qui n'ont pas d'autres décision de demandes en dehors de 'QO','RA','RM'
     * 
     * @param codeDeptMen
     *            code département du MEN
     * @return le filtre pour les élèves refusés sur toutes leurs demandes d'affectation
     * @throws ApplicationException
     *             en cas d'erreur d'accès aux données
     */
    Filtre filtreDemandesRefuseesPartout(String codeDeptMen) throws ApplicationException;

    /**
     * Donne un filtre pour sélectionner les élèves visibles par l'utilisateur.
     * 
     * Un principal ne verra que les élèves avec une décision d'affectation finale dans son collège. Les autres
     * profils voient les élèves du département.
     * 
     * @param profilUtilisateur
     *            le profil de l'utilisateur connecté
     * @return Filtre des élèves visibles par l'utilisateur connecté
     */
    List<Filtre> getFiltresStatiques(ProfilUtilisateur profilUtilisateur);

    /**
     * Donne un filtre pour sélectionner les élèves du département de l'utilisateur connecté.
     * 
     * @param profilUtilisateur
     *            le profil de l'utilisateur connecté
     * @return Filtre des élèves du département de l'utilisateur connecté
     */
    Filtre getFiltresStatiquesDepartement(ProfilUtilisateur profilUtilisateur);

    /**
     * Regroupe les responsables d'un élève en fonction de leur adresse.
     * 
     * @param eleve
     *            Un élève
     * @param seulementRepLegal
     *            si on considère uniquement les représentants légaux
     * @param adresseComplete
     *            si on sélectionne les responsables dont l'adresse est complète
     * @param adresseInconnueOuIncomplete
     *            si on sélectionne les responsables dont d'adresse est inconnue ou incomplètes
     * @return Les responsables regroupés par adresse
     */
    List<List<Responsable>> regroupeResponsablesParAdresse(Eleve eleve, boolean seulementRepLegal,
            boolean adresseComplete, boolean adresseInconnueOuIncomplete);

    /**
     * @param codeUai
     *            Le code UAI (RNE) de l'école dont on souhaite obtenir les
     *            indicateurs
     * @return Les indicateurs associées à l'école
     */
    SuiviEcole suiviEcole(String codeUai);

    /**
     * Donne les élèves dont l'établissement d'origine est celui passé en paramètre
     * pour le département de l'utilisateur.
     * 
     * @param uaiEcoleOrigine
     *            Le code uai de l'établissement d'origine des élèves.
     * @param codeDeptMen
     *            Le code du département MEN de l'utilisateur courant
     * @return La liste des élèves de l'établissement choisi.
     * @throws DaoException
     *             En cas de problème d'accès aux données.
     */
    List<Eleve> listerElevesEtablissementOrigine(String uaiEcoleOrigine, String codeDeptMen) throws DaoException;

    /**
     * Importe le dossier d'un élève provenant d'un autre département.
     * 
     * @param numDossier
     *            L'identifiant de l'élève à importer.
     * @param codeDeptMen
     *            Le département de l'utilisateur.
     * @param estMonoCollege
     *            indique si le département qui importe est en secteur mono-collège
     * @return l'élève importé
     * @throws ApplicationException
     *             Un problème lors de l'accès aux données.
     */
    Eleve importeDossierAutreDepartement(Long numDossier, String codeDeptMen, boolean estMonoCollege)
            throws ApplicationException;

    /**
     * @param codeDeptMen
     *            Le code du département de l'utilisateur.
     * @return Une map des élèves importés d'un autre département de l'académie avec en clé l'INE de l'élève et en
     *         valeur l'élève.
     * @throws ApplicationException
     *             Un problème d'accès aux données.
     */
    Map<String, Eleve> listerElevesImportesDuDepartement(String codeDeptMen) throws ApplicationException;

    /**
     * @param codeDeptMen
     *            Le code du département de l'utilisateur.
     * @return Une map des élèves importés depuis un établissement du département avec en clé l'INE de l'élève et
     *         en
     *         valeur l'élève.
     * @throws ApplicationException
     *             Un problème d'accès aux données.
     */
    Map<String, Eleve> listerElevesImportesDuPrive(String codeDeptMen) throws ApplicationException;

    /**
     * Filtre sur les élèves transférés ou non à BEE.
     * 
     * @param dejaExporte
     *            Booléen permettant de choisir si l'on veut les élèves déjà exportés : état RC (réceptionné) ou EN
     *            (envoyé)
     *            ou les élèves par encore exportés : état NE (non envoyé) ou ER (erreur à la réception)
     * @return Le filtre indiquant que les élèves ont été transférés à BEE ou non
     */
    Filtre filtreElevesTransferesBee(boolean dejaExporte);

    /**
     * @param ine
     *            L'INE de l'élève.
     * @param codeDeptMen
     *            Le code département de l'utilisateur.
     * @return
     *         <ul>
     *         <li>true s'il existe plusieurs élèves avec le même INE dans le département donné.</li>
     *         <li>false sinon.</li>
     *         </ul>
     * @throws DaoException
     *             Une erreur d'accès aux données.
     */
    boolean existePlusieursEleveParIne(String ine, String codeDeptMen) throws DaoException;

    /**
     * @param ine
     *            L'INE de l'élève.
     * @param codeDeptMen
     *            Le code département de l'utilisateur.
     * @return
     *         <ul>
     *         <li>true s'il existe au moins un élève avec l'INE donné dans le département donné.</li>
     *         <li>false sinon.</li>
     *         </ul>
     * @throws DaoException
     *             Une erreur d'accès aux données.
     */
    boolean existeEleveParIne(String ine, String codeDeptMen) throws DaoException;

    /**
     * @param ine
     *            L'INE de l'élève.
     * @param codeDeptMen
     *            Le code département de l'utilisateur.
     * @return L'élève du département ce l'utilisateur qui possède l'INE passé en paramètre.
     * @throws DaoException
     *             Un problème lors de l'accès aux données.
     */
    Eleve chargerParIne(String ine, String codeDeptMen) throws DaoException;

    /**
     * Met à jour l'état de l'export vers BEE d'un élève.
     * 
     * @param numDossier
     *            numéro de dossier de l'élève
     * @param codeEtatExportBee
     *            code état de l'export BEE
     * @throws DaoException
     *             Un problème lors de l'accès aux données.
     */
    void majEtatExportBee(Long numDossier, String codeEtatExportBee) throws DaoException;

    /**
     * Met à jour l'état de l'export vers BEE et l'identifiant d'échange BEE pour une liste d'élève.
     * 
     * @param eleves
     *            liste des élèves
     * @param codeEtatExportBee
     *            code état de l'export BEE
     * @param echangeIdSiecleSynchroBee
     *            l'identifiant de l'échange avec BEE
     * @param progMon
     *            Le moniteur de progression du batch
     * @return le nombre de dossiers mis à jour
     * @throws DaoException
     *             Un problème lors de l'accès aux données.
     */
    int majEtatExportBeeEtEchangeIdSiecleSynchroBee(List<Eleve> eleves, String codeEtatExportBee,
            String echangeIdSiecleSynchroBee, BatchProgressMonitor progMon) throws DaoException;

    /**
     * Met à jour l'état de l'export vers BEE pour les élèves d'un établissement avec un identifiant d'échange BEE
     * donné.
     * 
     * @param codeUaiEtab
     *            code UAI de l'établissement d'affectation de l'élève
     * @param codeEtatExportBee
     *            code état de l'export BEE
     * @param echangeIdSiecleSynchroBee
     *            l'identifiant de l'échange avec BEE
     * @return le nombre de dossiers mis à jour
     * @throws DaoException
     *             Un problème lors de l'accès aux données.
     */
    int majEtatExportBeeParEtabEtEchangeIdSiecleSynchroBee(String codeUaiEtab, String codeEtatExportBee,
            String echangeIdSiecleSynchroBee) throws DaoException;

    /**
     * Met à jour l'état de l'export vers BEE d'un élève à partir de l'identifiant d'échange.
     * 
     * @param codeEtatExportBee
     *            code état de l'export BEE
     * @param echangeIdSiecleSynchroBee
     *            l'identifiant de l'échange avec BEE
     * @return le nombre de dossiers mis à jour
     * @throws DaoException
     *             une erreur d'accès aux données.
     */
    int majEtatExportBeeParEchangeIdSiecleSynchroBee(String codeEtatExportBee, String echangeIdSiecleSynchroBee)
            throws DaoException;

    /**
     * @param ine
     *            L'ine de l'élève à contrôler.
     * @param codeDeptMen
     *            Le département de l'utilisateur.
     * @return
     *         <ul>
     *         <li>true si l'élève a déjà été importé depuis un autre département</li>
     *         <li>false sinon</li>
     *         </ul>
     * @throws ApplicationException
     *             Un problème d'accès aux données.
     */
    boolean estDejaImporteAutreDepartement(String ine, String codeDeptMen) throws ApplicationException;

    /**
     * @param ine
     *            L'ine de l'élève à contrôler.
     * @param codeDeptMen
     *            Le département de l'utilisateur.
     * @return
     *         <ul>
     *         <li>true si l'élève a déjà été importé depuis le privé</li>
     *         <li>false sinon</li>
     *         </ul>
     * @throws ApplicationException
     *             Un problème d'accès aux données.
     */
    boolean estDejaImporteEleveDuPrive(String ine, String codeDeptMen) throws ApplicationException;

    /**
     * Donne le prénom d'un élève à insérer dans RCP_*.
     * 
     * @param eleve
     *            Un élève
     * @param prenom
     *            le prénom
     * @return le prénom de l'élève
     */
    String prenomElevePourBee(Eleve eleve, String prenom);

    /**
     * Donne le nom d'un élève à insérer dans RCP_*.
     * 
     * @param eleve
     *            Un élève
     * 
     * @return le nom de l'élève
     */
    String nomElevePourBee(Eleve eleve);

    /**
     * Ajout un collège de secteur à la liste des collèges de secteurs de l'élève.
     * 
     * @param eleve
     *            L'élève auquel on veut ajouter un collège de secteur.
     * @param etablissementSconet
     *            L'établissement correspondant au collège de secteur.
     * @throws DaoException
     *             Une erreur lors de l'accès au données.
     */
    void ajouteCollegeSecteur(Eleve eleve, EtablissementSconet etablissementSconet) throws DaoException;

    /**
     * Met à jour le/les collèges de secteur d'un élève.
     * 
     * @param donneesCalcul
     *            les données pour le positionnement du/des collèges de secteur
     * @throws ApplicationException
     *             en cas d'erreur applicative
     */
    void updateCollegeSecteur(DonneesCalculCollegeSecteur donneesCalcul) throws ApplicationException;

    /**
     * Met à jour le/les collèges de secteur issus de la simulation d'un élève.
     * 
     * @param donneesCalcul
     *            les données pour la détermination du/des collèges de secteur
     * @throws ApplicationException
     *             en cas d'erreur applicative
     */
    void updateCollegeSecteurSimule(DonneesCalculCollegeSecteur donneesCalcul) throws ApplicationException;

    /**
     * Met à jour la cause du calcul du collège de secteur non abouti.
     * 
     * @param donneesCalcul
     *            les données pour la détermination du/des collèges de secteur
     */
    void updateCauseCalculCollegeSecteur(DonneesCalculCollegeSecteur donneesCalcul);

    /**
     * Mets à jour l'adresse de simulation à partir de la vrai adresse de l'élève lorsque c'est nécessaire.
     * 
     * @param donneesCalcul
     *            les données pour la détermination du/des collèges de secteur
     * @throws TechnicalException
     *             en cas d'erreur
     */
    void updateAdresseVersAdresseSimu(DonneesCalculCollegeSecteur donneesCalcul) throws TechnicalException;

    /**
     * Initialise la cause du calcul de collège de secteur.
     * 
     * @param donneesCalcul
     *            les données pour la détermination du/des collèges de secteur
     */
    void initCauseCalculCollegeSecteur(DonneesCalculCollegeSecteur donneesCalcul);

    /**
     * @param eleve
     *            L'élève pour lequel on veut saisir des voeux.
     * @return
     *         <ul>
     *         <li>vrai si
     *         <ul>
     *         <li>l'élève a un collège défini, par la DSDEN, pour la pré-affectation</li>
     *         <li>l'ordre des collèges de secteurs potentiels est saisi</li>
     *         <li>l'élève a son collège de secteur hors du département</li>
     *         </ul>
     *         </li>
     *         <li>faux sinon</li>
     *         </ul>
     */
    boolean estSaisieVoeuxPossible(Eleve eleve);

    /**
     * Retire toutes les demandes d'affectation de l'élève hormis sa proposition de SEGPA.
     * 
     * @param eleve
     *            L'élève sur dont on supprime les demandes d'affectation.
     * @throws ApplicationException
     *             Une erreur d'accès aux données.
     * @see #videDemandesAffect(Eleve)
     */
    void videDemandesAffecSansPropositionSegpa(Eleve eleve) throws ApplicationException;

    /**
     * @param eleves
     *            La liste des élèves.
     * @return La liste des élèves regroupés par similarité de leurs collèges de secteur.
     */
    List<SecteurAffectation> regrouperElevesParCollegeSecteur(Collection<Eleve> eleves);

    /**
     * <p>
     * Donne si le collège de secteur final est connu.
     * </p>
     * 
     * <p>
     * Dans le cas d'un élève habitant dans un secteur multi-collèges, il faut que l'IA-DASEN ait désigné un
     * collège de secteur.
     * </p>
     * 
     * @param eleve
     *            Un élève
     * @return Vrai si le collège de secteur final est connu.
     */
    boolean estCollegeSecteurFinalConnu(Eleve eleve);

    /**
     * Donne le nombre d'élèves sans collège de secteur final désigné par l'IA-DASEN et avec un ou plusieurs
     * collèges potentiels.
     * 
     * @param codeDept
     *            Le code MEN d'un département
     * @return nombre d'élèves sans collège de secteur final désigné par l'IA-DASEN et avec un ou plusieurs
     *         collèges potentiels.
     */
    int compteElevesSansCollegeSecteurFinalAvecCollegesSecteursPotentiels(String codeDept);

    /**
     * Donne les collèges de secteur d'un élève.
     * 
     * @param numDossier
     *            L'identifiant d'un élève.
     * @return La liste des collèges de secteur de l'élève.
     */
    SortedSet<CollegeSecteur> listerCollegeSecteurs(Long numDossier);

    /**
     * Initialise les voeux de la famille.
     * 
     * @param eleve
     *            un élève
     */
    void initialiserSaisieVoeux(Eleve eleve);

    /**
     * Compte les élèves du département pour la détermination.
     * 
     * @param codeDeptMen
     *            Le code du département MEN de l'utilisateur courant
     * @param sansCollegeUniquement
     *            pour lister les élèves sans collèges de secteur uniquement
     * @return le nombre d'élèves correspondant.
     * @throws DaoException
     *             En cas de problème d'accès aux données.
     */
    int compterElevesPourDetermination(String codeDeptMen, boolean sansCollegeUniquement) throws DaoException;

    /**
     * Donne le nombre d'élèves avec un identifiant d'échange SIECLE Synchro.
     * 
     * @param echangeIdSiecleSynchroBee
     *            L'identifiant d'échange SIECLE Synchro Bee.
     * @return Le nombre d'élèves avec un identifiant d'échange donné en paramètre.
     * @throws DaoException
     *             En cas de problème d'accès à la base de données
     */
    int compteElevesParEchangeIdSiecleSynchroBee(String echangeIdSiecleSynchroBee) throws DaoException;

    /**
     * Liste qui contient 1 seul élève avec un identifiant d'échange SIECLE Synchro.
     * 
     * @param echangeIdSiecleSynchroBee
     *            L'identifiant d'échange SIECLE Synchro Bee.
     * @return La liste des élèves correspondant.
     * @throws DaoException
     *             En cas de problème d'accès aux données.
     */
    List<Eleve> lister1EleveParEchangeIdSiecleSynchroBee(String echangeIdSiecleSynchroBee) throws DaoException;

    /**
     * Liste les 100 premier élèves du département dont le numéro de dossier est supérieur strictement au numéro en
     * argument.
     * 
     * @param codeDeptMen
     *            Le code du département MEN de l'utilisateur courant
     * @param numDossier
     *            le numéro de dossier inférieur
     * @param sansCollegeUniquement
     *            pour lister les élèves sans collèges de secteur uniquement
     * @return La liste des élèves correspondant.
     * @throws DaoException
     *             En cas de problème d'accès aux données.
     */
    List<Eleve> listerElevesPourDetermination(String codeDeptMen, int numDossier, boolean sansCollegeUniquement)
            throws DaoException;

    /**
     * <p>
     * Donne s'il existe des voeux avec décision finale pour l'élève.
     * </p>
     *
     * @param eleve
     *            Un élève
     * @return Vrai si une décision finale existe.
     * @throws DaoException
     *             Exception remontant en cas d'erreur d'accès aux données
     */
    boolean existeVoeuxAvecDecisionFinale(Eleve eleve) throws DaoException;

    // /**
    // * Met à jour la liste des collèges de secteur d'un élève.
    // *
    // * @param eleve
    // * L'élève pour lequel on met ou pas des collèges de secteur.
    // * @param codeRneCollegeSecteurs
    // * La liste des identifiants établissements des collèges de secteurs de l'élève.
    // * @param estCollegesDansDepartement
    // * Le booléen indiquant si le(s) collège(s) de secteur est bien dans le département.
    // * @param saisieAutomatique
    // * booléen indiquant si le calcul du collège de secteur a été paramétré en automatique.
    // * @param calculAbouti
    // * booléen indiquant si le calcul automatique du collège de secteur a abouti ou non.
    // * @param adresseEleveModifiee
    // * booléen indiquant si l'adresse de l'élève a été modifiée.
    // * @param isDsden
    // * booléen indiquant si l'utilisateur est la DSDEN.
    // * @throws ApplicationException
    // * Une erreur d'accès aux données.
    // */
    // void mettreAJourCollegesSecteursPourEleve(Eleve eleve, String[] codeRneCollegeSecteurs,
    // boolean estCollegesDansDepartement, Boolean saisieAutomatique, Boolean calculAbouti,
    // Boolean adresseEleveModifiee, Boolean isDsden) throws ApplicationException;

    /**
     * Supprime les collèges de secteur et les voeux d'un élève.
     * 
     * @param eleve
     *            L'élève pour lequel on supprime les collèges de secteur et les voeux.
     * @param estCollegeDeSecteurDansDept
     *            Le booléen indiquant si le(s) collège(s) de secteur est bien dans le département.
     * @param estCollegesSecteurModifies
     *            Le booléen indiquant si le(s) collège(s) de secteur ont été modifié.
     * @param codeRneCollegeSecteurs
     *            La liste des identifiants établissements des collèges de secteurs de l'élève.
     * @param adresseEtrangere
     *            Le booléen indiquant si l'adresse de l'élève est à l'étranger.
     * @throws ApplicationException
     *             Une erreur d'accès aux données.
     */
    void purgerCollegeEtVoeux(Eleve eleve, Boolean estCollegeDeSecteurDansDept, boolean estCollegesSecteurModifies,
            String[] codeRneCollegeSecteurs, boolean adresseEtrangere) throws ApplicationException;

    /**
     * Supprime les demandes d'affectation et le choix de la famille d'un élève.
     * 
     * @param eleve
     *            L'élève pour lequel on supprime les demandes d'affectation et le choix de la famille
     * @param avecPropositionSegpa
     *            Le booléen indiquant si l'on doit aussi supprimer les propositions SEGPA
     * @param avecTypeDemande
     *            Le booléen indiquant si l'on souhaite aussi réinitialiser la réponse à la question
     *            "Affectation demandée dans un collège public du département"
     * @throws ApplicationException
     *             Une erreur d'accès aux données.
     */
    void purgerDemandesAffecEtSaisieChoixFamille(Eleve eleve, boolean avecPropositionSegpa,
            boolean avecTypeDemande) throws ApplicationException;

    /**
     * Méthode empêchant le directeur d'école de retirer le collège de secteur de la liste des collèges
     * potentiels lorsque la DSDEN.
     * 
     * @param eleve
     *            L'élève concerné par la saisie.
     * @param isDsden
     *            booléen indiquant si l'utilisateur est la DSDEN.
     * @throws DaoException
     *             Une erreur lors de l'accès aux données;
     */
    void controleSaisieCollegeSecteur(Eleve eleve, boolean isDsden) throws DaoException;

    /**
     * Méthode qui crée le voeu de secteur automatique ou la demande d'affectation hors département.
     * 
     * @param eleve
     *            l'élève pour lequel on veut créer les demandes
     * @return si la demande a bien été créée
     */
    EtatDemandeAffectation creerDemandesDefautEleve(Eleve eleve);

    /**
     * Calcule et modifie le flag d'existence d'adresse à traiter pour l'élève.
     * 
     * @param numDossier
     *            numéro de dossier de l'élève concerné par la mise à jour
     * @throws ApplicationException
     *             Une erreur d'accès aux données
     */
    void majFlagExisteAdresseATraiter(Long numDossier) throws ApplicationException;

    /**
     * Calcul et modifie le flag d'existence d'adresse à traiter pour une liste d'élèves.
     * 
     * @param numDossiers
     *            numéros de dossier des élèves concernés par la mise à jour.
     * @throws ApplicationException
     *             Une erreur d'accès aux données
     */
    void majFlagExisteAdresseATraiter(Set<Long> numDossiers) throws ApplicationException;

    /**
     * Méthode qui met à jour les langues étudiées à l'école pour une liste d'élèves.
     * 
     * @param langueEtrangereOblEcole
     *            la langue étrangère obligatoire étudiée à l'école
     * @param langueEtrangereFacEcole
     *            la langue étrangère facultative étudiée à l'école
     * @param langueRegionaleEcole
     *            la langue régionale étudiée à l'école
     * @param numDossierEleves
     *            la liste des numéros de dossiers des élèves à mettre à jour
     */
    void majLanguesEcole(Matiere langueEtrangereOblEcole, Matiere langueEtrangereFacEcole,
            Matiere langueRegionaleEcole, Long[] numDossierEleves);

    /**
     * Compte l'ensemble des élèves.
     * 
     * @param filtre
     *            le filtre à appliquer
     * @return le nombre d'élèves.
     * @throws DaoException
     *             En cas de problème pour accéder aux données
     */
    int count(Filtre filtre) throws DaoException;

    /**
     * Compte le nombre d'élèves avec un calcul de collège de secteur abouti.
     * 
     * @param codeDeptMen
     *            Le code d'un département MEN
     * @return le nombre d'élèves correspondant
     * @throws DaoException
     *             En cas de problème pour accéder aux données
     */
    int compteElevesAvecCalculCollegeSecteurAbouti(String codeDeptMen) throws DaoException;

    /**
     * Compte le nombre d'élèves avec un calcul de collège de secteur est déjà effectué.
     * soit abouti ou abouti hors département ou non abouti ou impossible
     * 
     * @param codeDeptMen
     *            Le code d'un département MEN
     * @return le nombre d'élèves correspondant
     * @throws DaoException
     *             En cas de problème pour accéder aux données
     */
    int compteElevesAvecCalculCollegeSecteurDejaEffectue(String codeDeptMen) throws DaoException;

    /**
     * Liste des élèves par liste de collège de secteur.
     * (tri prioritaire par défaut et toujours actif : Tri sur les codes établissement)
     * 
     * @param infoListe
     *            les paramètres de Pagination, Filtre, Tri
     * @return Map liant les secteurs (liste de collège de secteur) à leurs élèves
     * @throws DaoException
     *             En cas de problème pour accéder aux données
     */
    Map<List<EtablissementSconet>, List<Eleve>> listerAvecSecteur(InfoListe<Eleve> infoListe) throws DaoException;

    /**
     * Liste des élèves par liste de collège de secteur.
     * (tri prioritaire par défaut et toujours actif : Tri sur les codes établissement)
     * 
     * @param pagination
     *            les paramètres de Pagination
     * @param tris
     *            les paramètres de tris
     * @param filtre
     *            les paramètres de filtrage
     * @return Map liant les secteurs (liste de collège de secteur) à leurs élèves
     * @throws DaoException
     *             En cas de problème pour accéder aux données
     */
    Map<List<EtablissementSconet>, List<Eleve>> listerAvecSecteur(Pagination<Eleve> pagination, List<Tri> tris,
            Filtre filtre) throws DaoException;

    /**
     * Vérifie qu'il existe une offre de formation 6ème dans le ou les collèges de secteur.
     * 
     * @param eleve
     *            L'élève
     * @return l'état de l'offre de formation 6ème du collège de secteur désigné ou de tous les collèges pour la
     *         création d'une demande d'affectation
     * @throws ApplicationException
     *             Une erreur d'accès aux données
     */
    EtatDemandeAffectation controleOffreDeFormation6emeDemandeAffectation(Eleve eleve) throws ApplicationException;

    /**
     * Gère les demandes d'affectation de l'élève.
     * 
     * @param eleve
     *            L'élève
     * @param codeDeptMen
     *            Le code d'un département MEN
     * @return si la demande d'affectation par défaut est créée ou pas
     * @throws ApplicationException
     *             Une erreur d'accès aux données
     */
    EtatDemandeAffectation gereDemandeAffectation(Eleve eleve, String codeDeptMen) throws ApplicationException;

    /**
     * Vérifie si l'adresse de l'élève est dans le département contexte de gestion.
     * 
     * @param eleve
     *            L'élève
     * @return vrai si l'adresse de l'élève est dans le département
     */
    boolean estAdresseDansDepartement(Eleve eleve);

    /**
     * @param eleve
     *            l'élève
     * @return vrai si l'élève a au moins un représentant légal
     */
    boolean possedeRepresentantLegal(Eleve eleve);

    /**
     * @param eleves
     *            la liste de élèves
     * @return
     *         la liste les décisions de passage par numéro de dossier
     */
    Map<Long, DecisionDepartement> getDecisionsParNumDossier(Collection<Eleve> eleves);

    /**
     * Liste les élèves avec une demande de dérogation sans accusé réception édité.
     * 
     * @param codeUai
     *            Le code UAI de l'école
     * @param codeDept
     *            Le code MEN du département
     * @return la liste des élèves trouvés
     */
    List<Eleve> listerElevesAvecDerogSansEditionAccuseReceptionEcole(String codeUai, String codeDept);

    /**
     * Restaure les données d'un élève.
     * 
     * @param eleve
     *            élève
     * @param sauvegardeEleve
     *            données de sauvegarde de l'élève
     * @throws ApplicationException
     *             en cas d'erreur lors de l'accès aux données
     */
    void restaurerEleve(Eleve eleve, SauvegardeEleve sauvegardeEleve) throws ApplicationException;

    /**
     * Indique si l'élève est issu d'un import Onde d'une école publique.
     * 
     * @param eleve
     *            l'élève
     * @return vrai si l'élève est issu d'un import Onde d'une école publique sinon faux.
     */
    boolean isImportEcolePubliqueOnde(Eleve eleve);

    /**
     * Indique si l'établissement d'origine de l'élève est un DSDEN (école virtuelle).
     * 
     * @param eleve
     *            l'élève
     * @return vrai si l'établissement d'origine est un DSDEN sinon faux
     */
    boolean isEtabOrigineDsden(Eleve eleve);
}
