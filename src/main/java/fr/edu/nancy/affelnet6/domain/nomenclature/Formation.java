/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.nomenclature;

import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import fr.edu.nancy.affelnet6.helper.DemandeAffectationHelper;

/**
 * Formations 6ème sur lesquelles les élèves peuvent être affectés.
 */
public class Formation implements java.io.Serializable, Comparable<Formation> {

    /** Code MEF 6ème. */
    public static final String CODE_MEF_6EME = "10010012110";
    /** Code MEF 6ème SEGPA. */
    public static final String CODE_MEF_6SEGPA = "16410002110";
    /** Code MEF 6ème musique. */
    public static final String CODE_MEF_6EME_MUSIQUE = "10010002110";
    /** Code MEF 6ème danse. */
    public static final String CODE_MEF_6EME_DANSE = "10010022110";
    /** Code MEF 6ème théâtre. */
    public static final String CODE_MEF_6EME_THEATRE = "10010023110";

    /** Code MEF de la formation 6ème bilangue de continuité musique. */
    public static final String CODE_MEF_6EME_BILANGUE_CONTINUITE_MUSIQUE = "10010029110";
    /** Code MEF de la formation 6ème bilangue de continuité théâtre . */
    public static final String CODE_MEF_6EME_BILANGUE_CONTINUITE_THEATRE = "10010030110";
    /** Code MEF de la formation 6ème bilangue de continuité . */
    public static final String CODE_MEF_6EME_BILANGUE_CONTINUITE = "10010027110";
    /** Code MEF de la formation 6ème bilangue de continuité danse. */
    public static final String CODE_MEF_6EME_BILANGUE_CONTINUITE_DANSE = "10010028110";

    /**
     * Codes de formation bilangue continuité.
     */
    //@formatter:off
    public static final String[] CODES_MEF_BILANGUE_CONTINUITE = {
            CODE_MEF_6EME_BILANGUE_CONTINUITE_MUSIQUE,
            CODE_MEF_6EME_BILANGUE_CONTINUITE_THEATRE,
            CODE_MEF_6EME_BILANGUE_CONTINUITE,
            CODE_MEF_6EME_BILANGUE_CONTINUITE_DANSE
    };
    //@formatter:on

    /**
     * Codes de formation à horaires aménagés.
     */
    //@formatter:off
    public static final String[] CODES_MEF_CHAM = {
            CODE_MEF_6EME_MUSIQUE,
            CODE_MEF_6EME_DANSE,
            CODE_MEF_6EME_THEATRE,
            CODE_MEF_6EME_BILANGUE_CONTINUITE_MUSIQUE,
            CODE_MEF_6EME_BILANGUE_CONTINUITE_THEATRE,
            CODE_MEF_6EME_BILANGUE_CONTINUITE,
            CODE_MEF_6EME_BILANGUE_CONTINUITE_DANSE
    };
    //@formatter:on

    /** Dernière lettre du code MEF indiquant qu'il s'agit d'une MEF 6ème UPE2A. */
    public static final String TYPE_MEF_6EME_UPE2A = "F";

    /** Dernière lettre du code MEF indiquant qu'il s'agit d'un MEF Ulis. */
    public static final String TYPE_MEF_ULIS = "U";

    /** Le code dispositif caractérisant une formation 6ÈME SEGPA. */
    public static final String CODE_DISPOSITIF_SEGPA = "164";

    /** serialVersionUID. */
    private static final long serialVersionUID = 4710743393389625999L;

    /** Code MEF. */
    private String code;

    /** Mnémonique métier . */
    private String mnemonique;

    /** Spécialité. */
    private String specialite;

    /** Code MEFSTAT 11. */
    private String codeMefStat11;

    /** Code MEFSTAT 4. */
    private String codeMefStat4;

    /** Libellé. */
    private String libelle;

    /** Date d'ouverture. */
    private Date dtOuverture;

    /** Date de fermeture. */
    private Date dtFermeture;

    /** MEF national de rattachement. */
    private Formation acaFormationNat;

    /** Offres de formation. */
    private Set<OffreFormation> offreFormations = new HashSet<>(0);

    /**
     * Constructeur par défaut.
     */
    public Formation() {
    }

    /**
     * Constructeur.
     * 
     * @param mnemonique
     *            Mnémonique
     */
    public Formation(String mnemonique) {
        this.mnemonique = mnemonique;
    }

    /**
     * Constructeur.
     * 
     * @param mnemonique
     *            mnémonique
     * @param libelle
     *            libellé
     */
    public Formation(String mnemonique, String libelle) {
        this.mnemonique = mnemonique;
        this.libelle = libelle;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the mnemonique
     */
    public String getMnemonique() {
        return this.mnemonique;
    }

    /**
     * @param mnemonique
     *            the mnemonique to set
     */
    public void setMnemonique(String mnemonique) {
        this.mnemonique = mnemonique;
    }

    /**
     * @return the specialite
     */
    public String getSpecialite() {
        return this.specialite;
    }

    /**
     * @param specialite
     *            the specialite to set
     */
    public void setSpecialite(String specialite) {
        this.specialite = specialite;
    }

    /**
     * @return the codeMefStat11
     */
    public String getCodeMefStat11() {
        return this.codeMefStat11;
    }

    /**
     * @param codeMefStat11
     *            the codeMefStat11 to set
     */
    public void setCodeMefStat11(String codeMefStat11) {
        this.codeMefStat11 = codeMefStat11;
    }

    /**
     * @return the codeMefStat4
     */
    public String getCodeMefStat4() {
        return this.codeMefStat4;
    }

    /**
     * @param codeMefStat4
     *            the codeMefStat4 to set
     */
    public void setCodeMefStat4(String codeMefStat4) {
        this.codeMefStat4 = codeMefStat4;
    }

    /**
     * @return the libelle
     */
    public String getLibelle() {
        return this.libelle;
    }

    /**
     * @param libelle
     *            the libelle to set
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /**
     * @return the dtOuverture
     */
    public Date getDtOuverture() {
        return this.dtOuverture;
    }

    /**
     * @param dtOuverture
     *            the dtOuverture to set
     */
    public void setDtOuverture(Date dtOuverture) {
        this.dtOuverture = dtOuverture;
    }

    /**
     * @return the dtFermeture
     */
    public Date getDtFermeture() {
        return this.dtFermeture;
    }

    /**
     * @param dtFermeture
     *            the dtFermeture to set
     */
    public void setDtFermeture(Date dtFermeture) {
        this.dtFermeture = dtFermeture;
    }

    /**
     * @return the acaFormationNat
     */
    public Formation getAcaFormationNat() {
        return acaFormationNat;
    }

    /**
     * @param acaFormationNat
     *            the acaFormationNat to set
     */
    public void setAcaFormationNat(Formation acaFormationNat) {
        this.acaFormationNat = acaFormationNat;
    }

    /**
     * @return the offreFormations
     */
    public Set<OffreFormation> getOffreFormations() {
        return this.offreFormations;
    }

    /**
     * @param offreFormations
     *            the offreFormations to set
     */
    public void setOffreFormations(Set<OffreFormation> offreFormations) {
        this.offreFormations = offreFormations;
    }

    /**
     * Retourne vrai si c'est une formation ULIS.
     * 
     * @return vrai si c'est une formation ULIS sinon faux
     */
    public boolean getEstFormationUlis() {
        return DemandeAffectationHelper.estFormationUlis(this.code);
    }

    /**
     * Retourne vrai si c'est une formation SEGPA.
     * 
     * @return vrai si c'est une formation SEGPA sinon faux
     */
    public boolean getEstFormationSegpa() {
        return DemandeAffectationHelper.estFormationSegpa(this.code);
    }

    @Override
    public int compareTo(Formation autreFormation) {
        if (autreFormation == null || StringUtils.isEmpty(autreFormation.getCode())) {
            return -1;
        }

        if (this.getLibelle().compareTo(autreFormation.getLibelle()) != 0) {
            return this.getLibelle().compareTo(autreFormation.getLibelle());
        }

        return this.getCode().compareTo(autreFormation.getCode());
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Formation)) {
            return false;
        }
        Formation f = (Formation) obj;
        return Objects.equals(code, f.code) && Objects.equals(libelle, f.libelle);
    }

    @Override
    public int hashCode() {
        return code == null ? 0 : code.hashCode();
    }
}
