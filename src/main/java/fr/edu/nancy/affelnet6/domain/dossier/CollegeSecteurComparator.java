/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.dossier;

import java.util.Comparator;

public class CollegeSecteurComparator implements Comparator<CollegeSecteur> {

    @Override
    public int compare(CollegeSecteur o1, CollegeSecteur o2) {
        if (o1 == null) {
            if (o2 != null) {
                return 1;
            }
            return 0;
        }
        if (o2 == null) {
            return -1;
        }

        int comparaisonEtab = o1.getEtablissementSconet().getCodeRne()
                .compareTo(o2.getEtablissementSconet().getCodeRne());

        if (o1.getNumeroOrdre() == null) {
            if (o2.getNumeroOrdre() != null) {
                return 1;
            }
            return comparaisonEtab;
        }

        if (o2.getNumeroOrdre() == null) {
            return -1;
        }

        if (o1.getNumeroOrdre() != null && o2.getNumeroOrdre() != null
                && o1.getNumeroOrdre().compareTo(o2.getNumeroOrdre()) != 0) {
            return o1.getNumeroOrdre().compareTo(o2.getNumeroOrdre());
        }
        return comparaisonEtab;
    }
}
