/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.dossier;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import fr.edu.nancy.affelnet6.domain.nomenclature.DeptMen;
import fr.edu.nancy.affelnet6.domain.nomenclature.EtatDecision;
import fr.edu.nancy.affelnet6.domain.nomenclature.Formation;
import fr.edu.nancy.affelnet6.domain.nomenclature.OffreFormation;
import fr.edu.nancy.affelnet6.domain.nomenclature.TypeDemandeAffec;

/**
 * Une demande d'affectation, avec le niveau de priorité en plus.
 */
public class DemandeAffectationVue implements Serializable {

    /** SerialID. */
    private static final long serialVersionUID = -4319121278357371819L;

    /**
     * id de la demande.
     */
    private Long id;
    /**
     * numéro dossier élève.
     */
    // TODO renommer cet attribut en eleve (et modifier mapping Hibernate)
    private Eleve numDossier;
    /**
     * état de la demande.
     */
    private EtatDecision etatDemande;
    /**
     * type de la demande.
     */
    private TypeDemandeAffec typeDemande;
    /**
     * département MEN.
     */
    private DeptMen dept;
    /**
     * offre de formation.
     */
    private OffreFormation offreformation;
    /**
     * rang de la demande.
     */
    private short rang;
    /**
     * indicateur de type de demande spécifique.
     */
    private String indicateurTypeDemandeSpecifique;
    /**
     * élève concerné.
     */
    private Set<Eleve> eleves = new HashSet<Eleve>(0);
    /**
     * justificatifs de dérogation attachés à la demande.
     */
    private SortedSet<JustificatifDerog> justificatifDerogs = new TreeSet<JustificatifDerog>();

    /**
     * Le niveau de priorité de la demande en fonction des motifs de dérogation.
     */
    private Integer niveauPrioriteDemande;

    /** La formation liée à la demande. */
    private Formation formation;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    // TODO renommer cette méthode en getEleve()
    public Eleve getNumDossier() {
        return this.numDossier;
    }

    // TODO renommer cette méthode en setEleve()
    /** @return élève associé à la demande */
    public Eleve getEleve() {
        return this.numDossier;
    }

    public void setNumDossier(Eleve numDossier) {
        this.numDossier = numDossier;
    }

    public EtatDecision getEtatDemande() {
        return this.etatDemande;
    }

    public void setEtatDemande(EtatDecision etatDemande) {
        this.etatDemande = etatDemande;
    }

    public TypeDemandeAffec getTypeDemande() {
        return this.typeDemande;
    }

    public void setTypeDemande(TypeDemandeAffec typeDemande) {
        this.typeDemande = typeDemande;
    }

    public DeptMen getDept() {
        return this.dept;
    }

    public void setDept(DeptMen dept) {
        this.dept = dept;
    }

    public OffreFormation getOffreformation() {
        return this.offreformation;
    }

    public void setOffreformation(OffreFormation offreformation) {
        this.offreformation = offreformation;
    }

    public short getRang() {
        return this.rang;
    }

    public void setRang(short rang) {
        this.rang = rang;
    }

    public String getIndicateurTypeDemandeSpecifique() {
        return this.indicateurTypeDemandeSpecifique;
    }

    public void setIndicateurTypeDemandeSpecifique(String indicateurTypeDemandeSpecifique) {
        this.indicateurTypeDemandeSpecifique = indicateurTypeDemandeSpecifique;
    }

    public Set<Eleve> getEleves() {
        return this.eleves;
    }

    public void setEleves(Set<Eleve> eleves) {
        this.eleves = eleves;
    }

    public SortedSet<JustificatifDerog> getJustificatifDerogs() {
        return this.justificatifDerogs;
    }

    public void setJustificatifDerogs(SortedSet<JustificatifDerog> justificatifDerogs) {
        this.justificatifDerogs = justificatifDerogs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (o == null) {
            return false;
        } else if (!(o instanceof DemandeAffectation)) {
            return false;
        }
        DemandeAffectation demandeAffectation = (DemandeAffectation) o;
        return (this.compareTo(demandeAffectation) == 0);
    }

    public int compareTo(DemandeAffectation anotherDemandeAffectation) {
        if (anotherDemandeAffectation == null) {
            return 1;
        }
        Long idB = anotherDemandeAffectation.getId();
        Long idA = this.getId();
        if (idA == null) {
            return -1;
        }
        return idA.compareTo(idB);
    }

    @Override
    public int hashCode() {
        int result = 17;

        result = 37 * result + (getId() == null ? 0 : getId().hashCode());

        return result;
    }

    /**
     * Donne une chaîne de synthèse de la demande.
     * 
     * @return chaine de synthèse
     */
    public String toGuruString() {

        // mise en commentaire car l'appel n'est plus statique et ça introduit une
        // dépendance non souhaitée (un DTO qui dépend de la couche service)
        // int bestPri = DemandeAffectationManagerImpl.niveauPrioritePlusHaute(this);

        String gs = "(" + id + ", ";
        gs += "dep:" + ((dept == null) ? "NULL" : dept.getDeptCode()) + ", ";
        gs += "elv:" + ((numDossier == null) ? "NULL" : numDossier.getNumDossier()) + ", ";
        gs += "t:" + ((typeDemande == null) ? "NULL" : typeDemande.getCode()) + ", ";
        gs += "of:" + ((offreformation == null) ? "NULL" : offreformation.getCod()) + ", ";
        gs += "rg:" + rang + ", ";
        // gs+= "pri:"+bestPri+", ";
        gs += "dec:" + ((etatDemande == null) ? "NULL" : etatDemande.getCode()) + ")";
        return gs;
    }

    /**
     * Donne une chaîne de affichable décrivant la demande.
     * 
     * @return chaine de affichable
     */
    public String toPrettyString() {
        // mise en commentaire car l'appel n'est plus statique et ça introduit une
        // dépendance non souhaitée (un DTO qui dépend de la couche service)
        // int bestPri = DemandeAffectationManagerImpl.niveauPrioritePlusHaute(this);

        StringBuffer prettyString = new StringBuffer();
        prettyString.append("de l'élève ");
        prettyString.append(numDossier.getNom());
        prettyString.append(" ");
        prettyString.append(numDossier.getPrenom());
        prettyString.append(" sur l'offre ");
        prettyString.append(offreformation.getCod());
        prettyString.append(" de rang ");
        prettyString.append(rang);
        // +" et de priorité "+bestPri;

        return prettyString.toString();
    }

    /**
     * Teste si une demande ne concerne pas un collège public du département.
     * 
     * @return vrai si la demande ne concerne pas un collège public du département
     */
    public boolean isHorsPublicDepartmental() {

        return TypeDemandeAffec.CODE_TYPE_DEMANDE_HORS_COLLEGE_PUBLIC_DEPT.equals(typeDemande.getCode());
    }

    /**
     * Teste si une demande concerne une SEGPA.
     * 
     * @return vrai si la demande concerne une SEGPA
     */
    public boolean isDemandeSegpa() {
        return TypeDemandeAffec.CODE_TYPE_DEMANDE_SEGPA.equals(typeDemande.getCode());
    }

    /**
     * @return Le niveau de priorité de la demande en fonction des motifs de dérogation.
     */
    public Integer getNiveauPrioriteDemande() {
        return niveauPrioriteDemande;
    }

    /**
     * @param niveauPrioriteDemande
     *            Le niveau de priorité de la demande.
     */
    public void setNiveauPrioriteDemande(Integer niveauPrioriteDemande) {
        this.niveauPrioriteDemande = niveauPrioriteDemande;
    }

    /**
     * @return La formation sur laquelle la demande d'affectation est faîte.
     */
    public Formation getFormation() {
        return formation;
    }

    /**
     * @param formation
     *            La formation à lier à la demande d'affectation.
     * */
    public void setFormation(Formation formation) {
        this.formation = formation;
    }

}
