/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.dossier;

import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.engine.spi.PersistentAttributeInterceptable;
import org.hibernate.engine.spi.PersistentAttributeInterceptor;

import fr.edu.nancy.affelnet6.domain.be1d.EleveBe1d;
import fr.edu.nancy.affelnet6.domain.nomenclature.CauseCalculCollegeSecteur;
import fr.edu.nancy.affelnet6.domain.nomenclature.CommuneInsee;
import fr.edu.nancy.affelnet6.domain.nomenclature.Cycle1d;
import fr.edu.nancy.affelnet6.domain.nomenclature.DecisionPassage;
import fr.edu.nancy.affelnet6.domain.nomenclature.DeptMen;
import fr.edu.nancy.affelnet6.domain.nomenclature.EtablissementOrigine;
import fr.edu.nancy.affelnet6.domain.nomenclature.EtablissementSconet;
import fr.edu.nancy.affelnet6.domain.nomenclature.EtatCollegeSecteur;
import fr.edu.nancy.affelnet6.domain.nomenclature.EtatDecision;
import fr.edu.nancy.affelnet6.domain.nomenclature.EtatExportBee;
import fr.edu.nancy.affelnet6.domain.nomenclature.Formation;
import fr.edu.nancy.affelnet6.domain.nomenclature.IndicateurPCS;
import fr.edu.nancy.affelnet6.domain.nomenclature.Matiere;
import fr.edu.nancy.affelnet6.domain.nomenclature.Niveau1d;
import fr.edu.nancy.affelnet6.domain.nomenclature.NiveauResponsabilite;
import fr.edu.nancy.affelnet6.domain.nomenclature.Sexe;
import fr.edu.nancy.affelnet6.domain.nomenclature.StatutAdresse;
import fr.edu.nancy.affelnet6.domain.nomenclature.TypeOrigineDossier;
import fr.edu.nancy.affelnet6.utils.EtabUtils;
import fr.edu.nancy.commun.domain.Role;

/** Dossier élève. */
public class Eleve implements java.io.Serializable, PersistentAttributeInterceptable  {

    /** SerialTag. */
    private static final long serialVersionUID = 3735419277160032157L;

    private Long numDossier;
    private EleveBe1d numDossierBe1d;
    private Matiere langueEtrangereOblEcole;
    private Matiere langueEtrangereFacEcole;
    private Matiere langueRegionaleEcole;
    private Niveau1d niveau;
    private EtatDecision codeDecisionFinale;
    private String codUaiOri;
    private EtablissementOrigine ecoleOrigine;
    private DecisionPassage decisionPassage;
    private Sexe sexe;
    private Adresse adresseResidence;
    private Adresse adresseSecondaire;
    private DemandeAffectation affectationFinale;
    private Cycle1d cycle;
    private CommuneInsee communeNaissance;
    private EtablissementSconet collegeSecteur;
    private DeptMen dept;
    private EtablissementSconet collegeSaisie;
    private String ine;
    private String origineDossier;
    private String nom;
    private String nomUsage;
    private String prenom;
    private String prenom2;
    private String prenom3;
    private Date dateNaissance;
    private Boolean naissanceFrance;
    private Boolean confidentialiteResp = false;
    private Date editionVolet1;
    private Date editionVolet1Bis;
    private Date editionVolet2;
    private int nombreDeDerogation;
    private String collegeSecteurCodes;
    private String circonscription;
    private Date dateEnvoiNotif;

    /** Date de dernière édition de la notification d''affectation pour le responsable. */
    private Date dateEditNotif;
    /**
     * langue vivante 1.
     */
    private Matiere lv1;
    /**
     * langue vivante 2.
     */
    private Matiere lv2;
    /**
     * langue vivante 2.
     */
    private Matiere lv3;
    private Date dateCreation;
    private Date dateMaj;
    private Boolean estCollegeSecteurDansDepartement = true;
    private Eleve numDossierOrigine;
    private SortedSet<DemandeAffectation> demandeAffectation = new TreeSet<>(
            new DemandeAffectationComparatorRang());
    private SortedSet<Responsable> responsables = new TreeSet<>(new ResponsableComparator());
    private SortedSet<CollegeSecteur> collegeSecteurs = new TreeSet<>(new CollegeSecteurComparator());
    private SortedSet<CollegeSecteurSimule> collegeSecteursSimules = new TreeSet<>(
            new CollegeSecteurSimuleComparator());

    /** Origine du dossier (basée sur le origineDossier). */
    private TypeOrigineDossier typeOrigineDossier;

    private Boolean possedeHandicap = false;

    private Formation formationDemandeSecteur;

    private Boolean estCollegeSecteurSaisiParDsden = false;

    private Boolean estScolarisationDansCollegeSecteur;

    private SortedSet<DemandeFamille> demandeFamilles = new TreeSet<>();

    private Boolean estAffectationDemandeeCollegePublicDept;

    private Date editionAccuseReception;

    private Boolean existeAdresseATraiter;

    private EtatCollegeSecteur etatCollegeSecteur;

    private EtatCollegeSecteur etatCollegeSecteurSimule;

    /** Indicateur PCS le plus élevé parmi les responsables de l'élève. */
    private IndicateurPCS indicateurPcs;

    /** Rôle de l'utilisateur qui a fait la dernière mise à jour du collège de secteur. */
    private Role roleMajCollegeSecteur;

    /**
     * Identifiant de l'adresse de l'élève à la rentrée scolaire d'affectation enregistrée suite à la simulation
     * d'une détermination automatique.
     */
    private Adresse adresseResidenceSimu;

    /**
     * Cause en cas de simulation de détermination automatique non aboutie.
     */
    private CauseCalculCollegeSecteur causeCalculCollegeSecteur;

    /**
     * Dispositifs de personnalisation des parcours.
     */
    private Set<ElevePersonnalisationParcours> elevePersonnalisationsParcours = new LinkedHashSet<>();

    /** Code de l'état de l'export d'un élève vers BEE (non envoyé par défaut). */
    private String codeEtatExportBee = EtatExportBee.NON_ENVOYE;

    /** Etat de l'export d'un élève vers BEE (basée sur codeEtatExportBee). */
    private EtatExportBee etatExportBee;

    /** Identifiant de l'échange avec BEE permettant d'effectuer un suivi dans la console Siecle Synchro. */
    private String echangeIdSiecleSynchroBee;

    /** Date de réception du volet 2. */
    private Date dateReceptionVolet2;
    
    /** Date de restauration des choix de la famille */
    private Date dateRestoDemandeFamille;

    /**
     * Objet utilisé par Hibernate pour faire du lazy-loading sans bytecode instrumentation.
     */
    private PersistentAttributeInterceptor interceptor;

    /**
     * @return Le nombre de dérogation hors voeux et hors demande en SEGPA
     *         Une dérogation compte pour un même si elle est associée à deux justificatifs
     *         de dérogation
     */
    public int getNombreDeDerogation() {
        return nombreDeDerogation;
    }

    /**
     * Attention cette propriété est une propriété calculée dans Eleve.hbm.xml.
     * Elle n'est donc pas modifiable.
     * 
     * @param nombreDeDerogation
     *            nombre de dérogation
     */
    public void setNombreDeDerogation(int nombreDeDerogation) {
        this.nombreDeDerogation = nombreDeDerogation;
    }

    public Long getNumDossier() {
        return this.numDossier;
    }

    public void setNumDossier(Long numDossier) {
        this.numDossier = numDossier;
    }

    public EleveBe1d getNumDossierBe1d() {
        return this.numDossierBe1d;
    }

    public void setNumDossierBe1d(EleveBe1d numDossierBe1d) {
        this.numDossierBe1d = numDossierBe1d;
    }

    public Matiere getLangueEtrangereOblEcole() {
        return langueEtrangereOblEcole;
    }

    public void setLangueEtrangereOblEcole(Matiere langueEtrangereOblEcole) {
        this.langueEtrangereOblEcole = langueEtrangereOblEcole;
    }

    public Matiere getLangueEtrangereFacEcole() {
        return langueEtrangereFacEcole;
    }

    public void setLangueEtrangereFacEcole(Matiere langueEtrangereFacEcole) {
        this.langueEtrangereFacEcole = langueEtrangereFacEcole;
    }

    public Matiere getLangueRegionaleEcole() {
        return langueRegionaleEcole;
    }

    public void setLangueRegionaleEcole(Matiere langueRegionaleEcole) {
        this.langueRegionaleEcole = langueRegionaleEcole;
    }

    public Niveau1d getNiveau() {
        return this.niveau;
    }

    public void setNiveau(Niveau1d niveau) {
        this.niveau = niveau;
    }

    public EtatDecision getCodeDecisionFinale() {
        return this.codeDecisionFinale;
    }

    public void setCodeDecisionFinale(EtatDecision codeDecisionFinale) {
        this.codeDecisionFinale = codeDecisionFinale;
    }

    /**
     * Ne doit pas être utilisée, sert uniquement à l'optmisation des requêtes.
     * 
     * @return unused: retourne le code de l'établissement d'origine
     */
    public String getCodUaiOri() {
        return codUaiOri;
    }

    /**
     * Ne doit pas être utilisée, sert uniquement à l'optmisation des requêtes.
     * 
     * @param codUaiOri
     *            le code de l'établissement d'origine
     */
    public void setCodUaiOri(String codUaiOri) {
        this.codUaiOri = codUaiOri;
    }

    public EtablissementOrigine getEcoleOrigine() {
        return this.ecoleOrigine;
    }

    public void setEcoleOrigine(EtablissementOrigine ecoleOrigine) {
        this.ecoleOrigine = ecoleOrigine;
    }

    public DecisionPassage getDecisionPassage() {
        return this.decisionPassage;
    }

    public void setDecisionPassage(DecisionPassage decisionPassage) {
        this.decisionPassage = decisionPassage;
    }

    public Sexe getSexe() {
        return this.sexe;
    }

    public void setSexe(Sexe sexe) {
        this.sexe = sexe;
    }

    public Adresse getAdresseResidence() {
        return this.adresseResidence;
    }

    public void setAdresseResidence(Adresse adresseResidence) {
        this.adresseResidence = adresseResidence;
    }

    public Adresse getAdresseSecondaire() {
        return this.adresseSecondaire;
    }

    public void setAdresseSecondaire(Adresse adresseSecondaire) {
        this.adresseSecondaire = adresseSecondaire;
    }

    public DemandeAffectation getAffectationFinale() {
        return this.affectationFinale;
    }

    public void setAffectationFinale(DemandeAffectation affectationFinale) {
        this.affectationFinale = affectationFinale;
    }

    public Cycle1d getCycle() {
        return this.cycle;
    }

    public void setCycle(Cycle1d cycle) {
        this.cycle = cycle;
    }

    public CommuneInsee getCommuneNaissance() {
        return this.communeNaissance;
    }

    public void setCommuneNaissance(CommuneInsee communeNaissance) {
        this.communeNaissance = communeNaissance;
    }

    public EtablissementSconet getCollegeSecteur() {
        return this.collegeSecteur;
    }

    public void setCollegeSecteur(EtablissementSconet collegeSecteur) {
        this.collegeSecteur = collegeSecteur;
    }

    public DeptMen getDept() {
        return this.dept;
    }

    public void setDept(DeptMen dept) {
        this.dept = dept;
    }

    public EtablissementSconet getCollegeSaisie() {
        return this.collegeSaisie;
    }

    public void setCollegeSaisie(EtablissementSconet collegeSaisie) {
        this.collegeSaisie = collegeSaisie;
    }

    public String getIne() {
        return this.ine;
    }

    public void setIne(String ine) {
        this.ine = ine;
    }

    public String getOrigineDossier() {
        return this.origineDossier;
    }

    public void setOrigineDossier(String origineDossier) {
        this.origineDossier = origineDossier;
    }

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getNomUsage() {
        return this.nomUsage;
    }

    public void setNomUsage(String nomUsage) {
        this.nomUsage = nomUsage;
    }

    public String getPrenom() {
        return this.prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getPrenom2() {
        return this.prenom2;
    }

    public void setPrenom2(String prenom2) {
        this.prenom2 = prenom2;
    }

    public String getPrenom3() {
        return this.prenom3;
    }

    public void setPrenom3(String prenom3) {
        this.prenom3 = prenom3;
    }

    public Date getDateNaissance() {
        return this.dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public Boolean getNaissanceFrance() {
        return this.naissanceFrance;
    }

    public void setNaissanceFrance(Boolean naissanceFrance) {
        this.naissanceFrance = naissanceFrance;
    }

    public Boolean getConfidentialiteResp() {
        return confidentialiteResp;
    }

    public void setConfidentialiteResp(Boolean confidentialiteResp) {
        this.confidentialiteResp = confidentialiteResp;
    }

    public Date getEditionVolet1() {
        return this.editionVolet1;
    }

    public void setEditionVolet1(Date editionVolet1) {
        this.editionVolet1 = editionVolet1;
    }

    public Date getEditionVolet1Bis() {
        return editionVolet1Bis;
    }

    public void setEditionVolet1Bis(Date editionVolet1Bis) {
        this.editionVolet1Bis = editionVolet1Bis;
    }

    public Date getEditionVolet2() {
        return this.editionVolet2;
    }

    public void setEditionVolet2(Date editionVolet2) {
        this.editionVolet2 = editionVolet2;
    }

    public Date getDateCreation() {
        return this.dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Date getDateMaj() {
        return this.dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public SortedSet<DemandeAffectation> getDemandeAffectation() {
        return this.demandeAffectation;
    }

    public void setDemandeAffectation(SortedSet<DemandeAffectation> demandeAffectation) {
        this.demandeAffectation = demandeAffectation;
    }

    public SortedSet<Responsable> getResponsables() {
        return this.responsables;
    }

    public void setResponsables(SortedSet<Responsable> responsables) {
        this.responsables = responsables;
    }

    public Boolean getEstCollegeSecteurDansDepartement() {
        return estCollegeSecteurDansDepartement;
    }

    public void setEstCollegeSecteurDansDepartement(Boolean estCollegeSecteurDansDepartement) {
        this.estCollegeSecteurDansDepartement = estCollegeSecteurDansDepartement;
    }

    public Eleve getNumDossierOrigine() {
        return numDossierOrigine;
    }

    public void setNumDossierOrigine(Eleve numDossierOrigine) {
        this.numDossierOrigine = numDossierOrigine;
    }

    /** @return the typeOrigineDossier */
    public TypeOrigineDossier getTypeOrigineDossier() {
        return typeOrigineDossier;
    }

    /**
     * @param typeOrigineDossier
     *            the typeOrigineDossier to set
     */
    public void setTypeOrigineDossier(TypeOrigineDossier typeOrigineDossier) {
        this.typeOrigineDossier = typeOrigineDossier;
    }

    /**
     * Donne une représentation courte de élève.
     * 
     * @return chaîne d'infos sur l'élève
     */
    public String toShortString() {
        return numDossier + " " + nom + " " + prenom + " (" + ine + ")";
    }

    /** Vide l'ensemble des demandes d'affectation de l'élève (sans cascade). */
    public void viderDemandesAffectation() {
        this.demandeAffectation.clear();
    }

    /**
     * @return vrai si l'élève a au moins un représentant légal dont l'adresse est inconnue ou incomplète, faux
     *         sinon
     */
    public boolean possedeRepLegAdresseInconnueOuIncomplete() {
        if (getResponsables() == null) {
            return false;
        }
        for (Responsable r : getResponsables()) {
            // représentant légal
            if (r.getNiveauResponsabilite() != null
                    && NiveauResponsabilite.CODE_REPRESENTANT_LEGAL.equals(r.getNiveauResponsabilite().getCode())
                    // adresse inconnue ou incomplète
                    && (r.getAdresseResponsable() == null || r.getAdresseResponsable().getIncomplete())) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return vrai si l'élève dispose d'un adresse validée/confirmée ou d'une adresse à l'étranger
     */
    public boolean possedeAdresseValidee() {

        if (this.getAdresseResidence() == null) {
            return false;
        }

        StatutAdresse statutAdresse = this.getAdresseResidence().getStatutAdresse();

        if (statutAdresse == null) {
            return false;
        }

        return StatutAdresse.ADR_VALIDEE.equals(statutAdresse.getCodeEtat())
                || StatutAdresse.ADR_ETRANGERE.equals(statutAdresse.getCode());
    }

    /**
     * Indique si l'élève partage aucune adresse avec les responsables.
     * 
     * @return vrai si l'élève partage aucune adresse avec ses responsables sinon faux
     */
    public boolean partageAucuneAdresseAvecResponsables() {

        Adresse adresseEleve = this.getAdresseResidence();
        Adresse adresseSecondaireEleve = this.getAdresseSecondaire();

        // si l'élève n'a aucune adresse il ne partage aucune adresse
        if (adresseEleve == null && adresseSecondaireEleve == null) {
            return true;
        }

        Comparator<Adresse> adrCpr = new AdresseComparator();

        // pour chaque responsable, on compare l'adresse 1 et 2 de l'élève
        for (Responsable responsable : this.getResponsables()) {
            Adresse adresseResp = responsable.getAdresseResponsable();
            // on compare l'adresse 1 et 2 uniquement si le responsable a une adresse
            if (adresseResp != null && (adrCpr.compare(adresseResp, adresseEleve) == 0
                    || adrCpr.compare(adresseResp, adresseSecondaireEleve) == 0)) {
                return false;
            }
        }

        return true;
    }

    public Matiere getLv1() {
        return this.lv1;
    }

    public void setLv1(Matiere lv1) {
        this.lv1 = lv1;
    }

    public Matiere getLv2() {
        return this.lv2;
    }

    public void setLv2(Matiere lv2) {
        this.lv2 = lv2;
    }

    public Matiere getLv3() {
        return this.lv3;
    }

    public void setLv3(Matiere lv3) {
        this.lv3 = lv3;
    }

    public SortedSet<CollegeSecteur> getCollegeSecteurs() {
        return collegeSecteurs;
    }

    public void setCollegeSecteurs(SortedSet<CollegeSecteur> collegeSecteurs) {
        this.collegeSecteurs = collegeSecteurs;
    }

    public SortedSet<CollegeSecteurSimule> getCollegeSecteursSimules() {
        return collegeSecteursSimules;
    }

    public void setCollegeSecteursSimules(SortedSet<CollegeSecteurSimule> collegeSecteursSimules) {
        this.collegeSecteursSimules = collegeSecteursSimules;
    }

    public Boolean getPossedeHandicap() {
        return possedeHandicap;
    }

    public void setPossedeHandicap(Boolean possedeHandicap) {
        this.possedeHandicap = possedeHandicap;
    }

    public void addCollegeSecteur(EtablissementSconet etablissementSconet) {
        this.collegeSecteurs.add(new CollegeSecteur(this.numDossier, etablissementSconet));
    }

    public void addCollegeSecteurSimule(EtablissementSconet etablissementSconet) {
        this.collegeSecteursSimules.add(new CollegeSecteurSimule(this.numDossier, etablissementSconet));
    }

    /**
     * 
     * @return Date de dernière édition de la notification d''affectation pour le responsable.
     */
    public Date getDateEditNotif() {
        return dateEditNotif;
    }

    /**
     * 
     * @param dateEditNotif
     *            Date de dernière édition de la notification d''affectation pour le responsable.
     */
    public void setDateEditNotif(Date dateEditNotif) {
        this.dateEditNotif = dateEditNotif;
    }

    /**
     * @return Le nombre de collège de secteur potentiel ayant une prise
     *         en charge médicale.
     */
    public Integer getNbCollegeSecteurAvecPriseEnChargeMedicale() {
        int compteur = 0;
        if (CollectionUtils.isNotEmpty(collegeSecteurs)) {
            for (CollegeSecteur cs : collegeSecteurs) {
                if (cs.getPriseEnChargeMedical() != null && cs.getPriseEnChargeMedical()) {
                    compteur++;
                }
            }
        }
        return compteur;
    }

    public Formation getFormationDemandeSecteur() {
        return formationDemandeSecteur;
    }

    public void setFormationDemandeSecteur(Formation formationDemandeSecteur) {
        this.formationDemandeSecteur = formationDemandeSecteur;
    }

    public Boolean getEstCollegeSecteurSaisiParDsden() {
        return estCollegeSecteurSaisiParDsden;
    }

    public void setEstCollegeSecteurSaisiParDsden(Boolean estCollegeSecteurSaisiParDsden) {
        this.estCollegeSecteurSaisiParDsden = estCollegeSecteurSaisiParDsden;
    }

    public Boolean getEstScolarisationDansCollegeSecteur() {
        return estScolarisationDansCollegeSecteur;
    }

    public void setEstScolarisationDansCollegeSecteur(Boolean estScolarisationDansCollegeSecteur) {
        this.estScolarisationDansCollegeSecteur = estScolarisationDansCollegeSecteur;
    }

    public SortedSet<DemandeFamille> getDemandeFamilles() {
        return demandeFamilles;
    }

    public void setDemandeFamilles(SortedSet<DemandeFamille> demandeFamilles) {
        this.demandeFamilles = demandeFamilles;
    }

    public Boolean getEstAffectationDemandeeCollegePublicDept() {
        return estAffectationDemandeeCollegePublicDept;
    }

    public void setEstAffectationDemandeeCollegePublicDept(Boolean estAffectationDemandeeCollegePublicDept) {
        this.estAffectationDemandeeCollegePublicDept = estAffectationDemandeeCollegePublicDept;
    }

    public void addDemandeFamille(DemandeFamille demandeFamille) {
        if (demandeFamilles == null) {
            demandeFamilles = new TreeSet<DemandeFamille>();
        }
        if (demandeFamille != null) {
            demandeFamilles.add(demandeFamille);
        }
    }

    public void addDemandeAffectation(DemandeAffectation demande) {
        if (this.demandeAffectation == null) {
            this.demandeAffectation = new TreeSet<DemandeAffectation>(new DemandeAffectationComparatorRang());
        }

        this.demandeAffectation.add(demande);
    }

    /**
     * @return
     *         <ul>
     *         <li>vrai lorsque l'élève est en attente d'un établissement désigné par l'IA-DASEN pour son offre de
     *         secteur</li>
     *         <li>faux sinon</li>
     *         </ul>
     */
    public boolean getEstOffreDeSecteurADeterminer() {
        return BooleanUtils.isTrue(this.estCollegeSecteurDansDepartement)
                && (BooleanUtils.isFalse(this.estScolarisationDansCollegeSecteur)
                        || (this.formationDemandeSecteur != null && !StringUtils
                                .equals(this.formationDemandeSecteur.getCode(), Formation.CODE_MEF_6EME)))
                && BooleanUtils.isTrue(this.estAffectationDemandeeCollegePublicDept)
                && this.collegeSecteur == null;
    }

    /**
     * @return
     *         <ul>
     *         <li>vrai lorsque l'élève est en attente d'un établissement désigné par l'IA-DASEN pour ses
     *         demandes</li>
     *         <li>faux sinon</li>
     *         </ul>
     */
    public boolean getEstDemandeFamilleADeterminer() {
        return this.collegeSecteur == null && BooleanUtils.isTrue(this.estScolarisationDansCollegeSecteur);
    }
    
    /**
     * Indique si l'élève est dans un établissement privé
     * 
     * @return
     * 		TRUE si l'élève est dans un établissement privé, FALSE sinon
     */
    public boolean getEstElevePrive() {
    	// Si l'école d'origine est connue, on se base sur son secteur
    	EtablissementOrigine ecoleOrigine = this.getEcoleOrigine();
    	if (ecoleOrigine != null) {
    		return EtabUtils.isSecteurPrive(ecoleOrigine);
    	}
    	
    	// A défaut, on se base sur l'origine du dossier
    	return 
    			TypeOrigineDossier.ORIGINE_DOSSIER_IMPORT_PRIVE.equals(this.origineDossier) ||
    			TypeOrigineDossier.ORIGINE_DOSSIER_IMPORT_PRIVE_PRINCIPAL_COLLEGE.equals(this.origineDossier);
    }

    public Date getEditionAccuseReception() {
        return this.editionAccuseReception;
    }

    public void setEditionAccuseReception(Date editionAccuseReception) {
        this.editionAccuseReception = editionAccuseReception;
    }

    public Boolean getExisteAdresseATraiter() {
        return existeAdresseATraiter;
    }

    public void setExisteAdresseATraiter(Boolean existeAdresseATraiter) {
        this.existeAdresseATraiter = existeAdresseATraiter;
    }

    public EtatCollegeSecteur getEtatCollegeSecteur() {
        return etatCollegeSecteur;
    }

    public void setEtatCollegeSecteur(EtatCollegeSecteur etatCollegeSecteur) {
        this.etatCollegeSecteur = etatCollegeSecteur;
    }

    public EtatCollegeSecteur getEtatCollegeSecteurSimule() {
        return etatCollegeSecteurSimule;
    }

    public void setEtatCollegeSecteurSimule(EtatCollegeSecteur etatCollegeSecteurSimule) {
        this.etatCollegeSecteurSimule = etatCollegeSecteurSimule;
    }

    /**
     * @return the elevePersonnalisationsParcours
     */
    public Set<ElevePersonnalisationParcours> getElevePersonnalisationsParcours() {
        return elevePersonnalisationsParcours;
    }

    /**
     * @param elevePersonnalisationsParcours
     *            the elevePersonnalisationsParcours to set
     */
    public void setElevePersonnalisationsParcours(
            Set<ElevePersonnalisationParcours> elevePersonnalisationsParcours) {
        this.elevePersonnalisationsParcours = elevePersonnalisationsParcours;
    }

    /**
     * @return the roleMajCollegeSecteur
     */
    public Role getRoleMajCollegeSecteur() {
        return roleMajCollegeSecteur;
    }

    /**
     * @param roleMajCollegeSecteur
     *            the roleMajCollegeSecteur to set
     */
    public void setRoleMajCollegeSecteur(Role roleMajCollegeSecteur) {
        this.roleMajCollegeSecteur = roleMajCollegeSecteur;
    }

    /**
     * @return La liste des codes RNE des colleges de secteur potentiels
     *
     */
    public String[] getCodeRneCollegesSecteur() {
        int compteur = 0;
        String[] listeCodeRne = null;
        if (CollectionUtils.isNotEmpty(collegeSecteurs)) {
            listeCodeRne = new String[collegeSecteurs.size()];
            for (CollegeSecteur cs : collegeSecteurs) {
                String codeRne = cs.getEtablissementSconet().getCodeRne();
                listeCodeRne[compteur] = codeRne;
                compteur++;
            }
        }
        return listeCodeRne;
    }

    public String getCollegeSecteurCodes() {
        return collegeSecteurCodes;
    }

    public void setCollegeSecteurCodes(String collegeSecteurCodes) {
        this.collegeSecteurCodes = collegeSecteurCodes;
    }

    /**
     * @return the indicateurPcs
     */
    public IndicateurPCS getIndicateurPcs() {
        return indicateurPcs;
    }

    /**
     * @param indicateurPcs
     *            the indicateurPcs to set
     */
    public void setIndicateurPcs(IndicateurPCS indicateurPcs) {
        this.indicateurPcs = indicateurPcs;
    }

    /**
     * @return the adresseResidenceSimu
     */
    public Adresse getAdresseResidenceSimu() {
        return this.adresseResidenceSimu;
    }

    /**
     * @param adresseResidenceSimu
     *            the adresseResidenceSimu to set
     */
    public void setAdresseResidenceSimu(Adresse adresseResidenceSimu) {
        this.adresseResidenceSimu = adresseResidenceSimu;
    }

    /**
     * @return the causeCalculCollegeSecteur
     */
    public CauseCalculCollegeSecteur getCauseCalculCollegeSecteur() {
        return causeCalculCollegeSecteur;
    }

    /**
     * @param causeCalculCollegeSecteur
     *            the causeCalculCollegeSecteur to set
     */
    public void setCauseCalculCollegeSecteur(CauseCalculCollegeSecteur causeCalculCollegeSecteur) {
        this.causeCalculCollegeSecteur = causeCalculCollegeSecteur;
    }

    /**
     * @return the codeEtatExportBee
     */
    public String getCodeEtatExportBee() {
        return codeEtatExportBee;
    }

    /**
     * @param codeEtatExportBee
     *            the codeEtatExportBee to set
     */
    public void setCodeEtatExportBee(String codeEtatExportBee) {
        this.codeEtatExportBee = codeEtatExportBee;
    }

    /**
     * @return the etatExportBee
     */
    public EtatExportBee getEtatExportBee() {
        return etatExportBee;
    }

    /**
     * @param etatExportBee
     *            the etatExportBee to set
     */
    public void setEtatExportBee(EtatExportBee etatExportBee) {
        this.etatExportBee = etatExportBee;
    }

    /**
     * @return the echangeIdSiecleSynchroBee
     */
    public String getEchangeIdSiecleSynchroBee() {
        return echangeIdSiecleSynchroBee;
    }

    /**
     * @param echangeIdSiecleSynchroBee
     *            the echangeIdSiecleSynchroBee to set
     */
    public void setEchangeIdSiecleSynchroBee(String echangeIdSiecleSynchroBee) {
        this.echangeIdSiecleSynchroBee = echangeIdSiecleSynchroBee;
    }

    /**
     * @return the dateReceptionVolet2
     */
    public Date getDateReceptionVolet2() {
        return dateReceptionVolet2;
    }

    /**
     * @param dateReceptionVolet2
     *            the dateReceptionVolet2 to set
     */
    public void setDateReceptionVolet2(Date dateReceptionVolet2) {
        this.dateReceptionVolet2 = dateReceptionVolet2;
    }

    /**
     * @return the dateEnvoiNotif
     */
    public Date getDateEnvoiNotif() {
        return dateEnvoiNotif;
    }

    /**
     * @param dateEnvoiNotif
     *            the dateEnvoiNotif to set
     */
    public void setDateEnvoiNotif(Date dateEnvoiNotif) {
        this.dateEnvoiNotif = dateEnvoiNotif;
    }

    public Date getDateRestoDemandeFamille() {
		return dateRestoDemandeFamille;
	}

	public void setDateRestoDemandeFamille(Date dateRestoDemandeFamille) {
		this.dateRestoDemandeFamille = dateRestoDemandeFamille;
	}
	
	public boolean hasRestoDemandeFamille() {
		return this.dateRestoDemandeFamille != null;
	}

	/**
     * @return the circonscription
     */
    public String getCirconscription() {
        if (interceptor != null) {
            return (String) interceptor.readObject(this, "circonscription", circonscription);
        }

        return circonscription;
    }

    /**
     * @param circonscription
     *            the circonscription to set
     */
    public void setCirconscription(String circonscription) {
        if (interceptor != null) {
            this.circonscription = (String) interceptor.writeObject(this, "circonscription", this.circonscription,
                    circonscription);
        } else {
            this.circonscription = circonscription;
        }
    }

    /**
     * Donne la liste des adresses des responsables avec un distinct sur l'id adresse.
     * 
     * @return la liste des adresses des responsables de l'élève
     */
    public Set<Adresse> getAdressesResponsables() {
        Set<Adresse> adresses = new HashSet<>();

        if (CollectionUtils.isNotEmpty(getResponsables())) {
            adresses = getResponsables().stream().filter(r -> r.getAdresseResponsable() != null)
                    .map(Responsable::getAdresseResponsable).collect(Collectors.toCollection(
                            () -> new TreeSet<>((a1, a2) -> a1.getAdrId().compareTo(a2.getAdrId()))));
        }

        return adresses;
    }

    @Override
    public PersistentAttributeInterceptor $$_hibernate_getInterceptor() {
        return interceptor;
    }

    @Override
    public void $$_hibernate_setInterceptor(PersistentAttributeInterceptor interceptor) {
        this.interceptor = interceptor;
    }
}
