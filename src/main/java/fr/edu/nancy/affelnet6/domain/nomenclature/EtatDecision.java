/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.nomenclature;

import java.util.HashSet;
import java.util.Set;

import fr.edu.nancy.affelnet6.domain.dossier.DemandeAffectation;
import fr.edu.nancy.affelnet6.domain.dossier.Eleve;

/** Objet métier pour modéliser les états de décision. */
public class EtatDecision implements java.io.Serializable {

    /** serialVersionUID. */
    public static final long serialVersionUID = -3798877593939245974L;

    // Codes d'états des demandes d'affectation

    /** La demande est prise de façon automatique. */
    public static final String CODE_PRIS_AUTO = "PA";

    /** La demande est prise de façon manuelle. */
    public static final String CODE_PRIS_MANUEL = "PM";

    /** La demande est en attente. */
    public static final String CODE_ATTENTE = "AT";

    /** La demande n'est pas enode traitée. */
    public static final String CODE_NON_TRAITE = "NT";

    /** La demande est refusée automatiquement. */
    public static final String CODE_REFUSE_AUTO = "RA";

    /** La demande est refusée manuellement. */
    public static final String CODE_REFUSE_MANUEL = "RM";

    /** L'élève quitte l'offre (pris ailleurs). */
    public static final String CODE_QUITTE_OFFRE = "QO";

    /** La demande qui ne participe pas à l'affectation. */
    public static final String CODE_HORS_AFFECTATION = "HA";

    // Code d'états finaux

    /** Code des états finaux pour une demande acceptée (affectation). */
    public static final String CODE_ETAT_FINAL_ACCEPTE = "A";

    /** Code des états finaux pour une demande refusée (refus). */
    public static final String CODE_ETAT_FINAL_REFUSE = "R";

    private String code;
    private String libelleInterne;
    private String libelleDecision;
    private String codeEtatFinal;
    private String libelleEtat;
    private Boolean flagDecisionManu;
    private Boolean flagFinal;
    private Set<Eleve> eleves = new HashSet<Eleve>(0);
    private Set<DemandeAffectation> demandeAffectation = new HashSet<DemandeAffectation>(0);

    public EtatDecision() {
    }

    public EtatDecision(String libelleInterne, String libelleEtat, Boolean flagDecisionManu, Boolean flagFinal) {
        this.libelleInterne = libelleInterne;
        this.libelleEtat = libelleEtat;
        this.flagDecisionManu = flagDecisionManu;
        this.flagFinal = flagFinal;
    }

    public EtatDecision(String libelleInterne, String libelleDecision, String codeEtatFinal, String libelleEtat,
            Boolean flagDecisionManu, Boolean flagFinal, Set<Eleve> eleves,
            Set<DemandeAffectation> demandeAffectation) {
        this.libelleInterne = libelleInterne;
        this.libelleDecision = libelleDecision;
        this.codeEtatFinal = codeEtatFinal;
        this.libelleEtat = libelleEtat;
        this.flagDecisionManu = flagDecisionManu;
        this.flagFinal = flagFinal;
        this.eleves = eleves;
        this.demandeAffectation = demandeAffectation;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLibelleInterne() {
        return this.libelleInterne;
    }

    public void setLibelleInterne(String libelleInterne) {
        this.libelleInterne = libelleInterne;
    }

    public String getLibelleDecision() {
        return this.libelleDecision;
    }

    public void setLibelleDecision(String libelleDecision) {
        this.libelleDecision = libelleDecision;
    }

    public String getCodeEtatFinal() {
        return this.codeEtatFinal;
    }

    public void setCodeEtatFinal(String codeEtatFinal) {
        this.codeEtatFinal = codeEtatFinal;
    }

    public String getLibelleEtat() {
        return this.libelleEtat;
    }

    public void setLibelleEtat(String libelleEtat) {
        this.libelleEtat = libelleEtat;
    }

    public Boolean getFlagDecisionManu() {
        return this.flagDecisionManu;
    }

    public void setFlagDecisionManu(Boolean flagDecisionManu) {
        this.flagDecisionManu = flagDecisionManu;
    }

    public Boolean getFlagFinal() {
        return this.flagFinal;
    }

    public void setFlagFinal(Boolean flagFinal) {
        this.flagFinal = flagFinal;
    }

    public Set<Eleve> getEleves() {
        return this.eleves;
    }

    public void setEleves(Set<Eleve> eleves) {
        this.eleves = eleves;
    }

    public Set<DemandeAffectation> getdemandeAffectation() {
        return this.demandeAffectation;
    }

    public void setdemandeAffectation(Set<DemandeAffectation> demandeAffectation) {
        this.demandeAffectation = demandeAffectation;
    }

    /**
     * Teste si la demande est prise (manuellement ou automatiquement).
     * 
     * @return vrai si la décision est à 'pris.
     */
    public boolean isDecisionPris() {

        // Pris automatiquement ou manuel.
        return EtatDecision.CODE_PRIS_AUTO.equals(code) || EtatDecision.CODE_PRIS_MANUEL.equals(code);
    }

    /**
     * Teste si une décision manuelle a été prise sur la demande.
     * 
     * @return vrai si une décision manuelle a été prise
     */
    public boolean isDecisionManuelle() {

        // Pris manuel ou refusé manuel
        return EtatDecision.CODE_PRIS_MANUEL.equals(code) || EtatDecision.CODE_REFUSE_MANUEL.equals(code);
    }

    /**
     * Teste si la demande est prise manuellement.
     * 
     * @return vrai si la décision est à 'pris manuellement'.
     */
    public boolean isDecisionManuellePris() {

        // Pris manuel.
        return EtatDecision.CODE_PRIS_MANUEL.equals(code);
    }

    /**
     * Teste si la demande est refusée manuellement.
     * 
     * @return vrai si la décision est à 'refusé manuellement'.
     */
    public boolean isDecisionManuelleRefuse() {

        // Refusé manuel.
        return EtatDecision.CODE_REFUSE_MANUEL.equals(code);
    }

    /**
     * Teste si la demande n'est pas déterminée.
     * 
     * @return vrai si la décision n'est pas déterminée.
     */
    public boolean isDecisionNonDeterminee() {

        // En attente ou non-traité
        return EtatDecision.CODE_ATTENTE.equals(code) || EtatDecision.CODE_NON_TRAITE.equals(code);
    }

    /**
     * Teste si la demande n'est pas prise.
     * 
     * @return vrai si la décision n'est pas prise (refusé ou quitte l'offre).
     */
    public boolean isDecisionNonPris() {

        // Refusé manuellement (RM)
        // ou Refusé automatiquement (RA)
        // ou Quitte l'offre / pris ailleurs (QO)
        return EtatDecision.CODE_REFUSE_MANUEL.equals(code) || EtatDecision.CODE_REFUSE_AUTO.equals(code)
                || EtatDecision.CODE_QUITTE_OFFRE.equals(code);
    }

    /** @return Teste si l'élève est pris sur une autre demande (quitte l'offre). */
    public boolean isDecisionPrisAilleurs() {
        return EtatDecision.CODE_QUITTE_OFFRE.equals(code);
    }

}
