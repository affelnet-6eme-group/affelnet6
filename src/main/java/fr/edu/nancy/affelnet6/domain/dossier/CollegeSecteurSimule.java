/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.dossier;

import java.io.Serializable;

import fr.edu.nancy.affelnet6.domain.nomenclature.EtablissementSconet;

/**
 * Collège de secteur d'un élève issu de la simulation de détermination automatique.
 * 
 * 
 *
 */
public class CollegeSecteurSimule implements Serializable {

    /** Tag de sérialisation. */
    private static final long serialVersionUID = 1L;
    /** La clé primaire. */
    private CollegeSecteurSimulePK id;
    /** L'élève. */
    private Eleve eleve;
    /** L'établissement. */
    private EtablissementSconet etablissementSconet;
    /** l'id d'établissement. */
    private Long etabId;

    /** Constructeur par défaut. */
    public CollegeSecteurSimule() {
    }

    /**
     * Constructeur.
     * 
     * @param numDossier
     *            le numéro de dossier de l'élève
     * @param etablissementSconet
     *            l'établissement
     */
    public CollegeSecteurSimule(Long numDossier, EtablissementSconet etablissementSconet) {
        if (etablissementSconet != null) {
            this.id = new CollegeSecteurSimulePK(numDossier, etablissementSconet.getEtabId().longValue());
            setEtablissementSconet(etablissementSconet);
        }
    }

    /**
     * @return the id
     */
    public CollegeSecteurSimulePK getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(CollegeSecteurSimulePK id) {
        this.id = id;
    }

    /**
     * @return the eleve
     */
    public Eleve getEleve() {
        return eleve;
    }

    /**
     * @param eleve
     *            the eleve to set
     */
    public void setEleve(Eleve eleve) {
        this.eleve = eleve;
    }

    /**
     * @return the etablissementSconet
     */
    public EtablissementSconet getEtablissementSconet() {
        return etablissementSconet;
    }

    /**
     * @param etablissementSconet
     *            the etablissementSconet to set
     */
    public void setEtablissementSconet(EtablissementSconet etablissementSconet) {
        this.etablissementSconet = etablissementSconet;
    }

    /**
     * @return the etabId
     */
    public Long getEtabId() {
        return etabId;
    }

    /**
     * @param etabId
     *            the etabId to set
     */
    public void setEtabId(Long etabId) {
        this.etabId = etabId;
    }

}
