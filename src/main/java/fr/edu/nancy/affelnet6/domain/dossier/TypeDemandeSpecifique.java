/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.dossier;

/**
 * Liste des types de demande spécifique.
 */
public enum TypeDemandeSpecifique {

    /** Proposition d'affectation pour les SEGPA. */
    PROPOSITION("P"),
    /** Demande banalisée pour les SEGPA (pas de collège défini). */
    BANALISEE("B"),
    /** Demande normale. */
    NORMALE("D");

    /** Code du type de demande spécifique. */
    private final String code;

    /**
     * Constructeur.
     * 
     * @param code
     *            Code du type de demande spécifique.
     */
    private TypeDemandeSpecifique(String code) {
        this.code = code;
    }

    /**
     * Donne le code du type de demande spécifique.
     * 
     * @return Code du type de demande spécifique.
     */
    public String getCode() {
        return code;
    }

}
