/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.nomenclature;

/**
 * MotifDerog generated by hbm2java.
 */
public class MotifDerog implements java.io.Serializable, java.lang.Comparable<MotifDerog> {

    /** Priorité pour une demande de secteur. */
    public static final int PRIORITE_SECTEUR = 0;

    /** Borne inférieure des priorités de demandes. */
    public static final int PRIORITE_MINIMALE = 99999;

    /** Taille maximale pour les libellés longs des motifs de dérogation. */
    public static final int TAILLE_MAX_LIBELLE_LONG = 110;

    /** Tag de sérialisation. */
    private static final long serialVersionUID = -7353756056556513276L;

    private Integer idMtfDrg;
    private DeptMen dept;
    private String code;
    private Boolean national;
    private String libelleCourt;
    private String libelleLong;
    private Integer nivPrioDef;
    private Integer nivPrio;

    public MotifDerog() {
    }

    public MotifDerog(DeptMen dept, String code, Boolean national, String libelleCourt, String libelleLong,
            Integer nivPrioDef, Integer nivPrio) {
        this.dept = dept;
        this.code = code;
        this.national = national;
        this.libelleCourt = libelleCourt;
        this.libelleLong = libelleLong;
        this.nivPrioDef = nivPrioDef;
        this.nivPrio = nivPrio;
    }

    public Integer getIdMtfDrg() {
        return this.idMtfDrg;
    }

    public void setIdMtfDrg(Integer idMtfDrg) {
        this.idMtfDrg = idMtfDrg;
    }

    public DeptMen getDept() {
        return this.dept;
    }

    public void setDept(DeptMen dept) {
        this.dept = dept;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Boolean getNational() {
        return this.national;
    }

    public void setNational(Boolean national) {
        this.national = national;
    }

    public String getLibelleCourt() {
        return this.libelleCourt;
    }

    public void setLibelleCourt(String libelleCourt) {
        this.libelleCourt = libelleCourt;
    }

    public String getLibelleLong() {
        return this.libelleLong;
    }

    public void setLibelleLong(String libelleLong) {
        this.libelleLong = libelleLong;
    }

    public Integer getNivPrioDef() {
        return this.nivPrioDef;
    }

    public void setNivPrioDef(Integer nivPrioDef) {
        this.nivPrioDef = nivPrioDef;
    }

    public Integer getNivPrio() {
        return this.nivPrio;
    }

    public void setNivPrio(Integer nivPrio) {
        this.nivPrio = nivPrio;
    }

    @Override
    public int compareTo(MotifDerog o) {
        int nombre1 = o.getNivPrio();
        int nombre2 = this.getNivPrio();
        if (nombre1 > nombre2) {
            return -1;
        } else if (nombre1 == nombre2) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (!(o instanceof MotifDerog)) {
            return false;
        }
        MotifDerog motifDerog = (MotifDerog) o;
        if (getIdMtfDrg() == null) {
            return false;
        } else {
            return (getIdMtfDrg().compareTo(motifDerog.getIdMtfDrg()) == 0);
        }
    }

    @Override
    public int hashCode() {
        int result = 17;

        result = 37 * result + (getIdMtfDrg() == null ? 0 : getIdMtfDrg().hashCode());

        return result;
    }

}
