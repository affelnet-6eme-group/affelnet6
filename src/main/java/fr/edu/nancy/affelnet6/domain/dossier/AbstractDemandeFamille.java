/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.dossier;

import java.util.SortedSet;
import java.util.TreeSet;

import fr.edu.nancy.affelnet6.domain.nomenclature.EtablissementSconet;
import fr.edu.nancy.affelnet6.domain.nomenclature.MotifDerog;

/**
 * Classe abstraite pour les demandes des familles et les sauvegardes.
 */
public abstract class AbstractDemandeFamille {

    /** Identifiant technique de la demande de la famille. */
    protected Long id;

    /** Elève concerné par la demande. */
    protected Eleve eleve;

    /** Rang de la demande de la famille. */
    protected short rang;

    /** Etablissement lié à la demande de la famille. */
    protected EtablissementSconet etablissement;

    /** Motifs de dérogation pour la demande de la famille. */
    protected SortedSet<MotifDerog> motifs = new TreeSet<>();

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the eleve
     */
    public Eleve getEleve() {
        return eleve;
    }

    /**
     * @param eleve
     *            the eleve to set
     */
    public void setEleve(Eleve eleve) {
        this.eleve = eleve;
    }

    /**
     * @return the rang
     */
    public short getRang() {
        return rang;
    }

    /**
     * @param rang
     *            the rang to set
     */
    public void setRang(short rang) {
        this.rang = rang;
    }

    /**
     * @return the etablissement
     */
    public EtablissementSconet getEtablissement() {
        return etablissement;
    }

    /**
     * @param etablissement
     *            the etablissement to set
     */
    public void setEtablissement(EtablissementSconet etablissement) {
        this.etablissement = etablissement;
    }

    /**
     * @return the motifs
     */
    public SortedSet<MotifDerog> getMotifs() {
        return motifs;
    }

    /**
     * @param motifs
     *            the motifs to set
     */
    public void setMotifs(SortedSet<MotifDerog> motifs) {
        this.motifs = motifs;
    }

    /**
     * Ajoute un motif de dérogation.
     * 
     * @param motifDerog
     *            le motif de dérogation à ajouter
     */
    public void ajouteMotifDerog(MotifDerog motifDerog) {
        if (motifs == null) {
            motifs = new TreeSet<>();
        }
        if (motifDerog != null) {
            motifs.add(motifDerog);
        }
    }
}
