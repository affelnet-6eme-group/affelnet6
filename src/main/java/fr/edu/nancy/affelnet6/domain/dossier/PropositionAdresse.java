/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.dossier;

import java.io.Serializable;

import fr.edu.nancy.affelnet6.domain.rnvp.PropositionRNVP;

/**
 * Une proposition d'adresse transmise par le service RNVP.
 */
public class PropositionAdresse extends PropositionRNVP implements Serializable {

    /** */
    private static final long serialVersionUID = -7347918061607132023L;

    /** Adresse concernée par la proposition. */
    private Adresse adresse;

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    @Override
    public int hashCode() {
        return super.hashCode() + this.adresse.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        PropositionAdresse other = (PropositionAdresse) obj;

        if (!super.equals(obj)) {
            return super.equals(obj);
        }

        return (adresse.compareTo(other.adresse) == 0);
    }
}
