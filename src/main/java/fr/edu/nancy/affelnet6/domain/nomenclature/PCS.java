/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.nomenclature;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/** Catégorie socio-professionnelle. */
public class PCS implements Serializable {

    /** Code la PCS 'non renseignée (inconnue ou sans objet). */
    public static final String CODE_NON_RENSEIGNEE = "99";
    /** Code de la PCS Agriculture . */
    public static final String CODE_AGRI = "10";
    /** Code de la PCS Agriculture (petite exploitation). */
    public static final String CODE_AGRI_PETITE_EXPLOITATION = "11";
    /** Code de la PCS Agriculture (moyenne exploitation). */
    public static final String CODE_AGRI_MOYENNE_EXPLOITATION = "12";
    /** Code de la PCS Agriculture (grande exploitation). */
    public static final String CODE_AGRI_GRANDE_EXPLOITATION = "13";
    /** Liste des codes agricultures qui doivent disparaître pour être remplacée par le code 'CODE_AGRI'. */
    public static final List<String> CODES_AGRI = Collections.unmodifiableList(Arrays
            .asList(CODE_AGRI_PETITE_EXPLOITATION, CODE_AGRI_MOYENNE_EXPLOITATION, CODE_AGRI_GRANDE_EXPLOITATION));

    /** Tag de sérialisation. */
    private static final long serialVersionUID = 1L;

    /** Identifiant. */
    private Integer id;

    /** Code. */
    private String code;

    /** Libellé court. */
    private String libelleCourt;

    /** Libellé long. */
    private String libelleLong;

    /** Libellé édition. */
    private String libelleEdition;

    /** Indicateur PCS. */
    private IndicateurPCS indicateur;

    /** Date d'ouverture. */
    private Date dateOuverture;

    /** Date de fermeture. */
    private Date dateFermeture;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the libelleCourt
     */
    public String getLibelleCourt() {
        return libelleCourt;
    }

    /**
     * @param libelleCourt
     *            the libelleCourt to set
     */
    public void setLibelleCourt(String libelleCourt) {
        this.libelleCourt = libelleCourt;
    }

    /**
     * @return the libelleLong
     */
    public String getLibelleLong() {
        return libelleLong;
    }

    /**
     * @param libelleLong
     *            the libelleLong to set
     */
    public void setLibelleLong(String libelleLong) {
        this.libelleLong = libelleLong;
    }

    /**
     * @return the libelleEdition
     */
    public String getLibelleEdition() {
        return libelleEdition;
    }

    /**
     * @param libelleEdition
     *            the libelleEdition to set
     */
    public void setLibelleEdition(String libelleEdition) {
        this.libelleEdition = libelleEdition;
    }

    /**
     * @return the dateOuverture
     */
    public Date getDateOuverture() {
        return dateOuverture;
    }

    /**
     * @param dateOuverture
     *            the dateOuverture to set
     */
    public void setDateOuverture(Date dateOuverture) {
        this.dateOuverture = dateOuverture;
    }

    /**
     * @return the dateFermeture
     */
    public Date getDateFermeture() {
        return dateFermeture;
    }

    /**
     * @param dateFermeture
     *            the dateFermeture to set
     */
    public void setDateFermeture(Date dateFermeture) {
        this.dateFermeture = dateFermeture;
    }

    /**
     * @return the indicateur
     */
    public IndicateurPCS getIndicateur() {
        return indicateur;
    }

    /**
     * @param indicateur
     *            the indicateur to set
     */
    public void setIndicateur(IndicateurPCS indicateur) {
        this.indicateur = indicateur;
    }

}
