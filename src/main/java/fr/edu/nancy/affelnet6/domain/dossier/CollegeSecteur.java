/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.dossier;

import fr.edu.nancy.affelnet6.domain.nomenclature.EtablissementSconet;

public class CollegeSecteur {

    private CollegeSecteurPK id;
    private Integer numeroOrdre;
    private Boolean priseEnChargeMedical = false;
    private Eleve eleve;
    private EtablissementSconet etablissementSconet;
    private Long etabId;

    public CollegeSecteur() {
    }

    public CollegeSecteur(Long numDossier, EtablissementSconet etablissementSconet) {
        if (etablissementSconet != null) {
            this.id = new CollegeSecteurPK(numDossier, etablissementSconet.getEtabId().longValue());
            setEtablissementSconet(etablissementSconet);
        }
    }

    public CollegeSecteurPK getId() {
        return id;
    }

    public void setId(CollegeSecteurPK id) {
        this.id = id;
    }

    public Integer getNumeroOrdre() {
        return numeroOrdre;
    }

    public void setNumeroOrdre(Integer numeroOrdre) {
        this.numeroOrdre = numeroOrdre;
    }

    public Boolean getPriseEnChargeMedical() {
        return priseEnChargeMedical;
    }

    public void setPriseEnChargeMedical(Boolean priseEnChargeMedical) {
        this.priseEnChargeMedical = priseEnChargeMedical;
    }

    public Eleve getEleve() {
        return eleve;
    }

    public void setEleve(Eleve eleve) {
        this.eleve = eleve;
    }

    public EtablissementSconet getEtablissementSconet() {
        return etablissementSconet;
    }

    public void setEtablissementSconet(EtablissementSconet etablissementSconet) {
        this.etablissementSconet = etablissementSconet;
    }

    public Long getEtabId() {
        return etabId;
    }

    public void setEtabId(Long etabId) {
        this.etabId = etabId;
    }

}
