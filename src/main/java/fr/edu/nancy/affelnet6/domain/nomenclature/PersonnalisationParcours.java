/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.nomenclature;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Dispositifs de personnalisation des parcours proposés aux élèves.
 */
public class PersonnalisationParcours implements Serializable, Comparable<PersonnalisationParcours> {

    /** Tag de sérialisation. */
    private static final long serialVersionUID = 5237483501354459L;

    /** Code. */
    private String code;

    /** Libellé court. */
    private String libelleCourt;

    /** Libellé long. */
    private String libelleLong;

    /** Libellé édition. */
    private String libelleEdition;

    /**
     * Degré(s) : valeurs 1D (1er degré uniquement), 2D (2nd degré uniquement) ou 1D2D (valable pour les deux
     * degrés).
     */
    private String usage;

    /** Reconductible d'une année à l'autre. */
    private boolean reconductible;

    /** Date d'ouverture. */
    private Date dateOuverture;

    /** Date de fermeture. */
    private Date dateFermeture;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the libelleCourt
     */
    public String getLibelleCourt() {
        return libelleCourt;
    }

    /**
     * @param libelleCourt
     *            the libelleCourt to set
     */
    public void setLibelleCourt(String libelleCourt) {
        this.libelleCourt = libelleCourt;
    }

    /**
     * @return the libelleLong
     */
    public String getLibelleLong() {
        return libelleLong;
    }

    /**
     * @param libelleLong
     *            the libelleLong to set
     */
    public void setLibelleLong(String libelleLong) {
        this.libelleLong = libelleLong;
    }

    /**
     * @return the libelleEdition
     */
    public String getLibelleEdition() {
        return libelleEdition;
    }

    /**
     * @param libelleEdition
     *            the libelleEdition to set
     */
    public void setLibelleEdition(String libelleEdition) {
        this.libelleEdition = libelleEdition;
    }

    /**
     * @return the usage
     */
    public String getUsage() {
        return usage;
    }

    /**
     * @param usage
     *            the usage to set
     */
    public void setUsage(String usage) {
        this.usage = usage;
    }

    /**
     * @return the reconductible
     */
    public boolean isReconductible() {
        return reconductible;
    }

    /**
     * @param reconductible
     *            the reconductible to set
     */
    public void setReconductible(boolean reconductible) {
        this.reconductible = reconductible;
    }

    /**
     * @return the dateOuverture
     */
    public Date getDateOuverture() {
        return dateOuverture;
    }

    /**
     * @param dateOuverture
     *            the dateOuverture to set
     */
    public void setDateOuverture(Date dateOuverture) {
        this.dateOuverture = dateOuverture;
    }

    /**
     * @return the dateFermeture
     */
    public Date getDateFermeture() {
        return dateFermeture;
    }

    /**
     * @param dateFermeture
     *            the dateFermeture to set
     */
    public void setDateFermeture(Date dateFermeture) {
        this.dateFermeture = dateFermeture;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        if (obj.getClass() != getClass()) {
            return false;
        }

        PersonnalisationParcours other = (PersonnalisationParcours) obj;

        return new EqualsBuilder().append(getCode(), other.getCode()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getCode()).toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append(getCode()).append(getLibelleLong()).toString();
    }

    @Override
    public int compareTo(PersonnalisationParcours o) {
        return new CompareToBuilder().append(getCode(), o.getCode()).toComparison();
    }
}
