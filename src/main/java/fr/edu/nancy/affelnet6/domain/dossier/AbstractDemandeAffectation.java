/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.dossier;

import fr.edu.nancy.affelnet6.domain.nomenclature.DeptMen;
import fr.edu.nancy.affelnet6.domain.nomenclature.EtatDecision;
import fr.edu.nancy.affelnet6.domain.nomenclature.Formation;
import fr.edu.nancy.affelnet6.domain.nomenclature.OffreFormation;
import fr.edu.nancy.affelnet6.domain.nomenclature.TypeDemandeAffec;

/**
 * Classe abstraite pour les demandes d'affectation et les sauvegardes.
 */
public abstract class AbstractDemandeAffectation {

    /**
     * id de la demande.
     */
    protected Long id;

    /**
     * numéro dossier élève.
     */
    // TODO renommer cet attribut en eleve (et modifier mapping Hibernate)
    protected Eleve numDossier;

    /**
     * état de la demande.
     */
    protected EtatDecision etatDemande;

    /**
     * type de la demande.
     */
    protected TypeDemandeAffec typeDemande;

    /**
     * département MEN.
     */
    protected DeptMen dept;

    /**
     * offre de formation.
     */

    protected OffreFormation offreformation;

    /**
     * rang de la demande.
     */
    protected short rang;

    /**
     * indicateur de type de demande spécifique.
     */
    protected String indicateurTypeDemandeSpecifique;

    /** La formation demandée. */
    protected Formation formation;

    /**
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the numDossier
     */
    // TODO renommer cette méthode en getEleve()
    public Eleve getNumDossier() {
        return this.numDossier;
    }

    /** @return élève associé à la demande */
    public Eleve getEleve() {
        return this.numDossier;
    }

    /**
     * @param numDossier
     *            the numDossier to set
     */
    // TODO renommer cette méthode en setEleve()
    public void setNumDossier(Eleve numDossier) {
        this.numDossier = numDossier;
    }

    /**
     * @return the etatDemande
     */
    public EtatDecision getEtatDemande() {
        return this.etatDemande;
    }

    /**
     * @param etatDemande
     *            the etatDemande to set
     */
    public void setEtatDemande(EtatDecision etatDemande) {
        this.etatDemande = etatDemande;
    }

    /**
     * @return the typeDemande
     */
    public TypeDemandeAffec getTypeDemande() {
        return this.typeDemande;
    }

    /**
     * @param typeDemande
     *            the typeDemande to set
     */
    public void setTypeDemande(TypeDemandeAffec typeDemande) {
        this.typeDemande = typeDemande;
    }

    /**
     * @return the dept
     */
    public DeptMen getDept() {
        return this.dept;
    }

    /**
     * @param dept
     *            the dept to set
     */
    public void setDept(DeptMen dept) {
        this.dept = dept;
    }

    /**
     * @return the offreformation
     */
    public OffreFormation getOffreformation() {
        return this.offreformation;
    }

    /**
     * @param offreformation
     *            the offreformation to set
     */
    public void setOffreformation(OffreFormation offreformation) {
        this.offreformation = offreformation;
    }

    /**
     * @return the rang
     */
    public short getRang() {
        return this.rang;
    }

    /**
     * @param rang
     *            the rang to set
     */
    public void setRang(short rang) {
        this.rang = rang;
    }

    /**
     * @param rang
     *            the rang to set
     */
    public void setRang(int rang) {
        this.rang = (short) rang;
    }

    /**
     * @return the indicateurTypeDemandeSpecifique
     */
    public String getIndicateurTypeDemandeSpecifique() {
        return this.indicateurTypeDemandeSpecifique;
    }

    /**
     * @param indicateurTypeDemandeSpecifique
     *            the indicateurTypeDemandeSpecifique to set
     */
    public void setIndicateurTypeDemandeSpecifique(String indicateurTypeDemandeSpecifique) {
        this.indicateurTypeDemandeSpecifique = indicateurTypeDemandeSpecifique;
    }

    /**
     * @return the formation
     */
    public Formation getFormation() {
        return formation;
    }

    /**
     * @param formation
     *            the formation to set
     */
    public void setFormation(Formation formation) {
        this.formation = formation;
    }

    /**
     * Donne une chaîne de synthèse de la demande.
     * 
     * @return chaine de synthèse
     */
    public String toGuruString() {

        // mise en commentaire car l'appel n'est plus statique et ça introduit une
        // dépendance non souhaitée (un DTO qui dépend de la couche service)
        // int bestPri = DemandeAffectationManagerImpl.niveauPrioritePlusHaute(this);

        String gs = "(" + id + ", ";
        gs += "dep:" + ((dept == null) ? "NULL" : dept.getDeptCode()) + ", ";
        gs += "elv:" + ((numDossier == null) ? "NULL" : numDossier.getNumDossier()) + ", ";
        gs += "t:" + ((typeDemande == null) ? "NULL" : typeDemande.getCode()) + ", ";
        gs += "of:" + ((offreformation == null) ? "NULL" : offreformation.getCod()) + ", ";
        gs += "rg:" + rang + ", ";
        // gs+= "pri:"+bestPri+", ";
        gs += "dec:" + ((etatDemande == null) ? "NULL" : etatDemande.getCode()) + ")";
        return gs;
    }

    /**
     * Donne une chaîne de affichable décrivant la demande.
     * 
     * @return chaine de affichable
     */
    public String toPrettyString() {
        // mise en commentaire car l'appel n'est plus statique et ça introduit une
        // dépendance non souhaitée (un DTO qui dépend de la couche service)
        // int bestPri = DemandeAffectationManagerImpl.niveauPrioritePlusHaute(this);

        StringBuffer prettyString = new StringBuffer();
        prettyString.append("de l'élève ");
        prettyString.append(numDossier.getNom());
        prettyString.append(" ");
        prettyString.append(numDossier.getPrenom());
        prettyString.append(" sur l'offre ");
        prettyString.append(offreformation.getCod());
        prettyString.append(" de rang ");
        prettyString.append(rang);
        // +" et de priorité "+bestPri;

        return prettyString.toString();
    }

    /**
     * Teste si une demande ne concerne pas un collège public du département.
     * 
     * @return vrai si la demande ne concerne pas un collège public du département
     */
    public boolean isHorsPublicDepartmental() {
        return TypeDemandeAffec.CODE_TYPE_DEMANDE_HORS_COLLEGE_PUBLIC_DEPT.equals(typeDemande.getCode());
    }

    /**
     * Teste si une demande concerne une SEGPA.
     * 
     * @return vrai si la demande concerne une SEGPA
     */
    public boolean isDemandeSegpa() {
        return TypeDemandeAffec.CODE_TYPE_DEMANDE_SEGPA.equals(typeDemande.getCode());
    }
}
