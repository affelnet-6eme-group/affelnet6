/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.nomenclature;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import fr.edu.nancy.affelnet6.domain.dossier.DemandeAffectation;

/**
 * Modélise une offre de formation pour l'affectation en 6ème.
 * 
 * Une offre de formation représente, pour un département MEN,
 * l'association entre un établissement et un formation.
 * 
 * Un code lui est attibué à la création par le gestionnaire.
 * On lui associe une capacité d'accueil.
 * 
 * Un certain nombre de compteurs sont également ajoutés dans l'offre.
 */
public class OffreFormation implements java.io.Serializable {

    /** Tag de sérialisation. */
    private static final long serialVersionUID = 9173650118129792205L;

    /** Code de l'offre de formation. */
    private String cod;

    /** Département MEN de rattachement dans lequel l'offre est proposée. */
    private DeptMen deptMen;

    /** Formation d'accueil. */
    private Formation formation;

    /** Etablissement d'accueil (Sconet). */
    private EtablissementSconet etablissementSconet;

    /** Capacité d'accueil (nombre d'élèves pouvant être accueillis dans l'établissement pour la formation). */
    private Integer capAcc;

    /**
     * Compteur d'affectation : Nombre de demandes de secteur satisfaites.
     * Théoriquement égal au nombre de demandes de secteur (nbDemandesSecteur), sauf cas particulier.
     */
    private Integer nbrAffDemSct;

    /** Compteur d'affectation : Nombre de demandes de dérogations affectés sur cette offre. */
    private Integer nbrAffDrg;

    /** Compteur d'affectation : Nombre de demandes acceptées manuellement (commission) sur cette offre. */
    private Integer nbrAffMan;

    /**
     * Compteur d'affectation : Nombre d'élèves affectés sur l'offre (uniquement ceux sur leur collège de secteur).
     */
    private Integer nbrAffSct;

    /**
     * Compteur d'affectation : Nombre de demandes de dérogation en attente sur cette offre.
     * L'attente concerne une décision externe à cette offre).
     */
    private Integer nbrAttDrg;

    /**
     * Compteur d'affectation : Nombre de demandes de secteur en attente (d'une décision dans une autre offre).
     * Cela indique les élèves pouvant encore revenir sur le secteur et pour lesquels il faut conserver une place.
     */
    private Integer nbrAttSct;

    /**
     * Nombre de demandes dérogatoires.
     * Compteur de demande : Nombre d'élèves ayant demandé cette offre
     * uniquement dans le cadre d'une demande de dérogation.
     */
    private Integer nbrDemDrg;

    /**
     * Nombre maximum d'élèves de secteur.
     * Compteur de demande : Nombre maximum des élèves de secteur déterminée d'après les dossiers.
     * C'est le nombre d'élèves pouvant potentiellement être affectés sur cette offre
     * s'ils ne fesaient que des voeux de secteur (théoriquement = capacité d'accueil).
     */
    private Integer nbrDemMaxSct;

    /**
     * Nombre de demandes du secteur.
     * Compteur de demande :
     * Typiquement ayant formulé un voeu de rang 1 sur le secteur
     * (et donc n'ayant aucune autre demande).
     * Ils seront 'pris de droit' sauf cas très particulier forcé par l'administration.
     */
    private Integer nbrDemSct;

    /** Compteur d'affectation : Nombre de demandes non traitées sur cette offre. */
    private Integer nbrDemNonTrt;

    /** Dernière priorité traitée sur cette offre lors de la préaffectation. */
    private Integer valDerBlcPri;

    /** Dernier rang traité pour la priorité d'arrêt (valDerBlcPri) sur cette offre lors de la préaffectation. */
    private Integer valDerRngPri;

    /** Date de création de l'offre. */
    private Date datCre;

    /** Date de dernière mise à jour de l'offre. */
    private Date datMaj;

    /** Demandes d'affectation effectuées sur cette offre. */
    private Set<DemandeAffectation> demandeAffectations = new HashSet<DemandeAffectation>(0);

    /** Le nombre de demande Ulis. */
    private Integer nbDemandeUlis;

    /** Le nombre d'élèves ayant fait un voeux sur cette offre. */
    private Integer nbCandidats;

    /** Constructeur. */
    public OffreFormation() {
    }

    /** @return Code de l'offre de formation */
    public String getCod() {
        return this.cod;
    }

    /**
     * @param cod
     *            Code de l'offre de formation
     */
    public void setCod(String cod) {
        this.cod = cod;
    }

    /** @return Département MEN de rattachement dans lequel l'offre est proposée */
    public DeptMen getDeptMen() {
        return this.deptMen;
    }

    /**
     * @param deptMen
     *            Département MEN de rattachement dans lequel l'offre est proposée
     */
    public void setDeptMen(DeptMen deptMen) {
        this.deptMen = deptMen;
    }

    /** @return Formation d'accueil */
    public Formation getFormation() {
        return this.formation;
    }

    /**
     * @param formation
     *            Formation d'accueil
     */
    public void setFormation(Formation formation) {
        this.formation = formation;
    }

    /** @return Etablissement d'accueil (Sconet) */
    public EtablissementSconet getEtablissementSconet() {
        return this.etablissementSconet;
    }

    /**
     * @param etablissementSconet
     *            Etablissement d'accueil (Sconet)
     */
    public void setEtablissementSconet(EtablissementSconet etablissementSconet) {
        this.etablissementSconet = etablissementSconet;
    }

    /** @return Capacité d'accueil */
    public Integer getCapAcc() {
        return this.capAcc;
    }

    /**
     * @param capAcc
     *            Capacité d'accueil
     */
    public void setCapAcc(Integer capAcc) {
        this.capAcc = capAcc;
    }

    /** @return Nombre de demandes de secteur satisfaites */
    public Integer getNbrAffDemSct() {
        if (this.nbrAffDemSct != null) {
            return this.nbrAffDemSct;
        }
        return 0;
    }

    /**
     * @param nbrAffDemSct
     *            Nombre de demandes de secteur satisfaites
     */
    public void setNbrAffDemSct(Integer nbrAffDemSct) {
        this.nbrAffDemSct = nbrAffDemSct;
    }

    /** @return Nombre de demandes de dérogations affectés sur cette offre */
    public Integer getNbrAffDrg() {
        if (this.nbrAffDrg != null) {
            return this.nbrAffDrg;
        }
        return 0;
    }

    /**
     * @param nbrAffDrg
     *            Nombre de demandes de dérogations affectés sur cette offre
     */
    public void setNbrAffDrg(Integer nbrAffDrg) {
        this.nbrAffDrg = nbrAffDrg;
    }

    /** @return Nombre de demandes acceptées manuellement sur cette offre. */
    public Integer getNbrAffMan() {
        if (this.nbrAffMan != null) {
            return this.nbrAffMan;
        }
        return 0;
    }

    /**
     * @param nbrAffMan
     *            Nombre de demandes acceptées manuellement sur cette offre
     */
    public void setNbrAffMan(Integer nbrAffMan) {
        this.nbrAffMan = nbrAffMan;
    }

    /** @return Nombre d'élèves affectés en tant qu'offre de secteur */
    public Integer getNbrAffSct() {
        if (this.nbrAffSct != null) {
            return this.nbrAffSct;
        }
        return 0;
    }

    /**
     * @param nbrAffSct
     *            Nombre d'élèves affectés en tant qu'offre de secteur
     */
    public void setNbrAffSct(Integer nbrAffSct) {
        this.nbrAffSct = nbrAffSct;
    }

    /** @return Nombre de demandes de dérogation sur cette offre en attente d'une autre décision. */
    public Integer getNbrAttDrg() {
        if (this.nbrAttDrg != null) {
            return this.nbrAttDrg;
        }
        return 0;
    }

    /**
     * @param nbrAttDrg
     *            Nombre de demandes de dérogation sur cette offre en attente d'une autre décision.
     */
    public void setNbrAttDrg(Integer nbrAttDrg) {
        this.nbrAttDrg = nbrAttDrg;
    }

    /** @return Nombre de demandes de secteur en attente d'une autre décision. */
    public Integer getNbrAttSct() {
        if (this.nbrAttSct != null) {
            return this.nbrAttSct;
        }
        return 0;
    }

    /**
     * @param nbrAttSct
     *            Nombre de demandes de secteur en attente d'une autre décision.
     */
    public void setNbrAttSct(Integer nbrAttSct) {
        this.nbrAttSct = nbrAttSct;
    }

    /** @return Nombre de demandes dérogatoires */
    public Integer getNbrDemDrg() {
        if (this.nbrDemDrg != null) {
            return this.nbrDemDrg;
        }
        return 0;
    }

    /**
     * @param nbrDemDrg
     *            Nombre de demandes dérogatoires
     */
    public void setNbrDemDrg(Integer nbrDemDrg) {
        this.nbrDemDrg = nbrDemDrg;
    }

    /** @return Nombre maximum d'élèves dont cette offre est l'offre de secteur */
    public Integer getNbrDemMaxSct() {
        if (this.nbrDemMaxSct != null) {
            return this.nbrDemMaxSct;
        }
        return 0;
    }

    /**
     * @param nbrDemMaxSct
     *            Nombre maximum d'élèves dont cette offre est l'offre de secteur
     */
    public void setNbrDemMaxSct(Integer nbrDemMaxSct) {
        this.nbrDemMaxSct = nbrDemMaxSct;
    }

    /** @return Nombre de demandes comme offre de secteur. */
    public Integer getNbrDemSct() {
        if (this.nbrDemSct != null) {
            return this.nbrDemSct;
        }
        return 0;
    }

    /**
     * @param nbrDemSct
     *            Nombre de demandes comme offre de secteur.
     */
    public void setNbrDemSct(Integer nbrDemSct) {
        this.nbrDemSct = nbrDemSct;
    }

    /** @return Nombre de demandes non traitées sur cette offre */
    public Integer getNbrDemNonTrt() {
        if (this.nbrDemNonTrt != null) {
            return this.nbrDemNonTrt;
        }
        return 0;
    }

    /**
     * @param nbrDemNonTrt
     *            Nombre de demandes non traitées sur cette offre
     */
    public void setNbrDemNonTrt(Integer nbrDemNonTrt) {
        this.nbrDemNonTrt = nbrDemNonTrt;
    }

    /** @return Dernière priorité traitée sur cette offre lors de la préaffectation. */
    public Integer getValDerBlcPri() {
        return this.valDerBlcPri;
    }

    /**
     * @param valDerBlcPri
     *            Dernière priorité traitée sur cette offre lors de la préaffectation.
     */
    public void setValDerBlcPri(Integer valDerBlcPri) {
        this.valDerBlcPri = valDerBlcPri;
    }

    /** @return Dernier rang traité pour la priorité d'arrêt sur cette offre lors de la préaffectation */
    public Integer getValDerRngPri() {
        return this.valDerRngPri;
    }

    /**
     * @param valDerRngPri
     *            Dernier rang traité pour la priorité d'arrêt sur cette offre lors de la préaffectation
     */
    public void setValDerRngPri(Integer valDerRngPri) {
        this.valDerRngPri = valDerRngPri;
    }

    /** @return Date de création */
    public Date getDatCre() {
        return this.datCre;
    }

    /**
     * @param datCre
     *            Date de création
     */
    public void setDatCre(Date datCre) {
        this.datCre = datCre;
    }

    /** @return Date de dernière mise à jour */
    public Date getDatMaj() {
        return this.datMaj;
    }

    /**
     * @param datMaj
     *            Date de dernière mise à jour
     */
    public void setDatMaj(Date datMaj) {
        this.datMaj = datMaj;
    }

    /** @return Demandes d'affectations liées à cette offre de formation */
    public Set<DemandeAffectation> getDemandeAffectations() {
        return this.demandeAffectations;
    }

    /**
     * @param demandeAffectations
     *            Demandes d'affectations liées à cette offre de formation
     */
    public void setDemandeAffectations(Set<DemandeAffectation> demandeAffectations) {
        this.demandeAffectations = demandeAffectations;
    }

    public Integer getNbDemandeUlis() {
        return nbDemandeUlis;
    }

    public void setNbDemandeUlis(Integer nbDemandeUlis) {
        this.nbDemandeUlis = nbDemandeUlis;
    }

    /**
     * @return getNbrDemSct() + getNbrDemDrg()
     */
    public Integer getNbDemandesTotales() {
        return getNbrDemSct() + getNbrDemDrg();
    }

    /**
     * @return getNbrDemMaxSct() + getNbrDemDrg()
     */
    public Integer getNbMaxDemandesTotales() {
        return getNbrDemMaxSct() + getNbrDemDrg();
    }

    /**
     * @see #getNbAffectes()
     * @return getCapAcc() - getNbAffectes()
     */
    public Integer getNbPlacesDispo() {
        return getCapAcc() - getNbAffectes();
    }

    /**
     * @return <ul>
     *         <li>pour les offres SEGPA et/ou ULIS: getNbrAffDrg()</li>
     *         <li>pour les autres offres : getNbrAffSct() + getNbrAffDrg()</li>
     *         </ul>
     */
    public Integer getNbAffectes() {
        if (formation.getEstFormationSegpa() || formation.getEstFormationUlis()) {
            return getNbrAffDrg();
        } else {
            return getNbrAffSct() + getNbrAffDrg();
        }
    }

    /**
     * @return <ul>
     *         <li>pour les offres SEGPA et/ou ULIS : getNbMaxDemandesTotales() - getNbAffectes() -
     *         getNbrDemNonTrt()</li>
     *         <li>pour les autres offres : getNbMaxDemandesTotales() - getNbAffectes() - getNbrAttSct() -
     *         getNbrAttDrg() - getNbrDemNonTrt()</li>
     *         </ul>
     */
    public Integer getNbRefuses() {
        if (formation.getEstFormationSegpa() || formation.getEstFormationUlis()) {
            return getNbMaxDemandesTotales() - getNbAffectes() - getNbrDemNonTrt();
        } else {
            return getNbMaxDemandesTotales() - getNbAffectes() - getNbrAttSct() - getNbrAttDrg()
                    - getNbrDemNonTrt();
        }
    }

    public Integer getNbCandidats() {
        return nbCandidats;
    }

    public void setNbCandidats(Integer nbCandidats) {
        this.nbCandidats = nbCandidats;
    }

    @Override
    public boolean equals(Object autre) {

        if (this == autre) {
            // Même référence => égalité
            return true;
        }

        if (!(autre instanceof OffreFormation)) {
            // Types non compatibles
            return false;
        }

        // Ici on a une compatiblité de type => transTypage possible
        OffreFormation autreOffre = (OffreFormation) autre;

        // On s'intéresse au code qui est l'identifiant unique de l'offre
        String autreCode = autreOffre.getCod();

        if (cod == null) {
            // Si le code est à null, l'égalité n'est vérifiée que si le code
            // de l'autre objet est également à null.
            return (autreCode == null);
        }

        // Égalité des codes
        return cod.equals(autreCode);
    }

    @Override
    public int hashCode() {

        if (cod != null) {
            // C'est l'identifiant => on prend son hashCode
            return cod.hashCode();
        } else {
            // Pas d'élément discriminant à ce niveau => remonte au parent
            return super.hashCode();
        }
    }

}
