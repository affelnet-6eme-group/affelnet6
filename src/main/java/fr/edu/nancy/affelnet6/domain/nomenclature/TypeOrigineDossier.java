/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.nomenclature;

import java.io.Serializable;
import java.util.Date;

/** Origines des dossiers élèves. */
public class TypeOrigineDossier implements Serializable {

    /** Constante indiquant que le responsable du dossier est directeur d'école. */
    public static final String ORIGINE_DOSSIER_DIRECTEUR_ECOLE = "D";

    /** Constante indiquant que le responsable du dossier DSDEN. */
    public static final String ORIGINE_DOSSIER_INSPECTEUR_ACA = "I";

    /** Constante indiquant que le responsable du dossier est un principal de collège. */
    public static final String ORIGINE_DOSSIER_PRINCIPAL_COLLEGE = "C";

    /** Constante indiquant que le dossier a été importé depuis un autre département de l'académie. */
    public static final String ORIGINE_DOSSIER_IMPORT_AUTRE_DEPT = "A";

    /** Constante indiquant que le dossier a été importé pour un élève d'une école privée par le DSDEN. */
    public static final String ORIGINE_DOSSIER_IMPORT_PRIVE = "P";

    /** Constante indiquant que le dossier a été importé pour une école virtuelle. */
    public static final String ORIGINE_DOSSIER_ECOLE_VIRTUELLE = "V";

    /** Constante indiquant que le dossier a été importé manuellement depuis Onde par le DSDEN. */
    public static final String ORIGINE_DOSSIER_IMPORT_ONDE_DSDEN = "S";

    /** Constante indiquant que le dossier a été importé manuellement depuis Onde par le directeur d'école. */
    public static final String ORIGINE_DOSSIER_IMPORT_ONDE_DIRECTEUR_ECOLE = "E";

    /**
     * Constante indiquant que le dossier a été importé pour un élève d'une école privée par un principal de
     * collège.
     */
    public static final String ORIGINE_DOSSIER_IMPORT_PRIVE_PRINCIPAL_COLLEGE = "R";

    /** Constante indiquant que le dossier a été importé pour un élève d'une école publique de Onde. */
    //@formatter:off
    public static final String[] ORIGINE_DOSSIER_IMPORT_ONDE_ECOLE_PUBLIQUE = {
            ORIGINE_DOSSIER_DIRECTEUR_ECOLE,
            ORIGINE_DOSSIER_IMPORT_ONDE_DSDEN,
            ORIGINE_DOSSIER_IMPORT_ONDE_DIRECTEUR_ECOLE,
            ORIGINE_DOSSIER_ECOLE_VIRTUELLE
    };
    //@formatter:on

    /** Tag de sérialisation. */
    private static final long serialVersionUID = -2445866284198428443L;

    /** Code origine. */
    private String code;

    /** Libellé. */
    private String libelle;

    /** Ordre d'affichage. */
    private Integer ordre;

    /** Horodatage de mise à jour. */
    private Date timeUpdate;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the libelle
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * @param libelle
     *            the libelle to set
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /**
     * @return the ordre
     */
    public Integer getOrdre() {
        return ordre;
    }

    /**
     * @param ordre
     *            the ordre to set
     */
    public void setOrdre(Integer ordre) {
        this.ordre = ordre;
    }

    /**
     * @return the timeUpdate
     */
    public Date getTimeUpdate() {
        if (timeUpdate == null) {
            return null;
        }
        return new Date(timeUpdate.getTime());
    }

    /**
     * @param timeUpdate
     *            the timeUpdate to set
     */
    public void setTimeUpdate(Date timeUpdate) {
        if (timeUpdate == null) {
            this.timeUpdate = null;
        } else {
            this.timeUpdate = new Date(timeUpdate.getTime());
        }
    }

}
