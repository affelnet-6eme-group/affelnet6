/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.nomenclature;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Type d'établissement (collège, EREA, école...).
 */
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler", "fieldHandler" })
public class TypeEtablissement implements java.io.Serializable {

    /** Type établissement pour les collèges. */
    public static final String COLLEGE = "CLG";

    /** Type établissement pour les EREA (Etablissement Régional d'Enseignement Adapté). */
    public static final String EREA = "EREA";

    /** Type établissement pour les écoles. */
    public static final String ECOLES = "ECOLES";

    /** Type établissement pour les écoles du 1er degré ordinaires. */
    public static final String ECOLE_1D_ORDINAIRE = "1ORD";

    /** Type établissement pour les écoles d'application du 1er degré. */
    public static final String ECOLE_1D_APPLICATION = "APPL";

    /** Type établissement pour les écoles régionales du 1er degré. */
    public static final String ECOLE_1D_REGIONALE = "ERPD";

    /** Type établissement pour les écoles du 1er degré spécialisées. */
    public static final String ECOLE_1D_SPECIALISEE = "SPEC";

    /** serialVersionUID. */
    private static final long serialVersionUID = 1090305548887922714L;

    /** Identifiant du type d'établissement. */
    private Integer typeEtabId;

    /** Code du type d'établissement. */
    private String codeTypeEtab;

    /** Libellé court du type d'établissement. */
    private String lcTypeEtab;

    /** Libellé long du type d'établissement. */
    private String llTypeEtab;

    /** Date d'ouverture. */
    private Date dateOuverture;

    /** Date de fermeture. */
    private Date dateFermeture;

    /** Etablissements de ce type. */
    @JsonBackReference
    private Set<EtablissementSconet> etablissementSconets = new HashSet<>(0);

    /** Etablissements du 1er degré de ce type. */
    @JsonBackReference
    private Set<EtablissementOrigine> etablissementOrigines = new HashSet<>(0);

    /**
     * Crée un type d'établissement (constructeur par défaut).
     */
    public TypeEtablissement() {
    }

    /**
     * Crée un type d'établissement.
     * 
     * @param codeTypeEtab
     *            Le code du type d'établissement.
     */
    public TypeEtablissement(String codeTypeEtab) {
        this.codeTypeEtab = codeTypeEtab;
    }

    /** @return Identifiant du type d'établissement. */
    public Integer getTypeEtabId() {
        return this.typeEtabId;
    }

    /**
     * @param typeEtabId
     *            Identifiant du type d'établissement.
     */
    public void setTypeEtabId(Integer typeEtabId) {
        this.typeEtabId = typeEtabId;
    }

    /**
     * 
     * @return Code du type d'établissement.
     */
    public String getCodeTypeEtab() {
        return this.codeTypeEtab;
    }

    /**
     * 
     * @param codeTypeEtab
     *            Code du type d'établissement.
     */
    public void setCodeTypeEtab(String codeTypeEtab) {
        this.codeTypeEtab = codeTypeEtab;
    }

    /**
     * 
     * @return Libellé court du type d'établissement.
     */
    public String getLcTypeEtab() {
        return this.lcTypeEtab;
    }

    /**
     * 
     * @param lcTypeEtab
     *            Libellé court du type d'établissement.
     */
    public void setLcTypeEtab(String lcTypeEtab) {
        this.lcTypeEtab = lcTypeEtab;
    }

    /**
     * 
     * @return Libellé long du type d'établissement.
     */
    public String getLlTypeEtab() {
        return this.llTypeEtab;
    }

    /**
     * 
     * @param llTypeEtab
     *            Libellé long du type d'établissement.
     */
    public void setLlTypeEtab(String llTypeEtab) {
        this.llTypeEtab = llTypeEtab;
    }

    /**
     * @return the dateOuverture
     */
    public Date getDateOuverture() {
        return dateOuverture;
    }

    /**
     * @param dateOuverture
     *            the dateOuverture to set
     */
    public void setDateOuverture(Date dateOuverture) {
        this.dateOuverture = dateOuverture;
    }

    /**
     * @return the dateFermeture
     */
    public Date getDateFermeture() {
        return dateFermeture;
    }

    /**
     * @param dateFermeture
     *            the dateFermeture to set
     */
    public void setDateFermeture(Date dateFermeture) {
        this.dateFermeture = dateFermeture;
    }

    /**
     * 
     * @return Etablissements de ce type.
     */
    public Set<EtablissementSconet> getEtablissementSconets() {
        return this.etablissementSconets;
    }

    /**
     * 
     * @param etablissementSconets
     *            Etablissements de ce type.
     */
    public void setEtablissementSconets(Set<EtablissementSconet> etablissementSconets) {
        this.etablissementSconets = etablissementSconets;
    }

    /**
     * 
     * @return Etablissements du 1er degré de ce type.
     */
    public Set<EtablissementOrigine> getEtablissementOrigines() {
        return this.etablissementOrigines;
    }

    /**
     * 
     * @param etablissementOrigines
     *            Etablissements du 1er degré de ce type.
     */
    public void setEtablissementOrigines(Set<EtablissementOrigine> etablissementOrigines) {
        this.etablissementOrigines = etablissementOrigines;
    }

}
