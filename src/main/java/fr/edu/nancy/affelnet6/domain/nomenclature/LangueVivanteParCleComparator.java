/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.nomenclature;

import java.util.Comparator;

/** Comparateur de langue vivante basé sur la clé. */
public class LangueVivanteParCleComparator implements Comparator<LangueVivante> {

    @Override
    public int compare(LangueVivante o1, LangueVivante o2) {
        if (o1 == null) {
            if (o2 == null) {
                return 0;
            }
            return -1;
        }
        if (o2 == null) {
            return 1;
        }

        if (o1.getMatiere().getCle() == null) {
            if (o2.getMatiere().getCle() == null) {
                return 0;
            }
            return -1;
        }

        if (o2.getMatiere().getCle() == null) {
            return 1;
        }

        return o1.getMatiere().getCle().compareTo(o2.getMatiere().getCle());
    }

}
