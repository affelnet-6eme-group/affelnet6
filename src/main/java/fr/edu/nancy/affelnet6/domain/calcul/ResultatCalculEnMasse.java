/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.calcul;

import fr.edu.nancy.affelnet6.domain.nomenclature.EtatCollegeSecteur;
import fr.edu.nancy.affelnet6.service.interfaces.DeterminationAutomatiqueManager;

/**
 * Gère différents compteurs pour une détermination en masse des collèges de secteur.
 * 
 * 
 *
 */
public class ResultatCalculEnMasse {
    /** Nombre d'élèves pour lesquels le calcul a abouti. */
    private int nbAbouti;
    /** Nombre d'élèves pour lesquels le calcul n'a abouti. */
    private int nbNonAbouti;
    /** Nombre d'élèves pour lesquels le calcul n'est pas possible. */
    private int nbImpossible;
    /** Nombre d'élèves pour lesquels le calcul a été avorté (déjà calculé). */
    private int nbAvorte;
    /** Nombre d'élèves pour lesquels le calcul a abouti hors du département. */
    private int nbAboutiHorsDep;

    /**
     * Incrémente le bon compteur en fonction de l'état du calcul.
     * 
     * @param etat
     *            l'état du calcul pour l'élève courant
     */
    public void increment(EtatCollegeSecteur etat) {
        if (DeterminationAutomatiqueManager.CALCUL_AUTO_ABOUTI.equals(etat)) {
            nbAbouti++;
        } else if (DeterminationAutomatiqueManager.CALCUL_AUTO_NON_ABOUTI.equals(etat)) {
            nbNonAbouti++;
        } else if (DeterminationAutomatiqueManager.CALCUL_ABOUTI_HORS_DEPARTEMENT.equals(etat)) {
            nbAboutiHorsDep++;
        } else if (DeterminationAutomatiqueManager.CALCUL_IMPOSSIBLE.equals(etat)) {
            nbImpossible++;
        } else {
            nbAvorte++;
        }
    }

    /**
     * @return the nbAbouti
     */
    public int getNbAbouti() {
        return nbAbouti;
    }

    /**
     * @return the nbNonAbouti
     */
    public int getNbNonAbouti() {
        return nbNonAbouti;
    }

    /**
     * @return the nbImpossible
     */
    public int getNbImpossible() {
        return nbImpossible;
    }

    /**
     * @return the nbAvorte
     */
    public int getNbAvorte() {
        return nbAvorte;
    }

    /**
     * @return the nbAboutiHorsDep
     */
    public int getNbAboutiHorsDep() {
        return nbAboutiHorsDep;
    }

}
