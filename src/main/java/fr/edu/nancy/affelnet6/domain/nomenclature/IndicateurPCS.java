/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.nomenclature;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import fr.edu.nancy.affelnet6.domain.affectation.IndicateurPcsEnum;

/**
 * Indicateur du niveau social.
 * 
 * 
 *
 */
public class IndicateurPCS implements Serializable {

    /** Tag de sérialisation. */
    private static final long serialVersionUID = 1L;

    /** Identifiant. */
    private Integer id;

    /** Code. */
    private String code;

    /** Libellé court. */
    private String libelleCourt;

    /** Libellé long. */
    private String libelleLong;

    /** Libellé édition. */
    private String libelleEdition;

    /** Enumération des indicateurs. */
    private IndicateurPcsEnum indicateurEnum;

    /** Date d'ouverture. */
    private Date dateOuverture;

    /** Date de fermeture. */
    private Date dateFermeture;

    /** Les catégories socio-professionnelles attachées à l'indicateur. */
    private Set<PCS> pcs = new HashSet<>();

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(String code) {
        this.code = code;
        this.indicateurEnum = IndicateurPcsEnum.fromCode(code);
    }

    /**
     * @return l'indicateurEnum
     */
    public IndicateurPcsEnum getIndicateurEnum() {
        return indicateurEnum;
    }

    /**
     * @return the libelleCourt
     */
    public String getLibelleCourt() {
        return libelleCourt;
    }

    /**
     * @param libelleCourt
     *            the libelleCourt to set
     */
    public void setLibelleCourt(String libelleCourt) {
        this.libelleCourt = libelleCourt;
    }

    /**
     * @return the libelleLong
     */
    public String getLibelleLong() {
        return libelleLong;
    }

    /**
     * @param libelleLong
     *            the libelleLong to set
     */
    public void setLibelleLong(String libelleLong) {
        this.libelleLong = libelleLong;
    }

    /**
     * @return the libelleEdition
     */
    public String getLibelleEdition() {
        return libelleEdition;
    }

    /**
     * @param libelleEdition
     *            the libelleEdition to set
     */
    public void setLibelleEdition(String libelleEdition) {
        this.libelleEdition = libelleEdition;
    }

    /**
     * @return the dateOuverture
     */
    public Date getDateOuverture() {
        return dateOuverture;
    }

    /**
     * @param dateOuverture
     *            the dateOuverture to set
     */
    public void setDateOuverture(Date dateOuverture) {
        this.dateOuverture = dateOuverture;
    }

    /**
     * @return the dateFermeture
     */
    public Date getDateFermeture() {
        return dateFermeture;
    }

    /**
     * @param dateFermeture
     *            the dateFermeture to set
     */
    public void setDateFermeture(Date dateFermeture) {
        this.dateFermeture = dateFermeture;
    }

    /**
     * @return the pcs
     */
    public Set<PCS> getPcs() {
        return pcs;
    }

    /**
     * @param pcs
     *            the pcs to set
     */
    public void setPcs(Set<PCS> pcs) {
        this.pcs = pcs;
    }

}
