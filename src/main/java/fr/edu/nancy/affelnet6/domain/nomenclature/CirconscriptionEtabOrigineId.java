/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.nomenclature;

import org.apache.commons.lang3.builder.HashCodeBuilder;

/** Classe d'identification pour les liens circonscription - établissement d'origine. */
public class CirconscriptionEtabOrigineId implements java.io.Serializable {

    /** Serial tag. */
    private static final long serialVersionUID = 73766866274926938L;

    /** Code de la circonscription concernée par le lien. */
    private String codUaiCirco;

    /** Code de l'établissement d'origne concerné par le lien. */
    private String codUaiEtb;

    /** Constructeur du bean. */
    public CirconscriptionEtabOrigineId() {
    }

    /**
     * @return the codUaiCirco
     */
    public String getCodUaiCirco() {
        return codUaiCirco;
    }

    /**
     * @param codUaiCirco
     *            the codUaiCirco to set
     */
    public void setCodUaiCirco(String codUaiCirco) {
        this.codUaiCirco = codUaiCirco;
    }

    /**
     * @return the codUaiEtb
     */
    public String getCodUaiEtb() {
        return codUaiEtb;
    }

    /**
     * @param codUaiEtb
     *            the codUaiEtb to set
     */
    public void setCodUaiEtb(String codUaiEtb) {
        this.codUaiEtb = codUaiEtb;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null) {
            return false;
        }
        if (!(other instanceof CirconscriptionEtabOrigineId)) {
            return false;
        }
        CirconscriptionEtabOrigineId castOther = (CirconscriptionEtabOrigineId) other;

        return this.getCodUaiCirco() != null && this.getCodUaiCirco().equals(castOther.getCodUaiCirco())
                && this.getCodUaiEtb() != null && this.getCodUaiEtb().equals(castOther.getCodUaiEtb());
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(codUaiCirco).append(codUaiEtb).toHashCode();
    }
}
