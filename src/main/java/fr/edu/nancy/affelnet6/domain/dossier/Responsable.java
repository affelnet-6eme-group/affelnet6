/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.dossier;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import fr.edu.nancy.affelnet6.domain.nomenclature.Civilite;
import fr.edu.nancy.affelnet6.domain.nomenclature.DeptMen;
import fr.edu.nancy.affelnet6.domain.nomenclature.LienParente;
import fr.edu.nancy.affelnet6.domain.nomenclature.NiveauResponsabilite;
import fr.edu.nancy.affelnet6.domain.nomenclature.PCS;
import fr.edu.nancy.commun.utils.ChaineUtils;

/**
 * Responsable d'un élève.
 * 
 * Il peut s'agir d'une personne physique ou d'une personne morale.
 */
public class Responsable implements java.io.Serializable, Comparable<Responsable> {

    /** Code pour les types de responsables ne tant que personnes physiques. */
    public static final String TYPE_RESPONSABLE_PERSONNE_PHYSIQUE = "P";

    /** Code pour les types de responsables ne tant que personnes morales. */
    public static final String TYPE_RESPONSABLE_PERSONNE_MORALE = "M";

    /** Tag de sérialisation. */
    private static final long serialVersionUID = -1648000240154573272L;

    /** Identifiant technique du responsable. */
    private ResponsableId id;

    /** Numéro de dossier de l'élève. */
    private Long numDossierEleve;

    /** Eleve rattaché au responsable. */
    private Eleve eleve;

    /** Niveau de responsabilité. */
    private NiveauResponsabilite niveauResponsabilite;

    /** Lien de parenté du responsable avec l'élève. */
    private LienParente lienParente;

    /** Département MEN gestionnaire de ce responsable. */
    private DeptMen dept;

    /** Adresse du responsable. */
    private Adresse adresseResponsable;

    /** Type du responsable (physique ou moral). */
    private String typeResponsable;

    /** Nom patronymique du responsable (physiques). */
    private String nom;

    /** Nom d'usage du responsable (physiques). */
    private String nomUsage;

    /** Prénom du responsable (physiques). */
    private String prenom;

    /** Intitulé du responsable (moral). */
    private String intitule;

    /** Numéro de téléphone du domicile du responsable. */
    private String telephoneDomicile;

    /** Numéro de téléphone professionnel du responsable. */
    private String telephoneProfessionnel;

    /** Numéro de téléphone portable du responsable. */
    private String telephonePortable;

    /** Date de dernière édition de la notification d''affectation pour le responsable. */
    private Date dateEditNotif;

    /** Date d'envoi de la notification d'affectation par email. */
    private Date dateEnvoiNotif;

    /** Civilité du responsable. */
    private Civilite civilite;

    /** Courriel du responsable. */
    private String mel;

    /** Catégorie socio-professionnelle. */
    private PCS pcs;

    /** Constructeur par défaut. */
    public Responsable() {
    }

    /**
     * Constructeur complet.
     * 
     * @param id
     *            Identifiant technique du responsable
     * @param dept
     *            Département MEN gestionnaire
     * @param typeResponsable
     *            Type de responsable ('P'hysique ou 'M'oral)
     */
    public Responsable(ResponsableId id, DeptMen dept, String typeResponsable) {
        this.id = id;

        this.dept = dept;
        this.typeResponsable = typeResponsable;
    }

    /**
     * Constructeur complet.
     * 
     * @param id
     *            Identifiant technique du responsable
     * @param lienParente
     *            Lien de parenté avec l'élève
     * @param dept
     *            Département MEN gestionnaire
     * @param adresseResponsable
     *            Adresse du responsable
     * @param typeResponsable
     *            Type de responsable ('P'hysique ou 'M'oral)
     * @param nom
     *            Nom patronymique du responsable (physique)
     * @param nomUsage
     *            Nom d'usage du responsable (physique)
     * @param prenom
     *            Prénom du responsable (physique)
     * @param intitule
     *            Intitulé du responsable (moral)
     * @param telephoneDomicile
     *            Numéro de téléphone du domicile du responsable
     * @param telephoneProfessionnel
     *            Numéro de téléphone professionnel du responsable
     * @param telephonePortable
     *            Numéro de téléphone portable du responsable
     * @param civilite
     *            civilité du responsable
     * @param mel
     *            Courriel du responsable
     * @param dateEditNotif
     *            Date de dernière édition de la notification d''affectation pour le responsable
     * 
     */
    public Responsable(ResponsableId id, LienParente lienParente, DeptMen dept, Adresse adresseResponsable,
            String typeResponsable, String nom, String nomUsage, String prenom, String intitule,
            String telephoneDomicile, String telephoneProfessionnel, String telephonePortable, Civilite civilite,
            String mel, Date dateEditNotif) {
        this.id = id;

        this.lienParente = lienParente;
        this.dept = dept;
        this.adresseResponsable = adresseResponsable;
        this.typeResponsable = typeResponsable;
        this.nom = nom;
        this.nomUsage = nomUsage;
        this.prenom = prenom;
        this.intitule = intitule;
        this.telephoneDomicile = telephoneDomicile;
        this.telephoneProfessionnel = telephoneProfessionnel;
        this.telephonePortable = telephonePortable;
        this.civilite = civilite;
        this.mel = mel;
        this.dateEditNotif = dateEditNotif;

    }

    /** @return Identifiant technique du responsable. */
    public ResponsableId getId() {
        return this.id;
    }

    /**
     * @param id
     *            Identifiant technique du responsable
     */
    public void setId(ResponsableId id) {
        this.id = id;
    }

    /**
     * @return the niveauResponsabilite
     */
    public NiveauResponsabilite getNiveauResponsabilite() {
        return niveauResponsabilite;
    }

    /**
     * @param niveauResponsabilite
     *            the niveauResponsabilite to set
     */
    public void setNiveauResponsabilite(NiveauResponsabilite niveauResponsabilite) {
        this.niveauResponsabilite = niveauResponsabilite;
    }

    /** @return lien de parenté avec l'élève */
    public LienParente getLienParente() {
        return this.lienParente;
    }

    /**
     * @param lienParente
     *            lien de parenté avec l'élève.
     */
    public void setLienParente(LienParente lienParente) {
        this.lienParente = lienParente;
    }

    /** @return Département MEN gestionnaire */
    public DeptMen getDept() {
        return this.dept;
    }

    /**
     * @param dept
     *            Département MEN gestionnaire.
     */
    public void setDept(DeptMen dept) {
        this.dept = dept;
    }

    /** @return Adresse du responsable */
    public Adresse getAdresseResponsable() {
        return this.adresseResponsable;
    }

    /**
     * @param adresseResponsable
     *            Adresse du responsable.
     */
    public void setAdresseResponsable(Adresse adresseResponsable) {
        this.adresseResponsable = adresseResponsable;
    }

    /** @return Type de responsable ('P'hysique ou 'M'oral) */
    public String getTypeResponsable() {
        return this.typeResponsable;
    }

    /**
     * @param typeResponsable
     *            Type de responsable ('P'hysique ou 'M'oral).
     */
    public void setTypeResponsable(String typeResponsable) {
        this.typeResponsable = typeResponsable;
    }

    /** @return Nom patronymique du responsable (physique) */
    public String getNom() {
        return this.nom;
    }

    /**
     * @param nom
     *            Nom patronymique du responsable (physique).
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /** @return Nom d'usage du responsable (physique) */
    public String getNomUsage() {
        return this.nomUsage;
    }

    /**
     * @param nomUsage
     *            Nom d'usage du responsable (physique).
     */
    public void setNomUsage(String nomUsage) {
        this.nomUsage = nomUsage;
    }

    /** @return Prénom du responsable (physique) */
    public String getPrenom() {
        return this.prenom;
    }

    /**
     * @param prenom
     *            Prénom du responsable (physique).
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /** @return Intitulé du responsable (moral). */
    public String getIntitule() {
        return this.intitule;
    }

    /**
     * @param intitule
     *            Intitulé du responsable (moral).
     */
    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    /** @return Numéro de téléphone du domicile du responsable */
    public String getTelephoneDomicile() {
        return this.telephoneDomicile;
    }

    /**
     * @param telephoneDomicile
     *            Numéro de téléphone du domicile du responsable.
     */
    public void setTelephoneDomicile(String telephoneDomicile) {
        this.telephoneDomicile = telephoneDomicile;
    }

    /** @return Numéro de téléphone professionnel du responsable */
    public String getTelephoneProfessionnel() {
        return this.telephoneProfessionnel;
    }

    /**
     * @param telephoneProfessionnel
     *            Numéro de téléphone professionnel du responsable.
     */
    public void setTelephoneProfessionnel(String telephoneProfessionnel) {
        this.telephoneProfessionnel = telephoneProfessionnel;
    }

    /** @return Numéro de téléphone portable du responsable */
    public String getTelephonePortable() {
        return this.telephonePortable;
    }

    /**
     * @param telephonePortable
     *            Numéro de téléphone portable du responsable.
     */
    public void setTelephonePortable(String telephonePortable) {
        this.telephonePortable = telephonePortable;
    }

    /**
     * 
     * @return Date de dernière édition de la notification d''affectation pour le responsable.
     */
    public Date getDateEditNotif() {
        return dateEditNotif;
    }

    /**
     * 
     * @param dateEditNotif
     *            Date de dernière édition de la notification d''affectation pour le responsable.
     */
    public void setDateEditNotif(Date dateEditNotif) {
        this.dateEditNotif = dateEditNotif;
    }

    /** @return Civilité du responsable */
    public Civilite getCivilite() {
        return this.civilite;
    }

    /**
     * @param civilite
     *            Civilité du responsable.
     */
    public void setCivilite(Civilite civilite) {
        this.civilite = civilite;
    }

    /** @return courriel du responsable */
    public String getMel() {
        return this.mel;
    }

    /**
     * @param mel
     *            l'email du responsable
     */
    public void setMel(String mel) {
        this.mel = mel;
    }

    /**
     * @return le numéro de dossier de l'élève
     */
    public Long getNumDossierEleve() {
        return this.numDossierEleve;
    }

    /**
     * @return l'élève rattaché au responsable.
     */
    public Eleve getEleve() {
        return eleve;
    }

    /**
     *
     * @param eleve
     *            L'élève rattaché au responsable.
     */
    public void setEleve(Eleve eleve) {
        this.eleve = eleve;
    }

    /**
     * @param numDossierEleve
     *            numéro de dossier de l'élève
     */
    public void setNumDossierEleve(Long numDossierEleve) {
        this.numDossierEleve = numDossierEleve;
    }

    @Override
    public boolean equals(Object otherObj) {
        if (otherObj == null) {
            return false;
        }

        if (otherObj == this) {
            return true;
        }

        if (otherObj.getClass() != getClass()) {
            return false;
        }

        Responsable otherResponsable = (Responsable) otherObj;

        if (id == null) {
            return false;
        }

        return id.equals(otherResponsable.getId());
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public int compareTo(Responsable o) {
        ResponsableComparator responsableComparator = new ResponsableComparator();
        return responsableComparator.compare(this, o);
    }

    /** @return nom et prénom ou intitulé du responsable selon son type */
    public String composeChaineNomResponsable() {

        if (Responsable.TYPE_RESPONSABLE_PERSONNE_PHYSIQUE.equals(typeResponsable)) {
            return ChaineUtils.implode(" ", nom, prenom);
        } else {
            return StringUtils.defaultString(intitule, "");
        }
    }

    /**
     * @return the pcs
     */
    public PCS getPcs() {
        return pcs;
    }

    /**
     * @param pcs
     *            the pcs to set
     */
    public void setPcs(PCS pcs) {
        this.pcs = pcs;
    }

    /**
     * @return the dateEnvoiNotif
     */
    public Date getDateEnvoiNotif() {
        return dateEnvoiNotif;
    }

    /**
     * @param dateEnvoiNotif
     *            the dateEnvoiNotif to set
     */
    public void setDateEnvoiNotif(Date dateEnvoiNotif) {
        this.dateEnvoiNotif = dateEnvoiNotif;
    }

    /**
     * Retourne vrai si le responsable est un responsable légal.
     *
     * @return vrai si le responsable est un responsable légal
     */
    public boolean isResponsableLegal() {
        return getNiveauResponsabilite() != null
                && NiveauResponsabilite.CODE_REPRESENTANT_LEGAL.equals(getNiveauResponsabilite().getCode());
    }
}
