/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.dossier;

import java.util.Comparator;
import java.util.List;

import fr.edu.nancy.affelnet6.domain.nomenclature.EtablissementSconet;

/**
 * Comparateur de secteur (liste d'établissement).
 * Ordre alphabétique (null &lt; n'importe quelle chaîne)
 * ex : 05400004M &lt; 05400004M, 0540000LH
 */
public class SecteurComparator implements Comparator<List<EtablissementSconet>> {

    @Override
    public int compare(List<EtablissementSconet> o1, List<EtablissementSconet> o2) {
        if (o1 == null) {
            if (o2 == null) {
                return 0;
            }
            return 1;
        }
        int i = 0;
        for (; i < o1.size(); i++) {
            if (i >= o2.size()) {
                return 1;
            }
            if (!o1.get(i).getCodeRne().equals(o2.get(i).getCodeRne())) {
                return o1.get(i).getCodeRne().compareTo(o2.get(i).getCodeRne());
            }
        }
        if (i < o2.size()) {
            return -1;
        }
        return 0;
    }
}
