/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.affectation;

/** Indicateur des catégories socio-professionnelles. */
public enum IndicateurPcsEnum {
    /** Très favorisé. */
    A("A"),
    /** Favorisé. */
    B("B"),
    /** Défavorisé. */
    C("C"),
    /** Très défavorisé. */
    D("D"),
    /** Non précisé. */
    NP("9");

    /** Code de l'indicateur. */
    private final String code;

    /**
     * Constructeur.
     * 
     * @param code
     *            le code
     */
    IndicateurPcsEnum(String code) {
        this.code = code;
    }

    /**
     * @return le code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            le code à chercher
     * @return la valeur de l'énumération correspondant au code
     */
    public static IndicateurPcsEnum fromCode(String code) {
        for (IndicateurPcsEnum i : values()) {
            if (i.code.equals(code)) {
                return i;
            }
        }
        return null;
    }

}
