/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.nomenclature;

import org.apache.commons.lang3.StringUtils;

/**
 * Objet métier pour modéliser l'état de l'export d'un élève vers BEE.
 */
public class EtatExportBee implements java.io.Serializable {

    /** État export vers BEE non envoyé. */
    public static final String NON_ENVOYE = "NE";

    /** État export vers BEE envoyé. */
    public static final String ENVOYE = "EN";

    /** État export vers BEE réceptionné. */
    public static final String RECEPTIONNE = "RC";

    /** État export vers BEE erreur à la réception. */
    public static final String ERREUR_RECEPTION = "ER";

    /** Tag de sérialisation. */
    private static final long serialVersionUID = -6419045117764193896L;

    /** Code de l'état. */
    private String code;

    /** Libellé long de l'état. */
    private String libelleLong;

    /**
     * Constructeur par défaut.
     */
    public EtatExportBee() {

    }

    /**
     * Constructeur.
     * 
     * @param code
     *            code de l'état
     */
    public EtatExportBee(String code) {
        this.code = code;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the libelleLong
     */
    public String getLibelleLong() {
        return libelleLong;
    }

    /**
     * @param libelleLong
     *            the libelleLong to set
     */
    public void setLibelleLong(String libelleLong) {
        this.libelleLong = libelleLong;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof EtatExportBee) {
            EtatExportBee etat = (EtatExportBee) obj;
            return StringUtils.equals(this.getCode(), etat.getCode());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.getCode().hashCode();
    }

    @Override
    public String toString() {
        return this.getCode();
    }
}
