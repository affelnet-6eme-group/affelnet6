/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.dossier;

import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import fr.edu.nancy.affelnet6.domain.nomenclature.EtablissementSconet;
import fr.edu.nancy.affelnet6.domain.nomenclature.EtablissementSconetUaiComparator;

public class SecteurAffectation {

    private SortedSet<Eleve> eleves;
    private SortedSet<EtablissementSconet> etablissementSconets;

    public void ajouteEleve(Eleve eleve) {
        if (this.eleves == null) {
            this.eleves = new TreeSet<Eleve>(new EleveNomPrenomComparator());
        }
        if (eleve != null) {
            this.eleves.add(eleve);
        }
    }

    public void ajouteEtablissements(Collection<EtablissementSconet> etablissementSconets) {
        controleEtablissements();

        if (CollectionUtils.isNotEmpty(etablissementSconets)) {
            this.etablissementSconets.addAll(etablissementSconets);
        }
    }

    public void ajouteEtablissement(EtablissementSconet etablissementSconet) {
        controleEtablissements();

        if (etablissementSconet != null) {
            this.etablissementSconets.add(etablissementSconet);
        }
    }

    public void ajouteCollegesSecteur(Collection<CollegeSecteur> collegeSecteurs) {
        controleEtablissements();

        if (CollectionUtils.isNotEmpty(collegeSecteurs)) {
            for (CollegeSecteur collegeSecteur : collegeSecteurs) {
                ajouteEtablissement(collegeSecteur.getEtablissementSconet());
            }
        }
    }

    private void controleEtablissements() {
        if (this.etablissementSconets == null) {
            this.etablissementSconets = new TreeSet<EtablissementSconet>(new EtablissementSconetUaiComparator());
        }
    }

    public String getId() {
        return StringUtils.join(etablissementSconets, "_");
    }

    /**
     * @return the eleves
     */
    public SortedSet<Eleve> getEleves() {
        return eleves;
    }

    /**
     * @return the etablissementSconets
     */
    public SortedSet<EtablissementSconet> getEtablissementSconets() {
        return etablissementSconets;
    }

}
