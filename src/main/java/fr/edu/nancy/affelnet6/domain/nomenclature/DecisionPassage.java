/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.nomenclature;

import java.util.HashSet;
import java.util.Set;

import fr.edu.nancy.affelnet6.domain.dossier.Eleve;

/**
 * Classe définissant une décision de passage.
 */
public class DecisionPassage implements java.io.Serializable {

    /** Le numéro de version de sérialisation. */
    private static final long serialVersionUID = 1L;

    /** Code. */
    private String cod;

    /** Libellé court. */
    private String libCou;

    /** Libellé long. */
    private String libLon;

    /** Liste des élèves. */
    private Set<Eleve> eleves = new HashSet<>(0);

    /**
     * Constructeur par défaut.
     */
    public DecisionPassage() {
    }

    /**
     * Constructeur.
     * 
     * @param libCou
     *            libellé court
     * @param libLon
     *            libellé long
     * @param eleves
     *            liste des élèves
     */
    public DecisionPassage(String libCou, String libLon, Set<Eleve> eleves) {
        this.libCou = libCou;
        this.libLon = libLon;
        this.eleves = eleves;
    }

    /**
     * @return the cod
     */
    public String getCod() {
        return this.cod;
    }

    /**
     * @param cod
     *            the cod to set
     */
    public void setCod(String cod) {
        this.cod = cod;
    }

    /**
     * @return the libCou
     */
    public String getLibCou() {
        return this.libCou;
    }

    /**
     * @param libCou
     *            the libCou to set
     */
    public void setLibCou(String libCou) {
        this.libCou = libCou;
    }

    /**
     * @return the libLon
     */
    public String getLibLon() {
        return this.libLon;
    }

    /**
     * @param libLon
     *            the libLon to set
     */
    public void setLibLon(String libLon) {
        this.libLon = libLon;
    }

    /**
     * @return the eleves
     */
    public Set<Eleve> getEleves() {
        return this.eleves;
    }

    /**
     * @param eleves
     *            the eleves to set
     */
    public void setEleves(Set<Eleve> eleves) {
        this.eleves = eleves;
    }
}
