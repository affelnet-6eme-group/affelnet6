/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.dossier;

import java.io.Serializable;
import java.util.Date;

import fr.edu.nancy.affelnet6.domain.nomenclature.PersonnalisationParcours;

/**
 * Association entre un élève et un dispositif de personnalisation des parcours.
 */
public class ElevePersonnalisationParcours implements Serializable {

    /** Tag de sérialisation. */
    private static final long serialVersionUID = -1175232616581217997L;

    /**
     * Identifiant de l'association entre un élève et un dispositif de personnalisation des parcours.
     */
    private Long id;

    /** Numéro de dossier de l'élève. */
    private Eleve eleve;

    /** Code du dispositif de personnalisation des parcours. */
    private PersonnalisationParcours personnalisationParcours;

    /** Date de début. */
    private Date dateDebut;

    /** Date de fin. */
    private Date dateFin;

    /**
     * Constructeur par défaut.
     */
    public ElevePersonnalisationParcours() {
    }

    /**
     * Constructeur.
     * 
     * @param eleve
     *            l'élève
     * @param personnalisationParcours
     *            dispositif de personnalisation des parcours
     */
    public ElevePersonnalisationParcours(Eleve eleve, PersonnalisationParcours personnalisationParcours) {
        this.eleve = eleve;
        this.personnalisationParcours = personnalisationParcours;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the eleve
     */
    public Eleve getEleve() {
        return eleve;
    }

    /**
     * @param eleve
     *            the eleve to set
     */
    public void setEleve(Eleve eleve) {
        this.eleve = eleve;
    }

    /**
     * @return the personnalisationParcours
     */
    public PersonnalisationParcours getPersonnalisationParcours() {
        return personnalisationParcours;
    }

    /**
     * @param personnalisationParcours
     *            the personnalisationParcours to set
     */
    public void setPersonnalisationParcours(PersonnalisationParcours personnalisationParcours) {
        this.personnalisationParcours = personnalisationParcours;
    }

    /**
     * @return the dateDebut
     */
    public Date getDateDebut() {
        return dateDebut;
    }

    /**
     * @param dateDebut
     *            the dateDebut to set
     */
    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    /**
     * @return the dateFin
     */
    public Date getDateFin() {
        return dateFin;
    }

    /**
     * @param dateFin
     *            the dateFin to set
     */
    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    // @Override
    // public boolean equals(Object obj) {
    // if (obj == null) {
    // return false;
    // }
    //
    // if (obj == this) {
    // return true;
    // }
    //
    // if (obj.getClass() != getClass()) {
    // return false;
    // }
    //
    // EleveBe1dPersonnalisationParcours other = (EleveBe1dPersonnalisationParcours) obj;
    //
    // return new EqualsBuilder().append(getId(), other.getId()).isEquals();
    // }
    //
    // @Override
    // public int hashCode() {
    // return new HashCodeBuilder().append(getId()).toHashCode();
    // }
}
