/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.nomenclature;

import org.apache.commons.lang3.StringUtils;

/** Objet métier pour modéliser l'état des collèges de secteur. */
public class EtatCollegeSecteur implements java.io.Serializable {

    /** serialVersionUID. */
    public static final long serialVersionUID = -3798877593939245974L;

    /** État saisie manuelle. */
    public static final String SAISIE_MANUELLE = "manuel";

    /** État calcul abouti. */
    public static final String CALCUL_ABOUTI = "autoAbouti";

    /** État calcul non abouti. */
    public static final String CALCUL_NON_ABOUTI = "autoNonAbouti";

    /** État calcul impossible. */
    public static final String CALCUL_IMPOSSIBLE = "autoImpossible";

    /** État calcul abouti hors du département. */
    public static final String CALCUL_ABOUTI_HORS_DEPARTEMENT = "autoAboutiHorsDep";

    /** Code de l'état. */
    private String code;

    /** Libellé court de l'état. */
    private String libelleCourt;

    /** Libellé long de l'état. */
    private String libelleLong;

    /** Icône à afficher pour l'état collège de secteur. */
    private String icone;

    /**
     * Constructeur.
     */
    public EtatCollegeSecteur() {
    }

    /**
     * Constructeur.
     * 
     * @param code
     *            code
     */
    public EtatCollegeSecteur(String code) {
        this.code = code;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the libelleCourt
     */
    public String getLibelleCourt() {
        return libelleCourt;
    }

    /**
     * @param libelleCourt
     *            the libelleCourt to set
     */
    public void setLibelleCourt(String libelleCourt) {
        this.libelleCourt = libelleCourt;
    }

    /**
     * @return the libelleLong
     */
    public String getLibelleLong() {
        return libelleLong;
    }

    /**
     * @param libelleLong
     *            the libelleLong to set
     */
    public void setLibelleLong(String libelleLong) {
        this.libelleLong = libelleLong;
    }

    /**
     * @return the icône
     */
    public String getIcone() {
        return this.icone;
    }

    /**
     * @param icone
     *            the icône to set
     */
    public void setIcone(String icone) {
        this.icone = icone;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof EtatCollegeSecteur) {
            EtatCollegeSecteur etat = (EtatCollegeSecteur) obj;
            return StringUtils.equals(this.getCode(), etat.getCode());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.getCode().hashCode();
    }

    @Override
    public String toString() {
        return this.getCode();
    }
}
