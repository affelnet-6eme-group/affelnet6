/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.nomenclature;

import java.util.ArrayList;
import java.util.List;

/** Décisions de passage possibles. */
public enum DecisionPassageEnum {

    // @formatter:off
    /** Passage en 6e. */
    PASSAGE_6EME("P", "Passage en 6ème", true),
    /** Appel. */
    APPEL("A", "Appel", true),
    /** Maintien. */
    MAINTENU("M", "Maintien à l'école primaire", false),
    /** Vide. */
    AUCUNE( "", "", true);
    // @formatter:on

    /** Code correspondant à la décision de passage. */
    private String code;

    /** Libellé correspondant à la décision de passage. */
    private String libelle;

    /** Indique si le voeux familles sont actifs pour cette décision de passage. */
    private boolean voeuxFamillesActifs;

    /**
     * Constructeur.
     * 
     * @param code
     *            code de la décision de passage
     * @param libelle
     *            libellé de la décision de passage
     * @param voeuxFamillesActifs
     *            indique si les voeux de familles sont actifs
     */
    DecisionPassageEnum(String code, String libelle, boolean voeuxFamillesActifs) {
        this.code = code;
        this.libelle = libelle;
        this.voeuxFamillesActifs = voeuxFamillesActifs;
    }

    /**
     * Retourne la liste des décisions de passages permettant les voeux familles.
     * 
     * @return la liste des décisions de passages permettant les voeux familles
     */
    public List<DecisionPassageEnum> getDecisionsPassagesAvecVoeux() {
        return getDecisionsPassages(true);
    }

    /**
     * Retourne la liste des décisions de passages ne permettant pas de saisir les voeux familles.
     * 
     * @return la liste des décisions de passages ne permettant pas de saisir les voeux familles
     */
    public List<DecisionPassageEnum> getDecisionsPassagesSansVoeux() {
        return getDecisionsPassages(false);
    }

    /**
     * Liste les décisions de passage.
     * 
     * @param isVoeuxFamillesActifs
     *            indique si les voeux sont actifs
     * @return la liste des décisions de passage
     */
    private List<DecisionPassageEnum> getDecisionsPassages(boolean isVoeuxFamillesActifs) {
        ArrayList<DecisionPassageEnum> decisionsPassages = new ArrayList<>();
        for (DecisionPassageEnum curDecisionPassage : values()) {
            if (curDecisionPassage.isVoeuxFamillesActifs() == isVoeuxFamillesActifs) {
                decisionsPassages.add(curDecisionPassage);
            }
        }
        return decisionsPassages;
    }

    /**
     * Détermine la décision de passage correspondant au code spécifié.
     * 
     * @param decisionPassage
     *            la décision de passage à retrouver
     * @return
     *         la décision de passage correspondante
     */
    public static DecisionPassageEnum getByDecisionPassage(DecisionPassage decisionPassage) {
        return getByCode(decisionPassage != null ? decisionPassage.getCod() : null);
    }

    /**
     * Détermine la décision de passage correspondant au code spécifié.
     * 
     * @param code
     *            le code de la décision de passage à retrouver
     * @return
     *         la décision de passage correspondante
     */
    public static DecisionPassageEnum getByCode(String code) {
        for (DecisionPassageEnum curDecisionPassage : values()) {
            if (curDecisionPassage.getCode().equalsIgnoreCase(code)) {
                return curDecisionPassage;
            }
        }
        return DecisionPassageEnum.AUCUNE;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @return the libelle
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * @return the voeuxFamillesActifs
     */
    public boolean isVoeuxFamillesActifs() {
        return voeuxFamillesActifs;
    }
}
