/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.nomenclature;

import org.apache.commons.lang3.StringUtils;

/** Code de l'état de décision d'affectation. */
public enum CodeEtatDecision {
    /** Décision non traitée. */
    NON_TRAITE("null", "Non traité"),
    /** Décision affectée. */
    AFFECTE("A", "Affecté"),
    /** Décision refusée. */
    REFUSE("R", "Refusé");

    /** Code. */
    private final String code;
    /** Libellé. */
    private final String libelle;

    private CodeEtatDecision(String code, String libelle) {
        this.code = code;
        this.libelle = libelle;
    }

    public String getCode() {
        return code;
    }

    public String getLibelle() {
        return libelle;
    }

    public static CodeEtatDecision getCodeEtatDecisionByCode(String code) {
        for (CodeEtatDecision codeEtat : CodeEtatDecision.values()) {
            if (StringUtils.equals(codeEtat.getCode(), code)) {
                return codeEtat;
            }
        }

        return NON_TRAITE;
    }

}
