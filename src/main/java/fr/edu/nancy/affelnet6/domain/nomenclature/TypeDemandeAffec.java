/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.nomenclature;

import java.util.HashSet;
import java.util.Set;

import fr.edu.nancy.affelnet6.domain.dossier.DemandeAffectation;

/** Type de demande d'affectation. */
public class TypeDemandeAffec implements java.io.Serializable {

    /** Code pour les demandes d'affectation dans un collège public du département. */
    public static final String CODE_TYPE_DEMANDE_DANS_COLLEGE_PUBLIC_DEPT = "PUBDEP";

    /** Code pour les demandes d'affectation hors d'un collège. */
    public static final String CODE_TYPE_DEMANDE_HORS_COLLEGE_PUBLIC_DEPT = "RETIRE";

    /** Code pour les demandes de SEGPA. */
    public static final String CODE_TYPE_DEMANDE_SEGPA = "6SEGPA";

    /** Code pour les demandes d'orientation en ULIS. */
    public static final String CODE_TYPE_DEMANDE_ULIS = "ULIS";

    /** Tag de sérialisation. */
    private static final long serialVersionUID = -3489887102058651679L;

    /** Code du type de la demande. */
    private String code;

    /** Libellé court. */
    private String libelleCourt;

    /** Libellé long. */
    private String libelleLong;

    /** Ensemble des demandes d'affectation de ce type. */
    private Set<DemandeAffectation> demandeAffectation = new HashSet<DemandeAffectation>(0);

    /** Constructeur simple. */
    public TypeDemandeAffec() {
    }

    /**
     * Constructeur avec paramètres.
     * 
     * @param code
     *            Code du type de la demande
     * @param libelleCourt
     *            Libellé court
     * @param libelleLong
     *            Libellé long
     * @param demandeAffectation
     *            Ensemble des demandes d'affectation de ce type
     */
    public TypeDemandeAffec(String code, String libelleCourt, String libelleLong,
            Set<DemandeAffectation> demandeAffectation) {
        this.code = code;
        this.libelleCourt = libelleCourt;
        this.libelleLong = libelleLong;
        this.demandeAffectation = demandeAffectation;
    }

    /** @return Code du type de la demande */
    public String getCode() {
        return this.code;
    }

    /**
     * @param code
     *            Code du type de la demande
     */
    public void setCode(String code) {
        this.code = code;
    }

    /** @return Libellé court */
    public String getLibelleCourt() {
        return this.libelleCourt;
    }

    /**
     * @param libelleCourt
     *            Libellé court
     */
    public void setLibelleCourt(String libelleCourt) {
        this.libelleCourt = libelleCourt;
    }

    /** @return Libellé long */
    public String getLibelleLong() {
        return this.libelleLong;
    }

    /**
     * @param libelleLong
     *            Libellé long
     */
    public void setLibelleLong(String libelleLong) {
        this.libelleLong = libelleLong;
    }

    /** @return Ensemble des demandes d'affectation de ce type */
    public Set<DemandeAffectation> getdemandeAffectation() {
        return this.demandeAffectation;
    }

    /**
     * @param demandeAffectation
     *            Ensemble des demandes d'affectation de ce type
     */
    public void setdemandeAffectation(Set<DemandeAffectation> demandeAffectation) {
        this.demandeAffectation = demandeAffectation;
    }
}
