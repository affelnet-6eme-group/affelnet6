/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.nomenclature;

/**
 * Civilité.
 */
public class Civilite implements java.io.Serializable {

    /** Tag de sérialisation. */
    private static final long serialVersionUID = -2956525956875692953L;

    /** Id civilité. */
    private int idCivilite;

    /** Code civilité. */
    private Character codeCivilite;

    /** libellé long civilité. */
    private String llCivilite;

    /** libellé court civilité. */
    private String lcCivilite;

    /** libellé édition civilité. */
    private String leCivilite;

    /**
     * @return the idCivilite
     */
    public int getIdCivilite() {
        return idCivilite;
    }

    /**
     * @param idCivilite
     *            the idCivilite to set
     */
    public void setIdCivilite(int idCivilite) {
        this.idCivilite = idCivilite;
    }

    /**
     * @return the codeCivilite
     */
    public Character getCodeCivilite() {
        return codeCivilite;
    }

    /**
     * @param codeCivilite
     *            the codeCivilite to set
     */
    public void setCodeCivilite(Character codeCivilite) {
        this.codeCivilite = codeCivilite;
    }

    /**
     * @return the llCivilite
     */
    public String getLlCivilite() {
        return llCivilite;
    }

    /**
     * @param llCivilite
     *            the llCivilite to set
     */
    public void setLlCivilite(String llCivilite) {
        this.llCivilite = llCivilite;
    }

    /**
     * @return the lcCivilite
     */
    public String getLcCivilite() {
        return lcCivilite;
    }

    /**
     * @param lcCivilite
     *            the lcCivilite to set
     */
    public void setLcCivilite(String lcCivilite) {
        this.lcCivilite = lcCivilite;
    }

    /**
     * @return the leCivilite
     */
    public String getLeCivilite() {
        return leCivilite;
    }

    /**
     * @param leCivilite
     *            the leCivilite to set
     */
    public void setLeCivilite(String leCivilite) {
        this.leCivilite = leCivilite;
    }
}
