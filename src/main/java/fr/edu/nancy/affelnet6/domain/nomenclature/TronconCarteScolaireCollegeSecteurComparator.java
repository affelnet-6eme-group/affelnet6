/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.nomenclature;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;

/**
 * Compare 2 tronçons de la carte scolaire en prenant en compte les collèges de secteur.<br>
 * Deux tronçons sont identiques si tous les champs sont identiques et qu'ils ont les mêmes collèges de secteurs.
 */
public class TronconCarteScolaireCollegeSecteurComparator extends TronconCarteScolaireComparator implements
        Comparator<TronconCarteScolaire>, Serializable {

    /** Serial ID. */
    private static final long serialVersionUID = -7439496230448374750L;

    @Override
    public int compare(TronconCarteScolaire troncon1, TronconCarteScolaire troncon2) {

        // même objet
        if (troncon1 == troncon2) {
            return 0;
        }

        if (troncon1 == null) {
            return -1;
        }

        if (troncon2 == null) {
            return 1;
        }

        int compareValeur = super.compare(troncon1, troncon2);
        if (compareValeur != 0) {
            return compareValeur;
        }

        // colleges de secteur
        Set<EtablissementSconet> collegesSecteur1 = troncon1.getColleges();
        Set<EtablissementSconet> collegesSecteur2 = troncon2.getColleges();

        if (CollectionUtils.isEmpty(collegesSecteur1) ^ CollectionUtils.isEmpty(collegesSecteur2)) {
            return -1;
        }

        if (CollectionUtils.size(collegesSecteur1) != CollectionUtils.size(collegesSecteur2)) {
            return -1;
        }

        // même taille donc on vérifie si les collèges sont identiques
        boolean tousSimilaire = true;
        for (EtablissementSconet college1 : collegesSecteur1) {
            boolean estDansLesDeux = false;
            for (EtablissementSconet college2 : collegesSecteur2) {
                estDansLesDeux |= college2.getCodeRne().equals(college1.getCodeRne());
            }
            tousSimilaire &= estDansLesDeux;
        }

        if (!tousSimilaire) {
            return -1;
        }

        // si on arrive là, c'est que les tronçons sont identiques
        return 0;
    }
}
