/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.nomenclature;

/**
 * CommuneInseePttId generated by hbm2java.
 */
public class CommuneInseePttId implements java.io.Serializable {

    /** Tag de sérialisation. */
    private static final long serialVersionUID = 1L;

    /** Identifiant du code postal. */
    private Integer cpId;

    /** Identifiant de la commune INSEE. */
    private Integer communeInseeId;

    /**
     * Constructeur par défaut.
     */
    public CommuneInseePttId() {
    }

    /**
     * Constructeur.
     * 
     * @param cpId
     *            identifiant du code postal
     * @param communeInseeId
     *            identifiant de la commune INSEE
     */
    public CommuneInseePttId(Integer cpId, Integer communeInseeId) {
        this.cpId = cpId;
        this.communeInseeId = communeInseeId;
    }

    /**
     * @return the cpId
     */
    public Integer getCpId() {
        return this.cpId;
    }

    /**
     * @param cpId
     *            the cpId to set
     */
    public void setCpId(Integer cpId) {
        this.cpId = cpId;
    }

    /**
     * @return the communeInseeId
     */
    public Integer getCommuneInseeId() {
        return this.communeInseeId;
    }

    /**
     * @param communeInseeId
     *            the communeInseeId to set
     */
    public void setCommuneInseeId(Integer communeInseeId) {
        this.communeInseeId = communeInseeId;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null) {
            return false;
        }
        if (!(other instanceof CommuneInseePttId)) {
            return false;
        }
        CommuneInseePttId castOther = (CommuneInseePttId) other;

        return this.getCpId() != null && this.getCpId().equals(castOther.getCpId())
                && this.getCommuneInseeId() != null
                && this.getCommuneInseeId().equals(castOther.getCommuneInseeId());
    }

    @Override
    public int hashCode() {
        int result = 17;

        result = 37 * result + (getCpId() == null ? 0 : this.getCpId().hashCode());
        result = 37 * result + (getCommuneInseeId() == null ? 0 : this.getCommuneInseeId().hashCode());
        return result;
    }

}
