/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.nomenclature;

import java.io.Serializable;
import java.util.Comparator;

import org.apache.commons.lang3.StringUtils;

/**
 * Compare 2 tronçons de la carte scolaire sans les collèges de secteur.<br>
 * Deux tronçons sont identiques si tous les champs autres que collèges de secteur sont identiques.
 */
public class TronconCarteScolaireComparator implements Comparator<TronconCarteScolaire>, Serializable {

    /** Serial ID. */
    private static final long serialVersionUID = 5431541183207779818L;

    @Override
    public int compare(TronconCarteScolaire troncon1, TronconCarteScolaire troncon2) {

        // même objet
        if (troncon1 == troncon2) {
            return 0;
        }

        if (troncon1 == null) {
            return -1;
        }

        if (troncon2 == null) {
            return 1;
        }

        // département
        DeptMen d1 = troncon1.getDept();
        DeptMen d2 = troncon2.getDept();
        if ((d1 != null && d2 == null) || (d1 == null && d2 != null)) {
            return -1;
        } else if (d1 != null && d1.getDeptCode().compareTo(d2.getDeptCode()) != 0) {
            return d1.getDeptCode().compareTo(d2.getDeptCode());
        }

        // commune qui ne peut pas être nulle
        CommuneInsee c1 = troncon1.getCommuneInsee();
        CommuneInsee c2 = troncon2.getCommuneInsee();
        if ((c1 != null && c2 == null) || (c1 == null && c2 != null)) {
            return -1;
        } else if (c1 != null && c1.compareTo(c2) != 0) {
            return c1.compareTo(c2);
        }

        // flag commune unique
        if (!troncon1.getFlagCommmuneClgUnique().equals(troncon2.getFlagCommmuneClgUnique())) {
            return troncon1.getFlagCommmuneClgUnique().compareTo(troncon2.getFlagCommmuneClgUnique());
        }

        // code postal
        CodePostal cp1 = troncon1.getCodePostal();
        CodePostal cp2 = troncon2.getCodePostal();

        if ((cp1 != null && cp2 == null) || (cp1 == null && cp2 != null)) {
            return -1;
        } else if (cp1 != null && cp1.compareTo(cp2) != 0) {
            return cp1.compareTo(cp2);
        }

        // ligne 3 de l'adresse
        String ligneA1 = troncon1.getLigne3Adr() == null ? null : troncon1.getLigne3Adr().trim();
        String ligneA2 = troncon2.getLigne3Adr() == null ? null : troncon2.getLigne3Adr().trim();

        int resCompareLigneA = compareLignes(ligneA1, ligneA2);

        if (resCompareLigneA != 0) {
            return resCompareLigneA;
        }

        // ligne 4 de l'adresse
        String ligneB1 = troncon1.getLigne4Adr() == null ? null : troncon1.getLigne4Adr().trim();
        String ligneB2 = troncon2.getLigne4Adr() == null ? null : troncon2.getLigne4Adr().trim();

        int resCompareLigneB = compareLignes(ligneB1, ligneB2);

        if (resCompareLigneB != 0) {
            return resCompareLigneB;
        }

        // numéro de voie de début
        Integer numVoieDebut1 = troncon1.getNumeroVoieDebut();
        Integer numVoieDebut2 = troncon2.getNumeroVoieDebut();

        if ((numVoieDebut1 != null && numVoieDebut2 == null) || (numVoieDebut1 == null && numVoieDebut2 != null)) {
            return -1;
        } else if (numVoieDebut1 != null && numVoieDebut1.compareTo(numVoieDebut2) != 0) {
            return numVoieDebut1.compareTo(numVoieDebut2);
        }

        // indice de répétition de début
        String indiceRepetitionDebut1 = troncon1.getIndiceRepetitionDebut();
        String indiceRepetitionDebut2 = troncon2.getIndiceRepetitionDebut();
        // si l'indice de répétition de début est vide ou null dans un des deux tronçons
        if (StringUtils.isEmpty(indiceRepetitionDebut1) ^ StringUtils.isEmpty(indiceRepetitionDebut2)) {
            if (indiceRepetitionDebut1 != null) {
                return 1;
            }
            // indiceRepetitionDebut2 != null
            else {
                return -1;
            }
        }

        if (indiceRepetitionDebut1 != null && indiceRepetitionDebut2 != null
                && !indiceRepetitionDebut1.equalsIgnoreCase(indiceRepetitionDebut2)) {
            return indiceRepetitionDebut1.compareTo(indiceRepetitionDebut2);
        }

        // numéro de voie de fin
        Integer numVoieFin1 = troncon1.getNumeroVoieFin();
        Integer numVoieFin2 = troncon2.getNumeroVoieFin();

        if ((numVoieFin1 != null && numVoieFin2 == null) || (numVoieFin1 == null && numVoieFin2 != null)) {
            return -1;
        } else if (numVoieFin1 != null && numVoieFin1.compareTo(numVoieFin2) != 0) {
            return numVoieFin1.compareTo(numVoieFin2);
        }

        // indice de répétition de fin
        String indiceRepetitionFin1 = troncon1.getIndiceRepetitionFin();
        String indiceRepetitionFin2 = troncon2.getIndiceRepetitionFin();
        // si l'indice de répétition de début est vide ou null dans un des deux tronçons
        if (StringUtils.isEmpty(indiceRepetitionFin1) ^ StringUtils.isEmpty(indiceRepetitionFin2)) {
            if (indiceRepetitionFin1 != null) {
                return 1;
            }
            // indiceRepetitionDebut2 != null
            else {
                return -1;
            }
        }

        if (indiceRepetitionFin1 != null && indiceRepetitionFin2 != null
                && !indiceRepetitionFin1.equalsIgnoreCase(indiceRepetitionFin2)) {
            return indiceRepetitionFin1.compareTo(indiceRepetitionFin2);
        }

        // parite
        Integer parite1 = troncon1.getParite();
        Integer parite2 = troncon2.getParite();

        if ((parite1 != null && parite2 == null) || (parite1 == null && parite2 != null)) {
            return -1;
        } else if (parite1 != null && parite1.compareTo(parite2) != 0) {
            return parite1.compareTo(parite2);
        }

        // si on arrive là, c'est que les tronçons sont identiques
        return 0;
    }

    /**
     * Compare les lignes des deux tronçons.
     * 
     * @param ligne1
     *            la ligne 1
     * @param ligne2
     *            la ligne 2
     * @return le résultat de la comparaison
     */
    private int compareLignes(String ligne1, String ligne2) {
        if (StringUtils.isEmpty(ligne1)) {
            if (StringUtils.isNotEmpty(ligne2)) {
                return -1;
            } else {
                return 0;
            }
        } else if (StringUtils.isEmpty(ligne2)) {
            return 1;
        } else {
            return ligne1.compareTo(ligne2);
        }
    }
}
