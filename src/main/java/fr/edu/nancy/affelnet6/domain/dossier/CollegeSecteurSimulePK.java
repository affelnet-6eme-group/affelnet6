/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.dossier;

import java.io.Serializable;

/** Objet clé primaire des collèges de secteur simulés. */
public class CollegeSecteurSimulePK implements Serializable {

    /** Le serial ID. */
    private static final long serialVersionUID = 5182200555684209130L;
    /** Numéro de dossier de l'élève. */
    private Long numDossier;

    /** Id de l'établissement. */
    private Long identifiantEtablissement;

    /** Constructeur par défaut. */
    public CollegeSecteurSimulePK() {
    }

    /**
     * Constructeur.
     * 
     * @param numDossier
     *            le numéro de dossier de l'élève
     * @param identifiantEtablissement
     *            l'identifiant de l'établissement
     */
    public CollegeSecteurSimulePK(Long numDossier, Long identifiantEtablissement) {
        this.numDossier = numDossier;
        this.identifiantEtablissement = identifiantEtablissement;
    }

    /**
     * @return the numDossier
     */
    public Long getNumDossier() {
        return numDossier;
    }

    /**
     * @param numDossier
     *            the numDossier to set
     */
    public void setNumDossier(Long numDossier) {
        this.numDossier = numDossier;
    }

    /**
     * @return the identifiantEtablissement
     */
    public Long getIdentifiantEtablissement() {
        return identifiantEtablissement;
    }

    /**
     * @param identifiantEtablissement
     *            the identifiantEtablissement to set
     */
    public void setIdentifiantEtablissement(Long identifiantEtablissement) {
        this.identifiantEtablissement = identifiantEtablissement;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((identifiantEtablissement == null) ? 0 : identifiantEtablissement.hashCode());
        result = prime * result + ((numDossier == null) ? 0 : numDossier.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        CollegeSecteurSimulePK other = (CollegeSecteurSimulePK) obj;
        if (identifiantEtablissement == null) {
            if (other.identifiantEtablissement != null) {
                return false;
            }
        } else if (!identifiantEtablissement.equals(other.identifiantEtablissement)) {
            return false;
        }
        if (numDossier == null) {
            if (other.numDossier != null) {
                return false;
            }
        } else if (!numDossier.equals(other.numDossier)) {
            return false;
        }
        return true;
    }

}
