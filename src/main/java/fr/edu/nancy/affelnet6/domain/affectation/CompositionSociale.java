/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.affectation;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fr.edu.nancy.affelnet6.domain.nomenclature.EtablissementSconet;
import fr.edu.nancy.affelnet6.domain.nomenclature.Formation;
import fr.edu.nancy.affelnet6.domain.nomenclature.OffreFormation;

/** Objet décrivant la composition sociale (indicateur PCS) d'un ensemble d'élèves. */
public class CompositionSociale {
    /** La constante 100, pour le calcul de pourcentage. */
    private static final BigDecimal CENT = BigDecimal.valueOf(100);

    /** Le nombre total d'élève de l'ensemble. */
    private int totalEleve;
    /** Le nombre d'élève par indicateur de PCS. */
    private int[] compteurParIndicateur = new int[IndicateurPcsEnum.values().length];
    /** La capacité de l'établissement. */
    private int capacite = 0;

    /**
     * Constructeur.
     * 
     * @param etab
     *            l'établissement (pour la détermination de la capacité d'accueil)
     */
    public CompositionSociale(EtablissementSconet etab) {
        if (etab != null) {
            for (OffreFormation of : etab.getOffreFormations()) {
                if (of.getFormation() != null && Formation.CODE_MEF_6EME.equals(of.getFormation().getCode())) {
                    capacite = of.getCapAcc();
                    break;
                }
            }
        }
    }

    /**
     * Ajoute un élève à l'ensemble.
     * 
     * @param indicateur
     *            indicateur PCS
     */
    public void ajouteEleve(IndicateurPcsEnum indicateur) {
        compteurParIndicateur[indicateur.ordinal()]++;
        totalEleve++;
    }

    /**
     * @param indicateur
     *            l'indicateur
     * @return le nombre d'élève de l'ensemble dont l'indicateur PCS est égal à l'indicateur donné en paramètre.
     */
    public int getNombreParIndicateur(IndicateurPcsEnum indicateur) {
        return compteurParIndicateur[indicateur.ordinal()];
    }

    /**
     * Set le compteurParIndicateur.
     * 
     * @param compteurParIndicateur
     *            le compteurParIndicateur
     */
    public void setCompteurParIndicateur(int[] compteurParIndicateur) {
        this.compteurParIndicateur = compteurParIndicateur;
        for (int i : compteurParIndicateur) {
            totalEleve += i;
        }
    }

    /**
     * Calcul les pourcentages en ajustant les résultats pour que la somme fasse 100.
     * 
     * @return la liste de pourcentage par composition sociale
     */
    public List<Pourcentage> getPourcentages() {
        BigDecimal total = new BigDecimal(totalEleve);
        List<Pourcentage> ret = new ArrayList<>(5);

        int somme = 0;
        for (IndicateurPcsEnum i : IndicateurPcsEnum.values()) {
            Pourcentage p = new Pourcentage();
            int nb = compteurParIndicateur[i.ordinal()];
            p.nombre = nb;

            int pourcentage = 0;
            if (totalEleve != 0) {
                BigDecimal n = new BigDecimal(nb);
                n = n.multiply(CENT).divide(total, RoundingMode.HALF_EVEN);
                pourcentage = n.intValue();
            }
            p.arrondi = pourcentage;
            p.largeurOptimale = Math.max(Pourcentage.LARGEUR_MIN, pourcentage);
            somme += p.largeurOptimale;
            ret.add(p);
        }

        Pourcentage p = null;
        while (somme > CENT.intValue()) {
            p = Collections.max(ret);
            p.largeurOptimale--;
            somme--;
        }

        while (somme < CENT.intValue()) {
            p = Collections.min(ret);
            p.largeurOptimale++;
            somme++;
        }

        return ret;
    }

    /**
     * Classe de gestion des pourcentages pour l'affichage sous forme de barres.
     * 
     * 
     *
     */
    public static class Pourcentage implements Comparable<Pourcentage> {

        /** La largeur minimale de la barre en pourcentage. */
        private static final int LARGEUR_MIN = 8;
        /** le nombre. */
        int nombre;

        /** l'arrondi. */
        int arrondi;
        /** la largeur pour l'affichage. */
        int largeurOptimale;

        @Override
        public int compareTo(Pourcentage o) {
            return largeurOptimale - o.largeurOptimale;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof Pourcentage) {
                return largeurOptimale == ((Pourcentage) obj).largeurOptimale;
            }
            return false;
        }

        @Override
        public int hashCode() {
            return largeurOptimale;
        }

        /**
         * @return l'arrondi
         */
        public int getArrondi() {
            return arrondi;
        }

        /**
         * @return la largeur optimale
         */
        public int getLargeurOptimale() {
            return largeurOptimale;
        }

        /**
         * @return le nombre
         */
        public int getNombre() {
            return nombre;
        }

        @Override
        public String toString() {
            return arrondi + "(" + largeurOptimale + ")";
        }
    }

    /**
     * @return the totalEleve
     */
    public int getTotalEleve() {
        return totalEleve;
    }

    /**
     * @param totalEleve
     *            the totalEleve to set
     */
    public void setTotalEleve(int totalEleve) {
        this.totalEleve = totalEleve;
    }

    /**
     * @return la capacité
     */
    public int getCapacite() {
        return capacite;
    }

    /**
     * @param capacite
     *            the capacite to set
     */
    public void setCapacite(int capacite) {
        this.capacite = capacite;
    }

    /**
     * @return the compteurParIndicateur
     */
    public int[] getCompteurParIndicateur() {
        return compteurParIndicateur;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i : compteurParIndicateur) {
            sb.append(i).append(",");
        }
        sb.append("]");
        return sb.toString();
    }
}
