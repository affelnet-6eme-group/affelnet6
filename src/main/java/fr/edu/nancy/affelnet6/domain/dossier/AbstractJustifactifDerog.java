/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.dossier;

import fr.edu.nancy.affelnet6.domain.nomenclature.DeptMen;
import fr.edu.nancy.affelnet6.domain.nomenclature.MotifDerog;

/**
 * Classe abstraite pour les justificatifs de dérog et les sauvegardes.
 */
public abstract class AbstractJustifactifDerog {

    /**
     * Identifiant de la demande de dérogation.
     */
    protected Long idDemandeDerog;

    /**
     * Motif de la dérogation.
     */
    protected MotifDerog idMotifDerog;

    /**
     * Département du MEN.
     */
    protected DeptMen dept;

    /**
     * @return the idDemandeDerog
     */
    public Long getIdDemandeDerog() {
        return this.idDemandeDerog;
    }

    /**
     * @param idDemandeDerog
     *            the idDemandeDerog to set
     */
    public void setIdDemandeDerog(Long idDemandeDerog) {
        this.idDemandeDerog = idDemandeDerog;
    }

    /**
     * @return the idMotifDerog
     */
    public MotifDerog getIdMotifDerog() {
        return this.idMotifDerog;
    }

    /**
     * @param idMotifDerog
     *            the idMotifDerog to set
     */
    public void setIdMotifDerog(MotifDerog idMotifDerog) {
        this.idMotifDerog = idMotifDerog;
    }

    /**
     * @return the dept
     */
    public DeptMen getDept() {
        return this.dept;
    }

    /**
     * @param dept
     *            the dept to set
     */
    public void setDept(DeptMen dept) {
        this.dept = dept;
    }
}
