/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.nomenclature;

import java.util.Date;

import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Bean métier décrivant un lien d'appartenance d'un établissement d'origine à une circonscription.
 * Note : il s'agit d'une facilité par rapport au lien Zone - Zone-Etablissement - EtablissementOrigine
 * dédié aux circonscriptions.
 * */
public class CirconscriptionEtabOrigine implements java.io.Serializable {

    /** Serial tag. */
    private static final long serialVersionUID = 5156440036564295345L;

    /** Identifiant du lien. */
    private CirconscriptionEtabOrigineId id;

    /** Établissement d'origine. */
    private EtablissementOrigine etablissementOrigine;

    /** Libellé court de la circonscription. */
    private String libelleCourtCirco;
    /** Libellé long de la circonscription. */
    private String libelleLongCirco;
    /** Date d'ouverture du lien. */
    private Date dateOuvertureLien;
    /** Date de fermeture du lien. */
    private Date dateFermetureLien;

    /** Pays de l'établissement. */
    private Pays pays;
    /** Commune Insee de l'établissement. */
    private CommuneInsee communeInsee;
    /** Type d'établissement. */
    private TypeEtablissement typeEtablissement;
    /** Académie de l'établissement. */
    private Academie academie;
    /** Secteur de l'établissement. */
    private Secteur secteur;
    /** Code postal de l'établissement. */
    private CodePostal codePostal;
    /** Code min de l'établissement. */
    private String codMin;
    /** Code ntr de l'établissement. */
    private String codNtr;
    /** Type ctr de l'établissement. */
    private String typCtr;
    /** Sigle de l'établissement. */
    private String sgl;
    /** Dénomination principale de l'établissement. */
    private String dnmPri;
    /** Patronyme de l'établissement. */
    private String pat;
    /** Libellé de commune étrangère de l'établissement. */
    private String comEtr;

    /** Constructeur du bean. */
    public CirconscriptionEtabOrigine() {
    }

    /**
     * @return the id
     */
    public CirconscriptionEtabOrigineId getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(CirconscriptionEtabOrigineId id) {
        this.id = id;
    }

    /**
     * @return the etablissementOrigine
     */
    public EtablissementOrigine getEtablissementOrigine() {
        return etablissementOrigine;
    }

    /**
     * @param etablissementOrigine
     *            the etablissementOrigine to set
     */
    public void setEtablissementOrigine(EtablissementOrigine etablissementOrigine) {
        this.etablissementOrigine = etablissementOrigine;
    }

    /**
     * @return the libelleCourtCirco
     */
    public String getLibelleCourtCirco() {
        return libelleCourtCirco;
    }

    /**
     * @param libelleCourtCirco
     *            the libelleCourtCirco to set
     */
    public void setLibelleCourtCirco(String libelleCourtCirco) {
        this.libelleCourtCirco = libelleCourtCirco;
    }

    /**
     * @return the libelleLongCirco
     */
    public String getLibelleLongCirco() {
        return libelleLongCirco;
    }

    /**
     * @param libelleLongCirco
     *            the libelleLongCirco to set
     */
    public void setLibelleLongCirco(String libelleLongCirco) {
        this.libelleLongCirco = libelleLongCirco;
    }

    /**
     * @return the dateOuvertureLien
     */
    public Date getDateOuvertureLien() {
        return dateOuvertureLien;
    }

    /**
     * @param dateOuvertureLien
     *            the dateOuvertureLien to set
     */
    public void setDateOuvertureLien(Date dateOuvertureLien) {
        this.dateOuvertureLien = dateOuvertureLien;
    }

    /**
     * @return the dateFermetureLien
     */
    public Date getDateFermetureLien() {
        return dateFermetureLien;
    }

    /**
     * @param dateFermetureLien
     *            the dateFermetureLien to set
     */
    public void setDateFermetureLien(Date dateFermetureLien) {
        this.dateFermetureLien = dateFermetureLien;
    }

    /**
     * @return the pays
     */
    public Pays getPays() {
        return pays;
    }

    /**
     * @param pays
     *            the pays to set
     */
    public void setPays(Pays pays) {
        this.pays = pays;
    }

    /**
     * @return the communeInsee
     */
    public CommuneInsee getCommuneInsee() {
        return communeInsee;
    }

    /**
     * @param communeInsee
     *            the communeInsee to set
     */
    public void setCommuneInsee(CommuneInsee communeInsee) {
        this.communeInsee = communeInsee;
    }

    /**
     * @return the typeEtablissement
     */
    public TypeEtablissement getTypeEtablissement() {
        return typeEtablissement;
    }

    /**
     * @param typeEtablissement
     *            the typeEtablissement to set
     */
    public void setTypeEtablissement(TypeEtablissement typeEtablissement) {
        this.typeEtablissement = typeEtablissement;
    }

    /**
     * @return the academie
     */
    public Academie getAcademie() {
        return academie;
    }

    /**
     * @param academie
     *            the academie to set
     */
    public void setAcademie(Academie academie) {
        this.academie = academie;
    }

    /**
     * @return the secteur
     */
    public Secteur getSecteur() {
        return secteur;
    }

    /**
     * @param secteur
     *            the secteur to set
     */
    public void setSecteur(Secteur secteur) {
        this.secteur = secteur;
    }

    /**
     * @return the codePostal
     */
    public CodePostal getCodePostal() {
        return codePostal;
    }

    /**
     * @param codePostal
     *            the codePostal to set
     */
    public void setCodePostal(CodePostal codePostal) {
        this.codePostal = codePostal;
    }

    /**
     * @return the codMin
     */
    public String getCodMin() {
        return codMin;
    }

    /**
     * @param codMin
     *            the codMin to set
     */
    public void setCodMin(String codMin) {
        this.codMin = codMin;
    }

    /**
     * @return the codNtr
     */
    public String getCodNtr() {
        return codNtr;
    }

    /**
     * @param codNtr
     *            the codNtr to set
     */
    public void setCodNtr(String codNtr) {
        this.codNtr = codNtr;
    }

    /**
     * @return the typCtr
     */
    public String getTypCtr() {
        return typCtr;
    }

    /**
     * @param typCtr
     *            the typCtr to set
     */
    public void setTypCtr(String typCtr) {
        this.typCtr = typCtr;
    }

    /**
     * @return the sgl
     */
    public String getSgl() {
        return sgl;
    }

    /**
     * @param sgl
     *            the sgl to set
     */
    public void setSgl(String sgl) {
        this.sgl = sgl;
    }

    /**
     * @return the dnmPri
     */
    public String getDnmPri() {
        return dnmPri;
    }

    /**
     * @param dnmPri
     *            the dnmPri to set
     */
    public void setDnmPri(String dnmPri) {
        this.dnmPri = dnmPri;
    }

    /**
     * @return the pat
     */
    public String getPat() {
        return pat;
    }

    /**
     * @param pat
     *            the pat to set
     */
    public void setPat(String pat) {
        this.pat = pat;
    }

    /**
     * @return the comEtr
     */
    public String getComEtr() {
        return comEtr;
    }

    /**
     * @param comEtr
     *            the comEtr to set
     */
    public void setComEtr(String comEtr) {
        this.comEtr = comEtr;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null) {
            return false;
        }
        if (!(other instanceof CirconscriptionEtabOrigine)) {
            return false;
        }
        CirconscriptionEtabOrigine castOther = (CirconscriptionEtabOrigine) other;

        return this.getId() != null && this.getId().equals(castOther.getId());
    }

    @Override
    public int hashCode() {
        return this.getId() == null ? 0 : new HashCodeBuilder().append(this.getId().getCodUaiCirco())
                .append(this.getId().getCodUaiEtb()).toHashCode();
    }
}
