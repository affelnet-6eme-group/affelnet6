/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.nomenclature;

import java.io.Serializable;

import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * POJO définissant un statut d'une adresse.
 */
public class StatutAdresse implements Serializable, Comparable<StatutAdresse> {

    /** Adresse validée automatiquement. */
    public static final String ADR_VALIDEE_AUTO = "VA";

    /** Adresse validée manuellement. */
    public static final String ADR_VALIDEE_MANU = "VM";

    /** Adresse à vérifier. */
    public static final String ADR_A_VERIFIER = "AV";

    /** Adresse non référencée. */
    public static final String ADR_NON_REFERENCEE = "NR";

    /** Adresse non controlée. */
    public static final String ADR_NON_CONTROLEE = "NC";

    /** Adresse confirmée. */
    public static final String ADR_CONFIRMEE = "CO";

    /** Adresse confirmée. */
    public static final String ADR_ETRANGERE = "ET";

    /** Adresse à traiter (adresse à vérifier ou non référencée ie codeEtat = A). */
    public static final String ADR_A_TRAITER = "A";

    /** Adresse validée (adresse validée ie codeEtat = V). */
    public static final String ADR_VALIDEE = "V";

    /** Identifiant de sérialisation. */
    private static final long serialVersionUID = 5414729551290264240L;

    /** Code du statut d'une adresse. */
    private String code;
    /** Libellé long du statut d'une adresse. */
    private String libelleLong;
    /** Code de l'état d'une adresse. */
    private String codeEtat;
    /** Icone à afficher pour le statut de l'adresse. */
    private String icone;

    /**
     * @return the code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the libelleLong
     */
    public String getLibelleLong() {
        return this.libelleLong;
    }

    /**
     * @param libelleLong
     *            the libelleLong to set
     */
    public void setLibelleLong(String libelleLong) {
        this.libelleLong = libelleLong;
    }

    /**
     * @return the codeEtat
     */
    public String getCodeEtat() {
        return this.codeEtat;
    }

    /**
     * @param codeEtat
     *            the codeEtat to set
     */
    public void setCodeEtat(String codeEtat) {
        this.codeEtat = codeEtat;
    }

    /**
     * @return the icone
     */
    public String getIcone() {
        return this.icone;
    }

    /**
     * @param icone
     *            the icone to set
     */
    public void setIcone(String icone) {
        this.icone = icone;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        if (obj.getClass() != getClass()) {
            return false;
        }

        StatutAdresse other = (StatutAdresse) obj;

        return new EqualsBuilder().append(getCode(), other.getCode()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getCode()).toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append(getCode()).append(getLibelleLong()).toString();
    }

    @Override
    public int compareTo(StatutAdresse o) {
        return new CompareToBuilder().append(getCode(), o.getCode()).toComparison();
    }
}
