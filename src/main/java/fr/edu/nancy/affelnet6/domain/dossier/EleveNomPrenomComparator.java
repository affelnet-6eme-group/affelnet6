/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.dossier;

import java.util.Comparator;

public class EleveNomPrenomComparator implements Comparator<Eleve> {

    @Override
    public int compare(Eleve o1, Eleve o2) {
        if (o1 == null) {
            if (o2 != null) {
                return 1;
            }
            return 0;
        }
        if (o2 == null) {
            return -1;
        }

        // Si deux élèves ont le même nom, alors on regarde le prénom 1.
        int comparaisonNom = o1.getNom().compareToIgnoreCase(o2.getNom());
        if (comparaisonNom == 0) {

            // Si le prénom 1 est identique, alors on compare sur l'INE.
            int comparaisonPrenom = o1.getPrenom().compareToIgnoreCase(o2.getPrenom());
            if (comparaisonPrenom == 0) {

                String ine1 = o1.getIne();
                String ine2 = o2.getIne();

                if (ine1 == null) {
                    if (ine2 != null) {
                        return 1;
                    }
                    return 0;
                }
                if (ine2 == null) {
                    return -1;
                }

                return o1.getIne().compareToIgnoreCase(o2.getIne());
            }

            return comparaisonPrenom;
        }

        return comparaisonNom;
    }
}
