/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.dossier;

/**
 * Classe pour les demandes de famille.
 */
public class DemandeFamille extends AbstractDemandeFamille implements Comparable<DemandeFamille> {

    @Override
    public int compareTo(DemandeFamille o) {
        if (o == null) {
            return 1;
        }

        return this.getRang() - o.getRang();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof DemandeFamille)) {
            return false;
        }

        DemandeFamille d = (DemandeFamille) obj;
        return d.rang == rang;
    }

    @Override
    public int hashCode() {
        return rang;
    }
}
