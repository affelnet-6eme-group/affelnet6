/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.nomenclature;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * Comparator pour les indices de répétition des voies.
 *
 */
public class IndicateurPCSComparator implements Serializable, Comparator<IndicateurPCS> {

    /** Tag de sérialisation. */
    private static final long serialVersionUID = -5432299437105694124L;

    /** Liste des codes d'indicateurs utilisés pour ordonner les indicateurs. */
    private static final List<String> CODES_INDICATEURS = Arrays.asList("A", "B", "C", "D", "9");

    @Override
    public int compare(IndicateurPCS o1, IndicateurPCS o2) {

        if (o1 == null) {
            if (o2 == null) {
                return 0;
            }
            return 1;
        }
        if (o2 == null) {
            return -1;
        }

        return CODES_INDICATEURS.indexOf(o1.getCode()) - CODES_INDICATEURS.indexOf(o2.getCode());

    }
}
