/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.calcul;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;

import fr.edu.nancy.affelnet6.domain.dossier.Eleve;
import fr.edu.nancy.affelnet6.domain.nomenclature.CauseCalculCollegeSecteur;
import fr.edu.nancy.affelnet6.domain.nomenclature.EtablissementSconet;
import fr.edu.nancy.affelnet6.domain.nomenclature.EtatCollegeSecteur;
import fr.edu.nancy.affelnet6.domain.nomenclature.TronconCarteScolaire;
import fr.edu.nancy.commun.domain.Role;
import fr.edu.nancy.commun.service.securite.engine.ProfilUtilisateur;

/**
 * Objet intermédiaire permettant de stocker indépendamment de la base de données les informations nécessaires à la
 * détermination des collèges de secteur pour un élève.
 * 
 * 
 *
 */
public class DonneesCalculCollegeSecteur {

    /** L'élève. */
    private Eleve eleve;

    /** Le profil de l'utilisateur effectuant la modification. */
    private ProfilUtilisateur profilUtilisateur;

    /** Le rôle de l'utilisateur effectuant la modification. */
    private Role roleUtilisateur;

    /** Si l'adresse de l'élève a été modifiée par l'action courante. */
    private boolean adresseEleveModifiee;

    /** Le code du département. */
    private String codeDeptMen;

    /** Map de cache tronçons à secteur unique. */
    private Map<Integer, TronconCarteScolaire> tronconCarteScolaireSecteurUnique;

    /** Les collèges de secteurs (saisis ou déterminés). */
    private Collection<EtablissementSconet> collegesSecteur;

    /** Code RNE du collège choisi par l'IA DASEN. */
    private String codeRneCollegeChoisiParIA;

    /** Indique si le déclenchement est manuel. */
    private boolean declenchementManuel;

    /** Indique si la saisie est possible. */
    private boolean actionAvecSaisieCollege;

    /**
     * L'état des collèges de secteur.
     * 
     * @see EtatCollegeSecteur
     */
    private EtatCollegeSecteur etatCalcul;

    /** Indique si le collège est dans le département. */
    private Boolean collegeDansDepartement;

    /** S'il s'agit d'une simulation de la détermination. */
    private boolean simulation;

    /** La cause du calcul de collège de secteur non abouti. */
    private CauseCalculCollegeSecteur causeCalculCollegeSecteur;

    /**
     * Constructeur.
     * 
     * @param eleve
     *            l'élève
     * @param profilUtilisateur
     *            le profil de l'utilisateur
     * @param codeDeptMen
     *            le code du département
     * @param tronconCarteScolaireSecteurUnique
     *            le cache des tronçons à secteur unique
     */
    public DonneesCalculCollegeSecteur(Eleve eleve, ProfilUtilisateur profilUtilisateur, String codeDeptMen,
            Map<Integer, TronconCarteScolaire> tronconCarteScolaireSecteurUnique) {
        super();
        this.eleve = eleve;
        this.profilUtilisateur = profilUtilisateur;
        this.codeDeptMen = codeDeptMen;
        this.tronconCarteScolaireSecteurUnique = tronconCarteScolaireSecteurUnique;

        if (profilUtilisateur != null) {
            this.roleUtilisateur = profilUtilisateur.getAccesCourant().getRole();
        }
    }

    /**
     * @return the eleve
     */
    public Eleve getEleve() {
        return eleve;
    }

    /**
     * @return the profilUtilisateur
     */
    public ProfilUtilisateur getProfilUtilisateur() {
        return profilUtilisateur;
    }

    /**
     * @param roleUtilisateur
     *            the roleUtilisateur to set
     */
    public void setRoleUtilisateur(Role roleUtilisateur) {
        this.roleUtilisateur = roleUtilisateur;
    }

    /**
     * @return the roleUtilisateur
     */
    public Role getRoleUtilisateur() {
        return roleUtilisateur;
    }

    /**
     * @return the adresseEleveModifiee
     */
    public boolean isAdresseEleveModifiee() {
        return adresseEleveModifiee;
    }

    /**
     * @param adresseEleveModifiee
     *            the adresseEleveModifiee to set
     */
    public void setAdresseEleveModifiee(boolean adresseEleveModifiee) {
        this.adresseEleveModifiee = adresseEleveModifiee;
    }

    /**
     * @return the codeDeptMen
     */
    public String getCodeDeptMen() {
        return codeDeptMen;
    }

    /**
     * @return the tronconCarteScolaireSecteurUnique
     */
    public Map<Integer, TronconCarteScolaire> getTronconCarteScolaireSecteurUnique() {
        return tronconCarteScolaireSecteurUnique;
    }

    /**
     * @param tronconCarteScolaireSecteurUnique
     *            the tronconCarteScolaireSecteurUnique to set
     */
    public void setTronconCarteScolaireSecteurUnique(
            Map<Integer, TronconCarteScolaire> tronconCarteScolaireSecteurUnique) {
        this.tronconCarteScolaireSecteurUnique = tronconCarteScolaireSecteurUnique;
    }

    /**
     * @return the collegesSecteur
     */
    public Collection<EtablissementSconet> getCollegesSecteur() {
        return collegesSecteur;
    }

    /**
     * @param collegesSecteur
     *            les collèges de secteur
     */
    public void setCollegesSecteur(Collection<EtablissementSconet> collegesSecteur) {
        this.collegesSecteur = collegesSecteur;
    }

    /**
     * @param collegeDansDepartement
     *            the collegeDansDepartement to set
     */
    public void setCollegeDansDepartement(Boolean collegeDansDepartement) {
        this.collegeDansDepartement = collegeDansDepartement;
    }

    /**
     * @return the codeRneCollegeChoisiParIA
     */
    public String getCodeRneCollegeChoisiParIA() {
        return codeRneCollegeChoisiParIA;
    }

    /**
     * @param codeRneCollegeChoisiParIA
     *            the codeRneCollegeChoisiParIA to set
     */
    public void setCodeRneCollegeChoisiParIA(String codeRneCollegeChoisiParIA) {
        this.codeRneCollegeChoisiParIA = codeRneCollegeChoisiParIA;
    }

    /**
     * @return the etatCalcul
     */
    public EtatCollegeSecteur getEtatCalcul() {
        return etatCalcul;
    }

    /**
     * @param etatCalcul
     *            the etatCalcul to set
     */
    public void setEtatCalcul(EtatCollegeSecteur etatCalcul) {
        this.etatCalcul = etatCalcul;
    }

    /**
     * @return the collegeDansDepartement
     */
    public Boolean getEstCollegeDansDepartement() {
        return collegeDansDepartement;
    }

    /**
     * @return the simulation
     */
    public boolean isSimulation() {
        return simulation;
    }

    /**
     * @param simulation
     *            the simulation to set
     */
    public void setSimulation(boolean simulation) {
        this.simulation = simulation;
    }

    /**
     * @return the causeCalculCollegeSecteur
     */
    public CauseCalculCollegeSecteur getCauseCalculCollegeSecteur() {
        return causeCalculCollegeSecteur;
    }

    /**
     * @param causeCalculCollegeSecteur
     *            the causeCalculCollegeSecteur to set
     */
    public void setCauseCalculCollegeSecteur(CauseCalculCollegeSecteur causeCalculCollegeSecteur) {
        this.causeCalculCollegeSecteur = causeCalculCollegeSecteur;
    }

    /**
     * @return La liste des Code RNE des collèges de secteurs
     */
    public Collection<String> getCodeCollegesSecteur() {
        List<String> codes = new ArrayList<String>();
        if (collegesSecteur != null) {
            for (EtablissementSconet e : collegesSecteur) {
                codes.add(e.getCodeRne());
            }
        }
        return codes;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        if (CollectionUtils.isEmpty(collegesSecteur)) {
            return "Aucun collège";
        }
        for (EtablissementSconet col : collegesSecteur) {
            sb.append(col.getCodeRne()).append(",");
        }
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb.toString();
    }

    /**
     * 
     * @return vrai si le déclenchement est manuel.
     */
    public boolean isDeclenchementManuel() {
        return declenchementManuel;
    }

    /**
     * @param declenchementManuel
     *            Déclenchement manuel
     */
    public void setDeclenchementManuel(boolean declenchementManuel) {
        this.declenchementManuel = declenchementManuel;
    }

    /**
     * 
     * @return vrai si la saisie est possible.
     */
    public boolean isActionAvecSaisieCollege() {
        return actionAvecSaisieCollege;
    }

    /**
     * 
     * @param actionAvecSaisieCollege
     *            Action avec saisie d'un ou des collège(s) de secteur
     */
    public void setActionAvecSaisieCollege(boolean actionAvecSaisieCollege) {
        this.actionAvecSaisieCollege = actionAvecSaisieCollege;
    }
}
