/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.nomenclature;

import java.util.Comparator;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;

/**
 * Comparator pour les indices de répétition des voies.
 *
 */
public class IndiceRepetitionComparator implements Comparator<String> {

    @Override
    public int compare(String indiceRepetition1, String indiceRepetition2) {

        // même objet
        if (StringUtils.equalsIgnoreCase(indiceRepetition1, indiceRepetition2)) {
            return 0;
        }

        if (StringUtils.isBlank(indiceRepetition1)) {
            return -1;
        }

        if (StringUtils.isBlank(indiceRepetition2)) {
            return 1;
        }

        // relation d'ordre pour les indices de répétition : A < B < T < Q < C < D < E < F ...
        // renvoi 1 si indiceRepetition1 <= indicerepetition2, -1 sinon
        return TronconCarteScolaire.INDICES_POSSIBLES.indexOf(indiceRepetition1.toUpperCase(Locale.ROOT))
                - TronconCarteScolaire.INDICES_POSSIBLES.indexOf(indiceRepetition2.toUpperCase(Locale.ROOT));
    }
}
