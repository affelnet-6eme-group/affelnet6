/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.nomenclature;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Compare 2 tronçons de la carte scolaire.<br>
 * Comparaison uniquement sur les communes et les flags secteur unique différents.
 */
public class TronconCarteScolaireFlagSecteurUniqueComparator implements Comparator<TronconCarteScolaire>,
        Serializable {

    /** Serial ID. */
    private static final long serialVersionUID = 5824363350228051539L;

    @Override
    public int compare(TronconCarteScolaire troncon1, TronconCarteScolaire troncon2) {

        // même objet
        if (troncon1 == troncon2) {
            return 0;
        }

        if (troncon1 == null) {
            return -1;
        }

        if (troncon2 == null) {
            return 1;
        }

        // commune qui ne peut pas être nulle
        CommuneInsee communeInsee1 = troncon1.getCommuneInsee();
        CommuneInsee communeInsee2 = troncon2.getCommuneInsee();

        if (communeInsee1 == null || communeInsee2 == null) {
            return -1;
        }

        // secteur unique
        String flagClgUnique1 = troncon1.getFlagCommmuneClgUnique();
        String flagClgUnique2 = troncon2.getFlagCommmuneClgUnique();

        if (communeInsee1.compareTo(communeInsee2) == 0 && !flagClgUnique1.equalsIgnoreCase(flagClgUnique2)) {
            return 0;
        } else {
            return 1;
        }
    }
}
