/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.dossier;

import java.util.Comparator;

import org.apache.commons.lang3.builder.CompareToBuilder;

/**
 * Comparateur responsables d'élèves.
 * 
 * Le tri se fait sur le niveau de responsabilité, puis sur le lien de parenté et enfin sur l'intitulé / le nom,
 * prénom.
 * 
 */
public class ResponsableComparator implements Comparator<Responsable> {

    @Override
    public int compare(Responsable r1, Responsable r2) {
        CompareToBuilder compareToBuilder = new CompareToBuilder();
        compareToBuilder.append(r1.getNiveauResponsabilite(), r2.getNiveauResponsabilite());
        compareToBuilder.append(r1.getLienParente(), r2.getLienParente());
        compareToBuilder.append(r1.getIntitule(), r2.getIntitule());
        compareToBuilder.append(r1.getNom(), r2.getNom());
        compareToBuilder.append(r1.getPrenom(), r2.getPrenom());
        compareToBuilder.append(r1.getId(), r2.getId(), new Comparator<ResponsableId>() {
            @Override
            public int compare(ResponsableId o1, ResponsableId o2) {
                return o1.getNumResponsable() - o2.getNumResponsable();
            }
        });
        return compareToBuilder.toComparison();
    }
}
