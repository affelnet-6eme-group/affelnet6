/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.dossier;

import fr.edu.nancy.affelnet6.domain.nomenclature.StatutAdresse;

/**
 * Comparateur d'adresses sensible à la casse et qui compare aussi les statuts d'adresse.
 */
public class AdresseCaseSensitiveStatutComparator extends AdresseCaseSensitiveComparator {

    /** Tag de sérialisation. */
    private static final long serialVersionUID = -4081949238356073147L;

    @Override
    public int compare(Adresse a1, Adresse a2) {

        int compareValeur = super.compare(a1, a2);
        if (compareValeur != 0) {
            return compareValeur;
        }

        // on compare les statuts d'adresse
        StatutAdresse sa1 = a1.getStatutAdresse();
        StatutAdresse sa2 = a2.getStatutAdresse();

        if ((sa1 != null && sa2 == null) || (sa1 == null && sa2 != null)) {
            return -1;
        } else if (sa1 != null && sa1.compareTo(sa2) != 0) {
            return sa1.compareTo(sa2);
        }

        // si on arrive là, c'est que les adresses sont identiques
        return 0;
    }
}
