/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.affectation;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import fr.edu.nancy.affelnet6.domain.nomenclature.EtablissementSconet;
import fr.edu.nancy.affelnet6.domain.nomenclature.EtablissementSconetUaiComparator;

/**
 * Objet regroupant les éléments de suivi de mixité d'un secteur multi-collèges.
 * 
 * 
 *
 */
public class SuiviMixite implements Comparable<SuiviMixite> {

    /** Les établissements d'accueil du secteur. */
    private SortedSet<EtablissementSconet> etablissements = new TreeSet<EtablissementSconet>(
            new EtablissementSconetUaiComparator());

    /** Composition sociale du secteur. */
    private CompositionSociale compositionSociale = new CompositionSociale(null);
    /** Objectifs de composition sociale par établissement. */
    private Map<EtablissementSconet, CompositionSociale> compositionObjectifs = new HashMap<>();
    /** Composition sociale par établissement (après classement par les familles). */
    private Map<EtablissementSconet, CompositionSociale> compositionsApresClassement = new HashMap<>();
    /** Composition sociale par établissement (après désignation par le DSDEN). */
    private Map<EtablissementSconet, CompositionSociale> compositionsApresDesignation = new HashMap<>();

    /**
     * Ajout d'un établissement.
     * 
     * @param etab
     *            l'établissement à ajouter
     */
    public void addEtablissement(EtablissementSconet etab) {
        etablissements.add(etab);
        compositionObjectifs.put(etab, new CompositionSociale(etab));
        compositionsApresClassement.put(etab, new CompositionSociale(etab));
        compositionsApresDesignation.put(etab, new CompositionSociale(etab));
    }

    @Override
    public int compareTo(SuiviMixite o) {
        EtablissementSconetUaiComparator cmp = new EtablissementSconetUaiComparator();
        Iterator<EtablissementSconet> it1 = etablissements.iterator();
        Iterator<EtablissementSconet> it2 = etablissements.iterator();
        while (it1.hasNext() && it2.hasNext()) {
            int ret = cmp.compare(it1.next(), it2.next());
            if (ret != 0) {
                return ret;
            }
        }
        if (it1.hasNext()) {
            return 1;
        } else if (it2.hasNext()) {
            return -1;
        }
        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof SuiviMixite) {
            return etablissements.equals(((SuiviMixite) obj).etablissements);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return etablissements.hashCode();
    }

    /**
     * @return les établissements
     */
    public SortedSet<EtablissementSconet> getEtablissements() {
        return etablissements;
    }

    /**
     * @param etablissements
     *            the etablissements to set
     */
    public void setEtablissements(SortedSet<EtablissementSconet> etablissements) {
        this.etablissements = etablissements;
    }

    /**
     * @return the compositionSociale
     */
    public CompositionSociale getCompositionSociale() {
        return compositionSociale;
    }

    /**
     * @param compositionSociale
     *            the compositionSociale to set
     */
    public void setCompositionSociale(CompositionSociale compositionSociale) {
        this.compositionSociale = compositionSociale;
    }

    /**
     * @return the compositionObjectifs
     */
    public Map<EtablissementSconet, CompositionSociale> getCompositionObjectifs() {
        return compositionObjectifs;
    }

    /**
     * @param compositionObjectifs
     *            the compositionObjectifs to set
     */
    public void setCompositionObjectifs(Map<EtablissementSconet, CompositionSociale> compositionObjectifs) {
        this.compositionObjectifs = compositionObjectifs;
    }

    /**
     * @return the compositionsApresClassement
     */
    public Map<EtablissementSconet, CompositionSociale> getCompositionsApresClassement() {
        return compositionsApresClassement;
    }

    /**
     * @param compositionsApresClassement
     *            the compositionsApresClassement to set
     */
    public void setCompositionsApresClassement(
            Map<EtablissementSconet, CompositionSociale> compositionsApresClassement) {
        this.compositionsApresClassement = compositionsApresClassement;
    }

    /**
     * @return the compositionsApresDesignation
     */
    public Map<EtablissementSconet, CompositionSociale> getCompositionsApresDesignation() {
        return compositionsApresDesignation;
    }

    /**
     * @param compositionsApresDesignation
     *            the compositionsApresDesignation to set
     */
    public void setCompositionsApresDesignation(
            Map<EtablissementSconet, CompositionSociale> compositionsApresDesignation) {
        this.compositionsApresDesignation = compositionsApresDesignation;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append("composition du secteur : ").append(compositionSociale);
        sb.append("]");
        return sb.toString();
    }
}
