/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.nomenclature;

import org.apache.commons.lang3.StringUtils;

/** Objet métier pour modéliser la cause du calcul du collège de secteur non abouti. */
public class CauseCalculCollegeSecteur implements java.io.Serializable {

    /** serialVersionUID. */
    private static final long serialVersionUID = -6652810203956031512L;

    /** Code de l'état. */
    private String code;

    /** Libellé long de l'état. */
    private String libelleLong;

    /**
     * Constructeur.
     */
    public CauseCalculCollegeSecteur() {
    }

    /**
     * Constructeur.
     * 
     * @param code
     *            code
     */
    public CauseCalculCollegeSecteur(String code) {
        this.code = code;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the libelleLong
     */
    public String getLibelleLong() {
        return libelleLong;
    }

    /**
     * @param libelleLong
     *            the libelleLong to set
     */
    public void setLibelleLong(String libelleLong) {
        this.libelleLong = libelleLong;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof CauseCalculCollegeSecteur) {
            CauseCalculCollegeSecteur cause = (CauseCalculCollegeSecteur) obj;
            return StringUtils.equals(this.getCode(), cause.getCode());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.getCode().hashCode();
    }

    @Override
    public String toString() {
        return this.getCode();
    }
}
