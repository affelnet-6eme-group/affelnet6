/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.nomenclature;

/**
 * Liste des statuts de retour possibles du service RNVP.
 */
public enum StatutRnvpAddress {

    /** Adresse validée. */
    VALIDEE("V"),
    /** Adresse à vérifier (avec des propositions). */
    A_VERIFIER("P"),
    /** Adresse non référencées. */
    NON_REFERENCEE("N"),
    /** Service indisponible. */
    INDISPONIBLE("I");

    /** Statut de retour du RNVP. */
    private final String statut;

    /**
     * Constructeur.
     * 
     * @param statut
     *            Statut du retour dur RNVP
     */
    StatutRnvpAddress(String statut) {
        this.statut = statut;
    }

    /**
     * Donne le statut de retour du RNVP.
     * 
     * @return le statut de retour du RNVP
     */
    public String getStatut() {
        return statut;
    }

    /**
     * @param statut
     *            le code du statut
     * @return l'énumération correspondante au code du statut
     */
    public static StatutRnvpAddress fromStatut(String statut) {
        for (StatutRnvpAddress s : values()) {
            if (s.statut.equals(statut)) {
                return s;
            }
        }
        return null;
    }

}
