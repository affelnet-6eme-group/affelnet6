/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.nomenclature;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/** Academie. */
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler", "fieldHandler" })
public class Academie implements java.io.Serializable {

    /** Code académie pour St-Pierre-et-Miquelon. */
    public static final String CODE_ACADEMIE_ST_PIERRE_MIQUELON = "44";

    /** Code académie pour Caen. */
    public static final String CODE_ACADEMIE_CAEN = "05";

    /** Tag de sérialisation. */
    private static final long serialVersionUID = 5788488692416335438L;

    /** Identifiant de l'enregistrement pour cette académie. */
    private Integer academieId;

    /** Code de l'académie. */
    private String codeAcademie;

    /** Libellé long. */
    private String llAcademie;

    /** Date d'ouverture. */
    private Date dateOuverture;

    /** Date de fermeture. */
    private Date dateFermeture;

    /** Etablissements Sconet (pour l'affectation). */
    @JsonBackReference
    private Set<EtablissementSconet> etablissementSconets = new HashSet<>(0);

    /** Départements composant l'académie. */
    @JsonBackReference
    private Set<DepartementAcademique> departementAcademiques = new HashSet<>(0);

    /** Établissements d'origine (pour l'affectation). */
    @JsonBackReference
    private Set<EtablissementOrigine> etablissementOrigines = new HashSet<>(0);

    /** @return Identifiant de l'enregistrement pour cette académie */
    public Integer getAcademieId() {
        return this.academieId;
    }

    /**
     * @param academieId
     *            Identifiant de l'enregistrement pour cette académie
     */
    public void setAcademieId(Integer academieId) {
        this.academieId = academieId;
    }

    /** @return Code de l'académie */
    public String getCodeAcademie() {
        return this.codeAcademie;
    }

    /**
     * @param codeAcademie
     *            Code de l'académie
     */
    public void setCodeAcademie(String codeAcademie) {
        this.codeAcademie = codeAcademie;
    }

    /** @return Libellé long */
    public String getLlAcademie() {
        return this.llAcademie;
    }

    /**
     * @param llAcademie
     *            Libellé long
     */
    public void setLlAcademie(String llAcademie) {
        this.llAcademie = llAcademie;
    }

    /**
     * @return the dateOuverture
     */
    public Date getDateOuverture() {
        return dateOuverture;
    }

    /**
     * @param dateOuverture
     *            the dateOuverture to set
     */
    public void setDateOuverture(Date dateOuverture) {
        this.dateOuverture = dateOuverture;
    }

    /**
     * @return the dateFermeture
     */
    public Date getDateFermeture() {
        return dateFermeture;
    }

    /**
     * @param dateFermeture
     *            the dateFermeture to set
     */
    public void setDateFermeture(Date dateFermeture) {
        this.dateFermeture = dateFermeture;
    }

    /** @return Etablissements Sconet (pour l'affectation) */
    public Set<EtablissementSconet> getEtablissementSconets() {
        return this.etablissementSconets;
    }

    /**
     * @param etablissementSconets
     *            Etablissements Sconet (pour l'affectation)
     */
    public void setEtablissementSconets(Set<EtablissementSconet> etablissementSconets) {
        this.etablissementSconets = etablissementSconets;
    }

    /** @return Départements composant l'académie */
    public Set<DepartementAcademique> getDepartementAcademiques() {
        return this.departementAcademiques;
    }

    /**
     * @param departementAcademiques
     *            Départements composant l'académie
     */
    public void setDepartementAcademiques(Set<DepartementAcademique> departementAcademiques) {
        this.departementAcademiques = departementAcademiques;
    }

    /** @return Établissements d'origine (pour l'affectation) */
    public Set<EtablissementOrigine> getEtablissementOrigines() {
        return this.etablissementOrigines;
    }

    /**
     * @param etablissementOrigines
     *            Établissements d'origine (pour l'affectation)
     */
    public void setEtablissementOrigines(Set<EtablissementOrigine> etablissementOrigines) {
        this.etablissementOrigines = etablissementOrigines;
    }

}
