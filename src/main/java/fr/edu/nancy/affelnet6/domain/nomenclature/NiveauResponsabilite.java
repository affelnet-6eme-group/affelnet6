/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.nomenclature;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import fr.edu.nancy.affelnet6.domain.dossier.Responsable;

/**
 * Nomenclature des niveaux de responsabilités pour les responsables des élèves.
 * 
 * 
 *
 */
public class NiveauResponsabilite implements java.io.Serializable, Comparable<NiveauResponsabilite> {

    /** Code du niveau de responsabilité représentant légal. */
    public static final String CODE_REPRESENTANT_LEGAL = "1";
    /** Code du niveau de responsabilité en charge de l'élève. */
    public static final String CODE_PERSONNE_EN_CHARGE = "2";
    /** Code du niveau de responsabilité contact (n'existe pas dans la table). */
    public static final String CODE_CONTACT = "3";

    /** Tag de sérialisation. */
    private static final long serialVersionUID = 1L;

    /** Le code du niveau de responsabilité. */
    private String code;
    /** Le libellé court du niveau de responsabilité. */
    private String libelleCourt;
    /** Le libellé long du niveau de responsabilité. */
    private String libelleLong;
    /** Le libellé édition du niveau de responsabilité. */
    private String libelleEdition;
    /** Date d'ouverture. */
    private Date dateOuverture;
    /** Date de fermeture. */
    private Date dateFermeture;
    /** Ordre pour l'affichage. */
    private Integer ordre;
    /** Les responsables. */
    private Set<Responsable> responsables = new HashSet<>(0);

    /** Constructeur par défaut. */
    public NiveauResponsabilite() {
    }

    /**
     * Constructeur.
     * 
     * @param code
     *            le code
     * @param libelleLong
     *            le libellé long
     * @param ordre
     *            l'ordre d'affichage
     */
    public NiveauResponsabilite(String code, String libelleLong, int ordre) {
        this.code = code;
        this.libelleLong = libelleLong;
        this.ordre = ordre;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the libelleCourt
     */
    public String getLibelleCourt() {
        return libelleCourt;
    }

    /**
     * @param libelleCourt
     *            the libelleCourt to set
     */
    public void setLibelleCourt(String libelleCourt) {
        this.libelleCourt = libelleCourt;
    }

    /**
     * @return the libelleLong
     */
    public String getLibelleLong() {
        return libelleLong;
    }

    /**
     * @param libelleLong
     *            the libelleLong to set
     */
    public void setLibelleLong(String libelleLong) {
        this.libelleLong = libelleLong;
    }

    /**
     * @return the libelleEdition
     */
    public String getLibelleEdition() {
        return libelleEdition;
    }

    /**
     * @param libelleEdition
     *            the libelleEdition to set
     */
    public void setLibelleEdition(String libelleEdition) {
        this.libelleEdition = libelleEdition;
    }

    /**
     * @return the dateOuverture
     */
    public Date getDateOuverture() {
        return dateOuverture;
    }

    /**
     * @param dateOuverture
     *            the dateOuverture to set
     */
    public void setDateOuverture(Date dateOuverture) {
        this.dateOuverture = dateOuverture;
    }

    /**
     * @return the dateFermeture
     */
    public Date getDateFermeture() {
        return dateFermeture;
    }

    /**
     * @param dateFermeture
     *            the dateFermeture to set
     */
    public void setDateFermeture(Date dateFermeture) {
        this.dateFermeture = dateFermeture;
    }

    /**
     * @return the ordre
     */
    public Integer getOrdre() {
        return ordre;
    }

    /**
     * @param ordre
     *            the ordre to set
     */
    public void setOrdre(Integer ordre) {
        this.ordre = ordre;
    }

    /**
     * @return the responsables
     */
    public Set<Responsable> getResponsables() {
        return responsables;
    }

    /**
     * @param responsables
     *            the responsables to set
     */
    public void setResponsables(Set<Responsable> responsables) {
        this.responsables = responsables;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }

        if (o == this) {
            return true;
        }

        if (o.getClass() != getClass()) {
            return false;
        }

        NiveauResponsabilite autreNiv = (NiveauResponsabilite) o;
        return new EqualsBuilder().append(getCode(), autreNiv.getCode()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getCode()).toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append(getCode()).append(getLibelleLong()).toString();
    }

    @Override
    public int compareTo(NiveauResponsabilite o) {
        return new CompareToBuilder().append(getOrdre(), o.getOrdre()).toComparison();
    }
}
