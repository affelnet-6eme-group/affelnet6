/*
 * Affelnet-6e - Application nationale d'affectation des élèves au collège
 * Copyright (C) 2022 Ministère de l'Éducation nationale et de la Jeunesse
 * Direction générale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package fr.edu.nancy.affelnet6.domain.affectation;

import fr.edu.nancy.affelnet6.domain.nomenclature.CodeEtatDecision;

/** Décision d'affectation prise par un département. */
public class DecisionDepartement {

    /** Le libellé "SANS DEMANDE". */
    private static final String SANS_DEMANDE = "Sans demande";

    /** L'état de la décision. */
    private String etat;

    /** Le statut de la décision (définitif ou non). */
    private String statut;

    /**
     * Constructeur.
     * 
     * @param etat
     *            l'état de la décision
     * @param definitif
     *            si la décision est définitive
     */
    public DecisionDepartement(String etat, boolean definitif) {
        this.etat = etat;
        if (this.etat == null) {
            this.etat = SANS_DEMANDE;
        }
        this.statut = getStatut(definitif);
    }

    /**
     * Constructeur.
     * 
     * @param etatDecision
     *            l'état de la décision
     * @param definitif
     *            si la décision est définitive
     */
    public DecisionDepartement(CodeEtatDecision etatDecision, boolean definitif) {
        this.statut = getStatut(definitif);
        if (etatDecision == null) {
            this.etat = SANS_DEMANDE;
        } else {
            switch (etatDecision) {
                case AFFECTE:
                    etat = CodeEtatDecision.AFFECTE.getLibelle();
                    break;
                case REFUSE:
                    etat = CodeEtatDecision.REFUSE.getLibelle();
                    break;
                case NON_TRAITE:
                    etat = "En attente";
                    statut = null;
                    break;
                default:
            }
        }
    }

    /**
     * @param definitif
     *            si la décision est définitive ou non
     * @return le libellé associé au caractère définitif ou non de la décision
     */
    private String getStatut(boolean definitif) {
        return definitif ? "décision définitive" : "en attente de validation de l'affectation";
    }

    /**
     * @return the etat
     */
    public String getEtat() {
        return etat;
    }

    /**
     * @return the statut
     */
    public String getStatut() {
        return statut;
    }
}
